<?php

function contact(){
	
	meta::adauga('title','Contact');
	meta::adauga('description','Contact');

	$breadcrumb[0]->title= "Contact";
	$breadcrumb[0]->link ='';
	
    $contact = new contact($_POST['nume'],$_POST['email'],$_POST['subiect'],$_POST['mesaj'],$_POST['total']);
    
    if($_POST){
     $erros = $contact->verifica_form();
     if(empty($erros)){
     	$to = 'suport@ujmag.ro';
     	$mesaj =  template::fetch('form/contact.tpl');
     	
     	$trimite = $contact->trimite_mail($to,$_POST['email'],$_POST['nume'],$mesaj,$_POST['subiect']);
        if($trimite==1){
        	template::assign('trimis',1);
        }else{
        	template::assign('trimis',0);
        }
       }
     
    }
    
   
    template::assign('errors',$erros);
	
	template::assign('breadcrumb',$breadcrumb);
	
	return template::fetch('contact.tpl');

}

?>