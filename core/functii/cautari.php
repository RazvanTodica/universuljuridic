<?php
function cautari(){
	$cautare=new stdClass();
	$cautare->q=$_GET['q'];
	if($cautare->q){
		$cautare->c=new colectie($_GET['c']);
		$filtru=new filtru();

		$filtru->join();
		$filtru->group();

		$filtru->nume($cautare->q);
		if($cautare->c->clc_id>0) $filtru->clc_id($cautare->c->clc_id);
		$publicatii=array ();
		foreach(db::obj_array($filtru->sql()) as $p){
			$publicatii[]=new publicatie($p);
		}
		db::uquery("INSERT INTO `cautari_pro` SET `q`='{$cautare->q}',`c`='{$cautare->c->clc_id}' ON DUPLICATE KEY UPDATE `nr`=`nr`+1,`timestamp`=NOW();");
		template::assign('cautare',$cautare);
		template::assign('publicatii',$publicatii);
		return template::fetch('cautare.tpl');
	}
	else{
		$cautari=array();
		foreach(db::obj_array("SELECT * FROM `cautari_pro` ORDER BY timestamp DESC LIMIT 30") as $c){
			$c->c=new colectie($c->c);
			$cautari[]=$c;
		}
		template::assign('cautari',$cautari);
		return template::fetch('cautari.tpl');
	}
}
?>