<?php

function cauta(){

	$breadcrumb[0]->title= "Cauta";
	$breadcrumb[0]->link = '';

	$c = new carte_business();
	if(isset($_GET['pagina']) && $_GET['pagina'] != '')
	$filtre['pagina'] = $_GET['pagina'];
	else 
	$filtre['pagina'] = 1;
	
	if(isset($_GET['carti']) && is_numeric($_GET['carti']))
	$filtre['carti_pe_pagina'] = $_GET['carti'];
	else
	$filtre['carti_pe_pagina'] = 16;
	
	if(isset($_GET['an']) && is_numeric($_GET['an']))
	$filtre['an'] = $_GET['an'];
	
	if(isset($_GET['luna']) && is_numeric($_GET['luna']))
	$filtre['luna'] = $_GET['luna'];
	
	$filtre['key_words'] = $_GET['key_words'];
	
	$parameters = array();
	if($_GET['key_words'] != '') {
		$parameters[] = "key_words=" . $_GET['key_words'];
	}
	if($_GET['autor'] != '') {
		$parameters[] = "autor=" . $_GET['autor'];
	}
	if($_GET['titlu'] != '') {
		$parameters[] = "titlu=" . $_GET['titlu'];
	}
	
	template::assign("filters", implode("&", $parameters));

	$carti = $c->cautaInDomeniiColectii($filtre,false);
	$carti_numar = $c->cautaInDomeniiColectii($filtre,true);
	
	template::assign('carti',$carti);
	template::assign('total',$carti_numar);
	template::assign('carti_numar',ceil($carti_numar/$filtre['carti_pe_pagina']));
	
	$nr_end = $filtre['carti_pe_pagina']+($filtre['pagina']-1)*$filtre['carti_pe_pagina'];
	if($nr_end>$carti_numar) {
		$nr_end = $carti_numar;
	}
	
	template::assign('start',($filtre['pagina']-1)*$filtre['carti_pe_pagina']);	
	template::assign('end',$nr_end);
	template::assign('pagina_curenta',$filtre['pagina']);
	
	
	template::assign('breadcrumb',$breadcrumb);
	meta::adauga('title','Cauta EdituraUniversulJuridic.ro');
	meta::adauga('description','Cauta EdituraUniversulJuridic.ro');
	
	return template::fetch('colectia.tpl');
}

?>