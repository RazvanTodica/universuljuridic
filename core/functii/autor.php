<?php

function autor(){
	global $CONF,$link;
	$autor = new autor();
	
	
	$id_autor = $autor->autor_id($link[1]);

	$autor_detalii = $autor->autor_detalii($id_autor);
	
	
	if(!$autor_detalii){
		$ok = 0;
	}else{
		$ok =1;
	}
	
	$breadcrumb[0]->title='Autori';
	$breadcrumb[0]->link = $CONF['sitepath'].'autori';
	
	$breadcrumb[1]->title= $autor_detalii->autor_nume;
	$breadcrumb[1]->link = '';
	
	if(isset($_GET['pagina'])) {
		$pagina_curenta = $_GET['pagina'];
	} else {
		$pagina_curenta = 1;
	}
	
	$per_page = 10;
	$start = ($pagina_curenta-1)*$per_page;

    // toate cartile autorului publicate la editura UJ
	$sql = "SELECT p.*
	        FROM PUBLICATII AS p
	            LEFT JOIN autor_publicatie AS a ON ( a.pbl_id=p.PBL_ID )
                LEFT JOIN editura AS e ON ( e.editura_id = p.ed_id )
	        WHERE a.autor_id={$id_autor} AND e.editura_id = 1
	        ";
	$carti = db::obj_array($sql. " limit $start, $per_page");
	$carti_numar = db::query($sql);
	$carti_numar = $carti_numar->numRows();
	template::assign('carti',$carti);
	template::assign('total',$carti_numar);
	template::assign('carti_numar',ceil($carti_numar/$per_page));
	
	$nr_end = $filtre['carti_pe_pagina']+($filtre['pagina']-1)*$filtre['carti_pe_pagina'];
	if($nr_end>$carti_numar) {
		$nr_end = $carti_numar;
	}

	template::assign('start',($pagina_curenta-1)*$per_page);	
	template::assign('end', $per_page*$pagina_curenta);
	template::assign('pagina_curenta',$pagina_curenta);
	template::assign('are_descriere',$ok);
	template::assign('autor',$autor_detalii);
	template::assign('breadcrumb',$breadcrumb);  
	return template::fetch('autor.tpl');

}

?>