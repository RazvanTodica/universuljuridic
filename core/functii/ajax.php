<?php

function ajax(){
	global $link;
	
	if($link[1] == 'trimite_prieten'){
		template::assign('pbl_id',$link[2]);
		echo template::fetch('formular_trimite_prieten.tpl');
	}
	#http://colectiabusiness.ro/beta/ajax/8734/?nume_from=mihai&email_from=as%40a.ro&nume_to=asdf&email_to=as2%40a2.ro
	if(isset($_GET['nume_from']) && $_GET['nume_from'] != '')
	recomanda();
	
	if($link[1] == 'aboneaza_newsletter'){
		ajax_insertNwl();
	}
	
	exit;
}

function recomanda(){
	global $template;
	global $login;
	global $cart;
	global $CONF;
	global $link;

	$from_nume=$_REQUEST['nume_from'];
	$from_email=$_REQUEST['email_from'];
	$to_nume=$_REQUEST['nume_to'];
	$to_email=$_REQUEST['email_to'];
	$carte = new carte_business($link[1]);
	$carte = $carte->carte;

	if (!checkEmail($from_email) ){
		echo 'Adresa ta de email nu este valida';
		exit;
	}
	if (!checkEmail($to_email)){
		echo 'Adresa de email a prietenului tau nu este valida';
		exit;
	}

	$mail=new PHPMailer();
	$mail->From=$from_email;
	$mail->FromName=$from_nume;
	$mail->Sender=$from_email;
	$mail->AddAddress($to_email,$to_nume);
	$mail->Subject="Salut {$to_nume} !";
	$mail->Body='
	<div style="font-size:14px;color:#353535;">
	Am vazuta cartea "<a href="'.$CONF["sitepath"].'cartile-colectiei-business/'.toAscii($carte->PBL_TITLU).'/'.$carte->PBL_ID.'">'.$carte->PBL_TITLU.'</a>" pe '.$CONF["sitepath"].' si vreau sa ti-o recomand.<br />
<br />
<br />
<br />
	Acest email a fost trimis prin formularul de recomandare a unei carti de pe site-ul '.$CONF["sitepath"].' de catre '.$from_nume.' cu adresa de email '.$from_email.'.
	</div>
	';
	$mail->AltBody=strip_tags($mail->Body);
					$mail->AddBcc('comenzi@ujmag.ro');
	$mail->Send();
	echo 'Recomandarea ta a fost trimisa.<br />';
	echo 'Multumim !';
	exit;
}

function ajax_insertNwl(){
	$adresa = $_POST['adresa'];
	$err="";
	if($adresa==''){
		$err = "Introduceti o adresa de email!<br />";
	}
	if(!checkEmail($adresa)){
		$err = "Adresa de email nu este valida!<br />";
	}

	if($err==''){
		file_get_contents("http://news.ad.ro/news/api.php?client=univers&cod_api=g8nm5887dfg9fghy&act=adauga_abonat&mail={$adresa}&nume=&categ=ColectiaBusiness");
		echo "Adresa a fost adaugata in baza noastra de date!";
	}else{
		echo $err;
	}

	exit;
}

?>