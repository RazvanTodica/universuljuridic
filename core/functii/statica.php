<?php

function statica($parametru){
    global $CONF,$link;
	meta::adauga('title',ucfirst(str_replace("-"," ",$parametru)));
	meta::adauga('description',ucfirst(str_replace("-"," ",$parametru)));
	
	if($parametru=='despre-noi'){
		$breadcrumb[0]->title= "Editura";
		$breadcrumb[0]->link = $CONF['sitepath'].'editura/despre-noi';
		
		$breadcrumb[1]->title= "Despre noi";
		$breadcrumb[1]->link = '';
		
		
	}elseif ($parametru=='referenti'){
		
		$breadcrumb[0]->title= "Editura";
		$breadcrumb[0]->link = $CONF['sitepath'].'editura/despre-noi';
		
		$breadcrumb[1]->title= "Referenti";
		$breadcrumb[1]->link = '';
	}elseif ($parametru=='echipa-redactionala'){
		
		$breadcrumb[0]->title= "Editura";
		$breadcrumb[0]->link = $CONF['sitepath'].'editura/echipa-redactionala';
		
		$breadcrumb[1]->title= "Echipa redactionala";
		$breadcrumb[1]->link = '';
	}
	
	

	
	template::assign('breadcrumb',$breadcrumb);
	
	return template::fetch("statice/{$parametru}.tpl");
}

?>