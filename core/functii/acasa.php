<?php

function acasa() {
	global $CONF;
	 
	
	
	
	$carti = carte_business::dinCategorie5(null,12);
	template::assign('carti',$carti);
	
	//Carti in curs de apartie cu limit 6
	$carti_apariti = carte::iaCartiInCursDeAparitie(3);
	template::assign("carti_apariti",$carti_apariti);  
	
	$carti_noi = db::obj_array("SELECT * FROM PUBLICATII WHERE PBL_STOC>0 AND PBL_DATA_APARITIE<NOW() and PBL_ARHIV=0 AND ED_ID=1  and PBL_VIZIBIL=1 ORDER BY PBL_DATA_APARITIE DESC LIMIT 3");
	foreach($carti_noi as $c) {
		$c->imagine = resize_pic($c->PBL_FILENAME,'110x155');
	}
	$cb = new carte_business();
	$nr_carti_noi = $cb->iaCartiColectii(array(),true, true);
	
	template::assign("numar_carti_noi", $nr_carti_noi);
	template::assign("carti_noi", $carti_noi);

	 $banner_home = new bannere();
	 //$bannere_header = $banner_home->bannere_header();
		
	$bannere_header = $banner_home->ia_bannere_sistem("slider");
	$banner_bottom = $banner_home->bannere_slider_promo(3);
	
	 template::assign("banner_header",$bannere_header);  
	 template::assign("banner_bottom",$banner_bottom);  
	 
	 
	$autor = autor::autorRandom();
	template::assign('autori',$autor);
	
	
	$eveniment = eveniment::ultimulEveniment(" and e.apare_in != 'conferinte' ");
	template::assign('even',$eveniment);
	
	$conferinta = eveniment::ultimaConferinta();
	template::assign('eveniment',$conferinta);
		
	$editorial = eveniment::editorial(0,4);
	template::assign('editorial',$editorial);

	     
	$periodice = reviste::periodice();
	template::assign('reviste_periodice',$periodice);
	

	$ultima_revista = reviste::ultima_revista();
	template::assign('ultima_revista',$ultima_revista);

	$interviu = interviuri::ultimulInterviu();
	template::assign('interviu',$interviu);
	
	meta::adauga('title','Universul Juridic');
	meta::adauga('description','Universul Juridic');
	
	$slider = json_decode(file_get_contents("http://www.ujmag.ro/banner_partener/?id=185&zona=header&json=1"));
	
	template::assign("slider",$slider);
	
	template::assign('is_admin', $is_admin);
	return template::fetch ( 'acasa.tpl' );

}

?>