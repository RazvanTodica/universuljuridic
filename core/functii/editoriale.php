<?php
 function editoriale(){
 	global $CONF,$link;
 	
 		$url_eveniment = $link[0]."/".$link[1];
 		
 		if($link[2] !=''){
 		$url_eveniment = $link[0]."/".$link[1];
 		
		$url = db::escape($link[2]);
		$id = db::obj("select ev_id from evenimente where link_nou='{$url}'")->ev_id;
		$eveniment = new eveniment($id);
		template::assign('eveniment',$eveniment);

		template::assign('url_eveniment',$url_eveniment);
		meta::adauga('title',$eveniment->ev_titlu);
		meta::adauga('description',$eveniment->ev_titlu);
	
		return template::fetch('eveniment.tpl');
	}
	
	$breadcrumb[0]->title='Evenimente';
	$breadcrumb[0]->link=$CONF['sitepath'].'evenimente';
	
	$breadcrumb[1]->title='Editoriale';
	$breadcrumb[1]->link='';
	
	
	template::assign('cu_dreapta',true);

	if(isset($_GET['pagina']) && $_GET['pagina'] != '')
	$filtre['pagina'] = $_GET['pagina'];
	else 
	$filtre['pagina'] = 1;
	
	if(isset($_GET['carti']) && is_numeric($_GET['carti']))
	$filtre['carti_pe_pagina'] = $_GET['carti'];
	else
	$filtre['carti_pe_pagina'] = 9;
	
	if(isset($_GET['an']) && is_numeric($_GET['an']))
	$filtre['an'] = $_GET['an'];
	
	if(isset($_GET['luna']) && is_numeric($_GET['luna']))
	$filtre['luna'] = $_GET['luna'];
	
	

	
	$evenimente = eveniment::evenimenteColectiaBusiness($filtre,false,4);
	
	
	template::assign('evenimente',$evenimente);
	
	$carti_numar = eveniment::evenimenteColectiaBusiness($filtre,true,4);
	template::assign('carti_pe_pagina',$filtre['carti_pe_pagina']);
	template::assign('total',$carti_numar);
	template::assign('carti_numar',ceil($carti_numar/$filtre['carti_pe_pagina']));
	//print_r($evenimente);
	
	
	$nr_end = $filtre['carti_pe_pagina']+($filtre['pagina']-1)*$filtre['carti_pe_pagina'];
	if($nr_end>$carti_numar) {
		$nr_end = $carti_numar;
	}
	
	template::assign('start',($filtre['pagina']-1)*$filtre['carti_pe_pagina']);	
	template::assign('end',$nr_end);
	template::assign('pagina_curenta',$filtre['pagina']);
	template::assign('ani',eveniment::iaAni());
	
	template::assign('url_eveniment',$url_eveniment);
	template::assign('breadcrumb',$breadcrumb);
	meta::adauga('title','Evenimente');
	meta::adauga('description','Evenimente');
 	
 	

 	return template::fetch('eveniment.lista.tpl');
 }
?>