<?php

function colectia(){
	global $link;
	global $CONF;
	

	$colectie = new colectie();
	$colectie_lista = $colectie->lista();
	
   	$breadcrumb[0]->title= "Colectii";
	$breadcrumb[0]->link = '';
	
	template::assign('colectie_lista',$colectie_lista);


	
	if($link[0]=='carte' || $link[0]=='revista'){
		
		
		if($link[2]=='cuprins'){
			$link[3] = 'cuprins';
		}
		if($link[2]=='rasfoire'){
			$link[3] = 'rasfoire';
		}
		
		
		template::assign('fara_dreapta',true);  
		$url =  db::escape($link[1]);
		
		
		$sql="SELECT PBL_ID FROM PUBLICATII WHERE PBL_SEO='{$url}' AND ED_ID = 1";

        $link[2]=db::obj($sql)->PBL_ID;

		if(!$link[2])
        {
            $url=str_replace("-"," ",$url);
            $url=trim($url);
            $url=str_replace(" ","-",$url);
            $sql="SELECT * FROM PUBLICATII WHERE PBL_SEO LIKE '{$url}%'
            and PBL_VIZIBIL=1 and PBL_ARHIV=0 AND ED_ID = 1
            ";

            $a=db::obj($sql);
//print_r($a);

            $newurl=$a->PBL_SEO;
            Header( "HTTP/1.1 301 Moved Permanently" );
            header("Location:http://Editurauniversuljuridic.ro/".$link[0]."/".$newurl);
            exit;
        }

		$carte = new carte_business($link[2]);
        if($link[0]=='revista')
        {
//            $reviste = reviste::lista_reviste();
//foreach($reviste as $r)
//    $categorii_reviste[]=$r->CAT_ID;
//print_r($categorii_reviste);
//          print_r($carte->carte->CLC_ID);
//            exit;
//            if(!in_array($carte->carte->CLC_ID,$categorii_reviste))
            if($carte->carte->CLC_ID!=21)
            {
                Header( "HTTP/1.1 301 Moved Permanently" );
                header("Location:http://Editurauniversuljuridic.ro/carte/".$url);

            }
//print_r($reviste);
        }
		// functia e in functii.php
		trimiteEmailPrieten($carte->carte);
	
	
		if($link[0]=='revista'){
			
			$din_categorie =  reviste::dinCategorie($carte->carte->PBL_ID);	
			
			$breadcrumb[0]->title= "Reviste";
			$breadcrumb[0]->link = $CONF['sitepath'].'reviste';
			
			$revista = reviste::dinCategorie($carte->carte->PBL_ID,true);
			
			$breadcrumb[1]->title= $revista->CAT_NUME;
			$breadcrumb[1]->link = $CONF['sitepath'].'reviste/'.$revista->CAT_SEO;
			
			$breadcrumb[2]->title= $carte->carte->PBL_TITLU;
			$breadcrumb[2]->link = '';
			
			
			 $abonamente = reviste::dinCategorie($carte->carte->PBL_ID,false,$tip='abonament');	

			
					
			
		} elseif($link[0]=='carte'){
			
			$categ = carte_business::get_categorie_nume($carte->carte->CLC_ID);	

			$breadcrumb[0]->title= "Colectii";
			$breadcrumb[0]->link = $CONF['sitepath'].'colectie';

			$breadcrumb[1]->title= $categ->CLC_NUME;
			$breadcrumb[1]->link = $CONF['sitepath'].'colectie/'.$categ->CLC_SEO;

			$breadcrumb[2]->title= $carte->carte->PBL_TITLU;
			$breadcrumb[2]->link = '';

			$din_categorie = carte_business::dinCategorie5($carte->carte->PBL_ID,$carte->carte->CLC_ID);
		
		}
		
		template::assign('din_categorie',$din_categorie);
			
		meta::adauga('title',$carte->carte->PBL_TITLU);
		meta::adauga('description',$carte->carte->PBL_TITLU);
		
		
		#pdf
		$sql="SELECT cale_server FROM publicatii_pdf WHERE pbl_id='{$carte->carte->PBL_ID}' and tip='cuprins' AND activ=1";
		
		$carte->carte->cale_server = db::obj($sql)->cale_server;
		
		$colectia = db::obj("SELECT * FROM COLECTII WHERE CLC_ID='{$carte->carte->CLC_ID}'");
		template::assign("colectia", $colectia);
		
		$sql="SELECT cale_server FROM publicatii_pdf WHERE pbl_id='{$carte->carte->PBL_ID}' and tip='rasfoire' AND activ=1";
		$carte->carte->pdf_rasfoire = db::obj($sql)->cale_server;

		template::assign('carte',$carte->carte);

		if(strtotime($carte->carte->PBL_DATA_APARITIE) > strtotime(date("Y-m-d"))) {
			template::assign("in_curs", 1);
		}
	
	if (in_array('Pages.xml',$link)){
		global $CONF;
		
		header("HTTP/1.1 200 OK");

		$tip='rasfoire';
		if(stristr($_SERVER['REQUEST_URI'],'cuprins'))$tip='cuprins';
			
		
		if($tip == 'rasfoire')
		$ce_pdf = $carte->carte->pdf_rasfoire;
		else
		$ce_pdf = $carte->carte->cale_server;
		
		if(strlen($ce_pdf)){
			template::assign('carte',$carte);
			
			
			#sa sterg swf vechi. caz in care au fost modificate pdf din adnim
			$comanda="find /home/univers/public_html/demo/core/templates/beta/lib/flip/pages/. -type f -name \"{$carte->carte->PBL_ID}_{$tip}_*.swf\" -exec rm -f {} \;";

			
			exec($comanda);
            
			
			
			exec("pdf2swf -o \"/home/univers/public_html/demo/core/templates/beta/lib/flip/pages/{$carte->carte->PBL_ID}_{$tip}_%.swf\" /home/univers/public_html/{$ce_pdf}");
				
			
			$output=array();
			
		
			exec("pdfinfo /home/univers/public_html/{$ce_pdf}",$output);
			
			
					
			foreach ($output as $v){
				if(stristr($v,'Page size'))$pageSize=$v;
			}
			preg_match("@([0-9.]+) x ([0-9.]+)@is",$pageSize,$sizes);
			template::assign('pdfWidth',ceil($sizes[1]));
			template::assign('pdfHeight',ceil($sizes[2]));
			
			
			
			$pagini=glob("/home/univers/public_html/demo/core/templates/beta/lib/flip/pages/{$carte->carte->PBL_ID}_{$tip}_*");
			
			
			
			
			foreach ($pagini as $k=>$v){
				$pagini[$k]=str_replace('/home/univers/public_html/demo/core/templates/beta','',$v);
			}

		
			natsort($pagini);
			
			
		}
		template::assign('pagini',$pagini);

//				#manevra
//				{
//		echo '<content width="'.ceil($sizes[1]).'" height="'.ceil($sizes[2]).'" bgcolor="ffffff" loadercolor="ffffff" panelcolor="ffffff" buttoncolor="666666" textcolor="333333">';
//		foreach ($pagini as $pag){
//			echo '<page src="'.$pag.'"/>';
//		}
//		echo '</content>';
//				}


		echo template::fetch('carte.xmlPagini.tpl');
		exit;
	}
	elseif (in_array('cuprins',$link) || in_array('rasfoire',$link)){
		
			
		
		$tip='rasfoire';
		if(stristr($_SERVER['REQUEST_URI'],'cuprins'))$tip='cuprins';

		if($tip == 'rasfoire') {
			$ce_pdf = $carte->carte->pdf_rasfoire;
			$type = "Rasfoire";
		}
		else {
			$ce_pdf = $carte->carte->cale_server;
			$type = "Cuprins";
		}
		
		$file = '/home/univers/public_html/'.$ce_pdf;
		$filename = $carte->carte->PBL_TITLU.'.pdf'; /* Note: Always use .pdf at the end. */

		ob_start();
		ob_clean();
		
		header('Content-type: application/pdf');
		header('Content-disposition: attachment; filename="' . $type .' '.$filename.'"');

		echo file_get_contents($file);
		exit;
		
		header("HTTP/1.1 200 OK");
		echo template::fetch('carte.cuprins.tpl');
		exit;
	}
	
	
		
		template::assign('breadcrumb',$breadcrumb);
		template::assign('abonamente',$abonamente);				

		return template::fetch('carte.tpl');
	}
	
	
	
	if(isset($_GET['pagina']) && $_GET['pagina'] != '')
	$filtre['pagina'] = $_GET['pagina'];
	else 
	$filtre['pagina'] = 1;
	
	if(isset($_GET['carti']) && is_numeric($_GET['carti']))
	$filtre['carti_pe_pagina'] = $_GET['carti'];
	else
	$filtre['carti_pe_pagina'] = 20;
	
	if(isset($_GET['an']) && is_numeric($_GET['an']))
	$filtre['an'] = $_GET['an'];
	
	if(isset($_GET['luna']) && is_numeric($_GET['luna']))
	$filtre['luna'] = $_GET['luna'];
	
	
	$cb = new carte_business();
	$carti_noi = false;
	if ($link[1]=='carti-in-curs-de-aparitie'){
		$start = ($filtre['pagina'] - 1) * $filtre['carti_pe_pagina'];
		$per_page = $filtre['carti_pe_pagina'];
		$carti = carte::iaCartiInCursDeAparitie("$start, $per_page");
		$carti_numar = $carti['nr_total_aparitii'];

	}else{

		
		
		$colectie = $cb->get_idColectii($link[1]);
		$id_colectie = $colectie->CLC_ID;
		$colectia = db::obj("SELECT * FROM COLECTII WHERE CLC_ID='{$id_colectie}'");
		template::assign("colectia", $colectia);
		if($id_colectie){
			$carti = $cb->iaCartiColectieCategorie($filtre,$id_colectie);
		}else{
			if(isset($_GET['noi']) && $_GET['noi'] == 1) {
				$carti_noi = true;
				$carti = $cb->cartiBusiness($filtre, false, true);
				template::assign("noi", "1");
			} else  {
				$carti = $cb->cartiBusiness($filtre);
			}
		
		}
		
		if($id_colectie){
			$carti_numar = $cb->iaCartiColectieCategorie($filtre,$id_colectie,true);	
		}else{
			if($carti_noi) {
				$carti_numar = $cb->cartiBusiness($filtre, true, true);				
			} else {
				$carti_numar = $cb->cartiBusiness($filtre, true);
			}
		}
	}
	
	
	
	
	template::assign('carti',$carti);
	template::assign('total',$carti_numar);
	template::assign('carti_numar',ceil($carti_numar/$filtre['carti_pe_pagina']));
	
	$nr_end = $filtre['carti_pe_pagina']+($filtre['pagina']-1)*$filtre['carti_pe_pagina'];
	if($nr_end>$carti_numar) {
		$nr_end = $carti_numar;
	}
	
	template::assign('start',($filtre['pagina']-1)*$filtre['carti_pe_pagina']);	
	template::assign('end',$nr_end);
	template::assign('pagina_curenta',$filtre['pagina']);
	
	
	
	template::assign('ani',carte_business::iaAni());
	template::assign('colectie',$colectie);
	#print_r($carti);
	
	template::assign('breadcrumb',$breadcrumb);
	if($carti_noi) {
		meta::adauga('title','Cartile Noi din Universul Juridic');
	}
	else {
		meta::adauga('title','Cartile Colectiei Universul Juridic');
	}
	meta::adauga('description','Cartile Colectiei Universul Juridic');
	
	return template::fetch('colectia.tpl');

}

?>