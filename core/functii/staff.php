<?php

function staff($parametru){
	global  $link,$CONF;  
	
	$editura = new editura();
	
	$staff = $editura->staff_editorial();
	

    if($link[2] !=''){
	$id_staff = $editura->get_id_staff($link[2]);

	
	$detalii = $editura->get_detalii_staf($id_staff);
	
    }
	
   
	if($detalii){
		$breadcrumb[0]->title= "Editura";
		$breadcrumb[0]->link = $CONF['sitepath'].'editura/despre-noi';
			
		$breadcrumb[1]->title= "Staff editorial";
		$breadcrumb[1]->link = $CONF['sitepath'].'editura/staff-editorial';
		
		$breadcrumb[2]->title= $detalii->staff_nume;
		$breadcrumb[2]->link = '';
		
	}else{
		$breadcrumb[0]->title= "Editura";
		$breadcrumb[0]->link = $CONF['sitepath'].'editura/despre-noi';
			
		$breadcrumb[1]->title= "Staff editorial";
		$breadcrumb[1]->link = '';
	}
	
//	echo '<pre>';
//	print_r($sfaff);
//	exit;
	
	template::assign('breadcrumb',$breadcrumb);
	template::assign('staff',$staff);
	template::assign('detalii',$detalii);
	
	
	return template::fetch("statice/staff.tpl");
}
?>