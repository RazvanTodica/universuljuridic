<?php

function sitemap(){
	global $CONF;
	$linkuri[] = $CONF['sitepath'];
	
	$cb = new carte_business();
	$cb->cartiBusiness(array('pagina'=>1,'carti_pe_pagina'=>1000));
	foreach ($cb->carti as $item){
		$linkuri[] = $CONF['sitepath'].'cartile-colectiei-business/'.toAscii($item->PBL_TITLU).'/'.$item->PBL_ID;
	}
	
	$cb = new carte_business();
	$autori = $cb->autoriBusiness();
	foreach ($autori as $item){
		$linkuri[] = $CONF['sitepath'].'autorii-colectiei-business/'.toAscii($item->autor_nume).'/'.$item->autor_id;
	}
	
	$evenimente = eveniment::evenimenteColectiaBusiness(array('pagina'=>1,'carti_pe_pagina'=>1000));
	foreach ($evenimente as $item){
		$linkuri[] = $CONF['sitepath'].'evenimente/'.toAscii($item->ev_titlu).'/'.$item->ev_id;
	}
	
	$interviuri = interviuri::interviuriColectiaBusiness(array('pagina'=>1,'carti_pe_pagina'=>1000));
	foreach ($interviuri as $item){
		$linkuri[] = $CONF['sitepath'].'interviuri/'.toAscii($item->titlu).'/'.$item->id;
	}
	
	$mass_media = eveniment::evenimenteColectiaBusiness(array('pagina'=>1,'carti_pe_pagina'=>1000),false,5);
	foreach ($mass_media as $item){
		$linkuri[] = $CONF['sitepath'].'mass_media/'.toAscii($item->ev_titlu).'/'.$item->ev_id;
	}
	
	template::assign("linkuri",$linkuri);
	echo header ("content-type: text/xml");
	echo template::fetch('sitemap.xml');
	exit;
}

?>