<?php

function interviuri() {
	global $link,$CONF;
	
	
	 $url_eveniment = $link[0]."/".$link[1]; 

	if($link[2] !=''){
		$url_eveniment = $link[0]."/".$link[1]; 
		
		$url = db::escape($link[2]);
		$id = db::obj("select id from interviu where href='{$url}'")->id;
		
		$eveniment = eveniment::evenimentInterviu($id);
		
		template::assign('url_eveniment',$url_eveniment);
		template::assign('eveniment',$eveniment);
		
//		if(is_admin()){
//		print_r($eveniment);
//		}
		
		
		
		meta::adauga('title',$eveniment->ev_titlu);
		meta::adauga('description',$eveniment->ev_titlu);
	
		return template::fetch('eveniment.tpl');
	}
	
	$breadcrumb[0]->title='Evenimente';
	$breadcrumb[0]->link=$CONF['sitepath'].'evenimente';
	
	$breadcrumb[1]->title='Interviuri';
	$breadcrumb[1]->link='';
	
	template::assign('cu_dreapta',true);

	if(isset($_GET['pagina']) && $_GET['pagina'] != '')
	$filtre['pagina'] = $_GET['pagina'];
	else 
	$filtre['pagina'] = 1;
	
	if(isset($_GET['carti']) && is_numeric($_GET['carti']))
	$filtre['carti_pe_pagina'] = $_GET['carti'];
	else
	$filtre['carti_pe_pagina'] = 9;
	
	if(isset($_GET['an']) && is_numeric($_GET['an']))
	$filtre['an'] = $_GET['an'];
	
	if(isset($_GET['luna']) && is_numeric($_GET['luna']))
	$filtre['luna'] = $_GET['luna'];
	
	
	
	
	
	
	$interviuri = eveniment::interviuri($filtre);
	template::assign('evenimente',$interviuri);
	
	$carti_numar = eveniment::interviuri($filtre,true);
	template::assign('carti_pe_pagina',$filtre['carti_pe_pagina']);
	template::assign('total',$carti_numar);
	template::assign('carti_numar',ceil($carti_numar/$filtre['carti_pe_pagina']));
	
	
	
	$nr_end = $filtre['carti_pe_pagina']+($filtre['pagina']-1)*$filtre['carti_pe_pagina'];
	if($nr_end>$carti_numar) {
		$nr_end = $carti_numar;
	}
	
	template::assign('start',($filtre['pagina']-1)*$filtre['carti_pe_pagina']);	
	template::assign('end',$nr_end);
	template::assign('pagina_curenta',$filtre['pagina']);
	template::assign('ani',eveniment::iaAni());
	
	template::assign('breadcrumb',$breadcrumb);
	meta::adauga('title','Interviuri');
	meta::adauga('description','Interviuri');

	
	return template::fetch('eveniment.lista_interviuri.tpl');  
}

?>