<?php
function recomanda(){
	global $template;
	global $login;
	global $cart;
	global $CONF;
	global $link;

	$from_nume=$_REQUEST['nume_from'];
	$from_email=$_REQUEST['email_from'];
	$to_nume=$_REQUEST['nume_to'];
	$to_email=$_REQUEST['email_to'];
	$carte=new carte_business($link[1]);


	if (!checkEmail($from_email) ){
		echo 'Adresa ta de email nu este valida';
		exit;
	}
	if (!checkEmail($to_email)){
		echo 'Adresa de email a prietenului tau nu este valida';
		exit;
	}


	$mail=new PHPMailer();
	$mail->From=$from_email;
	$mail->FromName=$from_nume;
	$mail->Sender=$from_email;
	$mail->AddAddress($to_email,$to_nume);
	$mail->Subject="Salut {$to_nume} !";
	$mail->Body='
	<div style="font-size:14px;color:#353535;">
	Am vazuta cartea "<a href="'.$CONF["sitepath"].'cartile-colectiei-business/'.toAscii($carte->carte->PBL_TITLU).'/'.$carte->carte->PBL_ID.'">'.$carte->carte->PBL_TITLU.'</a>" pe '.$CONF["sitepath"].' si vreau sa ti-o recomand.<br />
<br />
<br />
<br />
	Acest email a fost trimis prin formularul de recomandare a unei carti de pe site-ul '.$CONF["sitepath"].' de catre '.$from_nume.' cu adresa de email '.$from_email.'.
	</div>
	';
	$mail->AltBody=strip_tags($mail->Body);
					$mail->AddBcc('comenzi@ujmag.ro');
	$mail->Send();
	echo 'Recomandarea ta a fost trimisa.<br />';
	echo 'Multumim !';
	exit;
}
?>