<?php

/**
 * Created by PhpStorm.
 * User: Silviu
 * Date: 02.11.2016
 * Time: 16:55
 */
function echipa_redactionala()
{
	global  $link,$CONF;
	
	$breadcrumb[0]->title= "Editura";
	$breadcrumb[0]->link = $CONF['sitepath'].'editura/echipa-redactionala';
	
	$breadcrumb[1]->title= "Echipa redactionala";
	$breadcrumb[1]->link = '';
	
	template::assign('breadcrumb',$breadcrumb);
	
	return template::fetch("statice/echipa-redactionala.tpl");
	
	
}