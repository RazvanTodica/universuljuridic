<?php

function autori(){
	global $link;
	global $CONF;
	
	
	$breadcrumb[0]->title= "Autori";
	$breadcrumb[0]->link = '';

	if(isset($_GET['pagina']) && $_GET['pagina'] != '')
	$filtre['pagina'] = $_GET['pagina'];
	else 
	$filtre['pagina'] = 1;
	
	if(isset($_GET['carti']) && is_numeric($_GET['carti']))
	$filtre['carti_pe_pagina'] = $_GET['carti'];
	else
	$filtre['carti_pe_pagina'] = 64;
	
	if(isset($_GET['an']) && is_numeric($_GET['an']))
	$filtre['an'] = $_GET['an'];
	
	if(isset($_GET['luna']) && is_numeric($_GET['luna']))
	$filtre['luna'] = $_GET['luna'];
	
	
	
	
	
	
	$a = new carte_business();
	
	
	
	$litera = db::escape($_GET['nume']);
	
	if($_GET['nume'] ==''){
	 	$ordonare = "AND a.autor_nume like 'a%'";	
	 	
	 	$autori = $a->totiAutori($ordonare,$filtre);
	 	$cn = $a->totiAutori($ordonare,$filtre,true);
	 	$carti_numar=$cn[0]->total;
	}else{
		
		$ordonare = "AND a.autor_nume like '$litera%'";	
		$autori = $a->totiAutori($ordonare,$filtre);
		$cn = $a->totiAutori($ordonare,$filtre,true);
		$carti_numar=$cn[0]->total;
    }


    $new_query = $_GET;
    unset($new_query['pagina']);
    $new_query = http_build_query($new_query);


    $nr_end = $filtre['carti_pe_pagina']+($filtre['pagina']-1)*$filtre['carti_pe_pagina'];
    if($nr_end > $carti_numar) {
        $nr_end = $carti_numar;
    }




    template::assign('total',$carti_numar);
	template::assign('carti_numar',ceil($carti_numar/$filtre['carti_pe_pagina']));
	template::assign('start',($filtre['pagina']-1)*$filtre['carti_pe_pagina']);	
	template::assign('end',$nr_end);
	template::assign('pagina_curenta',$filtre['pagina']);
	
//	 echo '<pre>';
//       print_r($autori);
	
	template::assign('autori',$autori);
    template::assign('new_query',$new_query);
	template::assign('breadcrumb',$breadcrumb);  
	
	meta::adauga('title','Autori');
	meta::adauga('description','Autori');
	
	return template::fetch('autori.tpl');
}

?>