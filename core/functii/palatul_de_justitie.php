<?php

function palatul_de_justitie() {
	global $CONF;
	
	if(isset($_GET['id'])) {
		
		$carte = db::obj("SELECT * FROM `arhiva_palatul_de_justitie_uj` WHERE id='{$_GET['id']}'");
		if(!$carte) {
			header("Location: " . $CONF['sitepath'] . "reviste/palatul-de-justitie");
		}
		
		$firefox = strpos($_SERVER["HTTP_USER_AGENT"], 'Firefox') ? true : false;
		
		
		header('Content-type: application/pdf');
		if($firefox) {
			header('Content-Disposition: attachment; filename="revista.pdf"');
		} else {
			header('Content-Disposition: inline; filename="revista.pdf"');
		}

		readfile($carte->pdf);
		exit;
	}
	
	if(isset($_GET['pagina']) && $_GET['pagina'] != '')
		$filtre['pagina'] = $_GET['pagina'];
	else {
		$filtre['pagina'] = 1;
	}
	
	$filtre['carti_pe_pagina'] = 20;

	
	$res = db::obj("SELECT COUNT(id) as numar FROM `arhiva_palatul_de_justitie_uj`");
	$numar_total = $res->numar;
		
	$start = ($filtre['pagina'] - 1) * $filtre['carti_pe_pagina'];
	
	if(isset($_GET['sortare']) && $_GET['sortare'] == "aparitie-crescator") {
		$order = "ORDER BY `data_aparitie` ASC";
	} else {
		$order = "ORDER BY `data_aparitie` DESC";
	}
	
	$carti = db::obj_array("SELECT * FROM `arhiva_palatul_de_justitie_uj` {$order}
		LIMIT $start,{$filtre['carti_pe_pagina']}");
	
	$nr_end = $filtre['carti_pe_pagina']+($filtre['pagina']-1)*$filtre['carti_pe_pagina'];
	if($nr_end>$carti_numar) {
		$nr_end = $carti_numar;
	}
	
	template::assign('carti',$carti);
	template::assign('total',$numar_total);
	template::assign('carti_numar',ceil($numar_total/$filtre['carti_pe_pagina']));
	
	template::assign('start',($filtre['pagina']-1)*$filtre['carti_pe_pagina']);	
	template::assign('end',$nr_end);
	template::assign('pagina_curenta',$filtre['pagina']);
	
	meta::adauga('title','Revista Palatul de Justitie');
	meta::adauga('description','Editii ale revistei Palatul de Justitie');
	
	return template::fetch('palatul_de_justitie.tpl');
		
}

?>