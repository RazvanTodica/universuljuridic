<?php
function reviste(){
	global $link,$CONF;

	$breadcrumb[0]->title= "Reviste";
	$breadcrumb[0]->link = '';
	
	
	$url_sel = $link[0]."/".$link[1];
	
	$reviste = reviste::lista_reviste();
	
	if(isset($_GET['pagina']) && $_GET['pagina'] != '')
	$filtre['pagina'] = $_GET['pagina'];
	else 
	$filtre['pagina'] = 1;
	
	if(isset($_GET['carti']) && is_numeric($_GET['carti']))
	$filtre['carti_pe_pagina'] = $_GET['carti'];
	else
	$filtre['carti_pe_pagina'] = 12;
	
	if(isset($_GET['an']) && is_numeric($_GET['an']))
	$filtre['an'] = $_GET['an'];
	
	if(isset($_GET['luna']) && is_numeric($_GET['luna']))
	$filtre['luna'] = $_GET['luna'];
	
	
	$url_revista = db::escape($link[0].'/'.$link[1]);
	$revista = reviste::reviste_categorie_by_link($url_revista);
	

	
	$id_categorie_revista  = $revista->CAT_ID;
	
	if($id_categorie_revista !=''){
		$carti = reviste::reviste_by_categorie($id_categorie_revista,$filtre);
		
		$carti_numar = reviste::reviste_by_categorie($id_categorie_revista,$filtre,true);
		$carti_numar = $carti_numar[0]->total;
		
		$breadcrumb[0]->title= "Reviste";
		$breadcrumb[0]->link = $CONF['sitepath'].'reviste';
		
		$breadcrumb[1]->title= $revista->CAT_NUME;
		$breadcrumb[1]->link = '';
	}else{
		$carti=reviste::ultimele_reviste_aparute($filtre);
		$carti_numar=reviste::ultimele_reviste_aparute($filtre,87,true);
		$carti_numar= $carti_numar[0]->total;
		
		$breadcrumb[0]->title= "Reviste";
		$breadcrumb[0]->link = '';
	}
	
	$nr_end = $filtre['carti_pe_pagina']+($filtre['pagina']-1)*$filtre['carti_pe_pagina'];
	if($nr_end>$carti_numar) {
		$nr_end = $carti_numar;
	}
	
	
    
	template::assign('url_sel',$url_sel);
	template::assign('carti',$carti);
	template::assign('total',$carti_numar);
	template::assign('carti_numar',ceil($carti_numar/$filtre['carti_pe_pagina']));
	template::assign('start',($filtre['pagina']-1)*$filtre['carti_pe_pagina']);	
	template::assign('end',$nr_end);
	template::assign('pagina_curenta',$filtre['pagina']);
	template::assign('revista',$revista);
	
	template::assign('revista_lista',$reviste);
	template::assign('breadcrumb',$breadcrumb);
	
	return template::fetch('reviste.tpl');
}
?>