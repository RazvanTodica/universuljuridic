<?php

function mass_media() {
	global $link;
	
	if(isset($link[2]) && is_numeric($link[2])){

		$eveniment = new eveniment($link[2]);
		template::assign('eveniment',$eveniment);
		//print_r($eveniment);
		
		meta::adauga('title',$eveniment->ev_titlu);
		meta::adauga('description',$eveniment->ev_titlu);
		
		return template::fetch('mass_media.tpl');
	}
	
	template::assign('cu_dreapta',true);

	if(isset($_GET['pagina']) && $_GET['pagina'] != '')
	$filtre['pagina'] = $_GET['pagina'];
	else 
	$filtre['pagina'] = 1;
	
	if(isset($_GET['carti']) && is_numeric($_GET['carti']))
	$filtre['carti_pe_pagina'] = $_GET['carti'];
	else
	$filtre['carti_pe_pagina'] = 9;
	
	if(isset($_GET['an']) && is_numeric($_GET['an']))
	$filtre['an'] = $_GET['an'];
	
	if(isset($_GET['luna']) && is_numeric($_GET['luna']))
	$filtre['luna'] = $_GET['luna'];
	
	$evenimente = eveniment::evenimenteColectiaBusiness($filtre,false,5);
	template::assign('evenimente',$evenimente);
	
	$carti_numar = eveniment::evenimenteColectiaBusiness($filtre,true,5);
	template::assign('total',$carti_numar);
	template::assign('carti_numar',ceil($carti_numar/$filtre['carti_pe_pagina']));
	//print_r($evenimente);
	
	template::assign('ani',eveniment::iaAni());
	
	meta::adauga('title','Mass Media');
	meta::adauga('description',"Mass Media");
	
	return template::fetch('mass_media.lista.tpl');
}

?>