<?php
class curs_valutar{
	public $lista;
	private $abrevieri;
	function __construct(){
		$this->abrevieri['AUD']='Dolarul australian';
		$this->abrevieri['BGN']='Leva bulgărească';
		$this->abrevieri['CAD']='Dolarul canadian';
		$this->abrevieri['CHF']='Francul elveţian';
		$this->abrevieri['CZK']='Coroana cehă';
		$this->abrevieri['DKK']='Coroana daneză';
		$this->abrevieri['EGP']='Lira egipteană';
		$this->abrevieri['EUR']='Euro';
		$this->abrevieri['GBP']='Lira sterlină';
		$this->abrevieri['100HUF']='100 Forinti maghiari';
		$this->abrevieri['100JPY']='100 Yeni japonezi';
		$this->abrevieri['MDL']='Leul moldovenesc';
		$this->abrevieri['NOK']='Coroana norvegiană';
		$this->abrevieri['PLN']='Zlotul polonez';
		$this->abrevieri['RUB']='Rubla rusescă';
		$this->abrevieri['SEK']='Coroana suedeză';
		$this->abrevieri['SKK']='Coroana slovacă';
		$this->abrevieri['TRY']='Lira turcească';
		$this->abrevieri['USD']='Dolarul SUA';
		$this->abrevieri['XAU']='gramul de aur';
		$this->abrevieri['XDR']='Drept Special de Tragere';
	}
	function parse_bnr(){
		$data_curenta=date("Y-m-d");
		$xml=new XMLReader();
		$xml->open('http://www.bnro.ro/nbrfxrates.xml');
		while($xml->read())
		{
			if ($xml->getAttribute('date')){
				$data_curenta=$xml->getAttribute('date');
			}
			if ($xml->getAttribute('currency')){
				$el=new bnr_valuta();
				$el->simbol=$xml->getAttribute('multiplier').$xml->getAttribute('currency');
				$el->nume=$this->abrevieri["{$el->simbol}"];
				$el->data=$data_curenta;
				$value=$xml->readString();
				$el->valoare=$value;
				$xml->read();
				$xml->read();
				$this->lista[]=$el;
				$this->adauga($el);
			}
		}
	}
	/**
	 * Adauga in baza de date o valuta
	 *
	 * @param bnr_valuta $valuta
	 */
	function adauga($valuta){
		$sql="INSERT IGNORE INTO `curs_valutar` SET `valuta_simbol`=?,`valuta_valoare`=?,`valuta_data`=?";
		$valuta->valoare=str_replace(',','.',$valuta->valoare);
		uquery($sql,$valuta->simbol,$valuta->valoare,$valuta->data);
	}
}
?>