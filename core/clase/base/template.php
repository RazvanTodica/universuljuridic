<?php

class mesaje{
	function adauga($mesaj,$tip='info'){
		$_SESSION['mesaje']["{$tip}"][]=$mesaj;
	}
	function afisat($tip,$key){
		unset($_SESSION['mesaje']["{$tip}"]["{$key}"]);
	}
	function get(){
		$return=array();
		if (is_array($_SESSION['mesaje'])){
			foreach ($_SESSION['mesaje'] as $tip=>$mesaje){
				if (is_array($mesaje)){
					foreach ($mesaje as $key=>$mesaj){
						$return["{$tip}"][]=$mesaj;
						mesaje::afisat($tip,$key);
					}
				}
			}
		}
		return $return;
	}
}
function &mesaje_get(){
	return mesaje::get();
}

class template{
	function init($template_dir=null,$compile_dir=null,$cache_dir=null){

		global $CONF;
		global $template;
		if (!$template){
			require_once(SERVER_PATH.'core/clase/smarty.php');
			$template=new smarty_template();
		}
		$template->template_dir = $CONF['core_dir'].'templates/'.$CONF['template'] . '/tpl';
		$template->compile_dir  = $CONF['core_dir'] . 'tmp/tpl/'.$CONF['template'].'/';
		$template->cache_dir    = $CONF['core_dir'] . 'tmp/cache/'.$CONF['template'].'/';
		$template->config_dir   = $CONF['core_dir'];

		if ($template_dir){
			$template->template_dir=$template_dir;
		}
		if ($compile_dir){
			$template->compile_dir=$compile_dir;
		}
		if ($cache_dir){
			$template->cache_dir=$cache_dir;
		}


		if (!is_dir($template->compile_dir)){
			mkdir($template->compile_dir,0777,true);
			chmod($template->compile_dir,0777);
		}
		if (!is_dir($template->cache_dir)){
			mkdir($template->cache_dir,0777,true);
			chmod($template->cache_dir,0777);
		}

		$template->caching = false;
	}
	function __check(){
		global $template;
		if (!$template){
			template::init();
		}
	}
	function assign($variabila,$valoare){
		global $template;
		template::__check();
		return $template->assign($variabila,$valoare);
	}
	function fetch($tpl){
		global $template;
		template::__check();
		return $template->fetch($tpl);
	}
	function display($tpl){
		global $template;
		template::__check();
		return $template->display($tpl);
	}
}
?>