<?php
class resize_image{
	var $width;
	var $height;
	var $new_width;
	var $new_height;
	var $source;
	var $destination;
	var $type;
	function maxSize($max_width, $max_height) {
		$image_width=$this->width;
		$image_height=$this->height;
		$orientation = ($image_width >= $image_height) ? "landscape" : "portrait";
		if ($orientation == "landscape") {
			$factor = $image_height / $image_width;
		} else {
			$factor = $image_width / $image_height;
		}
		while ($image_width > $max_width || $image_height > $max_height) {
			if ($orientation == "landscape") {
				// width;
				$image_height = round(($image_width * $factor));
				if ($image_width > $max_width || $image_height > $max_height) {
					$image_width = round($image_width - 1);
					$image_height = round(($image_width * $factor));
				}
			} else {
				// height;
				$image_width = round(($image_height / $factor));
				if ($image_width > $max_width || $image_height > $max_height) {
					$image_height = round($image_height - 1);
					$image_width = round(($image_height * $factor));
				}
			}
		}
		$img = array($image_width, $image_height);
		return $img;
	}
	/**
 * Source is the location of the image on server.For example : /home/public_html/images/test.jpg
 * Destination is the location where you want to put the resized image (leave blank for direct display
 * You cand set both new_width and new_height or just one of them.(the other will be calculated to keep proportions)
 *
 *
 * @param string $source
 * @param string $destination
 * @param int $new_width
 * @param int $new_height
 * @return resize_image
 */
	function resize($source,$destination='',$new_width=0,$new_height=0,$proportii=0,$cu_container=false,$return_pic=false,$return_obj=false){
		set_time_limit(180);
		$info=getimagesize($source);
		if (is_array($info)){
			$this->width=$info[0];
			$this->height=$info[1];
			if ($this->width<$new_width && $this->height<$new_height && $cu_container==false){
				$new_width=$this->width;
				$new_height=$this->height;
			}
			$this->type=$info['mime'];
			$this->source=$source;
			$this->destination=$destination;
			if ($new_height==0 && $new_width==0){
				return 0;
			}
			if ($new_height==0){
				$this->new_height=($this->height*$new_width)/$this->width;
				$this->new_width=$new_width;
			}
			if ($new_width==0){
				$this->new_width=($this->width*$new_height)/$this->height;
				$this->new_height=$new_height;
			}
			if ($new_height!=0 && $new_width!=0){
				$this->new_height=$new_height;
				$this->new_width=$new_width;
			}
			if ($proportii==1){
				$dim=$this->maxSize($new_width,$new_height);
				$this->new_width=$dim[0];
				$this->new_height=$dim[1];
			}
			$this->new_height=round($this->new_height);
			$this->new_width=round($this->new_width);
			if ($cu_container==false){
				$thumb=imagecreatetruecolor($this->new_width,$this->new_height);
			}
			else {
				$thumb=imagecreatetruecolor($new_width,$new_height);
			}
			if ($this->bgcolor){
				$bgcolor=imagecolorallocate($thumb,$this->bgcolor[0],$this->bgcolor[1],$this->bgcolor[2]);
				imagefill($thumb,0,0,$bgcolor);
			}
			else{
				if (stristr($this->type,'png')){
					$alb=imagecolorallocatealpha($thumb,255,255,255,126);
					imagealphablending($thumb,false);
					imagesavealpha($thumb,true);
					imagefill($thumb,1,1,$alb);
				}
				else {
					$alb=imagecolorallocate($thumb,255,255,255);
					imagefill($thumb,0,0,$alb);
				}
			}
			if ($cu_container){
				$deplasare_x=(int)(($new_width-$this->new_width)/2);
				$deplasare_y=(int)(($new_height-$this->new_height)/2);
			}
			else {
				$deplasare_x=0;
				$deplasare_y=0;
			}
			if (stristr($this->type,'jp'))
			$original=imagecreatefromjpeg($this->source);
			if (stristr($this->type,'gif'))
			$original=imagecreatefromgif($this->source);
			if (stristr($this->type,'png'))
			$original=imagecreatefrompng($this->source);


			if (stristr($this->type,'png')){
				imagealphablending($original,false);
				imagesavealpha($original,true);
			}
			imagecopyresampled($thumb,$original,$deplasare_x,$deplasare_y,0,0,$this->new_width,$this->new_height,$this->width,$this->height);


			if ($this->destination){
				if (stristr($this->type,'jp')){
					imagejpeg($thumb,$this->destination,100);
				}
				if (stristr($this->type,'gif')){
					imagegif($thumb,$this->destination,100);
				}
				if (stristr($this->type,'png')){
					imagepng($thumb,$this->destination,9);
				}
			}
			if ($return_pic){
				if (stristr($this->type,'jp')){
					ob_start();
					imagejpeg($thumb,null,100);
					$contents = ob_get_contents();
					ob_end_clean();
					return $contents;
				}
				if (stristr($this->type,'gif')){
					ob_start();
					imagegif($thumb,null,100);
					$contents = ob_get_contents();
					ob_end_clean();
					return $contents;
				}
				if (stristr($this->type,'png')){
					ob_start();
					imagepng($thumb,null,9);
					$contents = ob_get_contents();
					ob_end_clean();
					return $contents;
				}
			}
			if ($return_obj){
				return $thumb;
			}
		}
	}
}