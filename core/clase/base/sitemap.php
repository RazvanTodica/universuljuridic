<?php
class sitemap{
	public $xml;
	// Elemente
	public $elemente;

	function sitemap(){
		global $CONF;
	}

	/**
	 * Adauga url nou.
	 *
	 * @param string $link
	 * @param date_YYYY-MM-DD $lastmod
	 * @param string $changefreq [always],[hourly],[daily],[weekly],[monthly],[yearly],[never]
	 * @param float(0.1->1) $priority
	 */
	public function add_url($link,$lastmod,$changefreq='weekly',$priority='0.5'){
		$element=new sitemap_url();
		$element->loc=$link;
		$element->lastmod=$lastmod;
		$element->changefreq=$changefreq;
		$element->priority=$priority;
		$this->elemente[]=$element;
	}
	/**
	 * Introduce direct un obiect de clasa sitemap_url
	 *
	 * @param sitemap_url $element
	 */
	public function insert_url($element){
		$this->elemente[]=$element;
	}
	/**
	 * Adauga un item in sitemap
	 *
	 * @param unknown_type $unde
	 * @param unknown_type $titlu
	 * @param unknown_type $continut
	 * @param array $atribute
	 * @return unknown
	 */
	private function add_item(&$unde,$titlu,$continut='',$atribute=''){
		$item=$this->xml->createElement($titlu);
		if (is_array($atribute)){
			foreach ($atribute as $key=>$value){
				$item->setAttribute($key,$value);
			}
		}
		if ($continut!=''){
			$continut=$this->xml->createTextNode($continut);
			$item->appendChild($continut);
		}
		$unde->appendChild($item);
		return $item;
	}
	/**
	 * Construieste xml
	 *
	 * se va salva in $this->xml;
	 *
	 */
	public function build_sitemap(){
		$xml= new DOMDocument('1.0','UTF-8');
		$this->xml=$xml;
		$atr['xmlns']='http://www.sitemaps.org/schemas/sitemap/0.9';
		$urlset=$this->add_item($this->xml,'urlset','',$atr);
		// Adaug url-uri in urlset
		foreach ($this->elemente as $element){
			$cur_url=$this->add_item($urlset,'url');
			foreach ($element as $key=>$value){
				$this->add_item($cur_url,$key,$value);
			}
		}
		$this->xml=$xml->saveXML();
	}
}
/**
 * Sitemap -> elementul url
 * loc -> URL
 * lastmod -> YYYY-MM-DD
 * changefreq -> [always],[hourly],[daily],[weekly],[monthly],[yearly],[never]
 * priority -> mai mica decat unu , default 0.5
 *
 */
class sitemap_url{
	public $loc;
	public $lastmod;
	public $changefreq;
	public $priority;
}
?>