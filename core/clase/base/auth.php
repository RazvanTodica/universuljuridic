<?php
class auth_admin {
	function __construct(){
		if (isset($_POST['auth_login']) && $_POST['auth_login']!=''){
			$this->login( $_POST['utilizator'], sha1($_POST['parola'] ));
			return ;
		}
		if (isset($_SESSION['auth_admin']['utilizator']) && $_SESSION['auth_admin']['parola']!=''){
			$this->login($_SESSION['auth_admin']['utilizator'],$_SESSION['auth_admin']['parola']);
		}
		isset($_GET['logout']) ? $this->logout() : NULL ;
	}
	function login($utilizator, $parola)
	{
		if( empty($utilizator) )
		{
			erori::adauga('login','Nu a fost completat campul user.');
		}
		if( empty($parola) )
		{
			erori::adauga('login','Nu a fost completat campul parola.');
		}if (!erori::ia('login'))
		{
			$sql="SELECT `id` FROM `user` WHERE `utilizator`='?' AND `parola`='?' LIMIT 1";
			$obj=db::obj($sql,$utilizator,$parola);
			if($obj)
			{
				$this->user=new user($obj->id);
				$_SESSION['auth_admin']['utilizator'] = $this->user->utilizator;
				$_SESSION['auth_admin']['parola'] = $this->user->parola;
			}
			else
			{
				erori::adauga('login','Userul sau parola au fost completate gresit.');
			}
		}
		return ;
	}
	function logout($cu_redirect=1)
	{
		global $CONF;
		$this->user=null;
		session_regenerate_id(true);
		unset ($_SESSION['auth_admin']);
		if ($cu_redirect==1){
			header("Location: ".$CONF['adminpath']);
		}
	}
}
class auth
{
	/**
	 * Enter description here...
	 *
	 * @var utilizator
	 */
	public $user;
	function __construct()
	{
		if (isset($_POST['auth_login']) && $_POST['auth_login']!=''){
			$this->login( $_POST['utilizator'], sha1($_POST['parola'] ));
			return ;
		}
		if (isset($_SESSION['auth']['utilizator']) && $_SESSION['auth']['parola']!=''){
			$this->login($_SESSION['auth']['utilizator'],$_SESSION['auth']['parola']);
		}
		isset($_GET['logout']) ? $this->logout() : NULL ;
	}
	function login($utilizator, $parola)
	{
		// Daca nu se logheaza folosind un cod , verifica-l
		if( empty($utilizator) )
		{
			erori::adauga('login','Nu a fost completat campul user.');
		}
		if( empty($parola) )
		{
			erori::adauga('login','Nu a fost completat campul parola.');
		}
		if (!erori::ia('login'))
		{
			$sql="SELECT `id` FROM `client` WHERE `utilizator`='?' AND `parola`='?' LIMIT 1";
			$obj=db::obj($sql,$utilizator,$parola);
			if($obj)
			{
				$this->user=new client($obj->id);
				$_SESSION['auth']['utilizator'] = $this->user->utilizator;
				$_SESSION['auth']['parola'] = $this->user->parola;
			}
			else
			{
				erori::adauga('login','Userul sau parola au fost completate gresit.');
			}
		}
		return ;
	}
	function logout($cu_redirect=1)
	{
		global $CONF;
		$this->user=null;
		session_regenerate_id(true);
		unset ($_SESSION['auth']);
		if ($cu_redirect==1){
			header("Location: ".$CONF['sitepath']);
		}
	}
}
?>