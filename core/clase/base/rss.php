<?php

class rss{
	public $xml;
	// Informatii generale
	public $channel;
	// Elemente
	public $elemente;

	function rss(){
		global $CONF;
		$this->channel=new rss_channel();
		$this->channel->title=$CONF['rss_titlu'];
		$this->channel->link=$CONF['sitepath'];
		$this->channel->description=$CONF['rss_descriere'];
		$this->channel->language='ro';
		$this->channel->copyright=$CONF['rss_copyright'];
		$this->channel->lastBuildDate=date(DATE_RFC822);
		$this->channel->generator='FilipNet RSS Generator 1.2';
		$this->channel->webMaster='office@filipnet.ro';
	}
	/**
	 * Enter description here...
	 *
	 * @param string $link
	 * @param string $title
	 * @param string $description
	 * @param DATE_RFC822 $pubDate
	 */
	function add_element($link,$title,$description,$pubDate){
		$element=new rss_element();
		$element->link=$link;
		$element->title=$title;
		$element->description=$description;
		$element->pubDate=$pubDate;
		$this->elemente[]=$element;
	}
	/**
	 * Introduce direct un obiect de clasa rss_element
	 *
	 * @param rss_element $element
	 */
	function insert_element($element){
		$this->elemente[]=$element;
	}
	/**
	 * Enter description here...
	 *
	 * @param unknown_type $unde
	 * @param unknown_type $titlu
	 * @param unknown_type $continut
	 * @param array $atribute
	 * @return unknown
	 */
	function add_item(&$unde,$titlu,$continut='',$atribute=''){
		$item=$this->xml->createElement($titlu);
		if (is_array($atribute)){
			foreach ($atribute as $key=>$value){
				$item->setAttribute($key,$value);
			}
		}
		if ($continut!=''){
			$continut=$this->xml->createTextNode($continut);
			$item->appendChild($continut);
		}
		$unde->appendChild($item);
		return $item;
	}
	function build_rss(){
		global $template;

		$xml= new DOMDocument('1.0');
		$this->xml=$xml;
		$atr['version']='2.0';
		$rss=$this->add_item($this->xml,'rss','',$atr);
		$channel=$this->add_item($rss,'channel');
		foreach ($this->channel as $key=>$value){
			$this->add_item($channel,$key,$value);
		}
		foreach ($this->elemente as $element){
			$item=$this->add_item($channel,'item');
			foreach ($element as $key=>$value){
				$this->add_item($item,$key,$value);
			}
		}
		// Adaug elemente in channel
		$this->xml=$xml->saveXML();
	}
}
class rss_element{
	public $link;
	public $title;
	public $description;
	public $pubDate;
}
class rss_channel{
	public $title;
	public $link;
	public $description;
	public $language;
	public $copyright;
	public $lastBuildDate;
	public $generator;
	public $webMaster;
}
?>