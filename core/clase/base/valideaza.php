<?php
class valideaza{
	function cnp($cnp){
		$clength=strlen($cnp);
		if ($clength != 13){
			return false;
		}
		for ($i = 0; $i <= 12; $i++){
			if (($cnp[$i]>'9') || ($cnp[$i]<'0'))
			return false;
		}
		$a = array(2,7,9,1,4,6,3,5,8,2,7,9);
		$c = 0;
		for ($i = 0; $i < 12; $i++){
			$c += $a[$i] * $cnp[$i];
		}
		$c= $c % 11;
		if ($c == 10){
			$c = 1;
		}
		if ($c == $cnp[12])
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	function cui ($cui)
	{
		$clength=strlen($cui);
		if(strlen($cui)>10)
		return 0;
		$control = substr($cui, -1);
		$cui = substr($cui, 0, $clength-1);
		$cdiff = 10 - $clength;
		$ncui='';
		for ($i=1; $i<=$cdiff;$i++)
		{
			$ncui = $ncui . "0";
		}
		$ncui = $ncui . $cui;
		$suma = $ncui[0]*7 + $ncui[1]*5 +  $ncui[2]*3 +  $ncui[3]*2 +
		$ncui[4]*1 +  $ncui[5]*7 +  $ncui[6]*5 +  $ncui[7]*3 +  $ncui[8] *2;
		$suma = $suma * 10;
		$d1 = $suma % 11;
		$d2 = $d1 % 10;

		if ($d2 == $control)
		return 1;
		else
		return 0;
	}
}
?>