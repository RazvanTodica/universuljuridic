﻿<?php
class fisier extends obiect {

	public $cale_server;

	function continut(){

		global $CONF;

		return file_get_contents($CONF['serverpath'].$this->cale_server);

	}

	function upload_url($url){

		global $CONF;

		$url_split=explode('/',$url);

		$this->nume=$url_split[count($url_split)-1];

		$this->cale_server=$this->upload_cale($this->nume);

		file_put_contents($CONF['serverpath'].$this->cale_server,file_get_contents($url));

	}

	function upload($fis){

		global $CONF;

		if($fis['tmp_name']){

			$this->nume=$fis['name'];

			$this->tip=$fis['type']; 

			$this->marime=$fis['size'];

			$this->cale_server=$this->upload_cale($this->nume);

			move_uploaded_file($fis['tmp_name'],$CONF['serverpath'].$this->cale_server);

		}

	}

	function upload_cale($nume_fisier){

		global $CONF;

		$cale_noua='fisiere/'.date("Y").'/'.date("m").'/'.date("d").'/';

		if (!file_exists($CONF['serverpath'].$cale_noua)){

			mkdir($CONF['serverpath'].$cale_noua,0777,true);

			chmod($CONF['serverpath'].$cale_noua,0777);

		}

		$i=2;

		$nume_posibil=$nume_fisier;

		while (file_exists($CONF['serverpath'].$cale_noua.$nume_posibil)){

			$nume_posibil=$i.'-'.$nume_fisier;

			$i++;

		}

		return $cale_noua.$nume_posibil;

	}

	function save(){

		if($this->cale_server){

			return parent::save();

		}

	}

}

?>