<?php
class confirmari{
	function adauga($unde,$text){
		global $PRIVATE_CONFIRMARI;
		$PRIVATE_CONFIRMARI[$unde][]=$text;
	}
	function exporta(){
		global $PRIVATE_CONFIRMARI;
		return $PRIVATE_CONFIRMARI;
	}
	function ia($unde='toate'){
		global $PRIVATE_CONFIRMARI;

		if ($unde=='toate'){
			if (is_array($PRIVATE_CONFIRMARI)){
				foreach ($PRIVATE_CONFIRMARI as $unde=>$notinter){
					unset($confirmare);
					$CONFIRMARI_UNDE=$PRIVATE_CONFIRMARI["$unde"];
					if (is_array($CONFIRMARI_UNDE)){
						foreach ($CONFIRMARI_UNDE as $text){
							$confirmare['unde']=$unde;
							$confirmare['text'][]=$text;
						}
					}
					$confirmari[]=$confirmare;
				}
			}
		}
		else {
			$ERORI_UNDE=$PRIVATE_CONFIRMARI["$unde"];
			if (is_array($CONFIRMARI_UNDE)){
				foreach ($CONFIRMARI_UNDE as $text){
					$confirmari[]=$text;
				}
			}
		}
		if (count($confirmari)>0){
			return $confirmari;
		}
		else {
			return false;
		}
	}
}
?>