<?php
class categorii{
	function __construct(){
	}
	function lista($parinte=0,$inclusiv=false){
		global $PRIVATE_categorii_aranjate;
		$PRIVATE_categorii_aranjate=array();
		//$sql="SELECT * FROM `CATEGORII` WHERE `cat_id`=? ORDER BY `cat_nume` ASC";

		//$res=query($sql,$parinte);
		$lista=array();
		//while ($obj=$res->fetchRow()){
		$obj=extrage_categorie($parinte);

		$categ=new categorie();
		if($inclusiv){
			$categ->detalii($obj,true,true);
		}
		else{
			$categ->detalii($obj,true);
		}


		//}
		return $PRIVATE_categorii_aranjate;
	}
}
class categorie{
		
	public $categ_adancime;

	public $categ_copii;
	public $categ_tree;
	function dupa_seo($nume_seo,$ia_copii=false){
		if ($nume_seo==''){
			return false;
		}
		$res=db::query("SELECT * FROM `CATEGORII` WHERE `cat_seo`=? LIMIT 1",$nume_seo);
		$obj=$res->fetchRow();
		$categ=$obj;
		$this->detalii($categ,$ia_copii);
		if (!isset($this->cat_id)){
			return false;
		}
		return true;
	}
	function detalii($categ,$ia_copii=false,$salveaza_private=true){
		if (is_numeric($categ)){
			$res=db::query("SELECT * FROM `CATEGORII` WHERE `cat_id`=?",$categ);
			$obj=$res->fetchRow();
			$categ=$obj;
		}
		if (is_object($categ)){
			foreach ($categ as $key=>$value){
				$this->$key=$value;
			}
			$this->categ_tree=$this->tree($categ->cat_id);
			$adancime=count(explode('/',$this->categ_tree))-1;
			$this->categ_adancime=$adancime;
			if ($salveaza_private){
				global $PRIVATE_categorii_aranjate;
				$PRIVATE_categorii_aranjate[]=$this;
			}
			if ($ia_copii){
				$this->categ_copii=$this->copii($categ->cat_id);
				if ($categ->cat_id_parent>0){
					$this->categ_parinti=$this->parinti($categ->cat_id_parent);
					sort($this->categ_parinti);
				}
			}
		}
	}
	function autori(){
		require_once('Cache/Lite.php');
		$options = array(
		'cacheDir' => '/tmp/ujmag/',
		'lifeTime' => 3600, //1 hour
		'automaticSerialization' => true
		);
		$cache = new Cache_Lite($options);
		if (($autori = $cache->get('autori')) === false) {
			//echo 'not hit cache';
			$autori=array();
			$ids_autori=array();
			$azi=date("Y-m-d");
			$sql = ("select autor.* from PUBLICATII
			left join autor_publicatie on autor_publicatie.pbl_id = PUBLICATII.PBL_ID
			left join autor on autor.autor_id = autor_publicatie.autor_id
			where PUBLICATII.PBL_DATA_APARITIE < '{$azi}'
			and autor_publicatie.autor_id is not null
			order by PUBLICATII.PBL_DATA_APARITIE desc limit 42");
//$sql = ("select autor.* from PUBLICATII
//			left join autor_publicatie on autor_publicatie.pbl_id = PUBLICATII.PBL_ID
//			left join autor on autor.autor_id = autor_publicatie.autor_id
//			where PUBLICATII.PBL_DATA_APARITIE < '{$azi}'
//			and autor_publicatie.autor_id is not null
//			group by autor_publicatie.autor_id
//			order by PUBLICATII.PBL_DATA_APARITIE desc limit 42");
//			$res=query($sql);
//			while ($obj=$res->fetchRow()){
//				$autori[] = $obj;
//			}
			$res=query($sql);
			while ($obj=$res->fetchRow()){
				if(!in_array($obj->autor_id,$ids_autori))
				$autori[] = $obj;
				$ids_autori[] = $obj->autor_id;
			}
			$cache->save($autori, 'autori');
		}
		else{
			//echo 'hit cache';
		}
		return $autori;
	}
	function edituri(){
		//$sql = ("select editura.editura_nume,editura.editura_seo,editura.link from editura order by ordine asc");
		$sql = ("select editura.editura_nume,editura.editura_seo,editura.link from editura order by editura_nume asc");
		$res=query($sql);
		while ($obj=$res->fetchRow()){
			$edituri[] = $obj;
		}
		return $edituri;
	}
	function copii($cat_id){
		#if (is_admin()){
		$obj_copii=extrage_categorie_copii($cat_id);
		
		foreach ($obj_copii as $obj){
			$copil=new categorie();
			$copil->detalii($obj,true);
			$copii[]=$copil;
		}
		if (isset($copii)){
			return $copii;
		}
		else {
			return false;
		}
		#}
		/*
		$res=query("SELECT * FROM `CATEGORII` WHERE `cat_id_parent`=? ORDER BY CAT_ORDER,`CAT_NUME` ASC",$cat_id);
		while ($obj=$res->fetchRow()){
		$copil=new categorie();
		$copil->detalii($obj,true);
		$copii[]=$copil;
		}
		if (isset($copii)){
		return $copii;
		}
		else {
		return false;
		}

		*/
	}
	function parinti($cat_id){
		while ($cat_id>0){
			//$res=query("SELECT * FROM `CATEGORII` WHERE `cat_id`=? ORDER BY CAT_ORDER,`CAT_NUME` ASC",$cat_id);
			//$obj=$res->fetchRow();
			$obj=extrage_categorie($cat_id);
			$cat_id=$obj->cat_id_parent;
			$parinte=new categorie();
			$parinte->detalii($obj,false,false);
			$parinti[]=$parinte;
		}
		if (isset($parinti)){
			return $parinti;
		}
		else {
			return false;
		}
	}
	function tree($cat_id){
		$obj=extrage_categorie($cat_id);
		//$res=query("SELECT * FROM `CATEGORII` WHERE `cat_id`=?",$cat_id);
		//$obj=$res->fetchRow();
		if ($obj->cat_id_parent){
			return $this->tree($obj->cat_id_parent).'/'.$obj->cat_seo;
		}
		else {
			return $obj->cat_seo;
		}
	}
}
function extrage_categorie_copii($cat_id){
	global $CACHE_CATEGORII_COPII;
	if (!is_array($CACHE_CATEGORII_COPII)){

		$sql="SELECT
				GROUP_CONCAT(CAST(`CAT_ID` as CHAR)) as `copii`,
				`CAT_ID_PARENT`
			FROM `CATEGORII`
			GROUP by cat_id_parent
			ORDER BY CAT_ORDER,`CAT_NUME` ASC";
		$res=db::query($sql);
		$CACHE_CATEGORII_COPII=array();
		while ($obj=$res->fetchRow()){
			$CACHE_CATEGORII_COPII[$obj->cat_id_parent]=array();
			$copii=explode(',',$obj->copii);
			$sql_c="SELECT `CAT_ID` FROM `CATEGORII` WHERE `CAT_ID` IN ($obj->copii) ORDER BY CAT_ORDER,`CAT_NUME` ASC";
			$res_c=db::query($sql_c);
			$copii=array();
			while ($obj_c=$res_c->fetchRow()) {
				$copii[]=$obj_c->cat_id;
			}
			if (is_array($copii)){
				foreach ($copii as $copil){
					$CACHE_CATEGORII_COPII[$obj->cat_id_parent][]=extrage_categorie($copil);
				}
			}
		}
	}
	if (!$CACHE_CATEGORII_COPII[$cat_id]){
		$CACHE_CATEGORII_COPII[$cat_id]=array();
	}
	return $CACHE_CATEGORII_COPII[$cat_id];
}
function extrage_categorie($cat_id){

	global $CACHE_CATEGORII;
	if (!is_array($CACHE_CATEGORII)){
		$CACHE_CATEGORII=array();
		$sql="SELECT * FROM `CATEGORII`";
		$res=db::query($sql);
		while ($obj=$res->fetchRow()){
			$CACHE_CATEGORII[$obj->cat_id]=$obj;
		}
	}

	return $CACHE_CATEGORII[$cat_id];
}
?>