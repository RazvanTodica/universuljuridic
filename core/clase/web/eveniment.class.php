<?php

class eveniment{

	function __construct($eveniment){

		global $CONF;

		if (is_numeric($eveniment)){

			$this->counter($eveniment);
			//$eveniment=db::query("SELECT * FROM `evenimente` WHERE `ev_id`='?' LIMIT 1",$eveniment);
			$eveniment=db::obj("SELECT e.*,ce.accesari FROM `evenimente` e
								left join counter_evenimente ce on ce.ev_id = e.ev_id
								WHERE e.`ev_id`='?' LIMIT 1",$eveniment);
			//$this->ev_id = $eveniment->ev_id;

		}

		if (is_object($eveniment)){

			foreach ($eveniment as $key=>$value){

				$this->$key=$value;

			}

			if ($this->ev_poza){

				$this->url_orig="http://editurauniversuljuridic.ro/img/orig/{$this->ev_poza}";

				$this->url_mica="http://editurauniversuljuridic.ro/img/thumbs/{$this->ev_poza}";

				$this->url_medie="http://editurauniversuljuridic.ro/img/medium/{$this->ev_poza}";

				$this->url_mare="http://editurauniversuljuridic.ro/img/large/{$this->ev_poza}";

			}

			foreach(db::obj_array("SELECT * FROM `imagine` WHERE `imagine_ev`='?' ORDER BY imagine_pozitie ASC",$this->ev_id) as $obj){

				$obj->url_orig="http://editurauniversuljuridic.ro/img/orig/{$this->imagine_poza}";

				$obj->url_mica="http://editurauniversuljuridic.ro/img/thumbs/{$obj->imagine_poza}";

				$obj->url_medie="http://editurauniversuljuridic.ro/img/medium/{$obj->imagine_poza}";

				$obj->url_mare="http://editurauniversuljuridic.ro/img/large/{$obj->imagine_poza}";

				$this->poze[]=$obj;

			}

		}

	}
	
	static function  evenimentInterviu($id){
		if(is_numeric($id)){
			$eveniment=db::obj("select e.*,ce.accesari from interviu e
				left join counter_interviuri ce on ce.id = e.id 
				where e.apare_colectia_business = 0 and e.activ=1 and e.id='{$id}' limit 1");		
			$sql="SELECT * FROM `interviu_intrebari` WHERE interviu='{$id}' ORDER BY `ordine` ASC";
			$eveniment->intrebari = db::obj_array($sql);
		}
			return $eveniment;
	}
	function evenimenteColectiaBusiness($filtre,$total=false,$ev_unde='',$or_unde=''){
		if($filtre['an'])
		$sql_and .= ' AND year(e.ev_data) = "'.$filtre['an'].'" ';
		if($filtre['luna'])
		$sql_and .= ' AND month(e.ev_data) = "'.$filtre['luna'].'" ';
		if($ev_unde && $or_unde =='')
		$sql_and .= ' AND e.ev_unde = '.$ev_unde.' ';
		if($or_unde)
		$sql_and .='AND (e.ev_unde ='.$ev_unde.' OR e.ev_unde ='.$or_unde.')';
		if($total){
			$sql = 'select count(*) as total from evenimente e
				left join counter_evenimente ce on ce.ev_id = e.ev_id
				where (e.ev_colectia = 0 OR  e.ev_colectia = 1)
				'.$sql_and.'
				';
			return db::obj($sql)->total;
		}
		$sql = 'select e.*,ce.accesari from evenimente e
				left join counter_evenimente ce on ce.ev_id = e.ev_id
				where (e.ev_colectia = 0 OR  e.ev_colectia = 1)
				'.$sql_and.'
				order by e.ev_data desc
				LIMIT '.($filtre['pagina']-1)*$filtre['carti_pe_pagina'].','.$filtre['carti_pe_pagina'].'
				';
		
		
		
		$evenimente = db::obj_array($sql);
		return $evenimente;
	}
	private function counter($eveniment){
		$data = db::obj("select * from counter_evenimente where ev_id = '".$eveniment."'");
		
		if($data->ev_id==""){
			db::query("insert into counter_evenimente set ev_id = '".$eveniment."', accesari = 1");
		}else{
			$accesari = $data->accesari;
			$accesari+=1;
			db::query("update counter_evenimente set accesari = '".$accesari."' where ev_id = '".$data->ev_id."'");
		}
	}
	function iaAni(){
		$sql = 'select DISTINCT(YEAR(e.ev_data)) as an
				from evenimente e
				where e.ev_colectia = 1
				and YEAR(e.ev_data) != 0
				order by YEAR(e.ev_data) desc
				';
		$ani = db::obj_array($sql);
		foreach ($ani as $an)
		$return[] = $an->an;
		return $return;
	}
	function ultimulEveniment($and = ""){
		$eveniment = db::obj("select e.*,ce.accesari from evenimente e
				left join counter_evenimente ce on ce.ev_id = e.ev_id
				where e.ev_colectia = 0 $and 
				order by e.ev_data desc limit 1");
		return $eveniment;
	}
	
	function ultimaConferinta(){
		$eveniment = db::obj("SELECT * FROM evenimente as e left join counter_evenimente ce on ce.ev_id = e.ev_id
				where (e.ev_colectia = 0 OR  e.ev_colectia = 1) and e.apare_in = 'conferinte'
				order by e.ev_data desc limit 1");
		return $eveniment;
	}
	
	function editorial($home,$unde){
		$editorial = db::obj("select * from evenimente where ev_unde={$unde} order by ev_id DESC limit 1");
		if($editorial->link ==''){
			$editorial->link_corect=$editorial->link_nou;
		}else{
			$editorial->link_corect = $editorial->link;
		}
		return $editorial;
	}
	
	function toate_evenimentele(){
		$evenimente = db::obj_array("select e.*,ce.accesari from evenimente e
				left join counter_evenimente ce on ce.ev_id = e.ev_id
				where e.ev_colectia = 0
				order by e.ev_data desc");
		
		return $evenimente;
	}
	
	function interviuri($filtre,$total=false,$ev_unde=''){
		if($filtre['an'])
		$sql_and .= ' AND year(e.data) = "'.$filtre['an'].'" ';
		if($filtre['luna'])
		$sql_and .= ' AND month(e.data) = "'.$filtre['luna'].'" ';
		if($ev_unde)
		$sql_and .= ' AND e.ev_unde = '.$ev_unde.' ';
		if($total){
			$sql = 'select count(*) as total from interviu e 
			left join counter_interviuri ce on ce.id = e.id 
			where e.apare_colectia_business = 0 and e.activ=1
				'.$sql_and.'
				';
			return db::obj($sql)->total;
		}
		$sql = 'select e.*,ce.accesari from interviu e
				left join counter_interviuri ce on ce.id = e.id 
				where e.apare_colectia_business = 0 and e.activ=1
				'.$sql_and.'
				order by e.data desc
				LIMIT '.($filtre['pagina']-1)*$filtre['carti_pe_pagina'].','.$filtre['carti_pe_pagina'].'
				';
		
		
		$interviuri = db::obj_array($sql);
		return $interviuri;
		
	}
	
	
	
}