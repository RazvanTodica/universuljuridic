<?php
class carte_business{
	function __construct($pbl_id = null){
		if(is_numeric($pbl_id)){
			$this->carte 			= db::obj("select * from PUBLICATII where PBL_ID = ?",$pbl_id);
			$this->carte->autori 	= db::obj_array("select a.autor_nume,a.autor_id,ap.titulatura,ap.editie,a.autor_seo,a.autor_descriere,a.autor_poza from autor a left join autor_publicatie ap on ap.autor_id = a.autor_id where ap.pbl_id = {$pbl_id}");  
			    
			
			$this->carte->editura 	=  db::obj("select * from editura where editura_id = '?'",$this->carte->ED_ID);
		}
	}
	public function cartiBusiness($filtre,$total=false, $noi=false){
		
	
		if($total){
			$total = $this->iaCartiColectii($filtre,true, $noi);
			return $total;
		}
		

		$colectii = $this->iaCartiColectii($filtre, false, $noi);
		$this->carti = $colectii;

		
		$this->puneAutori();
		return $this->carti;
		
		
	}
	private function puneAutori(){
		if(is_array($this->carti))
		foreach ($this->carti as $carte){
			$carte->autori = db::obj_array("select a.autor_nume,a.autor_id from autor a left join autor_publicatie ap on ap.autor_id = a.autor_id where ap.pbl_id = {$carte->PBL_ID}");
		}
	}
	
	function iaAni(){
		$sql = 'SELECT DISTINCT (
				YEAR(  `PBL_DATA_APARITIE` )) as an
				FROM  `PUBLICATII` p
				WHERE p.CLC_ID =227
				AND  `p`.`PBL_VIZIBIL` =1
				AND  `p`.`no_sale` =0
				AND  `p`.`PBL_ARHIV` =0 order by an desc';
		$ani_arr = db::obj_array($sql);
		foreach ($ani_arr as $an)
		$ani[] = $an->an;
		return $ani;
	}
	
	
	
	
	function dinCategorie5($fara='',$categorie,$limit=5){
		$sql = "SELECT  p.PBL_TITLU,p.PBL_FILENAME,p.PBL_SEO,p.PBL_ID,p.PBL_PRET,p.link,p.PBL_DATA_APARITIE,p.PBL_NR_APARITII,p.tip
				FROM  `PUBLICATII` p
				WHERE p.CLC_ID = '$categorie'
				AND  `p`.`PBL_VIZIBIL` =1
				AND  `p`.`no_sale` =0
				AND  `p`.`PBL_ARHIV` =0 
				AND  `p`.`PBL_ID` != '{$fara}'
				order by p.PBL_DATA_APARITIE desc limit {$limit}";
		
		$carti = db::obj_array($sql);
		foreach ($carti as $carte)
		$carte->autori = db::obj_array("select a.autor_nume,a.autor_id,a.autor_seo, ap.autor_default from autor a left join autor_publicatie ap on ap.autor_id = a.autor_id where ap.pbl_id = {$carte->PBL_ID}");
		
		return $carti;
	}
	function autoriBusiness($toti=false,$exclude_id=null){
		if($toti)
		$extra = " and autor.autor_poza != '' ";
		if($exclude_id)
		$extra .= " and autor.autor_id != {$exclude_id} ";
		
		$sql = "SELECT autor.* FROM `autor_publicatie` 
				left join autor on autor.autor_id = autor_publicatie.autor_id
				WHERE `pbl_id` in (select PBL_ID from PUBLICATII p
				where p.CLC_ID = 227
				AND `p`.`PBL_VIZIBIL`=1
				AND `p`.`no_sale`=0
				AND `p`.`PBL_ARHIV`=0)
				{$extra}
				order by autor.autor_nume
				";
		$autori = db::obj_array($sql);
		return $autori;
		
	}
	
	public function get_categorie_nume($id){
		$sql = "select CLC_NUME,CLC_SEO FROM COLECTII WHERE CLC_ID='{$id}'";
		$categ = db::obj($sql);
		
		return $categ;
		
	}
	
	public function cauta(){
		if(isset($_GET['key_words']) && $_GET['key_words'] !=''){
			$this->cartiBusiness(array('pagina'=>1,'carti_pe_pagina'=>1000));
			//1. titlu+autor+isbn+cod 
			if(is_array($this->carti)){
				foreach ($this->carti as $carte){
					if(stripos($carte->PBL_TITLU,$_GET['key_words']) !== false){
						$this->rezultate[] = $carte;
					}elseif(stripos($carte->PBL_ISBN,$_GET['key_words']) !== false){
						$this->rezultate[] = $carte;
					}elseif(stripos($carte->PBL_COD,$_GET['key_words']) !== false){
						$this->rezultate[] = $carte;
					}elseif(is_array($carte->autori)){
						foreach ($carte->autori as $autor){
							if(stripos($autor->autor_nume,$_GET['key_words']) !== false){
								$this->rezultate[] = $carte;
								break;
							}
						}
					}
				}
			}
		}
	}
	
	
	function iaCartiColectii($filtre,$total=false, $noi=false){
		global $db,$CONF;
		
				
		
		     //preia colectia universul juridic
			$univers_colectie = db::obj_array("SELECT CLC_ID, CLC_NUME,CLC_SEO FROM COLECTII where clc_id!=1  AND `editura_id`=1  ORDER BY CLC_ORDER");
			
			if(is_array($univers_colectie)){
				foreach ($univers_colectie as $key=>$val){
					$publicatii_id[] = db::obj("SELECT PBL_ID FROM PUBLICATII WHERE CLC_ID=" . $val->CLC_ID . " and PBL_VIZIBIL=1 order by PBL_DATA_APARITIE DESC LIMIT 1");
				}
//				print_r($publicatii_id)
				foreach ($publicatii_id as $key => $val){
					if($val->PBL_ID)
					  $id[] = $val->PBL_ID;
					 
				}

                $id_colectii = implode(",",$id);
				$id_colectii = substr($id_colectii,0,-1);

				
				if($total){
					if($noi) {
						return db::obj("SELECT count(*) as total FROM PUBLICATII as p LEFT JOIN apartine_categ as ac ON ac.PBL_ID=p.PBL_ID WHERE ac.CAT_ID=77 and p.PBL_ARHIV=0 AND p.ED_ID=1  and p.PBL_VIZIBIL=1 ")->total;
					} else {
						return db::obj("SELECT count(*) as total FROM PUBLICATII WHERE PBL_ID IN ($id_colectii) and PBL_ARHIV=0 AND ED_ID=1 AND PBL_ID != 9442  and PBL_VIZIBIL=1 " . ($noi ? " and PBL_DATA_APARITIE < NOW() AND PBL_STOC>0 " : "") . " ")->total;
					}
				}else{
					
						$ordine = $this->ordonare_carti();
						
						if($noi) {
							$colectii=db::obj_array("SELECT p.PBL_SEO, p.PBL_SEOA, p.PBL_ID, p.PBL_TITLU,p.PBL_PRET,p.PBL_FILENAME,p.PBL_STOC,p.link,p.PBL_DATA_APARITIE,p.PBL_NR_APARITII,p.tip FROM PUBLICATII as p LEFT JOIN apartine_categ as ac ON ac.PBL_ID=p.PBL_ID WHERE ac.CAT_ID=77 AND p.PBL_ARHIV=0 AND p.ED_ID=1  and p.PBL_VIZIBIL=1 $ordine LIMIT ".($filtre['pagina']-1)*$filtre['carti_pe_pagina'].",".$filtre['carti_pe_pagina']." ");
						} else {
						
							$colectii=db::obj_array("SELECT p.PBL_SEO, p.PBL_SEOA, p.PBL_ID, p.PBL_TITLU,p.PBL_PRET,p.PBL_FILENAME,p.PBL_STOC,p.link,p.PBL_DATA_APARITIE,p.PBL_NR_APARITII,p.tip FROM PUBLICATII as p WHERE  p.PBL_ID IN ($id_colectii) and p.PBL_ARHIV=0 AND p.ED_ID=1 AND p.PBL_ID != 9442  and p.PBL_VIZIBIL=1 $ordine LIMIT ".($filtre['pagina']-1)*$filtre['carti_pe_pagina'].",".$filtre['carti_pe_pagina']." ");
						}
					
				}
				
		
				  
					
					if(is_array($colectii)){
						foreach($colectii as $k=>$v){
							$colectii[$k]->PBL_PRET = number_format($v->PBL_PRET, 2, '.', '');
							$colectii[$k]->imagine = resize_pic($v->PBL_FILENAME,'110x155');
						}
						
					}
			}
			
			
			
			return $colectii;
		}
		
		
		
		
		
 function get_idColectii($url){
 	$id_colectii = db::obj("SELECT CLC_ID, CLC_NUME,CLC_SEO FROM COLECTII where    `editura_id`=1 AND CLC_SEO='{$url}'  ORDER BY CLC_ORDER");
 	
 	return $id_colectii;
 }
 
 function iaCartiColectieCategorie($filtre,$id_categorie,$total=false){
 	if($total){
 		$total = db::obj("SELECT count(*) as total FROM PUBLICATII WHERE PBL_ID!=153 and PBL_ARHIV=0 AND ED_ID=1 AND PUBLICATII.PBL_ID != 9442  AND PUBLICATII.PBL_VIZIBIL=1 and CLC_ID = $id_categorie ORDER BY PBL_DATA_APARITIE DESC")->total;
 		
 		return $total;
 			
 	}else{
 		
 		$ordine = $this->ordonare_carti();
 		
 		$colectia = db::obj_array("SELECT * FROM PUBLICATII as p WHERE PBL_ID!=153 and PBL_ARHIV=0 AND ED_ID=1 AND p.PBL_ID != 9442  AND p.PBL_VIZIBIL=1 and p.CLC_ID = '{$id_categorie}' $ordine  LIMIT ".($filtre['pagina']-1)*$filtre['carti_pe_pagina'].",".$filtre['carti_pe_pagina']."  ");
 		 		
 		
 		foreach ($colectia as $key=>$val){
 			
	//			$colectia[$key]->PBL_SEO = seo($v->PBL_ID);
			$colectia[$key]->PBL_PRET = number_format($v->PBL_PRET, 2, '.', '');
			$colectia[$key]->imagine = resize_pic($v->PBL_FILENAME,'110x155');
 		}
 		
 		
 		if(is_array($colectia)){
				foreach ($colectia as $k=>$v){
					$colectia[$k]->autori = db::obj_array("select a.autor_nume,a.autor_id, ap.editie ,ap.autor_default from autor a left join autor_publicatie ap on ap.autor_id = a.autor_id where ap.pbl_id = {$v->PBL_ID} order by ap.editie");
				}
			}
 		return $colectia;
 		
 			
 	}	
 	
 }
 
 function ColectieInCursAparitie($filtre){
 	
 	global $db,$CONF;
		
	//Numar total de aparitii	
	$aSql="SELECT SQL_CALC_FOUND_ROWS PBL_ID FROM PUBLICATII WHERE CLC_ID IS NOT NULL and PBL_ARHIV=0 AND ED_ID=1 AND PBL_ID != 9442   AND PBL_VIZIBIL=1 AND (tip='carte' OR tip='revista') AND DATE(`PBL_DATA_APARITIE`)>DATE(NOW()) ORDER BY PBL_DATA_APARITIE ASC";
	
	$total_nr = db::query($aSql);
	$total = $total_nr->num_rows;   
	
	
	//Preia PBL_ID 
	$sSql = "SELECT SQL_CALC_FOUND_ROWS PBL_ID FROM PUBLICATII WHERE CLC_ID IS NOT NULL and PBL_ARHIV=0 AND ED_ID=1 AND PBL_ID != 9442   AND PBL_VIZIBIL=1 AND (tip='carte' OR tip='revista') AND  DATE(`PBL_DATA_APARITIE`)>DATE(NOW()) ORDER BY PBL_DATA_APARITIE ASC";
	$pbl_id = db::obj_array($sSql);
	
	
				foreach ($pbl_id as $key => $val){
					 		$id[] = $val->PBL_ID;
				}
				
				$id_colectii = implode(",",$id);
				$id_colectii = substr($id_colectii,0,-1);
				
			$ordine = $this->ordonare_carti();
			
			//Preia Carti in functie de PBL_ID
			$cartiapartiti = db::obj_array("SELECT  p.PBL_SEO, p.PBL_SEOA, p.PBL_ID, p.PBL_TITLU, p.PBL_PRET, p.PBL_FILENAME, p.PBL_STOC, p.link,p.PBL_DATA_APARITIE,p.PBL_NR_APARITII,p.tip FROM PUBLICATII as p WHERE p.PBL_ID IN ($id_colectii) and p.PBL_ARHIV=0 AND p.ED_ID=1 AND p.PBL_ID != 9442  and p.PBL_VIZIBIL=1 AND (p.tip='carte' OR p.tip='revista') $ordine  LIMIT ".($filtre['pagina']-1)*$filtre['carti_pe_pagina'].",".$filtre['carti_pe_pagina']." ");	
			
//			Adaug in array imaginea cu resize_pic si autor
			foreach ($cartiapartiti as $key=>$val){
				$autorSql = "SELECT a.autor_nume,a.autor_seo,a.link FROM autor_publicatie as apub left join autor as a   on apub.autor_id=a.autor_id where apub.PBL_ID='{$val->PBL_ID}' order by  apub.autor_poz ASC";
				$autori = db::obj_array($autorSql);
				$cartiapartiti[$key]->PBL_PRET = number_format($val->PBL_PRET, 2, '.', '');
				$cartiapartiti[$key]->imagine = resize_pic($val->PBL_FILENAME,'110x155');
				$cartiapartiti[$key]->autori = $autori;
				
			}
			
			
			$cartiapartiti['nr_total_aparitii'] = $total;
 	
 	
 	
 			return $cartiapartiti;
 	
 }
 
 function ordonare_carti(){
 	if(!isset($_GET['ordonare'])) $_GET['ordonare']=2;
 	switch ($_GET['ordonare']){
		case '2':
		$ordine .= ' ORDER BY  `p`.`PBL_DATA_APARITIE` DESC ';		
		 break;	
		case '4':
		 	$ordine .= ' ORDER BY  `p`.`PBL_PRET` DESC ';
		break;
		case '3':
			$ordine .= ' ORDER BY  `p`.`PBL_PRET` ASC ';
		break;
		default:
			$ordine .= ' ORDER BY  `p`.`PBL_DATA_APARITIE` DESC ';		
		break;
	}
	return $ordine;
 }
 
 function cautaInDomeniiColectii($filtre,$total=false){
 	
 	if(!$total)
 		$sql = "SELECT * FROM `PUBLICATII` as p";
 	else {
 		$sql = "SELECT p.PBL_ID, COUNT(*) as total FROM `PUBLICATII` as p";
 	}

 	if($filtre['key_words']){
		$sql .= " left join autor_publicatie ap on ap.pbl_id = p.PBL_ID left join autor a on a.autor_id = ap.autor_id ";
		$sql .= " WHERE (";
		$sql .= " a.autor_nume like '%".db::escape($filtre['key_words'])."%' ";
		$sql .= " or ";
		$sql .= " p.PBL_ISBN like '%".db::escape($filtre['key_words'])."%' ";
		$sql .= " or ";
		$sql .= " p.PBL_COD like '%".db::escape($filtre['key_words'])."%' ";
		$sql .= " or ";
		$sql .= " p.PBL_TITLU like '%".db::escape($filtre['key_words'])."%' ";
		$sql .= "  ";
		$sql .= " )";
		}

	$sql .= " AND p.ED_ID=1 AND p.PBL_VIZIBIL=1 ";
	$sql .= " group by p.PBL_ID ";
	$sql .= " ORDER BY p.PBL_ID desc";
	
	if($total) {
		$sqlNum  = db::query($sql);
					
		$total = $sqlNum->num_rows;
		
		return $total;
	}

     $colectia = db::obj_array($sql);

     if(is_array($colectia)){
         foreach ($colectia as $k=>$v){
             $colectia[$k]->autori = db::obj_array("select a.autor_nume,a.autor_id, ap.editie, ap.autor_default from autor a left join autor_publicatie ap on ap.autor_id = a.autor_id where ap.pbl_id = {$v->PBL_ID} order by ap.editie");
         }
     }


	return $colectia;


 	//reviste
 	
	$sqlCat = "
	SELECT ac.CAT_ID
		FROM PUBLICATII AS pbl
		LEFT JOIN apartine_categ AS ac ON pbl.PBL_ID = ac.PBL_ID
		WHERE pbl.ED_ID =1
		AND pbl.PBL_VIZIBIL =1
		AND pbl.no_sale =0";
	
	$publCat = db::obj_array($sqlCat);
	
 	foreach ($publCat as $key=>$val){
 		$idPublCat .=$val->CAT_ID.',';
 	}
 	
 	$idPublCat = substr($idPublCat,0,-1);
 	
 	$sqlCategorii = "SELECT * FROM `CATEGORII` as cat WHERE `CAT_ID` in ($idPublCat) and `CAT_ID_PARENT` = '87'  order by CAT_NUME ASC "; 	
 	$categoriReviste = db::obj_array($sqlCategorii);
 	if(is_array($categoriReviste)){
 		
 		foreach ($categoriReviste as $k=>$v){
 			
 			$idCategorii .=$v->CAT_ID.',';
 		
		}
		$idCategorii = substr($idCategorii,0,-1);
		
		
		$publicatiCategorii = db::obj_array("select PBL_ID from apartine_categ where CAT_ID IN ($idCategorii)");
		
	
		
		foreach ($publicatiCategorii as $k=>$val){
		    $catid .= $val->PBL_ID.',';
			
		}
		
		$catid = substr($catid,0,-1);
 	}
 		  
 			  
 	if($filtre['key_words']){
					$sql_join .= " left join autor_publicatie ap on ap.pbl_id = p.PBL_ID left join autor a on a.autor_id = ap.autor_id ";
					$sql_or .= " and (";
					$sql_or .= " a.autor_nume like '%".db::escape($filtre['key_words'])."%' ";
					$sql_or .= " or ";
					$sql_or .= " p.PBL_ISBN like '%".db::escape($filtre['key_words'])."%' ";
					$sql_or .= " or ";
					$sql_or .= " p.PBL_COD like '%".db::escape($filtre['key_words'])."%' ";
					$sql_or .= " or ";
					$sql_or .= " p.PBL_TITLU like '%".db::escape($filtre['key_words'])."%' ";
					$sql_or .= "  ";
					$sql_or .= " )";
					$sql_group = " group by p.PBL_ID ";
			}
		
		     //preia colectia prouniversitaria
			$univers_colectie = db::obj_array("SELECT CLC_ID, CLC_NUME,CLC_SEO FROM COLECTII where clc_id!=1  AND `editura_id`=1  ORDER BY CLC_ORDER");
			
			if(is_array($univers_colectie)){
				foreach ($univers_colectie as $key=>$val){
					$publicatii_id[] = db::obj("SELECT PBL_ID FROM PUBLICATII WHERE CLC_ID=" . $val->CLC_ID . " and PBL_VIZIBIL=1 order by PBL_DATA_APARITIE DESC LIMIT 1");
				}
				
				foreach ($publicatii_id as $key => $val){
					 $id[] = $val->PBL_ID;
					 
				}
				
				$id_colectii = implode(",",$id);
				$id_colectii = substr($id_colectii,0,-1);
				
				$idColectiiDomenii = $id_colectii.$catid;	
				
					
				
				
				
				if($total){
					
					
					$sqlNum  = db::query("SELECT 
						p.PBL_ID,count(*) as total
						FROM PUBLICATII as p
							$sql_join
						WHERE p.PBL_ID IN ($idColectiiDomenii) 
							and p.PBL_ARHIV=0 
							AND p.ED_ID=1 
							AND p.PBL_ID != 9442  
							and p.PBL_VIZIBIL=1 
							$sql_and  
							$sql_or 
							$sql_group
							$ordine 
							");
					
					$total = $sqlNum->num_rows;
					
					return $total;
									
					
				}else{
					
						$ordine = $this->ordonare_carti();
					
						$sqlCol = "SELECT 
							p.PBL_SEO,
							p.PBL_SEOA,
							p.PBL_ID,
							p.PBL_TITLU, 
							p.PBL_PRET,
							p.PBL_FILENAME, 
							p.PBL_STOC,
							p.link 
						FROM PUBLICATII as p
							$sql_join
						WHERE p.PBL_ID IN ($idColectiiDomenii) 
							and p.PBL_ARHIV=0 
							AND p.ED_ID=1 
							AND p.PBL_ID != 9442  
							and p.PBL_VIZIBIL=1 
							$sql_and  
							$sql_or
							$sql_group  
							$ordine 
						LIMIT ".($filtre['pagina']-1)*$filtre['carti_pe_pagina'].",".$filtre['carti_pe_pagina']." ";
						
						
						
						$colectii=db::obj_array($sqlCol);
						
				}
				
		
				  
				
					
					if(is_array($colectii)){
						foreach($colectii as $k=>$v){
							$colectii[$k]->PBL_PRET = number_format($v->PBL_PRET, 2, '.', '');
							$colectii[$k]->imagine = resize_pic($v->PBL_FILENAME,'110x155');
						}
						
					}
			}
			
			return $colectii;
 	
 	
 	
 }
 
 
 public function totiAutori($ordonare,$filtre,$total=false){
		
 	if($total){   
 		 $autori = db::obj_array(
	   "select count(DISTINCT(a.autor_id)) as total from PUBLICATII as p
	   left join autor_publicatie as ap on ap.pbl_id=p.PBL_ID
	   left join autor as a on a.autor_id=ap.autor_id
       where  `p`.`PBL_VIZIBIL`=1 AND `p`.`no_sale`=0
	   AND `p`.`PBL_ARHIV`=0 AND `p`.`ED_ID`=1  $ordonare   order by a.autor_nume ASC");
	   
 	}else{
	   $autori = db::obj_array(
	   "select a.* from PUBLICATII as p
	   left join autor_publicatie as ap on ap.pbl_id=p.PBL_ID
	   left join autor as a on a.autor_id=ap.autor_id
       where  `p`.`PBL_VIZIBIL`=1 AND `p`.`no_sale`=0
	   AND `p`.`PBL_ARHIV`=0 AND `p`.`ED_ID`=1  $ordonare  group by autor_id  order by a.autor_nume ASC LIMIT ".($filtre['pagina']-1)*$filtre['carti_pe_pagina'].",".$filtre['carti_pe_pagina']." ");
 	}
	  
		return $autori;
	}
	
	
}
?>