<?php
class filtru{
	private $and;
	private $inner_join;
	private $left_join;
	private $lmit_nr;
	private $lmit_start;
	private $group;
	function __construct(){
		global $editura;
		$this->and=array ();
		$this->inner_join=array ();
		$this->and['p']=array ();
		$this->and['p'][]="(p.ED_ID = '{$editura->id}' )";
		$this->order->by='p.PBL_ID';
		$this->order->dir='DESC';
	}
	function join(){
		$this->inner_join[]=" join autor_publicatie as ap on ap.pbl_id = p.PBL_ID ";
		$this->inner_join[]=" join autor as a on a.autor_id = ap.autor_id ";
	}
	function group(){
		$this->group=" group by p.PBL_ID ";
	}
	function autor_id($autor_id){
		$this->inner_join[]=" INNER JOIN autor_publicatie as ap ON ap.autor_id='{$autor_id}' AND ap.pbl_id=p.PBL_ID ";
	}
	function clc_id($clc_id){
		$this->and['p'][]="( p.CLC_ID = '{$clc_id}' )";
	}
	function nume($nume){
		$nume=str_replace('%',' ',$nume);
		$nume=db::escape($nume);
		$this->and['p'][]="( p.PBL_TITLU LIKE  '%{$nume}%' or a.autor_nume LIKE '%{$nume}%' )";
	}
	function limit($nr,$start=0){
		$this->limit_nr=$nr;
		$this->limit_start=$start;
	}
	function order($by,$dir){
		$this->order->by=$by;
		$this->order->dir=$dir;
	}
	function publicatii(){
		$publicatii=array ();
		foreach(db::obj_array($this->sql()) as $p){
			$publicatii[]=new publicatie($p);
		}
		return $publicatii;
	}
	function sql(){
		$sql="SELECT SQL_CALC_FOUND_ROWS p.* FROM PUBLICATII p ";
		if(count($this->inner_join)){
			$sql.=implode("\n\t",$this->inner_join);
		}
		$sql.=" WHERE 1 ";
		$sql.=" AND ".implode(' AND ',$this->and['p']);
		
		if($this->group!=''){
			$sql.=" and PBL_VIZIBIL=1 and 	PBL_ARHIV=0  {$this->group}";
		}else
		$sql.=" and PBL_VIZIBIL=1 and 	PBL_ARHIV=0  ORDER BY {$this->order->by} {$this->order->dir}";


		if($this->limit_nr) $sql.=" LIMIT {$this->limit_start},{$this->limit_nr}";
if(is_admin()){
	//echo $sql;
}
		return $sql;
	}
}