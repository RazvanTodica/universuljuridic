<?php
/**
 * Definim bannere
 *
 */


class bannere {
	
	function ia_bannere_sistem($pagina, $status = 'activ') {
		
		global $CONF;
		
		$sqlBanner = "select * from sistem_bannere where site='uj' and tip='banner_intern' and pagina='$pagina' and stadiu='$status' and activ='1' order by ordine";
		
		$bannerePro = db::obj_array($sqlBanner);
		
		if(is_array($bannerePro)){
			foreach ($bannerePro as $key=>$val){
				$bannerePro[$key]->imagine = $CONF['sitepathuj'].'fisiere/sistem_bannere/'.$val->poza;
			}
		}

		return $bannerePro;

	}
	
	function bannere_header(){
		global $CONF;
	   $promovate = db::obj_array("select pb.PBL_TITLU,pb.PBL_ID,pb.PBL_PRET,pb.PBL_FILENAME,cp.imagine,pb.link,pb.PBL_AUTOR from  carti_promovate as cp  left join PUBLICATII as pb on cp.id_carte=pb.PBL_ID where cp.activ=1 and cp.tip=1 order by cp.pozitie DESC");
	   
	   foreach ($promovate as $key=>$val){
	   	$autorSql = "SELECT a.autor_nume,a.autor_seo,a.link FROM autor_publicatie as apub left join autor as a   on apub.autor_id=a.autor_id where apub.PBL_ID='{$val->PBL_ID}'";
	   	$autori = db::obj_array($autorSql);
	   	
	   	$imgcale = $CONF['sitepath'].'pub/img_univers/'.$val->imagine;
	   	$promovate[$key]->PBL_PRET = number_format($val->PBL_PRET, 2, '.', '');
		$promovate[$key]->imagine = $imgcale;
		$promovate[$key]->autori = $autori;	
	   }
		return $promovate;
	}
	
	
	function bannere_slider_promo($tip){
		global $CONF;
		$sql = "select * from carti_promovate where tip=$tip and activ=1 order by data_adaugare DESC";
		$slider_promo = db::obj($sql);
		
	
		return $slider_promo;
	}
	
}

?>