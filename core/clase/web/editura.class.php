<?php

class editura{

	public function staff_editorial(){
		$staff = db::obj_array("select * from staff where staff_redactie=1 and staff_site=1 ORDER BY staff_id=149 DESC,staff_id ASC");
		foreach ($staff as $k=>$v){
			if($v->staff_filename !=''){
				$staff[$k]->imagine_sfaff = resize_pic($v->staff_filename,'130x181');
			}else{
				$staff[$k]->imagine_sfaff = $CONF['sitepath'].'pub/images/no_image.png';
			}
		}
		
		return $staff;
	}
	
	public function cariere(){
		$cariere = db::obj_array("select * from CARIERE order by pozitie");
		return $cariere;
	}
	
	public  function get_id_staff($url){
		$url = db::escape($url);
		$id = db::obj("select staff_id from staff where staff_link='{$url}'")->staff_id;
		
		return $id;
	}
	
	public function get_detalii_staf($id){
		
		$staff = db::obj("select * from staff where staff_id='{$id}'");
		return $staff;
	}
}

?>