<?php
 class reviste   {
 	
 	public $categorie_nume;
 	
 	
 static function reviste_categorie_by_link($link){
       $id = db::obj("select * from CATEGORII where link='{$link}' and CAT_ID_PARENT='87'");
 		
       return $id;
 }
 	
 static function reviste_by_categorie($categorie,$filtre,$total=false){
 	
 				$ordine = reviste::ordonare_carti();
				
 				if($total){
 						$reviste = db::obj_array("
		 				select count(*) as total from apartine_categ as ap 
		 				inner join PUBLICATII as p on ap.PBL_ID=p.PBL_ID 
		 				AND ap.CAT_ID ='{$categorie}' 
		 				AND p.ED_ID=1
		 				AND `p`.`PBL_VIZIBIL`=1
						AND `p`.`no_sale`=0
						AND `p`.`PBL_ARHIV`=0 ");
 				}else{
	 				$reviste = db::obj_array("
		 				select p.* from apartine_categ as ap 
		 				inner join PUBLICATII as p on ap.PBL_ID=p.PBL_ID 
		 				AND ap.CAT_ID ='{$categorie}' 
		 				AND `p`.`PBL_VIZIBIL`=1
						AND `p`.`no_sale`=0
						AND p.ED_ID=1
						AND `p`.`PBL_ARHIV`=0 $ordine    LIMIT ".($filtre['pagina']-1)*$filtre['carti_pe_pagina'].",".$filtre['carti_pe_pagina']." ");
 				}
	 			
		
		return $reviste;
 	
 }
 	
 	
 static function ultimele_reviste_aparute($filtre,$cat_id_parinte=87,$total=false){
			if($total){
				$publicatii = db::obj_array("
 				select count(*) as total from apartine_categ as ap 
 				inner join PUBLICATII as p on ap.PBL_ID=p.PBL_ID 
 				AND ap.CAT_ID IN (SELECT CAT_ID FROM CATEGORII WHERE CAT_ID_PARENT='{$cat_id_parinte}' order by CAT_NUME ASC) 
 				AND `p`.`PBL_VIZIBIL`=1
 				AND p.ED_ID=1
				AND `p`.`no_sale`=0
				AND `p`.`PBL_ARHIV`=0  ORDER BY p.PBL_DATA_APARITIE DESC"
				);
			}else{
 			
				$ordine = reviste::ordonare_carti();
				
	 			$publicatii = db::obj_array("
	 				select p.* from apartine_categ as ap 
	 				inner join PUBLICATII as p on ap.PBL_ID=p.PBL_ID 
	 				AND ap.CAT_ID IN (SELECT CAT_ID FROM CATEGORII WHERE CAT_ID_PARENT='{$cat_id_parinte}' order by CAT_NUME ASC) 
	 				AND `p`.`PBL_VIZIBIL`=1
	 				AND p.ED_ID=1
					AND `p`.`no_sale`=0
					AND `p`.`PBL_ARHIV`=0 $ordine    LIMIT ".($filtre['pagina']-1)*$filtre['carti_pe_pagina'].",".$filtre['carti_pe_pagina']." "
				);
			}
			
 			
			return $publicatii;
 }
 
 
static function dinCategorie($publicatie_id,$categorie_nume=false,$tip='revista'){
	if($categorie_nume){
		 $sql = "
			select cat.CAT_NUME,cat.CAT_SEO from apartine_categ as ac
			left join CATEGORII as cat on ac.CAT_ID=cat.CAT_ID
			where ac.PBL_ID = '{$publicatie_id}' and cat.CAT_ID_PARENT=87
			";
		
			$categorie_produs = db::obj($sql);
			
			return $categorie_produs;
	}
	$sql = "
	select ac.CAT_ID from apartine_categ as ac
	left join CATEGORII as cat on ac.CAT_ID=cat.CAT_ID
	where ac.PBL_ID = '{$publicatie_id}' and cat.CAT_ID_PARENT=87
	";
	$categorie_produs = db::obj($sql)->CAT_ID;	
	
	
	$sql_reviste = "
	  select pbl.PBL_TITLU,pbl.PBL_FILENAME,pbl.PBL_SEO,pbl.PBL_ID,pbl.PBL_PRET,pbl.link from PUBLICATII as pbl
	  left join apartine_categ as ac ON pbl.PBL_ID=ac.PBL_ID
	  where ac.CAT_ID='{$categorie_produs}' 
	  AND  pbl.ED_ID =1
	  AND pbl.PBL_VIZIBIL =1
	  AND pbl.no_sale =0
	  AND pbl.tip='$tip'
	  AND pbl.PBL_ID !='{$publicatie_id}'
	  order by pbl.PBL_DATA_APARITIE DESC
	  limit 5
	";
	
	$reviste_din_categorie = db::obj_array($sql_reviste);
	


	foreach ($reviste_din_categorie as $key=>$val){
		$reviste_din_categorie[$key]->autori = db::obj_array("select a.autor_nume,a.autor_id,a.autor_seo from autor a left join autor_publicatie ap on ap.autor_id = a.autor_id where ap.pbl_id ={$val->PBL_ID}");
	}
	
	
	return $reviste_din_categorie;
	
}
 		
 	
 static function lista_reviste($parinte=87){
 	$sql = 
 	"SELECT * FROM `CATEGORII` as cat WHERE `CAT_ID` in 
 	(
 	SELECT ac.CAT_ID
	FROM PUBLICATII AS pbl
	LEFT JOIN apartine_categ AS ac ON pbl.PBL_ID = ac.PBL_ID
	WHERE pbl.ED_ID =1
	
	AND pbl.PBL_VIZIBIL =1
	AND pbl.no_sale =0
	)
	and `CAT_ID_PARENT` = '{$parinte}'  order by CAT_NUME ASC "; 
 	$reviste = db::obj_array($sql);  
 	
 	return $reviste;
 }
 
 
 

 
 	
  function periodice($limit=1){
  	//Preia publicatia id
  	$sSql = "SELECT PBL_ID FROM PUBLICATII WHERE REVISTA_ID!=0 and PBL_ARHIV=0 AND ED_ID=1  AND PBL_VIZIBIL=1 and PBL_ID!=153 and CLC_ID IS NOT NULL AND ED_ID=1 ORDER BY PBL_DATA_APARITIE DESC LIMIT 0,$limit";
  	$publicatii_id = db::obj_array($sSql);
  	
  	foreach ($publicatii_id as $key=>$val){
  		
  		$pSql = "SELECT PBL_TITLU, PBL_PRET, PBL_FILENAME, PBL_STOC,PBL_ID FROM PUBLICATII WHERE  REVISTA_ID != 0 AND PBL_VIZIBIL=1 and PBL_ID = '$val->PBL_ID'  and PBL_ARHIV=0 AND ED_ID=1";
  		$periodice = db::obj($pSql);
  		
  		
  		$periodice->imagine = resize_pic($periodice->PBL_FILENAME,'90x85');
  		
  	}
  		return $periodice;
  	
  }
  
  
   function ordonare_carti(){
 	switch ($_GET['ordonare']){
		case '2':
		$ordine .= ' ORDER BY  `p`.`PBL_DATA_APARITIE` DESC ';		
		 break;	
		case '4':
	 $ordine .= ' ORDER BY  `p`.`PBL_PRET` DESC ';
	break;
	case '3':
	$ordine .= ' ORDER BY  `p`.`PBL_PRET` ASC ';
	break;
	default:
	 $ordine .='ORDER BY p.PBL_DATA_APARITIE DESC';  
		 	
	}
	return $ordine;
 }
 
static function ultima_revista(){
 	$sql = "
 	 SELECT * FROM PUBLICATII
	 WHERE PBL_VIZIBIL=1 AND no_sale=0 AND
     PBL_ARHIV=0 AND tip='revista' AND ED_ID=1 order by PBL_DATA_APARITIE DESC LIMIT 1
	";
 	
 	$ultima_revista = db::obj_array($sql);
 	foreach ($ultima_revista as $key=>$val){
 		$ultima_revista[$key]->autori = db::obj_array("select a.autor_nume,a.autor_id,a.autor_seo,a.titulatura,ap.editie  from autor a left join autor_publicatie ap on ap.autor_id = a.autor_id where ap.pbl_id ={$val->PBL_ID}");
 		
 
 		
 	}
 	
 	 
 	
 	return $ultima_revista;
 }
 
 }
?>