<?php
	class contact extends PHPMailer   {
	function __construct($nume,$email,$subiect,$mesaj,$total){
			$this->nume = $nume;
			$this->email = $email;
			$this->subiect = $subiect;
			$this->mesaj = $mesaj;
			$this->total = $total;
	}
		
	 function verifica_form(){
	    	$errrors =  array();
	    	if(empty($this->nume)){
	    		$errrors['nume'] = 1;
	    	}
	    	if(empty($this->email) || !$this->checkEmail($this->email) ){
	    		$errrors['mail'] = 1;
	    	}
	    	if(empty($this->subiect)){
	    		$errrors['subiect']=1;
	    	}
	    	if(empty($this->mesaj)){
	    		$errrors['mesaj']=1;
	    	}
	    	if($this->total !=20){
	    		$errrors['total'] =1;
	    	}
	    	
	    	return $errrors;
	 }
	 
	 function trimite_mail($to,$from,$nume,$body,$subiect){
			$this->SetFrom($from,$nume);
			$this->AddAddress($to,"Universul Juridic");  
			$this->Subject    = $subiect;
			$this->MsgHTML($body);

			if(!$this->Send()) {
  				$send =0;
			} else {
  				$send = 1;
			}
	 	
			return $send;
	 }
	 
	 
	function checkEmail($email){

	if(!preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/",$email)){

		return false;

	}

	else{

		return true;

	}

	}

		
	}
?>