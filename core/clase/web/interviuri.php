<?php

class interviuri{
	function __construct($interviu){

		global $CONF;

		if (is_numeric($interviu)){

			$this->counter($interviu);
			//$eveniment=db::query("SELECT * FROM `evenimente` WHERE `ev_id`='?' LIMIT 1",$eveniment);
			$interviu=db::obj("SELECT i.*,c.accesari FROM `interviu` i
								left join counter_interviuri c on c.id = i.id
								WHERE i.`id`='?' LIMIT 1",$interviu);
		}
			
		if(is_object($interviu)) foreach($interviu as $k=>$v){

			$k=strtolower($k);

			$this->$k=$v;

		}
		
		$this->intrebari=array();
		$sql="SELECT * FROM `interviu_intrebari` WHERE interviu='{$this->id}' ORDER BY `ordine` ASC";
		$this->intrebari = db::obj_array($sql);
		
	}
	function interviuriColectiaBusiness($filtre,$total=false){
		if($filtre['an'])
		$sql_and .= ' AND year(i.data) = "'.$filtre['an'].'" ';
		if($filtre['luna'])
		$sql_and .= ' AND month(i.data) = "'.$filtre['luna'].'" ';
		if($total){
			$sql = 'select count(*) as total from interviu i
				where i.apare_colectia_business = 1
				'.$sql_and.'
				';
			return db::obj($sql)->total;
		}
		$sql = 'select i.*,ce.accesari from interviu i
				left join `counter_interviuri` as ce on ce.id = i.id
				where i.apare_colectia_business = 1
				'.$sql_and.'
				order by i.data desc
				LIMIT '.($filtre['pagina']-1)*$filtre['carti_pe_pagina'].','.$filtre['carti_pe_pagina'].'
				';
		
		
		
		$interviuri = db::obj_array($sql);
		return $interviuri;
	}
	private function counter($interviu){
		$data = db::obj("select * from counter_interviuri where id = '".$interviu."'");
		
		if($data->id==""){
			db::query("insert into counter_interviuri set id = '".$interviu."', accesari = 1");
		}else{
			$accesari = $data->accesari;
			$accesari+=1;
			db::query("update counter_interviuri set accesari = '".$accesari."' where id = '".$data->id."'");
		}
	}
	function iaAni(){
		$sql = 'select DISTINCT(YEAR(i.data)) as an
				from interviu i
				where i.interviu = 1
				and YEAR(i.data) != 0
				order by YEAR(i.data) desc
				';
		$ani = db::obj_array($sql);
		foreach ($ani as $an)
		$return[] = $an->an;
		return $return;
	}
	function ultimulInterviu(){
		$sql = 'select i.*,ce.accesari from interviu i
				left join `counter_interviuri` as ce on ce.id = i.id
				where i.apare_colectia_business = 0
				order by i.data desc
				LIMIT 1
				';
		$interviu = db::obj($sql);
		return $interviu;
	}
}

?>