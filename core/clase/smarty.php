<?php
require_once(SERVER_PATH.'core/smarty/Smarty.class.php');
class smarty_template extends Smarty
{

	function display($page)
	{
		echo $this->fetch($page);
	}
	function fetch($page){


		global $CONF;
		global $cart;
		global $login;
		global $nr_selecturi_sql;
		global $nr_updateuri_sql;
		global $SITE;
		global $link;
		global $uri_curent;

		$this->assign('erori',erori::exporta());
		$this->assign('confirmari',confirmari::exporta());
		$this->assign('CONF', $CONF);
		$this->assign('link' , $link);
		$this->assign('login',$login);
		$this->assign('is_admin',is_admin());
		$this->assign('nr_selecturi_sql',$nr_selecturi_sql);
		$this->assign('nr_updateuri_sql',$nr_updateuri_sql);

		$this->assign('timp_executie',number_format( microtime(true)-$SITE['timp_start'],4,',','.'));


		$this->assign('cart',$cart);
		$this->assign('META',meta::exporta());

		$this->assign('uri_curent', $uri_curent);

		$pagina= parent::fetch($page);

		$pagina=preg_replace("@(href=[\"'])(\#)@Uis",'$1'.$_SERVER['REQUEST_URI'].'$2',$pagina);

		$pagina=preg_replace("@href=([\"'])www@Uis",'href=$1http://www',$pagina);
		
		return $pagina;
	}
}
?>