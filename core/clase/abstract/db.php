<?php
function add_query_log($sql) {
	global $query_log;
	$query_log [] = $sql;
}
function save_query_log() {
	global $db;
	global $query_log;
	if (count ( $query_log )) {
		foreach ( $query_log as $sql ) {
			$db->nolog_query ( $sql );
		}
	}
}
register_shutdown_function ( 'save_query_log' );
class mysqli_log extends mysqli {
	public $fnet;
	function &query($sql) {
		if (strstr ( $sql, "SELECT" )) {
			$this->fnet->nr_selecturi_sql ++;
		}
		if (strstr ( $sql, "UPDATE" )) {
			$this->fnet->nr_updateuri_sql ++;
		}
		$start = microtime ( true );
		$qry = parent::query ( $sql );
		$this->fnet->error = $this->error;
		$this->fnet->errno = $this->errno;
		$timp = number_format ( microtime ( true ) - $start, 4, '.', '' );
		if (strstr ( $sql, 'SQL_CALC_FOUND_ROWS' )) {
			$this->fnet->found_rows = parent::query ( "SELECT FOUND_ROWS() as `nr`" )->fetch_object ()->nr;
		}
		$ins_qry = db::escape ( $sql );
		//add_query_log ( "INSERT INTO `sql_log` SET `qry`='{$ins_qry}',`timp`='{$timp}';" );

		return $qry;

	}
	function nolog_query($sql) {
		parent::query ( $sql );
	}
	function &multi_query($sql) {
		global $nr_updateuri_sql;
		$m = explode ( ';', $sql );
		$nr_updateuri_sql += count ( $m );
		return parent::multi_query ( $sql );
	}

}
class db {
	static function schimba_db($db) {
		global $CONF;
		$CONF ['db'] = $CONF ["db_{$db}"];
		db::connect ( true );
	}
	static function connect($forteaza = false) {
		global $db;
		global $CONF;
		if (get_class ( $db ) != 'mysqli_log' || ! $db->ping () || $forteaza) {
			$db = new mysqli_log ( $CONF ['db'] ['host'], $CONF ['db'] ['user'], $CONF ['db'] ['parola'], $CONF ['db'] ['db'] );
			$db->set_charset ( 'utf8' );
			$db->query ( "SET NAMES 'utf8';" );
			if (mysqli_connect_error ()) {
				trigger_error ( 'Nu am conexiune cu bd', E_USER_ERROR );
			}
		}
	}
	static function disconnect() {
		global $db;
		$db->close ();
	}
	static function build_sql($params) {
		$key_sql = rand () . '_KEY_SQL_' . rand ();
		$sql = $params [0];
		if (count ( $params ) > 1) {
			$parametri = array ();
			$sql = str_replace ( '?', $key_sql, $sql, $nr );
			if ($nr == count ( $params ) - 1) {
				for($i = 1; $i < count ( $params ); $i ++) {
					$params [$i] = db::escape ( $params [$i] );
					$sql = substr_replace ( $sql, $params [$i], strpos ( $sql, $key_sql ), strlen ( $key_sql ) );
				}
			} else {
				$a = debug_backtrace ();
				foreach ( $a as $key => $value ) {
					echo "Linia " . $value ['line'] . " functia " . $value ['function'] . " ,din " . $value ['file'] . '<br />';
				}
				trigger_error ( 'Ceva in neregula cu parametrii.', E_USER_ERROR );
				return;
			}
		}
		unset ( $params );
		unset ( $parametri );
		//echo $sql.'<br />';
		return $sql;
	}
	/**
	 * Query server pt select
	 *
	 * @param string $sql
	 * @return db_rez
	 */
	static function &query($sql) {
		global $db;
		if (count ( func_get_args () ) > 1) {
			$sql = db::build_sql ( func_get_args () );
		}
		db::connect ();

		return new db_rez ( $db->query ( $sql ) );
	}
	static function &multi_query($sql) {
		global $db;
		db::connect ();
		$db->multi_query ( $sql );
		if ($db->multi_query ( $sql )) {
			do {
				$result = $db->store_result ();
				if (is_object ( $result ))
					$result->free ();
			} while ( $db->next_result () );
		}
	}
	/**
	 * Query server pt update/insert
	 *
	 * @param string $sql
	 * @return db_rez
	 */
	static function &uquery($sql) {
		global $db;
		db::connect ();
		if (count ( func_get_args () ) > 1) {
			$sql = db::build_sql ( func_get_args () );
		}
		$rez = new db_rez ( $db->query ( $sql ) );
		if ($db->errno) {
			echo $db->error;
		}
		if (preg_match ( "@^INSERT@i", $sql )) {
			return $rez->insert_id;
		}
		if (preg_match ( "@^UPDATE@i", $sql )) {
			return $rez->affected_rows;
		}
	}
	static function obj($sql) {
		return db::query ( db::build_sql ( func_get_args () ) )->fetchRow ();
	}
	static function obj_array($sql) {
		$array = array ();
		$rez = db::query ( db::build_sql ( func_get_args () ) );
		while ( $obj = $rez->fetchRow () ) {
			$array [] = $obj;
		}
		return $array;
	}
	static function found_rows() {
		global $db;
		return $db->fnet->found_rows;
	}
	static function escape($string) {
		global $db;
		db::connect ();
		if (is_array ( $string )) {
			echo '<pre>';
			echo "db::escape trebuie sa primeasca string nu array<br/>";
			debug_print_backtrace ();
			echo '</pre>';
		}
		return $db->real_escape_string ( $string );
	}
}
class db_rez {
	private $rez;
	public $num_rows;
	public $insert_id;
	public $affected_rows;
	public $next_result;
	function __construct($rez) {
		global $db;
		if ($db->fnet->errno) {
			trigger_error ( $db->fnet->error, E_USER_WARNING );
			echo '<pre>';
			debug_print_backtrace ();
			echo '</pre>';
		}
		if ($rez->errno) {
			trigger_error ( $rez->error, E_USER_WARNING );
			echo '<pre>';
			debug_print_backtrace ();
			echo '</pre>';
		}
		$this->rez = $rez;
		$this->num_rows = $rez->num_rows;
		$this->insert_id = $db->insert_id;
		$this->affected_rows = $db->affected_rows;
		if ($this->num_rows) {
			$this->next_result = $rez->fetch_object ();
		}
	}
	function numRows() {
		return $this->num_rows;
	}
	function fetchRow() {
		if (! $this->num_rows)
			return false;
		if (! $this->rez)
			return false;
		$obj = clone $this->next_result;
		$this->next_result = $this->rez->fetch_object ();
		if (! $this->next_result) {
			$this->rez->free ();
			$this->rez = null;
		}
		return $obj;
	}
}
?>