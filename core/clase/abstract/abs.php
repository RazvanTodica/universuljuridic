<?php
function &lista($tip_obiect,$sql=null){
	if (class_exists($tip_obiect)){
		$c=new $tip_obiect;
		return $c->lista($sql);
	}
}
/**
 * @version 1.3
 *
 */
class obiect {
	public $id;
	public $skip;
	function __construct($obiect=null){
		if (!$this->skip->nume_tabel){
			$this->skip->nume_tabel=get_class($this);
		}
		$this->skip('skip');
		$this->skip('tpl');
		$this->skip('activ');
		$this->skip('timestamp');
		if ($obiect!==null){
			$this->detalii($obiect);
		}
	}
	function skip($camp){
		if (!isset($this->skip->campuri))$this->skip->campuri=array();
		$this->skip->campuri[]=$camp;
	}
	function lista($sql=null){
		if (!$sql){
			$sql="SELECT * FROM `{$this->skip->nume_tabel}` WHERE `activ`='1'";
			if ($this->skip->order_by){
				$sql.=" ORDER BY {$this->skip->order_by} {$this->skip->order_dir}";
			}
			else {
				$sql.=" ORDER BY `id` DESC ";
			}
		}
		return db::obj_array($sql);
	}
	function detalii($obiect=null){
		if (is_numeric($obiect) && $obiect>0){
			$sql="SELECT * FROM `{$this->skip->nume_tabel}` WHERE `id`='?' AND `activ`='1'";
			$obiect=db::obj($sql,$obiect);
		}
		if (is_object($obiect)){
			foreach ($obiect as $key=>$value){
				$this->$key=$value;
			}
		}
	}
	function save(){
		if ($this->id){
			$sql_header="UPDATE `{$this->skip->nume_tabel}` SET ";
			$sql_footer="WHERE `id`='{$this->id}' LIMIT 1;";
		}
		else {
			$sql_header="INSERT INTO `{$this->skip->nume_tabel}` SET ";
			$sql_footer="";
		}
		foreach ($this as $key=>$value){
			if (in_array($key,$this->skip->campuri))continue;
			if ($key=='id' && $value==0)continue;
			$value=db::escape(trim($value));
			$valori[]="`{$key}`='{$value}'";
		}
		$sql=$sql_header." ".implode(', ',$valori)." ".$sql_footer;

		$id_posibil=db::uquery($sql);
		if (!$this->id){
			$this->id=$id_posibil;
		}
		return $this->id;
	}
	function sterge(){
		if ($this->id){
			db::uquery("UPDATE `{$this->skip->nume_tabel}` SET `activ`=-1 WHERE `id`='{$this->id}' LIMIT 1;");
		}
	}
	function __toString(){
		return $this->id.'';
	}
}
?>