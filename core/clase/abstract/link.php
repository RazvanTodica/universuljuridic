<?php
class zlink extends obiect {
	function __construct(){
		parent::__construct();
		$this->skip->nume_tabel='links';
	}
	function save(){
		if (!$this->href || !$this->functie)return ;
		$this->href=strtolower($this->href);
		$this->href=preg_replace('@[^0-9A-Za-z_\-/.]@Uis','-',$this->href);
		if ($obj=db::obj("SELECT * FROM `links` WHERE `href`='?' AND `functie`='?' AND `param`='?'",$this->href,$this->functie,$this->param)){
			$this->id=$obj->id;
			if ($obj->activ!=1){
				db::uquery("UPDATE `links` SET `activ`=1 WHERE `id`='?'",$this->id);
			}
			return ;
		}
		while ($obj=db::obj("SELECT * FROM `links` WHERE `href`='?' AND `activ` =1",$this->href)){
			$this->href.='-';continue;
		}
		parent::save();
	}
}

class link{
	function &adauga($href,$functie,$parametrii=null,$link_unic=false){
		$lnk=new zlink();
		$lnk->href=$href;
		$lnk->functie=$functie;
		$lnk->param=$parametrii;
		$lnk->save();
		if ($link_unic){
			db::uquery("UPDATE `links` SET `activ`=-1 WHERE `href`!='?' AND `activ`=1 AND `functie`='?' AND `param`='?'",$lnk->href,$lnk->functie,$lnk->param);
		}
		return $lnk;
	}
	function sterge($functie,$parametrii=null){
		db::uquery("UPDATE `links` SET  activ=-1 WHERE `functie`='?',`param`='?'",$functie,$parametrii);
	}
}