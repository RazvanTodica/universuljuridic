<?php
session_start();
@include_once 'functii.php';

ob_start();
global $SITE;
$SITE['timp_start']=microtime(true);
error_reporting(0);
require_once ('config.php');
require_once ('functii.php');
# Include clase
include $CONF['core_dir']."clase/abstract/abs.php";
include $CONF['core_dir']."clase/abstract/link.php";
include $CONF['core_dir']."clase/abstract/db.php";

includes($CONF['core_dir']."clase/abstract");
includes($CONF['core_dir']."clase/base");
includes($CONF['core_dir']."clase/web");
require_once ('init.php');

if(isset($link[1])) {
    $carte = db::obj("SELECT * FROM PUBLICATII WHERE PBL_SEO='?'", $link[1]);
}


$link_incercat=$link;
$slider_promo = new bannere();
$slider_prom = $slider_promo->bannere_slider_promo(2);

$sql = "select * from carti_promovate where tip=2 and activ=1 order by data_adaugare DESC";
$bannere_bottom = db::obj_array($sql);

template::assign("bannere_slider_right",$bannere_bottom);

template::assign('slider_p',$slider_prom);
$extra_link=array ();
$rezultat=false;
if($link[0] == 'beta'){
    header("Location: ".$CONF['sitepath']);
    exit;
}


// Verific daca exista o carte
if($link[0]&&$link[1]&&$carte=db::obj("SELECT * FROM PUBLICATII WHERE   PBL_SEO='?'",$link[1])){
    $extra_link[]=array_pop($link_incercat);
    $extra_link[]=array_pop($link_incercat);
    $rezultat=new stdClass();
    $rezultat->functie='carte';
    $rezultat->param=$carte->PBL_ID;
}
// Verific daca exista un autor
elseif($link[0]&&$link[1]&&$autor=db::obj("SELECT * FROM autor WHERE autor_seo='?'",$link[1])){
    $extra_link[]=array_pop($link_incercat);
    $rezultat=new stdClass();
    $rezultat->functie='autor';
    $rezultat->param=$autor->autor_id;
}
// Verific daca exista o colectie
elseif($link[0] && $link[0] != 'reviste' &&$colectie=db::obj("SELECT * FROM  `COLECTII` WHERE CLC_SEO='?' AND editura_id ='?'",$link[0],$editura->id)){
    $extra_link[]=array_pop($link_incercat);
    $rezultat->functie='colectie';
    $rezultat->param=$colectie->CLC_ID;

}
else{
    while(!$rezultat){
        $preg_req=implode('/',$link_incercat);
        $preg_req=str_replace("'",'',$preg_req);
        $preg_req=preg_quote($preg_req,'@');
        $preg_req=db::escape($preg_req);

        $rezultat=db::obj("SELECT * FROM `links_universuljuridic` WHERE `href` LIKE '%{$preg_req}%' AND activ=1 ORDER BY activ DESC,LENGTH(`href`) ASC LIMIT 1;");

        if(!$rezultat){
            $extra_link[]=array_pop($link_incercat);
        }
    }
}
require_once('/home/univers/public_html/magazin/corev2/parteneri_uj.php');
$parteneri_univers_juridic = $parteneri['parteneri'];
$parteneri_media = $parteneri['parteneri_media'];
template::assign('parteneri_media',$parteneri_media);
template::assign('parteneri_univers_juridic',$parteneri_univers_juridic);

$cautare=false;
$extra_link=array_reverse($extra_link);
$used_link=$link_incercat;
$cale_functie="functii/{$rezultat->functie}.php";
function inregistreaza_afisare_sistem_bannere($id){
    db::query("update sistem_bannere set afisari = afisari + 1 where id = {$id}");
}
$sql = "select * from sistem_bannere where site = 'pro' and activ = 1 and stadiu = 'activ' order by id desc limit 1";
$banner = db::obj($sql);
template::assign("banner",$banner);
if(file_exists($cale_functie)){



    require_once ($cale_functie);
    eval("\$continut={$rezultat->functie}(\"{$rezultat->param}\");");
}
else{
    echo 'Nu am gasit functia '.$rezultat->functie.'in locatia '.$cale_functie;
}
require_once ('dupa_apelare.php');
if($go_ajax){
    echo $continut;
}

else{



    meta::adauga("title","Universul Juridic");
    template::assign('continut',$continut);


    $colectie = new colectie();
    $colectie_lista = $colectie->lista(1);

    $reviste = reviste::lista_reviste();

    /**
     * Algoritm de sortare pentru reviste.
     * Claudiu&Mihai
     */
    $mid = ceil(sizeof($reviste)/2);

    foreach($reviste as $i=>$r) {
        if($r->CAT_ID == '115') {
            unset($reviste[$i]);
            break;
        }
    }

    $j=0;
    for($i=0;$i<=sizeof($reviste);$i++){
        if($i%2==0)
            $reviste2[] = $reviste[$j];
        else{
            $reviste2[] = $reviste[$mid+$j];
            $j++;
        }
    }
    unset($reviste);
    $reviste = $reviste2;




    if($_POST['newsletter_send']==1){

        $news = new newsletter();
        $verifica = $news->verifica();
        //Daca este nevalid
        if($verifica['email_valid'] !=1){
            $alert[] = 1;
        }
        if($verifica['exista']==1){
            $alert[] = 2;
        }
        if(empty($alert)){
            $news->adauga($verifica['email'],$verifica['confirmationCode']);
            $alert[] = 3;
        }
    }

    if($_GET['confirmare_news'] !=''){

        $news = new newsletter();
        $confirmare = $news->confirmare();
        if($confirmare==1){
            $alert[] = 2;
        }else{
            $alert[] = 4;
        }


    }

    template::assign('alert',json_encode($alert));


    template::assign('colectie_lista',$colectie_lista);

    template::assign('revista_lista',$reviste);

    template::assign('rezultat', $rezultat);
    template::assign('functie_curenta', $rezultat->functie);


    template::display('structura/main.tpl');
}

?>
