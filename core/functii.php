<?php
global $CONF;
require_once($CONF['core_dir']."clase/Newsman/Client.php");
function aboneaza_newsman($cont='ujmag',$mail,$prenume='',$nume=''){
	
	
	
	$ip = $_SERVER['REMOTE_ADDR'];
	if($ip=="127.0.0.1"){
		$ip = "5.2.137.162";
	}
	if($cont=='ujmag') {
		$ses_key = "cbd9e7f22189be741179b3b7563878a5";
		$lista = "2654";
		$userId = 2907;
		$template_id = "c_UjmagTemplates";
	}elseif($cont=='uj')
	{
		$ses_key="7b6cc41b82a74e580608282a9e7ad148";
		$lista="2725";
		$userId = 2725;
	}
	else
		return ;
	$client = new Newsman_Client($userId, $ses_key);
	
	$props = array();
	$segments = array(54105);
	
	$ret = $client->subscriber->initSubscribe(
		$lista, /* The list id */
		$mail, /* The email address of subscriber */
		$prenume, /* Firstname of subscriber, can be null */
		$nume, /* Lastname of subscriber, can be null. */
		$ip, /* IP address of subscriber. Will be overwritten when the subscriber confirms (clicks the unique link) */
		NULL,NULL
//		$props, /* Hash array with props */
//		array(
//			"segments" => $segments, /* array with segment ids where user will be added */
//			"template_id" => $template_id /* string|int with the specific template used for sending out the confirmation email */
//		)
	);
	
	if ($ret == "")
	{
		// handle error here
		die("Error on method subscriber.initSubscribe");
	}
	
	if($cont=='ujmag')
	{
		
		db::query("update USERS set bifa_news = 1 where USR_EMAIL = '{$mail}'");
	}
	if($cont=='uj')
	{
		
		db::query("update USERS set bifa_news_uj = 1 where USR_EMAIL = '{$mail}'");
	}
}

function dezaboneaza_newsman($cont='ujmag',$mail){
	
	$ip = $_SERVER['REMOTE_ADDR'];
	if($ip=="127.0.0.1"){
		$ip = "5.2.137.162";
	}
	if($cont=='ujmag') {
		$ses_key = "cbd9e7f22189be741179b3b7563878a5";
		$lista = "2654";
		$userId = 2907;
		$template_id = "c_UjmagTemplates";
	}elseif($cont=='uj')
	{
		$ses_key="7b6cc41b82a74e580608282a9e7ad148";
		$lista="2725";
		$userId = 2725;
	}
	else
		return ;
	$userId = 2907;
	$ses_key = "cbd9e7f22189be741179b3b7563878a5";
	
	$client = new Newsman_Client($userId, $ses_key);
	
	$props = array("prop"=>"val");
	$segments = array("54105");
	
	$ret = $client->subscriber->initUnsubscribe(
		$lista, /* The list id */
		$mail, /* The email address of the subscriber */
		NULL
//		array(
//			"template_id" => $template_id /* string|int with the specific template used for sending out the unsubscribe confirmation email */
//		)
	);
	
	if ($ret == "")
	{
		// handle error here
		die("Error on method unsubscriber.initSubscribe");
	}
	
	if($cont=='ujmag')
	{
		
		query("update USERS set bifa_news = 0 where USR_EMAIL = '{$mail}'");
	}
	if($cont=='uj')
	{
		
		query("update USERS set bifa_news_uj = 0 where USR_EMAIL = '{$mail}'");
	}
}



function conv($el){

	if(is_array($el)){

		foreach($el as $k=>$v){

			$el["{$k}"]=conv($v);

		}

	}

	elseif(is_object($el)){



		foreach($el as $k=>$v){

			$el->$k=conv($v);

		}

	}

	else{

		$el=iconv('iso-8859-2','utf-8',$el);

	}

	return $el;

}

function curata_evenimente($string){
	return repara_xhtml($string);
}




function seo($s)
{
	global $db,$CONF;
	$sSql = "SELECT * from PUBLICATII where PBL_ID='$s'" ;
	$x = db::obj($sSql);
	if(!$x->PBL_SEOA)
	$x->PBL_SEOA='Univers';
		return $CONF['sitepath'].$x->PBL_SEOA."/".$x->PBL_SEO.".html";

}      

function resize_pic($img,$dim,$container=true,$culoare='#ffffff'){

	return rpic2($img,$dim,$fc='');

}


function resize_pic2($img,$dim,$container=true,$culoare='#ffffff'){

	return rpic($dimensiuni,$cale,$container=true);

}


/*
	Face resize_pic pe ujmag.ro
*/
function rPic2($cale,$dim,$fc=''){
	$CONF['serverpath']	= '/home/univers/public_html/magazin/'; // path-ul la care se afla site-ul
	$CONF['sitepath']	= 'http://www.ujmag.ro/'; // url-ul la care se afla site-ul
	$CONF['cdnpath']	= 'http://www.ujmag.ro/'; // url-ul la care se afla site-ul
	$numeFis=basename($cale);
	
	$cuWatermark=1;
	if(strstr($_SERVER['HTTP_HOST'],'juridice.ro')){
		$cuWatermark=2;
	}
	
	$fcC='';
	if(strlen($fc)){
		$fcC='fc.';
	}
	$cc="resize_pic/cache/{$dim}/{$cuWatermark}{$fcC}{$numeFis}";
	//echo $cc;
	if(file_exists($CONF['serverpath'].$cc)){
		return $CONF['cdnpath'].$cc;
	}
	else{
		return "{$CONF['sitepath']}resize_pic/{$dim}/{$cale}{$fc}";
	}
}


/*
Face resize_pic in site
*/
function rpic($dimensiuni,$cale,$container=true){

	global $CONF;
	

	if(!$cale) return;

	if(strstr($cale,' ')){

		$cale=escapeshellarg($cale);

	}

	$dimensiuni_w_h=explode('x',$dimensiuni);

	$cale_dir=explode('/',$cale);

	$nume_fisier=array_pop($cale_dir);

	if(!$container){

		$cale_dir=array_merge(array ('fc' ),$cale_dir);

	}

	$cale_dir="resize_pic/cache/{$dimensiuni}/".implode('/',$cale_dir);



	$cale_orig=$CONF['serverpath'].$cale;

	$cale_cache_server="{$CONF['serverpath']}{$cale_dir}/{$nume_fisier}";

	$cale_cache_site="{$CONF['sitepath']}{$cale_dir}/{$nume_fisier}";



	if(file_exists($cale_cache_server)&&filesize($cale_cache_server)){

		return $cale_cache_site;

	}

	else{

		if(!file_exists($cale_orig)){

			echo $cale_orig;

			return;

		}

		@mkdir("{$CONF['serverpath']}{$cale_dir}",0777,true);

		@chmod("{$CONF['serverpath']}{$cale_dir}",0777);

		if(!class_exists('resize_image')) require_once ("{$CONF['serverpath']}resize_pic/resize_pic.php");

		$resize=new resize_image();

		$resize->resize($cale_orig,$cale_cache_server,$dimensiuni_w_h[0],$dimensiuni_w_h[1],1,$container);

		return $cale_cache_site;

	}

}

function &excel2array($locatie){

	require_once ('PHPExcel.php');

	require_once ('PHPExcel/Reader/Excel5.php');

	$excel=new PHPExcel_Reader_Excel5();

	$excel->setReadDataOnly(true);

	$array=$excel->load($locatie)->getActiveSheet()->toArray();

	unset($excel);

	return $array;

}

function array2excel($array,$locatie,$nume){

	require_once ('PHPExcel/IOFactory.php');

	require_once ('PHPExcel/Writer/Excel5.php');



	if($locatie=='php://output'){

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

		header('Content-Disposition: attachment;filename="'.$nume.'"');

		header('Cache-Control: max-age=0');

	}



	$excel=new PHPExcel();

	$sheet=$excel->getActiveSheet();

	$sheet->fromArray($array);

	$save=PHPExcel_IOFactory::createWriter($excel,'Excel5');

	$save->save($locatie);

	unset($sheet);

	unset($save);

	unset($excel);

}

function string_encode($string){

	return htmlentities($string,null,'UTF-8');

}

function string_decode($string){

	return str_replace('&nbsp;',' ',html_entity_decode($string,null,'UTF-8'));

}

class SITE{

	function get($key){

		global $SITE;

		return $SITE["{$key}"];

	}

	function set($key,$val){

		global $SITE;

		$SITE["{$key}"]=$val;

	}

}

function convPret($pret,$moneda){

	$conv_in_ron=moneda::pret($moneda);

	return $pret*$conv_in_ron;

}

function nume_moneda($id){

	return moneda::nume($id);

}

function id_specificatie($tag){

	return specificatie::get_spec($tag)->id;

}

function tag_specificatie($id){

	return specificatie::get_spec(null,$id)->tag;

}

function dataRom($data,$cu_timp=false){

	if(preg_match("@[^0-9]@",$data)){

		$data=strtotime($data);

	}

	if(date("d.m.Y",$data)=='01.01.1970') return '&nbsp;';

	if($cu_timp) return date("d.m.Y \o\\r\a H:i",$data);

	return date("d.m.Y",$data);

}

function htmlent($string){

	return htmlentities($string,ENT_QUOTES,'UTF-8');

}

function clear_font($string){

	$string=preg_replace("@<o:p>@is",'',$string);

	$string=preg_replace("@</o:p>@is",'',$string);
	
	

	$string=preg_replace("@msonormal@is",'',$string);

	$string=preg_replace("@font-family@is",'font-family-not',$string);

	$string=repara_xhtml($string);

	//$string=strip_tags($string);





	$string=str_replace('<p class="0in\?"></p>','',$string);

	return $string;

}

function repara_xhtml($string){

	return tidy_repair_string($string,array ('output-xhtml'=>true,'drop-empty-paras'=>true,'show-body-only'=>true,'doctype'=>'strict','drop-font-tags'=>true,'drop-proprietary-attributes'=>true,'lower-literals'=>true,'quote-ampersand'=>true,'wrap'=>0 ),'utf8');

}

function clean_substr($string,$lungime){

	$string=strip_tags($string);

	$string=str_replace('&nbsp;',' ',$string);

	$string=string_decode($string);

	$max_decrement=$lungime-25;

	if($max_decrement<0) $max_decrement=0;

	while(!preg_match('@(\s|\.|,)@',$string[$lungime])&&$lungime>$max_decrement){

		$lungime--;

	}

	return substr($string,0,$lungime);

}



function este_data($data){

	if(preg_match("@[^0-9]@",$data)){

		$data=strtotime($data);

	}

	if($data>0){return true;}

	return false;

}

function get_link($null=null,$functie,$param=''){

	if(!SITE::get('all_links')){

		$obj_array=db::obj_array("SELECT * FROM `links` WHERE `activ`=1 ORDER BY id DESC");

		$links=array ();

		foreach($obj_array as $l){

			$links["{$l->functie}"]["{$l->param}"][]=$l;

		}

		SITE::set('all_links',$links);

	}

	$links=SITE::get('all_links');

	$link=$links["{$functie}"]["{$param}"][0];

	return $link->href;

}

function get($link,$param,$val){

	if(strstr($link,'?')){

		$del='&amp;';

	}

	else{

		$del='?';

	}

	return $link.$del.$param.'='.$val;

}

function redirect($url){



	global $CONF;

	$array=debug_backtrace();

	if(strpos($url,'http')===0){

		header("Location: {$url}");

	}

	else{

		if(strstr($array[0]['file'],'/admin/')){

			header("Location: {$CONF['adminpath']}{$url}");

		}

		else{

			header("Location: {$CONF['sitepath']}{$url}");

		}

	}

	exit();

}

function detalii_tip_grupare($id){

	global $CACHE;

	if(!isset($CACHE['detalii_tip_grupare']["{$id}"])){

		$CACHE['detalii_tip_grupare']["{$id}"]=new tip_grupare($id);

	}

	return $CACHE['detalii_tip_grupare']["{$id}"];

}

function detalii_specificatie($tag){

	global $CACHE;

	if(!$CACHE['detalii_specificatii']){

		$spec_tipuri_temp=new tip_specificatie();

		$spec_tipuri_temp=$spec_tipuri_temp->lista();

		foreach($spec_tipuri_temp as $spec_tmp){

			$spec_tipuri["{$spec_tmp->id}"]=$spec_tmp;

		}



		$specs=new specificatie();

		$specs=$specs->lista();

		foreach($specs as $spec){

			$spec=new specificatie($spec);

			$spec->detalii_tip=$spec_tipuri["{$spec->tip_specificatie_id}"];

			$spec->optiuni();

			$CACHE['detalii_specificatii']["{$spec->tag}"]=$spec;

		}

	}

	return $CACHE['detalii_specificatii']["{$tag}"];

}

function extract_link($url){

	global $CONF;

	$url=str_replace($CONF['sitepath'],'/',$url);

	$link=explode('/',$url);

	if($link[count($link)-1]==''){

		array_pop($link);

	}

	return $link;

}

function fstrip_tags($string){

	$s=str_replace('&nbsp;',' ',$s);

	$s=str_replace('<br>','<br/>',$s);

	$s=preg_replace("@\n@is","<br/>",strip_tags(preg_replace(array ("@(<[/\s]{0,2}p)@is" ),"\n$1",$string),"<br/>"));

	while(preg_match("@<[/\s]{0,2}br[/\s]{0,2}>[\n\r\s]*<[/\s]{0,2}br[/\s]{0,2}>@",$s)){

		$s=preg_replace("@<[/\s]{0,2}br[/\s]{0,2}>[\n\r\s]*<[/\s]{0,2}br[/\s]{0,2}>@","<br/>",$s);

	}

	$s=preg_replace("@^<br/>@","",$s);

	if(!$s){return "Click pentru a edita";}

	return $s;

}

function exista($request){

	if(isset($_REQUEST["{$request}"])&&$_REQUEST["{$request}"]!=''){return true;}

	return false;

}

function safe_string($text){

	return strtolower(preg_replace('@[^a-z0-9_\-]@Uis','_',$text));

}

function safe_url($text){

	//$text=preg_replace(array('@â@','@ă@','@î@','@ş@','@ţ@'),array('a','a','i','s','t'),$text);

	//$text=preg_replace(array('@Â@','@Ă@','@Î@','@Ş@','@Ţ@'),array('A','A','I','S','T'),$text);

	$text=preg_replace('@[^a-z0-9_\-.âăîşţ]@Uis','-',$text);

	$text=preg_replace('@--@Uis','-',$text);

	$text=iconv("UTF-8","ASCII//TRANSLIT",$text);

	return urlencode($text);

}

function chiar_este_text($text){

	$text=strip_tags($text);

	$text=html_entity_decode($text);

	$text=htmlspecialchars_decode($text);

	$test=preg_match('@[a-z,0-9]@',$text,$t);

	if($test){

		return true;

	}

	else{

		return false;

	}

}

function format_admin_titlu($string){

	$string=str_ireplace('_',' ',$string);

	while(strstr($string,'  ')){

		$string=str_ireplace('  ',' ',$string);

	}

	$string=strtolower($string);

	$string=ucfirst($string);

	return $string;

}

function format_data_carte($data){

	$data=strtotime($data);

	$an=date("Y",$data);

	$luni=array ('','Ianuarie','Februarie','Martie','Aprilie','Mai','Iunie','Iulie','August','Septembrie','Octombrie','Noiembrie','Decembrie' );

	$luna=$luni[date("n",$data)];

	return $luna.' '.$an;

}

function format_data_comentariu($data){

	$data_formatata=format_data_carte($data);

	$data_time=strtotime($data);

	$data_formatata=date("d",$data_time).' '.$data_formatata;

	return $data_formatata;

}

function format_pret($nr){

	return number_format($nr,2,',','.');

}

function strtofloat($string,$sep_dec=',',$sep_mii='.'){

	$nr=0.00;

	$string=str_replace($sep_mii,'',$string);

	$string=str_replace($sep_dec,'.',$string);

	$nr+=$string;



	return $nr;

}

function includes($dir){

	$dir=$dir."/";

	$handler=opendir($dir);



	while($file=readdir($handler)):



		if($file!='.'&&$file!='..'):

			$item[]=$file;



	endif;



	endwhile;



	closedir($handler);



	for($i=0;!empty($item[$i]);$i++){

		is_file($dir.$item[$i])?require_once ($dir.$item[$i]) : includes($dir.$item[$i]);



	}

}

function is_admin(){

	$ip=array ();



	$ip[]='82.76.73.7'; // Filipnet .1

	$ip[]='89.35.245.211'; // Filipnet .77

	$ip[]='89.165.137.78'; // rica acasa

	$ip [] = '89.35.245.211'; // Filipnet .77

	$ip [] = '89.38.129.74'; // vali ghita acasa
    $ip[] = '78.96.218.233';

	$ip[] = '94.52.113.145';
	$ip[] = '109.99.227.19';
	$ip[] = '89.38.129.74'; // vali ghita home
	$ip[] = '5.2.147.248'; //cocarascu75
	
	if(!in_array($_SERVER['REMOTE_ADDR'],$ip)){return 0;}

	return 1;

}

function randomstring($length){



	$template="2345679ABCDEFGHJKLMNPQRSTWXYZ";



	settype($length,"integer");

	settype($rndstring,"string");

	settype($a,"integer");

	settype($b,"integer");



	for($a=0;$a<=$length;$a++){

		$b=rand(0,strlen($template)-1);

		$rndstring.=$template[$b];

	}



	return $rndstring;

}

function seo_string($string){

	$acceptate='qwertyuiopasdfghjklzxcvbnm0987654321_';

	//$string=str_ireplace(' ','_',$string);

	for($i=0;$i<strlen($string);$i++){

		if(!stristr($acceptate,$string[$i])){

			$string[$i]='-';

		}

	}

	return $string;

}

function get_os(){



	$browser=str_replace("'","\'",$_SERVER['HTTP_USER_AGENT']);

	if(strstr($browser,'Win')){

		$os='Windows';

	}

	if(strstr($browser,'Mac')){

		$os='MacOS';

	}

	if(strstr($browser,'X11')){

		$os='UNIX';

	}

	if(strstr($browser,'Linux')){

		$os='Linux';

	}



	return $os;

}

function get_browser_agent(){

	$browser=str_replace("'","\'",$_SERVER['HTTP_USER_AGENT']);



	if(strstr($browser,'irefox/2')){

		$browser='firefox 2';

	}

	if(strstr($browser,'irefox/1')){

		$browser='firefox 1';

	}

	if(strstr($browser,'MSIE 7')){

		$browser='IE 7';

	}

	if(strstr($browser,'MSIE 6')){

		$browser='IE 6';

	}



	return $browser;

}



function checkEmail($email){

	if(!preg_match("/^([a-zA-Z0-9])+([a-zA-Z0-9\._-])*@([a-zA-Z0-9_-])+([a-zA-Z0-9\._-]+)+$/",$email)){

		return false;

	}

	else{

		return true;

	}

}

function highlight_php($string){

	if(!(strpos($string,'<?')===0)){

		$string="<?php\n".$string;

		$var=highlight_string($string,true);

		echo $var;

		//echo str_replace('<span style="color: #0000BB">&lt;?php<br />','',$var);

	}

	else{

		highlight_string($string);

	}

}

function trimite_email($unde,$subiect,$mesaj,$dela=DEFAULT_FROM_EMAIL){

	global $CONF;


	$mail=new PHPMailer();

	$mail->Mailer='mail';

	$mail->AddAddress($unde);

	$mail->Subject=$subiect;

	$mail->Body=$mesaj;

	$mail->AltBody=strip_tags($mesaj);

	$mail->From=$dela;

	$mail->Send();

}

function toAscii($str, $replace=array(), $delimiter='-') {
	if( !empty($replace) ) {
		$str = str_replace((array)$replace, ' ', $str);
	}

	$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
	$clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
	$clean = strtolower(trim($clean, '-'));
	$clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

	return $clean;
}


function nice_url($s) {

	$s=strip_tags($s);
	$abc = "1234567890abcdefghijklmnopqrstuvwxyzäâ�?şţîôéà-/.,";
	$s = strtolower($s);
	$r = "";
	for($i=0;$i<strlen($s);$i++)
	{
		if (strpos($abc,$s[$i])!==false)
		$r .= $s[$i];
		if (($s[$i]==' ')&&($r[strlen($r)-1]!=' '))
		$r .= ' ';
	}
	$r = trim($r);

	$r= str_replace('/','-',$r);
	$r= str_replace('ä','a',$r);
	$r= str_replace('â','a',$r);
	$r= str_replace('�?','a',$r);
	$r= str_replace('à','a',$r);
	$r= str_replace('î','i',$r);
	$r= str_replace('ţ','t',$r);
	$r= str_replace('ş','s',$r);
	$r= str_replace('ô','o',$r);
	$r= str_replace('é','e',$r);
	$r= str_replace('ß','b',$r);
	$r= str_replace('&','si',$r);


	$new_str = str_replace(' ','-',$r);

	while (strpos($new_str,'--')!=0) {
		$new_str = str_replace('--','-',$new_str);
	}

	$new_str = str_replace(',','',$new_str);
	$new_str = str_replace('.','',$new_str);
	$new_str = str_replace('?','',$new_str);

	return $new_str;
}   


function trimiteEmailPrieten($carte) {
	global $CONF;
	
	if(isset($_POST['trimite_prieten']) && $_POST['trimite_prieten'] == 'true') {

		$mail = new PHPMailer();

		$mail->From     = $_POST['adresa_ta'];
		$mail->AddAddress($_POST['adresa_prieten']);
		
		$mail->Subject  = "Recomandare primita de la " . $_POST['nume'];
		$mail->Body     = "Salut, <br/><br/>Prietenul tau, " . $_POST['nume'] . " iti recomanda urmatoarea carte pe site-ul Universul Juridic: <br/>";
		$mail->Body		.= "<a href='{$CONF['sitepath']}{$carte->link}'>{$carte->pbl_titlu}</a>";
		$mail->Body 	.= "<br/><br />Universul Juridic<br /><a href='{$CONF['sitepath']}'>www.editurauniversuljuridic.ro</a>";
		
		$mail->IsHTML(true);
		if(!$mail->Send()) {
		  $message = "A aparut o eroare si mail-ul nu a putut fi trimis. Te rugam sa incerci din nou";
		} else {
		  $message = "Mail-ul a fost trimis catre adresa " . $_POST['adresa_prieten'];
		}

		template::assign("message", $message);
		
	}
}


?>