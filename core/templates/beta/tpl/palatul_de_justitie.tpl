<!--begin breadcrumb -->
{include file="structura/breadcrumb.tpl"}
    <!--end breadcrumb -->
    
    <!--begin content_wrapper -->
    <div id="content_wrapper">
       
        <!--begin content -->
        <div id="content">

        <!--begin left -->
            <div id="left">
                
                <h1 class="page_title margin_bottom">Palatul de Justitie</h1>
                
            	<!-- begin pagination -->
                <div class="pagination_wrapper">
                	<div class="afiseaza_pagination">
                    	<span>Se afiseaza <span class="afiseaza_bold">{if $start==0}1{else}{$start}{/if} - {$end}</span> din <span class="afiseaza_bold">{$total}</span> Produse</span>
                    </div>
                	<form method="GET" name='form_sortare' action="" class="rezultate_form">
                        <select name="sortare" class="clasa_select" onchange="document.form_sortare.submit()">
                            <option value="" {if $smarty.get.ordonare==1}selected{/if}>Ordoneaza</option>
                            <option value="aparitie-crescator" {if $smarty.get.ordonare==2}selected{/if}>Data aparitiei crescator</option>
                            <option value="aparitie-descrescator" {if $smarty.get.ordonare==3}selected{/if}>Data aparitiei descrescator</option>
                        </select>
                    </form>
                </div>
            	<!-- end pagination -->
                
            	
            	<ul class="colectia_item_wrapper">
            	
            		{assign var=i value=0}
            		{foreach from=$carti key=k item=item}
            			{if is_numeric($k)}
            				{assign var=i value=$i+1}
                   		 <li class="colectia_item{if $i is div by 4} last{/if}">
	                        <a target="_blank" href="{$CONF.sitepath}reviste/palatul-de-justitie?id={$item->id}" title="{$item->titlu}"><img src="{if $item->coperta ==''}{$CONF.sitepathuj}resize_pic/cache/110x155/1no_pic.jpg{else}{$CONF.sitepathuj}resize_pic/110x155/{$item->coperta}{/if}" width="110" height="155" alt="{$item->titlu}" /></a>
	                        <a target="_blank" href="{$CONF.sitepath}reviste/palatul-de-justitie?id={$item->id}" title="{$item->titlu}" class="colectia_item_description">
	                       		{$item->titlu|truncate:"45":""}		                        
	                        </a>
		                 {/if}
               		 {/foreach}
				    
				 </ul>
            	
                <!-- begin pagination -->
                {include file="paginare2.tpl"}
                
            </div>
            <!--end left -->
            
            <!--begin right -->
            {include file="structura/dreapta.tpl"}
            <!--end right -->
        </div>
        <!--end content -->
        <div class="clear"></div>
        
    </div>
    <!--end content_wrapper -->
   