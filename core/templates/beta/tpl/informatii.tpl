<!--begin breadcrumb -->
{literal}
<style type="text/css">
.lista li {
	padding-left: 10px;
	list-style: circle;
}
</style>
{/literal}
{include file="structura/breadcrumb.tpl"}    
    <!--end breadcrumb -->
    <!--begin content_wrapper -->
    <div id="content_wrapper">
        <!--begin content -->
        <div id="content">
            <!--begin left -->
            <div id="left">
                <h1 class="page_title">Regulament</h1>
            	<div class="detail_page_box">
                  	<p>PENTRU UTILIZAREA ACESTUI SITE ESTE NECESAR S&#258; CITI&#354;I &#350;I S&#258; ACCEPTA&#354;I  TERMENII &#350;I CONDI&#354;IILE DE MAI JOS &Icirc;N TOTALITATE. VIZITAREA  &Icirc;N CONTINUARE A ACESTUI SITE PRESUPUNE C&#258; SUNTE&#354;I DE ACORD CU TERMENII &#350;I  CONDI&#354;IILE DE MAI   JOS.</p>
					  <p><strong>Drepturi de autor</strong></p>
					  <p>&Icirc;ntregul con&#355;inut al site-ului, indiferent de natura lui (text,  imagini, elemente grafice, software &#351;i alte elemente similare), este  proprietatea <em>&bdquo;UNIVERSUL JURIDIC&rdquo;</em> &#351;i,  dup&#259; caz, a altor p&#259;r&#355;i. Folosirea con&#355;inutului f&#259;r&#259; acordul <em>&bdquo;UNIVERSUL JURIDIC&rdquo;</em> sau al  proprietarilor de drept este interzis&#259; &#351;i se pedepse&#351;te conform legilor &icirc;n  vigoare.</p>
					  <p><strong>M&#259;rci &icirc;nregistrate</strong></p>
					  <p>Denumirea <em>&bdquo;UNIVERSUL JURIDIC&rdquo;</em>,  sub orice form&#259; ar ap&#259;rea &icirc;n site, logo-urile &#351;i simbolurile asociate ei,  precum &#351;i combina&#355;iile acestora cu orice cuv&acirc;nt sau simbol grafic, folosite &icirc;n  site, sunt m&#259;rci &icirc;nregistrate ale Editurii <em>&bdquo;UNIVERSUL  JURIDIC&rdquo;</em>. Ele pot fi utilizate numai cu acordul <em>&bdquo;UNIVERSUL JURIDIC&rdquo;</em> &#351;i numai &icirc;n leg&#259;tura cu produse sau servicii <em>&bdquo;UNIVERSUL JURIDIC&rdquo;</em>. Utilizarea altor  m&#259;rci &icirc;nregistrate afi&#351;ate &icirc;n site este permis&#259; numai cu acordul proprietarilor  lor de drept.</p>
					  <p><strong>Confiden&#355;ialitate</strong></p>
					  <p>Dac&#259; utiliza&#355;i site-ul, se presupune c&#259; sunte&#355;i de acord ca <em>&bdquo;UNIVERSUL JURIDIC&rdquo;</em> s&#259; poat&#259; &icirc;nregistra &#351;i  stoca informa&#355;ii despre dvs., ob&#355;inute cu acordul dvs. prealabil, &#351;i le poate  utiliza &icirc;n scopul desf&#259;&#351;ur&#259;rii &icirc;n condi&#355;ii optime a colabor&#259;rii noastre &#351;i al  promov&#259;rii produselor <em>&bdquo;UNIVERSUL JURIDIC&rdquo;</em>.  Aceste informa&#355;ii, dar nu &#351;i datele dvs. de identitate (nume, adres&#259; fizic&#259; &#351;i  de e-mail, nr. de telefon etc.), pot fi puse la dispozi&#355;ia unor ter&#355;e p&#259;r&#355;i &icirc;n  scopul identific&#259;rii unor solu&#355;ii de &icirc;mbun&#259;t&#259;&#355;ire a serviciilor pe care vi le  ofer&#259; <em>&bdquo;UNIVERSUL JURIDIC&rdquo;</em> (studii  statistice, studii de pia&#355;&#259; etc.). Fac excep&#355;ie cazurile &icirc;n care dezv&#259;luirea  identit&#259;&#355;ii dvs. este necesar&#259; &icirc;n scopul respect&#259;rii &#351;i aplic&#259;rii legii.</p>
					  <p><strong>Utilizarea informa&#355;iilor personale</strong></p>
					  <p style="padding-bottom: 0px; margin-bottom: 0px;">Informa&#355;iile  despre dvs. pot fi folosite de <em>&bdquo;UNIVERSUL  JURIDIC&rdquo;</em> doar &icirc;n scopul comercializ&#259;rii produselor oferite, de exemplu  pentru:</p>
					  <ul class='lista' style="list-style:square; ">
					    <li>- procesarea comenzilor sau cererilor dvs.</li>
					    <li>- verificarea creditului &#351;i a cheltuielilor (cu excep&#355;ia cazului &icirc;n care exist&#259;  o &icirc;n&#355;elegere prealabil&#259; &icirc;n acest sens) </li>
					    <li>- oferte de produse</li>
					    <li>- facturare</li>
					    <li>- solu&#355;ionarea cererilor, &icirc;ntreb&#259;rilor sau reclama&#355;iilor dvs.</li>
					    <li>- realizarea unor studii de pia&#355;&#259; &#351;i de produs &#351;i marketingul produselor, al  serviciilor <em>&bdquo;UNIVERSUL JURIDIC&rdquo;</em> &#351;i al  serviciilor furnizorilor, partenerilor &#351;i distribuitorilor s&#259;i</li>
					    <li>- contactarea prin po&#351;t&#259; sau prin oricare alte mijloace &icirc;n leg&#259;tur&#259; cu  produsele oferite de <em>&bdquo;UNIVERSUL JURIDIC&rdquo;</em> &nbsp;sau cu produse oferite de parteneri sau  furnizori ai <em>&bdquo;UNIVERSUL JURIDIC&rdquo;</em> care  prezint&#259;, &icirc;n opinia noastr&#259;, interes pentru dvs.</li>
					    <li>- &icirc;nregistrarea dvs. &icirc;n vederea acord&#259;rii de reduceri, bonusuri, credite etc.</li>
				      </ul>
				      <br />
					  <p><strong>Limitarea responsabilit&#259;&#355;ii</strong></p>
					  <p><em>&bdquo;UNIVERSUL JURIDIC&rdquo;</em> &#351;i furnizorii/partenerii s&#259;i nu pot fi tra&#351;i la r&#259;spundere pentru nici  un fel de daune, indiferent de natura acestora, ca rezultat, &icirc;n timpul  utiliz&#259;rii site-ului &#351;i a serviciilor oferite prin intermediul acestuia, sau ca  urmare a imposibilit&#259;&#355;ii utiliz&#259;rii acestora. De&#351;i face toate eforturile &icirc;n  acest sens, <em>&bdquo;UNIVERSUL JURIDIC&rdquo;</em> nu  garanteaz&#259; c&#259; func&#355;ionarea site-ului &#351;i a serverelor va fi ne&icirc;ntrerupt&#259; &#351;i  lipsit&#259; de erori, c&#259; utilizarea site-lui este lipsit&#259; de riscuri (viru&#351;i, alte  elemente sau ac&#355;iuni care pot provoca daune, specifice Internetului &#351;i  tehnologiilor digitale, inclusiv violarea securit&#259;&#355;ii informa&#355;iilor), c&#259;  informa&#355;iile furnizate prin intermediul site-ului sunt corecte &#351;i la zi  (inclusiv disponibilitatea c&#259;r&#355;ilor &#351;i altor produse prezentate &icirc;n site).</p>
					  <p><strong>Utilizarea site-ului</strong></p>
					  <p>&Icirc;n timpul utiliz&#259;rii site-ului &#351;i a serviciilor sale, dvs.,  utilizatorul, v&#259; angaja&#355;i s&#259; respecta&#355;i legile &icirc;n vigoare &#351;i s&#259; nu desf&#259;&#351;ura&#355;i  activit&#259;&#355;i de tip cracking, hacking sau altele similare, care pun &icirc;n pericol  buna func&#355;ionare a site-ului, a serverului, securitatea informa&#355;iilor etc.; v&#259;  angaja&#355;i s&#259; nu modifica&#355;i, copia&#355;i, distribui&#355;i, transmite&#355;i, afi&#351;a&#355;i,  publica&#355;i, reproduce&#355;i, crea&#355;i produse derivate sau s&#259; vinde&#355;i orice fel de  informa&#355;ii sau servicii ob&#355;inute prin intermediul site-ului; v&#259; angaja&#355;i s&#259;  suporta&#355;i orice costuri adi&#355;ionale legate de utilizarea site-ului, cum ar fi  cele solicitate de furnizorii de servicii telefonice &#351;i de servicii Internet.</p>
					  <p><strong>Modific&#259;ri</strong></p>
					  <p><em>&bdquo;UNIVERSUL JURIDIC&rdquo;</em> &icirc;&#351;i rezerv&#259; dreptul de a modifica oric&acirc;nd termenii &#351;i condi&#355;iile de  fa&#355;&#259;, f&#259;r&#259; vreo notificare prealabil&#259;. Modific&#259;rile vor fi afi&#351;ate &icirc;n aceast&#259;  pagin&#259; &#351;i vor avea efect imediat. Utilizarea site-ului dup&#259; efectuarea oric&#259;ror  modific&#259;ri &icirc;nseamn&#259; acceptarea implicit&#259; a acestora de c&#259;tre dvs. sunt  permanent &icirc;n siguran&#355;&#259;.</p>
				</div>
            </div>
            <!--end left -->
            
            <!--begin right -->
            {include file="structura/dreapta.tpl"}
            <!--end right -->
                     
        </div>
        <!--end content -->
        <div class="clear"></div>
        
    </div>
    <!--end content_wrapper -->