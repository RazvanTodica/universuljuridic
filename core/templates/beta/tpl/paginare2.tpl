                  
<div { if $link.0=='autori'} class="pagination_autori" {else} class="pagination" {/if}>
{if $carti_numar gt 1}
{assign var=pagina_uri value='&pagina='|cat:$smarty.get.pagina}
{section name=i loop=$carti_numar+1 start=1}
{assign var=pagina_c value=$smarty.section.i.index}
{if $smarty.section.i.first}
{if $pagina_curenta > 1}
<span class="inactive_arrow">
 <a href="{if $pagina_curenta-1 == 0}{else}{$CONF.sitepath}{if $link.0=='colectie'}colectie{elseif $link.0=='reviste'}reviste{elseif $link.0=='autori'}autori{elseif $link.0=='evenimente'}evenimente{else}colectie{/if}{if $link.1 !=''}/{$link.1}{/if}?{if $new_query}{$new_query}&{/if}pagina={$pagina_curenta-1}{if $smarty.get.an || $link.0=='evenimente'}&an={$smarty.get.an}{/if}{/if}{if $smarty.get.noi=='1'}&noi=1{if $smarty.get.ordonare}&ordonare={$smarty.get.ordonare}{/if}{/if}"><span class="pagination_arrow">&lsaquo;  Pagina anterioara</span></a>
</span>
{/if}
{/if}  

{if $pagina_c eq ($pagina_curenta-2) && $pagina_c neq 1}...{/if}

{if $pagina_c eq 1 || $pagina_c eq $carti_numar || ($pagina_c gt ($pagina_curenta-2) && $pagina_c lt ($pagina_curenta+2))}
	<span class="{if $smarty.get.pagina == $smarty.section.i.index || (!$smarty.get.pagina && $smarty.section.i.index == 1)}active{else}inactive{/if}">
	<a href="{$CONF.sitepath}{if $link.0=='cauta'}cauta{else}{if $link.0=='colectie'}colectie{elseif $link.0=='reviste'}reviste{elseif $link.0=='autori'}autori{elseif $link.0=='evenimente'}evenimente{else}colectie{/if}{/if}{if $link.1 !=''}/{$link.1}{/if}?{if $new_query}{$new_query}&{/if}pagina={$pagina_c}{if $smarty.get.an || $link.0=='evenimente'}&an={$smarty.get.an}{/if}{if $link.0=='cauta'}&{$filters}{/if}{if $smarty.get.noi=='1'}&noi=1{if $smarty.get.ordonare}&ordonare={$smarty.get.ordonare}{/if}{/if}">{$pagina_c}</a>
</span>

{/if}
{if $pagina_c eq ($pagina_curenta+2) && $pagina_c neq $carti_numar}...{/if}
{/section}
{if $numar_carti gt 9}{/if}{if $pagina_curenta+1 lte $pagina_c}
{/if}
{if $smarty.section.i.last}{if $pagina_curenta+1 <= $carti_numar}
  <span class="inactive_arrow">
	<a href="{$CONF.sitepath}{if $link.0=='colectie'}colectie{elseif $link.0=='reviste'}reviste{elseif $link.0=='autori'}autori{elseif $link.0=='evenimente'}evenimente{else}colectie{/if}{if $link.1 !=''}/{$link.1}{/if}?{if $new_query}{$new_query}&{/if}pagina={$pagina_curenta+1}{if $smarty.get.an || $link.0=='evenimente'}&an={$smarty.get.an}{/if}{if $smarty.get.noi=='1'}&noi=1{if $smarty.get.ordonare}&ordonare={$smarty.get.ordonare}{/if}{/if}"><span class="pagination_arrow">Pagina urmatoare &rsaquo;</span></a>{/if} {/if}
	</span>
{/if}




</div>
                  
             