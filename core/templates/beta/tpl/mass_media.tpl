{literal}
<script type="text/javascript">
	$(function(){
		$('#myGallery').galleryView();
		$(".poza_fancy").fancybox();
	});
</script>
{/literal}
			<!--begin left -->
            <div id="left">
            	<h1 class="page_title">Mass Media</h1>
                <div class="evenimente_single_box">
                    <a href="http://www.ujmag.ro/resize_pic/800x600/{$eveniment->ev_poza}" class="poza_fancy left" title="{$eveniment->ev_titlu}"><img src="http://www.ujmag.ro/resize_pic/194x128/{$eveniment->ev_poza}" width="194" height="128" alt="Picture" class="small_border_img" /></a>
                    <h2>{$eveniment->ev_titlu}</h2>
                    <span class="date_accesari">{$eveniment->ev_data|date_format:'%d %B %Y'}</a> | {if $eveniment->accesari}{$eveniment->accesari}{else}0{/if} accesari</span>
					{$eveniment->ev_continut}
                </div>
                
                <h3>Galerie Foto Mass Media</h3>
                
                {if $eveniment->poze|@count > 0}
                <div class="evenimente_second_box">
                    <ul id="myGallery">
                    {foreach from=$eveniment->poze item=item}
                        <li><img src="{$item->url_mare}" alt="" /></li>
                	{/foreach}
                    </ul>
                </div>
                {/if}
                <ul class="evenimente_list">
                	<li><a href="{$CONF.sitepath}aparitii-in-media" title="Vezi toate aparitiile in Mass Media">Vezi toate aparitiile in Mass Media &rsaquo;&rsaquo;</a></li>
                    <li>
{literal}
<div style="margin-top:45px">
<div id="fb-root"></div><script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:comments href="{/literal}{$CONF.sitepath}aparitii-in-media/{$eveniment->ev_titlu|toAscii}/{$eveniment->ev_id}{literal}" num_posts="2" width="650"></fb:comments>
</div>
{/literal}
                    </li>
                </ul>
            </div>
            <!--end left -->