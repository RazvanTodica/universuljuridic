<!--begin home_carti -->
	<div id='carti-container'>
		<div id="home_carti" style='border-right: 1px dotted #ddd;'>
	         <h2 class="page_title"><a href="{$CONF.sitepath}colectie/carti-in-curs-de-aparitie" title="Carti in curs de apartie">Carti in curs de aparitie <span>(vezi toate {$carti_apariti.nr_total_aparitii})</span></a></h2>
	          <ul class="lista_carti_homepage">
	          {foreach from=$carti_apariti key=k item=cart}   
	          {if is_numeric($k)}
	           <li {if $k==5}class="last qPbl" {/if} class="qPbl"><a href="{$CONF.sitepath}carte/{$cart->PBL_SEO}" title="{$cart->PBL_TITLU}"><img src="{$cart->imagine}" width="110" height="155" alt="{$cart->PBL_TITLU}" /></a>
	           
	           <span class="qtipData qtip_box">
	          	<a  class="title" href="{$CONF.sitepath}carte/{$cart->PBL_SEO}">{$cart->PBL_TITLU}</a>
	          	<div class="autor">
	          	
	          	{foreach from=$cart->autori item=autor}
	          		<a href="{$CONF.sitepath}{$autor->link}" title="{$autor->seo}">{$autor->autor_nume}</a><br/>
	          	{/foreach}
	          	</div>
	          	<span class="pret">{$cart->PBL_PRET} Lei</span>
	          	<a class="add" href="{$CONF.sitepath}carte/{$cart->PBL_SEO}">Detalii &raquo;</a>
	          </span>
						           
	           </li>
	           {/if}
	          
	          {/foreach}
	           </ul>
	       </div>
	       <div id="home_carti" class='right'>
	         <h2 class="page_title"><a href="{$CONF.sitepath}colectie?ordonare=2&noi=1" title="Noutati editoriale">Noutati editoriale <span>(vezi toate {$numar_carti_noi})</span></a></h2>
	          <ul class="lista_carti_homepage">
	          {foreach from=$carti_noi key=k item=cart}   
	          {if is_numeric($k)}
	           <li {if $k==2}class="last qPbl" {/if} class="qPbl"><a href="{$CONF.sitepath}carte/{$cart->PBL_SEO}" title="{$cart->PBL_TITLU}"><img src="{$cart->imagine}" width="110" height="155" alt="{$cart->PBL_TITLU}" /></a>
	           
	           <span class="qtipData qtip_box">
	          	<a  class="title" href="{$CONF.sitepath}carte/{$cart->PBL_SEO}">{$cart->PBL_TITLU}</a>
	          	<div class="autor">
	          	
	          	{foreach from=$cart->autori item=autor}
	          		<a href="{$CONF.sitepath}{$autor->link}" title="{$autor->seo}">{$autor->autor_nume}</a><br/>
	          	{/foreach}
	          	</div>
	          	<span class="pret">{$cart->PBL_PRET} Lei</span>
	          	<a class="add" href="{$CONF.sitepath}carte/{$cart->PBL_SEO}">Detalii &raquo;</a>
	          </span>
						           
	           </li>
	           {/if}
	          
	          {/foreach}
	           </ul>
	       </div>
	  </div>
<!--end home_carti -->


 <!--begin left -->
            <div id="left">
            
            	<div class="left_box_editorial">
            	{*	<h2 class="page_title"><a href="{$CONF.sitepath}evenimente/editoriale" title="Editorial">Editorial</a></h2>   *}
                    <a href="{$CONF.sitepath}evenimente/editoriale/{$editorial->link_corect}"  title="Picture" class="left"><img src="{$CONF.sitepath}pub/images/paper-logo.png" alt="Picture" class="" /></a>
                    <div style='float:right; width: 565px;'>
                    	<a href="{$CONF.sitepath}evenimente/editoriale/{$editorial->link_corect}" class="editorial_homepage">{$editorial->ev_continut|clear_font|strip_tags|truncate:300:"..."}</a>
                    	<p class="autor_editorial">Nicolae Cirstea<br /> <span>Presedinte Universul Juridic</span></p>
                    </div>
                </div>
                
                <div class="left_home_4col_wrapper">
                    <div class="left_home_4col">
                        <h2 class="page_title"><a href="{$CONF.sitepath}evenimente" title="Evenimente">Evenimente</a></h2>
                        <a href="{$CONF.sitepath}evenimente/{$even->link_nou}" title="{$even->ev_titlu}" class="left"><img src="{$CONF.sitepathuj}resize_pic/90x85/{$even->ev_poza}" width="90" height="85" alt="Picture" class="border_img" /></a>  
                        <a href="{$CONF.sitepath}evenimente/{$even->link_nou}" title="Detalii" class="read_more_home">Detalii <span>&gt;</span></a>
                        <h5><a href="{$CONF.sitepath}evenimente/{$even->link_nou}"  title="{$even->ev_titlu}">{$even->ev_titlu|clear_font|strip_tags|truncate:60:"..."}</a></h5>
                        <p class="home_4col_details">{$even->ev_data}</p>
                    </div>
                    <div class="left_home_4col last_box">
                  	  <h2 class="page_title"><a href="{$CONF.sitepath}evenimente/conferinte" title="Conferinte">Conferinte</a></h2>
                  	 <a href="{$CONF.sitepath}evenimente/{$eveniment->link_nou}" title="Detalii" class="read_more_home">Detalii <span>&gt;</span></a>
                        <a href="{$CONF.sitepath}evenimente/{$eveniment->link_nou}" title="{$eveniment->ev_titlu}" class="left"><img src="{$CONF.sitepathuj}resize_pic/90x85/{$eveniment->ev_poza}" width="90" height="85" alt="Picture" class="border_img" /></a>  
                        <h5><a href="{$CONF.sitepath}evenimente/{$eveniment->link_nou}"  title="{$eveniment->ev_titlu}">{$eveniment->ev_titlu|clear_font|strip_tags|truncate:60:"..."}</a></h5>
                        <p class="home_4col_details">{$eveniment->ev_data}</p>
                   
                    </div>
                    <div class="left_home_4col">
                        <h2 class="page_title"><a href="{$CONF.sitepath}reviste" title="Reviste">Reviste</a></h2>
                        {foreach from=$ultima_revista item=item}
                        <a href="{$CONF.sitepath}revista/{$item->PBL_SEO}" title="Detalii" class="read_more_home">Detalii <span>&gt;</span></a>
                        <a href="{$CONF.sitepath}revista/{$item->PBL_SEO}" title="{$item->PBL_TITLU}" class="left"><img src="{$item->PBL_FILENAME|resize_pic:'90x85'}" width="90" height="85" alt="{$item->PBL_TITLU}" class="border_img" /></a>
                        <h5><a href="{$CONF.sitepath}revista/{$item->PBL_SEO}" title="{$item->PBL_TITLU}">{$item->PBL_TITLU|truncate:45:"..."}</a></h5>
                        
                        <p class="home_4col_details">
                        {foreach from=$item->autori item=autor}
                        {if $autor->titulatura}{$autor->titulatura}{$autor->autor_nume}{else}{$autor->editie} {$autor->autor_nume}{/if}<br/>
                        {/foreach}   
                        </p>

                        {/foreach}
                    </div>
                    
                    
                    <div class="left_home_4col last_box">
                        <h2 class="page_title"><a href="{$CONF.sitepath}autori"  title="Autori">Autori </a></h2>
                       <a href="{$CONF.sitepath}autor/{$autori->autor_seo}"  title="Detalii" class="read_more_home">Detalii <span>&gt;</span></a>
                        <a href="{$CONF.sitepath}autor/{$autori->autor_seo}"  title="Picture" class="left"><img src="{$autori->imagine}" width="90" height="85" alt="Picture" class="border_img" /></a>
                        <h5><a href="{$CONF.sitepath}autor/{$autori->autor_seo}" title="{$autori->autor_nume}">{$autori->nume} {$autori->prenume}</a></h5>
                        <a href="{$CONF.sitepath}autor/{$autori->autor_seo}" target="_blank" title="#" class="home_4col_details">
                        {$autori->autor_descriere|clear_font|strip_tags|truncate:100:"..."}
                        </a>
                       
                    </div>
                </div>
				
                {if $banner_bottom}
                	<a href="{$banner_bottom->href}" title="" target="_blank" class="interviu_logo_homepage"><img src="{$CONF.sitepath}pub/img_univers/{$banner_bottom->imagine}"/></a>
                {/if}
			
                   
            </div>
            <!--end left -->