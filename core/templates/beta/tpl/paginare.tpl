<div class="pagination_wrapper">
                	<form method="post" action="" class="rezultate_form">
                        <select name="an" class="clasa_select" onchange="aplica_filtre('an',this.value)">
                        	<option value="0">Anul</option>
                        {foreach from=$ani item=item}
                            <option value="{$item}" {if $smarty.get.an == $item}selected="selected"{/if}>{$item}</option>
                        {/foreach}
                        </select>
                        <select name="luna" class="clasa_select" onchange="aplica_filtre('an',this.value)">
                            <option value="0">Luna</option>
                            <option value="1"{if $smarty.get.luna == 1} selected="selected"{/if}>Ianuarie</option>
                            <option value="2"{if $smarty.get.luna == 2} selected="selected"{/if}>Februarie</option>
                            <option value="3"{if $smarty.get.luna == 3} selected="selected"{/if}>Martie</option>
                            <option value="4"{if $smarty.get.luna == 4} selected="selected"{/if}>Aprilie</option>
                            <option value="5"{if $smarty.get.luna == 5} selected="selected"{/if}>Mai</option>
                            <option value="6"{if $smarty.get.luna == 6} selected="selected"{/if}>Iunie</option>
                            <option value="7"{if $smarty.get.luna == 7} selected="selected"{/if}>Iulie</option>
                            <option value="8"{if $smarty.get.luna == 8} selected="selected"{/if}>August</option>
                            <option value="9"{if $smarty.get.luna == 9} selected="selected"{/if}>Septembrie</option>
                            <option value="10"{if $smarty.get.luna == 10} selected="selected"{/if}>Octombrie</option>
                            <option value="11"{if $smarty.get.luna == 11} selected="selected"{/if}>Noiembrie</option>
                            <option value="12"{if $smarty.get.luna == 12} selected="selected"{/if}>Decembrie</option>
                        </select>
                    </form>
                    <div class="afiseaza_pagination">
                    	<span>Afiseaza: &nbsp;<select name="carti" onchange="aplica_filtre('carti',this.value)"><option value="9"{if $smarty.get.carti == 9} selected="selected"{/if}>9</option><option value="15"{if $smarty.get.carti == 15} selected="selected"{/if}>15</option><option value="21"{if $smarty.get.carti == 21} selected="selected"{/if}>21</option></select>&nbsp; pe pagina &nbsp;|&nbsp; <a href="{$CONF.sitepath}{if $link.0 == 'lansari-de-carte'}lansari-de-carte{elseif $link.0 == 'aparitii-in-media'}aparitii-in-media{elseif $link.0 == 'interviuri'}interviuri{elseif $link.0 == 'cauta'}cauta{else}colectie{/if}?pagina=1{if $smarty.get.an}&an={$smarty.get.an}{/if}{if $smarty.get.luna}&luna={$smarty.get.luna}{/if}{if $smarty.get.carti}&carti={$total}{/if}{if $smarty.get.key_words}&key_words={$smarty.get.key_words}{/if}">Toate {$total}</a></span>
                    </div>
                    <div class="pagination">
                    <span class="inactive"><a href="{$CONF.sitepath}{if $link.0 == 'lansari-de-carte'}lansari-de-carte{elseif $link.0 == 'aparitii-in-media'}aparitii-in-media{elseif $link.0 == 'interviuri'}interviuri{elseif $link.0 == 'cauta'}cauta{else}colectie{/if}?pagina=1{if $smarty.get.an}&an={$smarty.get.an}{/if}{if $smarty.get.luna}&luna={$smarty.get.luna}{/if}{if $smarty.get.carti}&carti={$smarty.get.carti}{/if}{if $smarty.get.key_words}&key_words={$smarty.get.key_words}{/if}"><span class="pagination_arrow">&lsaquo;&lsaquo;</span></a></span>
                    <span class="inactive"><a href="{$CONF.sitepath}{if $link.0 == 'lansari-de-carte'}lansari-de-carte{elseif $link.0 == 'aparitii-in-media'}aparitii-in-media{elseif $link.0 == 'interviuri'}interviuri{elseif $link.0 == 'cauta'}cauta{else}colectie{/if}?pagina={if $smarty.get.pagina >1}{math equation="(x-1)" x=$smarty.get.pagina}{else}1{/if}{if $smarty.get.an}&an={$smarty.get.an}{/if}{if $smarty.get.luna}&luna={$smarty.get.luna}{/if}{if $smarty.get.carti}&carti={$smarty.get.carti}{/if}{if $smarty.get.key_words}&key_words={$smarty.get.key_words}{/if}"><span class="pagination_arrow black">&lsaquo;</span></a></span>
                    {section name=pagini loop=$carti_numar+1 start=1}
                    <span class="{if $smarty.get.pagina == $smarty.section.pagini.index || (!$smarty.get.pagina && $smarty.section.pagini.index == 1)}active{else}inactive{/if}"><a href="{$CONF.sitepath}{if $link.0 == 'lansari-de-carte'}lansari-de-carte{elseif $link.0 == 'aparitii-in-media'}aparitii-in-media{elseif $link.0 == 'interviuri'}interviuri{elseif $link.0 == 'cauta'}cauta{else}colectie{/if}?pagina={$smarty.section.pagini.index}{if $smarty.get.an}&an={$smarty.get.an}{/if}{if $smarty.get.luna}&luna={$smarty.get.luna}{/if}{if $smarty.get.carti}&carti={$smarty.get.carti}{/if}{if $smarty.get.key_words}&key_words={$smarty.get.key_words}{/if}">{$smarty.section.pagini.index}</a></span>
                    {/section}
                    <span class="inactive"><a href="{$CONF.sitepath}{if $link.0 == 'lansari-de-carte'}lansari-de-carte{elseif $link.0 == 'aparitii-in-media'}aparitii-in-media{elseif $link.0 == 'interviuri'}interviuri{elseif $link.0 == 'cauta'}cauta{else}colectie{/if}?pagina={if $smarty.get.pagina && $smarty.get.pagina < $carti_numar}{math equation="(x+1)" x=$smarty.get.pagina}{else}{$carti_numar}{/if}{if $smarty.get.an}&an={$smarty.get.an}{/if}{if $smarty.get.luna}&luna={$smarty.get.luna}{/if}{if $smarty.get.carti}&carti={$smarty.get.carti}{/if}{if $smarty.get.key_words}&key_words={$smarty.get.key_words}{/if}"><span class="pagination_arrow black">&rsaquo;</span></a></span>
                    <span class="inactive"><a href="{$CONF.sitepath}{if $link.0 == 'lansari-de-carte'}lansari-de-carte{elseif $link.0 == 'aparitii-in-media'}aparitii-in-media{elseif $link.0 == 'interviuri'}interviuri{elseif $link.0 == 'cauta'}cauta{else}colectie{/if}?pagina={$carti_numar}{if $smarty.get.an}&an={$smarty.get.an}{/if}{if $smarty.get.luna}&luna={$smarty.get.luna}{/if}{if $smarty.get.carti}&carti={$smarty.get.carti}{/if}{if $smarty.get.key_words}&key_words={$smarty.get.key_words}{/if}"><span class="pagination_arrow">&rsaquo;&rsaquo;</span></a></span>
                    </div>
                </div>