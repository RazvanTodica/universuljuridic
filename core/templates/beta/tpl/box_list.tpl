<ul class="colectia_item_wrapper">
            	
            		{assign var=i value=0}
            			{foreach from=$carti key=k item=item}
            			{if is_numeric($k)}
            				{assign var=i value=$i+1}
                   		 <li class="colectia_item{if $i is div by 4} last{/if}">
		                        <a href="{$CONF.sitepath}{if $link.0=='colectie'}carte{else}revista{/if}/{$item->PBL_SEO}" title="{$item->PBL_TITLU}"><img src="{if $item->PBL_FILENAME ==''}{$CONF.sitepathuj}resize_pic/cache/110x155/1no_pic.jpg{else}{$CONF.sitepathuj}resize_pic/110x155/{$item->PBL_FILENAME}{/if}" width="110" height="155" alt="{$item->PBL_TITLU}" /></a>
		                        {*
		                        {foreach from=$item->autori item=autor}
		                        <a href="{$CONF.sitepath}carte/{$autor->autor_nume|toAscii}/{$autor->autor_id}" title="{$autor->autor_nume}" class="colectia_item_title">{$autor->autor_nume}</a>
								{/foreach}
								*}
{*{if $is_admin}<pre>{$item->autori|print_r}</pre>{/if}*}
                                {assign var=am_afisat value=0}
                                {foreach from=$item->autori item=autor}
                                    {if $autor->autor_default && !$am_afisat}
                                        <a href="{$CONF.sitepath}{if $link.0=='colectie'}carte{else}revista{/if}/{$autor->autor_nume|toAscii}/{$autor->autor_nume}" title="{$autor->autor_nume}" class="colectia_item_title">{$autor->autor_nume}</a>
                                        {assign var=am_afisat value=1}
                                    {/if}
                                {/foreach}
                                {foreach from=$item->autori item=autor}
                                    {if $autor->editie =="Autor" && !$am_afisat}
                                        <a href="{$CONF.sitepath}{if $link.0=='colectie'}carte{else}revista{/if}/{$autor->autor_nume|toAscii}/{$autor->autor_nume}" title="{$autor->autor_nume}" class="colectia_item_title">{$autor->autor_nume}</a>
                                        {assign var=am_afisat value=1}
                                    {/if}
                                {/foreach}
								{if $item->autori.0->autor_id && !$am_afisat}
								<a href="{$CONF.sitepath}{if $link.0=='colectie'}carte{else}revista{/if}/{$item->autori.0->autor_nume|toAscii}/{$item->autori.0->autor_id}" title="{$item->autori.0->autor_nume}" class="colectia_item_title">{$item->autori.0->autor_nume}</a>
								{/if}

		                        <a href="{$CONF.sitepath}{if $link.0=='colectie'}carte{else}revista{/if}/{$item->PBL_SEO}" title="{$item->PBL_TITLU}" class="colectia_item_description">
		                       		{$item->PBL_TITLU|truncate:"45":""}		                        
		                        </a>
                    	</li>
                    	{if $i == 4}      
    						<li class="separator_listing"></li>
                    	{/if}
                    	{/if}
                {/foreach}
               
    
 </ul>