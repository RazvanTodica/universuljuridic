<!--begin breadcrumb -->
{include file="structura/breadcrumb.tpl"}
    <!--end breadcrumb -->
    
    <!--begin content_wrapper -->
    <div id="content_wrapper">
       
        <!--begin content -->
        <div id="content">
            
            <!--begin left -->
            <div id="left">
            	
                <h1 class="page_title">{$autor->autor_nume}</h1>
            
            	<div class="detail_page_box">
            	   {if $autor->autor_poza !=''}
                    <a href="#" title="Picture" class="left"><img src="{$autor->poza}" width="130" height="181" alt="Picture" class="small_border_img" /></a>
                    {/if}
                    <p>
                    {if $autor->autor_descriere !=''}
                    {$autor->autor_descriere}
                    {else}
               			<center>Acest autor nu are descriere momentan.</center>
                    {/if}
                    </p>
                </div>
                <div class="clear"></div>
                
                <h1 class="page_title">Carti ale acestui autor</h1>
                 {include file="box_list.tpl"}
            <!-- begin pagination -->
            {include file="paginare2.tpl"}
            <!--end left -->
                
            </div>
            
           
            
            <!--begin right -->
            {include file="structura/dreapta.tpl"}
            <!--end right -->
            
        </div>
        <!--end content -->
        <div class="clear"></div>
        
    </div>
    <!--end content_wrapper -->