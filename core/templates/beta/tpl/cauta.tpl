{literal}
<script type="text/javascript">
function aplica_filtre(a,b){
	var f = ''
	if(a == 'an'){
		window.location = '{/literal}{$CONF.sitepath}cauta?pagina={if $smarty.get.pagina}{$smarty.get.pagina}{else}1{/if}&an='+b+'{if $smarty.get.luna}&luna={$smarty.get.luna}{/if}&carti={if $smarty.get.carti}{$smarty.get.carti}{else}9{/if}{if $smarty.get.key_words}&key_words={$smarty.get.key_words}{/if}{literal}';
	}else if(a == 'luna'){
		window.location = '{/literal}{$CONF.sitepath}cauta?pagina={if $smarty.get.pagina}{$smarty.get.pagina}{else}1{/if}{if $smarty.get.an}&an={$smarty.get.an}{/if}&luna='+b+'&carti={if $smarty.get.carti}{$smarty.get.carti}{else}9{/if}{if $smarty.get.key_words}&key_words={$smarty.get.key_words}{/if}{literal}';
	}else if(a == 'carti'){
		window.location = '{/literal}{$CONF.sitepath}cauta?pagina={if $smarty.get.pagina}{$smarty.get.pagina}{else}1{/if}{if $smarty.get.an}&an={$smarty.get.an}{/if}{if $smarty.get.luna}&luna={$smarty.get.luna}{/if}&carti='+b+'{if $smarty.get.key_words}&key_words={$smarty.get.key_words}{/if}{literal}';
	}
}
</script>
{/literal}
<!--begin left -->
            <div id="left">
            	<h1 class="page_title">Cauta</h1>
            	
            	<form action="{$CONF.sitepath}cauta/" method="get">
            	<input type="text" name="key_words" value="{$smarty.get.key_words}" />
            	<input type="submit" value="Cauta" />
            	</form>
            	<br />
            	
            	{if $carti|@count > 0 || $smarty.get.pagina}
                <!-- begin pagination -->
                {include file='paginare.tpl'}
            	<!-- end pagination -->
            	<ul class="colectia_item_wrapper">
            	{assign var=i value=0}
            	{foreach from=$carti item=item}
            	{assign var=i value=$i+1}
                    <li class="colectia_item{if $i is div by 3} last{/if}">
                        <a href="{$CONF.sitepath}cartile-colectiei-business/{$item->PBL_TITLU|toAscii}/{$item->PBL_ID}" title="{$item->PBL_TITLU}"><img src="http://www.ujmag.ro/resize_pic/129x182/{$item->PBL_FILENAME}" width="129" height="182" alt="Felicia Cutarescu" /></a>
                        {foreach from=$item->autori item=autor}
                        <a href="{$CONF.sitepath}autorii-colectiei-business/{$autor->autor_nume|toAscii}/{$autor->autor_id}" title="{$autor->autor_nume}" class="colectia_item_title">{$autor->autor_nume}</a>
						{/foreach}
                        <a href="{$CONF.sitepath}cartile-colectiei-business/{$item->PBL_TITLU|toAscii}/{$item->PBL_ID}" title="{$item->PBL_TITLU}" class="colectia_item_description">{$item->PBL_TITLU}</a>
                    </li>
                {/foreach}
                </ul>
                <!-- begin pagination -->
                {include file='paginare.tpl'}
            	<!-- end pagination -->
            	{else}
            	Nu am gasit rezultate.
            	{/if}
            	
            </div>
            <!--end left -->