 <!--begin breadcrumb -->
    <div class="breadcrumb">
    {include file="structura/breadcrumb.tpl"}
    </div>
    <!--end breadcrumb -->
       
    <!--begin content_wrapper -->
    <div id="content_wrapper">
       
        <!--begin content -->
        <div id="content">
            
            <h1 class="page_title">Autori</h1>
           
            
            
            <ul class="letters">
            	<li><span>Litera</span></li>
            	<li><a href="{$CONF.sitepath}autori?nume=a" {if $smarty.get.nume==a} class="selected" {/if}>A</a></li>
                <li><a href="{$CONF.sitepath}autori?nume=b" {if $smarty.get.nume==b} class="selected" {/if}>B</a></li>
                <li><a href="{$CONF.sitepath}autori?nume=c" {if $smarty.get.nume==c} class="selected" {/if}>C</a></li>
                <li><a href="{$CONF.sitepath}autori?nume=d" {if $smarty.get.nume==d} class="selected" {/if}>D</a></li>
                <li><a href="{$CONF.sitepath}autori?nume=e" {if $smarty.get.nume==e} class="selected" {/if}>E</a></li>
                <li><a href="{$CONF.sitepath}autori?nume=f" {if $smarty.get.nume==f} class="selected" {/if}>F</a></li>
                <li><a href="{$CONF.sitepath}autori?nume=g" {if $smarty.get.nume==g} class="selected" {/if}>G</a></li>
                <li><a href="{$CONF.sitepath}autori?nume=h" {if $smarty.get.nume==h} class="selected" {/if}>H</a></li>
                <li><a href="{$CONF.sitepath}autori?nume=i" {if $smarty.get.nume==i} class="selected" {/if}>I</a></li>
                <li><a href="{$CONF.sitepath}autori?nume=j" {if $smarty.get.nume==j} class="selected" {/if}>J</a></li>
                <li><a href="{$CONF.sitepath}autori?nume=k" {if $smarty.get.nume==k} class="selected" {/if}>K</a></li>
                <li><a href="{$CONF.sitepath}autori?nume=l" {if $smarty.get.nume==l} class="selected" {/if}>L</a></li>
                <li><a href="{$CONF.sitepath}autori?nume=m" {if $smarty.get.nume==m} class="selected" {/if}>M</a></li>
                <li><a href="{$CONF.sitepath}autori?nume=n" {if $smarty.get.nume==n} class="selected" {/if}>N</a></li>
                <li><a href="{$CONF.sitepath}autori?nume=o" {if $smarty.get.nume==o} class="selected" {/if}>O</a></li>
                <li><a href="{$CONF.sitepath}autori?nume=p" {if $smarty.get.nume==p} class="selected" {/if}>P</a></li>
                <li><a href="{$CONF.sitepath}autori?nume=q" {if $smarty.get.nume==q} class="selected" {/if}>Q</a></li>
                <li><a href="{$CONF.sitepath}autori?nume=r" {if $smarty.get.nume==r} class="selected" {/if}>R</a></li>
                <li><a href="{$CONF.sitepath}autori?nume=s" {if $smarty.get.nume==s} class="selected" {/if}>S</a></li>
                <li><a href="{$CONF.sitepath}autori?nume=t" {if $smarty.get.nume==t} class="selected" {/if}>T</a></li>
                <li><a href="{$CONF.sitepath}autori?nume=u" {if $smarty.get.nume==u} class="selected" {/if}>U</a></li>
                <li><a href="{$CONF.sitepath}autori?nume=v" {if $smarty.get.nume==v} class="selected" {/if}>V</a></li>
                <li><a href="{$CONF.sitepath}autori?nume=w" {if $smarty.get.nume==w} class="selected" {/if}>W</a></li>
                <li><a href="{$CONF.sitepath}autori?nume=x" {if $smarty.get.nume==x} class="selected" {/if}>X</a></li>
                <li><a href="{$CONF.sitepath}autori?nume=y" {if $smarty.get.nume==y} class="selected" {/if}>Y</a></li>
                <li><a href="{$CONF.sitepath}autori?nume=z" {if $smarty.get.nume==z} class="selected" {/if}>Z</a></li>
            </ul>
            
            
            
            
             
            {assign var="index" value="0"}

            {foreach from=$autori item=item}
            
            {assign var="i" value=$index++}
            {if $index==1 || $index==17 || $index==33 || $index==49}
            	<ul {if $index==49}  class="autori_list last" {else} class="autori_list" {/if}>
            {/if}
             <li><a href="{$CONF.sitepath}autor/{$item->autor_seo}" title="{$item->autor_nume}">{$item->autor_nume}</a></li>
             {if $index==16 || $index==32  || $index==48 || $index==64}
             </ul>
             {/if}
            {/foreach} 
           </ul>
            <!-- begin pagination_autori -->
            {include file="paginare2.tpl"}
            <!-- end pagination -->
            
        </div>
        <!--end content -->
        <div class="clear"></div>
        
    </div>
    <!--end content_wrapper -->


