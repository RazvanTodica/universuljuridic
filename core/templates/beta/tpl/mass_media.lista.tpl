{literal}
<script type="text/javascript">
function aplica_filtre(a,b){
	var f = ''
	if(a == 'an'){
		window.location = '{/literal}{$CONF.sitepath}aparitii-in-media?pagina={if $smarty.get.pagina}{$smarty.get.pagina}{else}1{/if}&an='+b+'{if $smarty.get.luna}&luna={$smarty.get.luna}{/if}&carti={if $smarty.get.carti}{$smarty.get.carti}{else}9{/if}{literal}';
	}else if(a == 'luna'){
		window.location = '{/literal}{$CONF.sitepath}aparitii-in-media?pagina={if $smarty.get.pagina}{$smarty.get.pagina}{else}1{/if}{if $smarty.get.an}&an={$smarty.get.an}{/if}&luna='+b+'&carti={if $smarty.get.carti}{$smarty.get.carti}{else}9{/if}{literal}';
	}else if(a == 'carti'){
		window.location = '{/literal}{$CONF.sitepath}aparitii-in-media?pagina={if $smarty.get.pagina}{$smarty.get.pagina}{else}1{/if}{if $smarty.get.an}&an={$smarty.get.an}{/if}{if $smarty.get.luna}&luna={$smarty.get.luna}{/if}&carti='+b+'{literal}';
	}
}
</script>
{/literal}
			<!--begin left -->
            <div id="left">
            	<h1 class="page_title">Mass Media</h1>
            	
            	{if $evenimente|@count > 0 || $smarty.get.pagina}
                <!-- begin pagination -->
                {include file='paginare.tpl}
            	<!-- end pagination -->
            	
            	
            	{foreach from=$evenimente item=item}
            	<div class="evenimente_box">
                    <a href="{$CONF.sitepath}aparitii-in-media/{$item->ev_titlu|toAscii}/{$item->ev_id}" title="{$item->ev_titlu}" class="left">{if $item->ev_poza}<img src="http://www.ujmag.ro/resize_pic/194x128/{$item->ev_poza}" width="194" height="128" alt="{$item->ev_titlu}" class="small_border_img" />{else}<div style="width:202px;height:136px;float:left;background-color:#cbcbcb"></div>{/if}</a>
                    <h2><a href="{$CONF.sitepath}aparitii-in-media/{$item->ev_titlu|toAscii}/{$item->ev_id}" title="{$item->ev_titlu}">{$item->ev_titlu}</a></h2>
                    <span class="date_accesari"><a>{$item->ev_data|date_format:'%d %B %Y'}</a> | <a>{if $item->accesari}{$item->accesari}{else}0{/if} accesari</a></span>
                    <a href="{$CONF.sitepath}aparitii-in-media/{$item->ev_titlu|toAscii}/{$item->ev_id}" title="{$item->ev_titlu}">{$item->ev_continut|strip_tags|truncate:340}</a>
                </div>
                {/foreach}
                
                <!-- begin pagination -->
                {include file='paginare.tpl}
            	<!-- end pagination -->
            	{else}
            	<p>Nu sunt aparitii in Mass Media.</p>
            	{/if}
            	
            </div>
            <!--end left -->