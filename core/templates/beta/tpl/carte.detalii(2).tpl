{include file=box_cautare.tpl}
	<a style="float:right;" href="http://www.ujmag.ro/resize_pic/550x550/{$carte->pbl_filename}"  rel="facebox" class="imagine"><img src="http://www.ujmag.ro/resize_pic/300x300/{$carte->pbl_filename}" alt="{$carte->pbl_titlu}" title="{$carte->pbl_titlu}" /></a>

<h1>{$carte->pbl_titlu}</h1>
<div class="pret" style="float:right;text-align:center;">{$carte->pbl_pret} lei <span class="extra_pret">(cu TVA)</span><br/>
{if $carte->no_sale!='1'}<a target="_blank" href="http://www.ujmag.ro/{$carte->pbl_seoa}/{$carte->pbl_seo}/"><img src="http://www.universuljuridic.ro/buton_uj_2.jpg" alt="Comanda" /></a>
{else}<a target="_blank" href="http://www.ujmag.ro/{$carte->pbl_seoa}/{$carte->pbl_seo}/"><img src="http://www.universuljuridic.ro/buton_uj_1.jpg" alt="Comanda" /></a>{/if}

</div>
<br/><b>Cod</b>: {$carte->cod_pbl}
{if $carte->pbl_isbn}<br/><b>{$carte->pbl_isbn}</b>{/if}
{foreach from=$carte->autori key=titlu_autor item=autor}
		<br/><b>{$titlu_autor}:</b>
		{section name=j loop=$autor}
			<a href="{$CONF.sitepath}{$autor[j]->autor_seo}/">{$autor[j]->autor_nume}</a>{if $autor[j.index_next]->autor_nume},{/if}
		{/section}
{/foreach}
<br/><b>Data aparitiei:</b> {$carte->pbl_data_aparitie|dataRom}
<br/><b>Numar de pagini:</b> {$carte->pbl_pagini}
<br/><b>Format:</b> {$carte->pbl_format}
<br/><b>Pret:</b> {$carte->pbl_pret} RON
<br/>
{if $carte->pbl_descriere}
<br/><b>Descriere:</b><br/>
<p style="text-indent:15px;">
{$carte->pbl_descriere}
</p>
{/if}