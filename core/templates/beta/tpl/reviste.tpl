{literal}
<script type="text/javascript">
function aplica_filtre(a,b){
	var f = ''
	if(a == 'ordonare'){
		window.location = '{/literal}{$CONF.sitepath}reviste{if $link.1 !=''}/{$link.1}{/if}?pagina={if $smarty.get.pagina}{$smarty.get.pagina}{else}1{/if}&ordonare='+b+'{if $smarty.get.luna}&luna={$smarty.get.luna}{/if}{literal}';
	}else if(a == 'luna'){
		window.location = '{/literal}{$CONF.sitepath}reviste?pagina={if $smarty.get.pagina}{$smarty.get.pagina}{else}1{/if}{if $smarty.get.an}&an={$smarty.get.an}{/if}&luna='+b+'&carti={if $smarty.get.carti}{$smarty.get.carti}{else}9{/if}{literal}';
	}else if(a == 'carti'){
		window.location = '{/literal}{$CONF.sitepath}reviste?pagina={if $smarty.get.pagina}{$smarty.get.pagina}{else}1{/if}{if $smarty.get.an}&an={$smarty.get.an}{/if}{if $smarty.get.luna}&luna={$smarty.get.luna}{/if}&carti='+b+'{literal}';
	}
}
</script>
{/literal}

<!--begin breadcrumb -->
{include file="structura/breadcrumb.tpl"}
    <!--end breadcrumb -->
    
    <!--begin content_wrapper -->
    <div id="content_wrapper">
       
        <!--begin content -->
        <div id="content">
            
            <!--begin left -->
            <div id="left">
                
                <h1 class="page_title margin_bottom"><a href="{if $colectie}{$CONF.sitepath}colectie/{$colectie->CLC_SEO}{else}{$CONF.sitepath}colectie{/if}" title="#"> {if $revista } {$revista->CAT_NUME} {else}Ultimele reviste aparute{/if}</a></h1>
                
            	<!-- begin pagination -->
                <div class="pagination_wrapper">
                	<div class="afiseaza_pagination">
                    	<span>Se afiseaza <span class="afiseaza_bold">{if $start==0}1{else}{$start}{/if} - {$end}</span> din <span class="afiseaza_bold">{$total}</span> Produse</span>
                    </div>
                	<form method="post" action="" class="rezultate_form">
                        <select name="1" class="clasa_select" onchange="aplica_filtre('ordonare',this.value)">
                            <option value="1" {if $smarty.get.ordonare==1}selected{/if}>Ordoneaza</option>
                            <option value="2" {if $smarty.get.ordonare==2}selected{/if}>Cele mai noi</option>
                            <option value="4" {if $smarty.get.ordonare==4}selected{/if}>Pret descrescator</option>
                            <option value="3" {if $smarty.get.ordonare==3}selected{/if}>Pret crescator</option>
                        </select>
                    </form>
                </div>
            	<!-- end pagination -->
                
            	{include file="box_list.tpl"}
                <!-- begin pagination -->
                {include file="paginare2.tpl"}
                
            </div>
            <!--end left -->
            
            <!--begin right -->
            {include file="structura/dreapta.tpl"}
            <!--end right -->
        </div>
        <!--end content -->
        <div class="clear"></div>
        
    </div>
    <!--end content_wrapper -->
   