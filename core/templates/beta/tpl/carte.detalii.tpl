<div id="trimite-popup" class='popup-box' {if $message}style='display: block'{/if}>
	<a href="#" class="close">X</a>
	{if $message}
		<h1>{$message}</h1>
	{else}
		<h2 class='title'>Trimite unui prieten</h2>
	{/if}
	<form action="" method="POST">
		<input type="hidden" name="trimite_prieten" value="true" />
		<p><label>Numele tau</label> <input name="nume" type="text" /></p>
		<p><label>Adresa ta de email</label> <input name="adresa_ta" type="text" /></p>
		<p><label>Adresa de email a prietenului</label> <input name="adresa_prieten" type="text" /></p>
		<p><input name="" type="submit" value="Trimite" /></p>
	</form>
</div>
</div>
<!--begin breadcrumb -->
{include file="structura/breadcrumb.tpl"}
<!--end breadcrumb -->

<!--begin content_wrapper -->
<div id="content_wrapper">

	<!--begin content -->
	<div id="content">

		<!--begin product_wrapper -->
		<div class="product_wrapper">
			<div class="product_left">
				<a href="#" title="#"><img src="{$CONF.sitepathuj}resize_pic/266x375/{$carte->pbl_filename}" width="266" height="375" alt="{$carte->pbl_filename}" /></a>

				<div class="detalii_carte">
					<ul class="rasfoieste_cuprins">

						{if $carte->pdf[0]->cale_server}
							<li ><a href="{$CONF.sitepathuj}{$carte->link}/cuprins/" title="Cuprins" type="iframe" data-width="780" data-height="550" class="modalIf orange nohover">Vezi cuprins</a></li>
							{*<a href="{$CONF.sitepath}{$carte->pbl_seoa}/{$carte->pbl_seo}/cuprins/" type="iframe" data-width="780" data-height="550" class="modalIf orange nohover"><b>Cuprins</b></a>*}
						{/if}

						{if $carte->pdf[1]->tip == 'rasfoire'}
							<li class="last"><a href="{$CONF.sitepathuj}{$carte->link}/rasfoire/" title="Rasfoire" type="iframe" data-width="780" data-height="550" class="modalIf orange nohover">Rasfoieste cartea</a></li>
							{*<a href="{$CONF.sitepath}{$carte->pbl_seoa}/{$carte->pbl_seo}/rasfoire/" type="iframe" data-width="780" data-height="550" class="modalIf orange"><b>Rasfoieste</b></a>*}
						{/if}

					</ul>
					<div class="clearBoth"></div>
					<p><strong>Cod:</strong> {$carte->pbl_cod}</p>
					<p><strong>ISBN:</strong> {$carte->pbl_isbn}</p>
					<p><strong>Editura:</strong> {$carte->editura->editura_nume}</p>
					<p><strong>Data aparitiei:</strong> {$carte->pbl_data_aparitie|date_format:"%Y"}</p>
					<p><strong>Colectia:</strong> {if $colectie}{$colectie->clc_nume}{/if}</p>
					{if $carte->pbl_pagini}<p><strong>Pagini:</strong> {$carte->pbl_pagini}</p>{/if}
					<p><strong>Disponibilitate:</strong> <span class="green">{if $carte->pbl_stoc}in stoc{else}rezervare{/if}</span></p>

				</div>
			</div>
			<div class="product_right">
				<h1 class="product_title no_margin"><a href="#" title="#">{$carte->pbl_titlu}</a></h1>
				<p class="functie_revista" style="width: 100%">
					{foreach from=$carte->autori item=item name=autori}
						{if $item->editie=='Autor'}
							{if !$autor}
								{assign var='autor' value=$item}
							{/if}
							<span style='float: left; width: 100px;'>{if $item->editie==''}Autor: {else}{$item->editie}:</span>{/if}<span style='float: none;' class="functie_name">{$item->autor_nume}</span><br/>
						{/if}
					{/foreach}
					{foreach from=$carte->autori item=item name=autori}
						{if $item->editie!='Autor'}
							{if !$autor}
								{assign var='autor' value=$item}
							{/if}
							<span style='float: left; width: 100px;'>{if $item->editie==''}Autor: {else}{$item->editie}:</span>{/if}<span style='float: none;' class="functie_name">{$item->autor_nume}</span><br/>
						{/if}
					{/foreach}
				</p>
				<div class="product_share">
					<a href="#" title="#" class="trimite_button" onclick="arataPopupBox('#trimite-popup')" >Trimite unui prieten</a>
					<div class="fb_share">
						<div class="fb-like" data-href="http://www.facebook.com/pages/Editura-Universul-Juridic/106631099383053?fref=ts" data-send="true" data-width="450" data-show-faces="false"  style="float:left"></div>
						<div class="clear"></div>
					</div>
				</div>
				<div class="clearBoth"></div>
				<div class="product_cumpara">
					<p class="price">{$carte->pbl_pret|number_format:2} <span>lei</span></p>
					{if $in_curs}
						<a href="{$CONF.sitepathuj}{$carte->link}" title="{$carte->pbl_titlu}" class="cumpara_button">precomanda din ujmag.ro</a>
					{elseif $carte->pbl_stoc<1}
						<a href="{$CONF.sitepathuj}{$carte->link}" title="{$carte->pbl_titlu}" class="cumpara_button">rezerva din ujmag.ro</a>
					{else}
						<a href="{$CONF.sitepathuj}{$carte->link}" title="{$carte->pbl_titlu}" class="cumpara_button">cumpara din ujmag.ro</a>
					{/if}
				</div>
				<div class="clearBoth"></div>
				<p>{$carte->pbl_descriere}</p>
			</div>
		</div>
		<!--end product_wrapper -->

		{if $abonamente}
		<div class="abonamente_anuare_wrapper">
			<div class="product_revista_one_half">
				<h2 class="page_title">Abonamente</h2>

				<a href="#" title="Picture" class="left"><img src="{$CONF.sitepathuj}resize_pic/110x110/{$abonamente.0->pbl_filename}" width="110" height="110" alt="Picture" /></a>
				<h4><a href="#" title="{$abonamente.0->pbl_titlu}">{$abonamente.0->pbl_titlu}</a></h4>
				<div class="pret_abonare">
					<a href="#" title="{$abonamente.0->pbl_titlu}" class="abonamente_pret">{$abonamente.0->pbl_pret|number_format:2:".":","} <span>lei</span></a>
					<a href="#" title="{$abonamente.0->pbl_titlu}" class="abonamente_abonare">MA ABONEZ</a>
				</div>
			</div>
			{/if}

		</div>

		{if $link.0 =='revista'}
			<div class="product_page_wrapper">
				<h2 class="page_title">{$carte->pbl_titlu} - Conducere</h2>

				{foreach from=$carte->autori key=k item=item name=autori}

					<div class="product_revista_one_half {if $k%2==0}last{/if}">

						<a href="{$CONF.sitepath}autor/{$item->autor_seo}" title="{$item->autor_nume}" class="left"><img src="{if $item->autor_poza==''}{$CONF.sitepath}pub/images/no_image.png{else}{$CONF.sitepathuj}resize_pic/130x181/{$item->autor_poza}{/if}" width="130" height="181" alt="{$item->autor_nume}" class="border_img" /></a>

						<h3><a href="{$CONF.sitepath}autor/{$item->autor_seo}" title="{$item->autor_nume}">{$item->autor_nume}<br /><span class="functie"> {if $item->editie==''}Autor{else}{$item->editie} {/if}</span></a></h3>
						<p>{$item->autor_descriere|clear_font|strip_tags|truncate:300:"..."}</p>
						<a href="{$CONF.sitepath}autor/{$item->autor_seo}" title="Detalii" class="read_more">Detalii <span>&gt;</span></a>
					</div>

				{/foreach}
			</div>
		{else}

			{if $autor->autor_descriere|strip_tags|clear_font}
				<div class="product_page_wrapper">

					<h2 class="page_title">Despre autor</h2>
					<a href="{$CONF.sitepath}autor/{$autor->autor_seo}" title="Picture" class="left"><img src="{if $autor->autor_poza ==''}{$CONF.sitepath}pub/images/no_image.png{else}{$CONF.sitepathuj}resize_pic/130x181/{$autor->autor_poza}{/if}" width="130" height="181" alt="Picture" class="border_img" /></a>
					<h3><a href="{$CONF.sitepath}autor/$carte->autori.0->autor_seo}" title="{$autor->autor_nume}">{$autor->autor_nume}</a></h3>
					<p>{$autor->autor_descriere|strip_tags|clear_font|truncate:200:"..."}</p>
					<a href="{$CONF.sitepath}autor/{$autor->autor_seo}" title="Detalii" class="read_more">Detalii <span>&gt;</span></a>
				</div>
			{/if}

		{/if}




		<h2 class="page_title left_margin">{if $link.0 =='revista'}Alte numere ale revistei{else}Carti din aceeasi colectie{/if}</h2>
		<ul class="product_item_wrapper">
			{foreach from=$din_categorie item=item name=din_categorie}
				<li class="product_item{if $smarty.foreach.din_categorie.last} last{/if}">
					<a href="{$CONF.sitepath}{$link.0}/{$item->PBL_SEO}" title="{$item->PBL_TITLU}"><img src="{$CONF.sitepathuj}resize_pic/110x155/{$item->PBL_FILENAME}" width="110" height="155" alt="{$item->PBL_TITLU}" /></a>

					{*{if $is_admin}<pre>{$item->autori|print_r}</pre>{/if}*}
					{assign var=am_afisat value=0}
					{foreach from=$item->autori item=autor}
						{if $autor->autor_default && !$am_afisat}
							<a href="{$CONF.sitepath}{if $link.0=='colectie'}carte{else}revista{/if}/{$autor->autor_nume|toAscii}/{$autor->autor_nume}" title="{$autor->autor_nume}" class="colectia_item_title">{$autor->autor_nume}</a>
							{assign var=am_afisat value=1}
						{/if}
					{/foreach}
					{foreach from=$item->autori item=autor}
						{if $autor->editie =="Autor" && !$am_afisat}
							<a href="{$CONF.sitepath}{if $link.0=='colectie'}carte{else}revista{/if}/{$autor->autor_nume|toAscii}/{$autor->autor_nume}" title="{$autor->autor_nume}" class="colectia_item_title">{$autor->autor_nume}</a>
							{assign var=am_afisat value=1}
						{/if}
					{/foreach}
					{if $item->autori.0->autor_id && !$am_afisat}
						<a href="{$CONF.sitepath}autor/{$item->autori.0->autor_seo}" title="{$item->autori.0->autor_nume}" class="colectia_item_title">{$item->autori.0->autor_nume}</a>
					{/if}
					<a href="{$CONF.sitepath}{$link.0}/{$item->PBL_SEO}" title="{$item->PBL_TITLU}" class="colectia_item_description">{$item->PBL_TITLU|truncate:50}</a>
				</li>
			{/foreach}
		</ul>
	</div>
	<!--end content -->
	<div class="clear"></div>

</div>
<!--end content_wrapper -->

