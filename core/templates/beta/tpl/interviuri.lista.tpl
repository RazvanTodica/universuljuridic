{literal}
<script type="text/javascript">
function aplica_filtre(a,b){
	var f = ''
	if(a == 'an'){
		window.location = '{/literal}{$CONF.sitepath}interviuri?pagina={if $smarty.get.pagina}{$smarty.get.pagina}{else}1{/if}&an='+b+'{if $smarty.get.luna}&luna={$smarty.get.luna}{/if}&carti={if $smarty.get.carti}{$smarty.get.carti}{else}9{/if}{literal}';
	}else if(a == 'luna'){
		window.location = '{/literal}{$CONF.sitepath}interviuri?pagina={if $smarty.get.pagina}{$smarty.get.pagina}{else}1{/if}{if $smarty.get.an}&an={$smarty.get.an}{/if}&luna='+b+'&carti={if $smarty.get.carti}{$smarty.get.carti}{else}9{/if}{literal}';
	}else if(a == 'carti'){
		window.location = '{/literal}{$CONF.sitepath}interviuri?pagina={if $smarty.get.pagina}{$smarty.get.pagina}{else}1{/if}{if $smarty.get.an}&an={$smarty.get.an}{/if}{if $smarty.get.luna}&luna={$smarty.get.luna}{/if}&carti='+b+'{literal}';
	}
}
</script>
{/literal}
			<!--begin left -->
            <div id="left">
            	<h1 class="page_title">Interviuri</h1>
            	
            	{if $interviuri|@count > 0  || $smarty.get.pagina}
                <!-- begin pagination -->
                {include file='paginare2.tpl}
            	<!-- end pagination -->
            	<div class="clear"></div>
            	<br />
            	<br />
            	
            	{foreach from=$interviuri item=item}
            	<div class="evenimente_box">
                    <a href="{$CONF.sitepath}interviuri/{$item->titlu|toAscii}/{$item->id}" title="{$item->titlu}" class="left">{if $item->poza}<img src="http://www.ujmag.ro/fisiere/interviu/{$item->poza}" width="194" height="128" alt="{$item->titlu}" class="small_border_img" />{else}<div style="width:202px;height:136px;float:left;background-color:#cbcbcb"></div>{/if}</a>
                    <h2><a href="{$CONF.sitepath}interviuri/{$item->titlu|toAscii}/{$item->id}" title="{$item->titlu}">{$item->titlu}</a></h2>
                    <span class="date_accesari"><a>{$item->data|date_format:'%d %B %Y'}</a> | <a>{if $item->accesari}{$item->accesari}{else}0{/if} accesari</a></span>
                    <a href="{$CONF.sitepath}interviuri/{$item->titlu|toAscii}/{$item->id}" title="{$item->titlu}">{$item->text|strip_tags|truncate:290}</a>
                    <span style="display: inline-block;float: right; float: right; margin-top: 1px; color:#07a2ca"><a href="{$CONF.sitepath}interviuri/{$item->titlu|toAscii}/{$item->id}" title="{$item->titlu}" style="color:#07a2ca">Citeste mai mult...</a></span>
                </div>
                {/foreach}
                
                <!-- begin pagination -->
                {include file='paginare.tpl}
            	<!-- end pagination -->
            	{else}
            	<p>Nu sunt interviuri.</p>
            	{/if}
            	
            </div>
            <!--end left -->