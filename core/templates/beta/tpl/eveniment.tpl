{literal}
<script type="text/javascript">
	$(function(){
		$('#myGallery').galleryView({

    panel_width:650,
    panel_height:500
		});
		$(".poza_fancy").fancybox();
	});
</script>
{/literal}
			<!--begin left -->
            <div id="left">
            	<h1 class="page_title">
				{if $url_eveniment=='evenimente/interviuri'}
                    	{$eveniment->titlu}
                 {else if $link.0=='evenimente'}
                 		{$eveniment->ev_titlu}
                {/if}
            	
            	
            	 </h1>
            	
                <div class="evenimente_single_box">
                    <a href="
						{if $url_eveniment=='evenimente/interviuri'}{$CONF.sitepathuj}fisiere/interviu/{$eveniment->poza} 
						{else if $link.0=='evenimente'}{$CONF.sitepathuj}resize_pic/194x128/{$eveniment->ev_poza}{/if}
						{if $url_eveniment=='evenimente/editoriale'}{$CONF.sitepathuj}fisiere/evenimente/mica_{$eveniment->poza}{/if}" class="poza_fancy left" title="{$eveniment->ev_titlu}">
                    	<img src="{if $url_eveniment=='evenimente/interviuri'}{$CONF.sitepathuj}fisiere/interviu/{$eveniment->poza}{/if}
						{if $link.0=='evenimente' && $link.1 !='editoriale' && $link.1 !='interviuri'}{$CONF.sitepathuj}resize_pic/194x128/{$eveniment->ev_poza}{/if}  
						{if $url_eveniment=='evenimente/editoriale'}{$CONF.sitepathuj}fisiere/evenimente/mica_{$eveniment->ev_poza}{/if}"  alt="Picture" class="small_border_img" /></a>  
                   
                   
                    {if $url_eveniment=='evenimente/interviuri'}
                      <span class="date_accesari">{$eveniment->data|date_format:'%d %B %Y'}</a> | {if $eveniment->accesari}{$eveniment->accesari}{else}0{/if} accesari</span><br/><br/>
						{$eveniment->text}
						<ol >
						{foreach from=$eveniment->intrebari item=raspuns}
							<li>
							<p>
								<b>{$raspuns->intrebare}</b><br>
								{$raspuns->text}
							</p><br>
							</li>
						{/foreach}
						</ol>
                    {else}
                     <span class="date_accesari">{$eveniment->ev_data|date_format:'%d %B %Y'}</a> | {if $eveniment->accesari}{$eveniment->accesari}{else}0{/if} accesari</span><br/><br/>
					{$eveniment->ev_continut}
					{/if}
                </div>
                
               
                
                {if $eveniment->poze|@count > 0}
                <div class="evenimente_second_box">
                    <ul id="myGallery">
                    {foreach from=$eveniment->poze item=item}
                    {*<li><img src="{$item->url_mare}" alt="" /></li>*}
                        <li><img src="http://www.ujmag.ro/resize_pic/650x500/{$item->imagine_poza}" alt="" /></li>
                	{/foreach}
                    </ul>
                </div>
                {/if}
                <ul class="evenimente_list">
                	<li><a href="{$CONF.sitepath}evenimente" title="Vezi toate evenimentele">Vezi toate evenimentele &rsaquo;&rsaquo;</a></li>
                    <li>
{literal}
<div style="margin-top:45px">
<div id="fb-root"></div><script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:comments href="{/literal}{$CONF.sitepath}lansari-de-carte/{$eveniment->ev_titlu|toAscii}/{$eveniment->ev_id}{literal}" num_posts="2" width="650"></fb:comments>
</div>
{/literal}
                    </li>
                </ul>
            </div>
            <!--end left -->
            
            {include file="structura/dreapta.tpl"}