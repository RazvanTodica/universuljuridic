{literal}
<script type="text/javascript">
function aplica_filtre(a,b){
	var f = ''
	if(a == 'ordonare'){
		window.location = '{/literal}{$CONF.sitepath}evenimente{if $link.1 !=''}/{$link.1}{/if}?pagina={if $smarty.get.pagina}{$smarty.get.pagina}{else}1{/if}&ordonare='+b+'{if $smarty.get.luna}&luna={$smarty.get.luna}{/if}{literal}';
	}else if(a == 'an'){
		window.location = '{/literal}{$CONF.sitepath}evenimente{if $link.1 !=''}/{$link.1}{/if}?pagina={if $smarty.get.pagina}{$smarty.get.pagina}{else}1{/if}&an='+b+'{literal}';
	}else if(a == 'carti'){
		window.location = '{/literal}{$CONF.sitepath}evenimente?pagina={if $smarty.get.pagina}{$smarty.get.pagina}{else}1{/if}{if $smarty.get.an}&an={$smarty.get.an}{/if}{if $smarty.get.luna}&luna={$smarty.get.luna}{/if}&carti='+b+'{literal}';
	}
}
</script>
{/literal}

<!--begin breadcrumb -->
{include file="structura/breadcrumb.tpl"}
    <!--end breadcrumb -->
    
    <!--begin content_wrapper -->
    <div id="content_wrapper">
       
        <!--begin content -->
        <div id="content">
            
            <!--begin left -->
            <div id="left">
                
                <h1 class="page_title margin_bottom"><a href="{$CONF.sitepath}evenimente" title="Evenimente">Interviuri</a></h1>
                
            	<!-- begin pagination -->
                <div class="pagination_wrapper">
                	<div class="afiseaza_pagination">
                    	<span>Se afiseaza <span class="afiseaza_bold">{if $start==0}1{else}{$start}{/if} - {$end}</span> din <span class="afiseaza_bold">{$total}</span> Produse</span>
                    </div>
                	<form method="post" action="" class="rezultate_form">
                        <select name="1" class="clasa_select" onchange="aplica_filtre('an',this.value)">
                           <option value="0" {if $smarty.get.an==0}selected{/if}>Alege anul</option>
                        	<option value="2013" {if $smarty.get.an==2013}selected{/if}>2013</option> 
                        	<option value="2012" {if $smarty.get.an==2012}selected{/if}>2012</option> 
                        	<option value="2011" {if $smarty.get.an==2011}selected{/if}>2011</option> 
                        	<option value="2010" {if $smarty.get.an==2010}selected{/if}>2010</option> 
                        	<option value="2009" {if $smarty.get.an==2009}selected{/if}>2009</option>
                        	<option value="2008" {if $smarty.get.an==2008}selected{/if}>2008</option> 
                        	<option value="2007" {if $smarty.get.an==2007}selected{/if}>2007</option>
                            <option value="2006" {if $smarty.get.an==2006}selected{/if}>2006</option>
                            
                        </select>
                    </form>
                </div>
            	<!-- end pagination -->
            	
            	{if $evenimente|@sizeof==0}
            	<p>Nu exista evenimente.</p>
            	{else}
            	{assign var="start" value="1"}
            	{foreach from=$evenimente item=item}
            	{assign var="nr" value=$start++}
            	
                <div class="evenimente_box {if $nr==$carti_pe_pagina}last{/if}">
                    <a href="{$CONF.sitepath}evenimente/{$item->href}" title="{$item->titlu}" class="left"><img src="{$CONF.sitepathuj}fisiere/interviu/{$item->poza}"  alt="{$item->titlu}" class="small_border_img" /></a>
                    <h2><a href="{$CONF.sitepath}evenimente/interviuri/{$item->href}" title="{$item->titlu}">{$item->titlu|truncate:100:"..."}</a></h2>
                    <span class="date_accesari"><a href="#" title="#">{$item->data}</a> | <a href="#" title="#">{$item->accesari} accesari</a></span><br/><br/>
                    <a href="{$CONF.sitepath}evenimente/interviuri/{$item->href}" title="{$item->titlu}">{$item->text|clear_font|strip_tags|truncate:200:"..."}</a>
                </div>
                {/foreach}
                {/if}
                <!-- begin pagination -->
                {include file="paginare2.tpl"}
                
            </div>
            <!--end left -->
            
            <!--begin right -->
            {include file="structura/dreapta.tpl"}
            <!--end right -->
        </div>
        <!--end content -->
        <div class="clear"></div>
        
    </div>
    <!--end content_wrapper -->