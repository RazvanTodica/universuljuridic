<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
           
<link rel="shortcut icon"
	href="http://www.editurauniversuljuridic.ro/templates/beta/pub/images/favicon_uj.ico"
	type="image/vnd.microsoft.icon" />
<link type="text/css" rel="stylesheet" href="{$CONF.sitepath}pub/css/style_new.css" />
<link type="text/css" rel="stylesheet" href="{$CONF.sitepath}pub/css/qtip.css" />
<script src="{$CONF.sitepath}pub/js/jquery-1.8.3.min.js" type="text/javascript"></script>
<script src="{$CONF.sitepath}pub/js/jquery.search-tooltip.js" type="text/javascript"></script>


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<script src="{$CONF.sitepath}pub/js/jquery.menu.js" type="text/javascript"></script>
<script src="{$CONF.sitepath}pub/js/jquery.qtip.min.js" type="text/javascript"></script>
<script src="{$CONF.sitepath}pub/js/main.js" type="text/javascript"></script>
  
<!-- begin slider script -->

<link rel="stylesheet" href="{$CONF.sitepath}pub/css/animate.css"> <!-- Optional -->
<link rel="stylesheet" href="{$CONF.sitepath}pub/css/liquid-slider.css">

<script type="text/javascript"  src="{$CONF.sitepath}pub/js/jquery.easing.1.3.js"></script>
<script type="text/javascript"  src="{$CONF.sitepath}pub/js/jquery.touchSwipe.min.js"></script>
<script type="text/javascript"  src="{$CONF.sitepath}pub/js/jquery.liquid-slider.min.js"></script>
{literal}
<script>
$(function(){
    $(function(){
		$('#liquid-slider').liquidSlider({
			'dynamicTabs' : false,
			'includeTitle' : false,
			'panelTitleSelector' : false,
			'autoSlide' : true,
			'minHeight' : 250,
			'hideSideArrows': false,
			'hoverArrows' : false
		});
	});
});
</script>
{/literal}
<!-- end slider script -->
<title>{$META.title}</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="title" content="{$META.title}" />
<meta name="description" content="{$META.description}" />
<meta name="robots" content="index, follow">
<meta name="author" content="" />

<meta name="verify-v1" content="upAxCM2xjpbJmukD3S3iH28y3R75gx102zjezXpOhzI=" />

</head>

<body>
{literal}
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-8509274-13', 'universuljuridic.ro');
  ga('send', 'pageview');
</script>
{/literal}

<div id="fb-root"></div>
{literal}
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ro_RO/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
{/literal}
{literal}
<script type="text/javascript">



$(document).ready(function() {

	var tip = {/literal}{$alert}{literal};
	if(tip) {
	
	 $.each(tip, function( key, value ) {
		
		
		if(value==1){
			alert('Adresa de email este invalida.');
		}
		if(value==2){
			alert('Adresa de email exista deja.');
		}
		if(value==3){
			alert('Multumim pentru abonarea la newsletter-ul Universul Juridic.');
		}
		
		if(value==4){
			alert('Adresa de email a fost confirmata cu succes.');
		}
	});
}
});

</script>
{/literal}
<div id="main">
	<div id="center-main">

    <!--begin header -->
    {include file=structura/header.tpl}
    <!--end header -->

                
      {$continut}
            
      
      {if $functie_curenta=='acasa'}
        <!--begin right -->
         {include file=structura/dreapta.tpl}
       <!--end right -->
       {/if}
       
    <!--begin footer -->
    {include file=structura/footer.tpl}
    <!--end footer -->
    </div>
</div>
</body>
</html>