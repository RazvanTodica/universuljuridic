 <div class="breadcrumb">
 <a href="{$CONF.sitepath}" title="Acasa">Acasa</a>
 {foreach from=$breadcrumb item=item}
 {if $item->link !=''}
 <a href="{$item->link}" title="{$item->title}">{$item->title}</a>
 {else}
 <span class="current_crumb">{$item->title}</span>
 {/if}
 {/foreach}   

 </div>