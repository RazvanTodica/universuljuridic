<!--begin right -->

   			<div id="right_wrapper">
            
                <div id="right_top"></div>
                
                <div id="right">
                
                {if $link.0=='evenimente'}
                <h3>Evenimente</h3>
                
                    <ul class="sidebar_menu sidebar_separator">
                    	<li><a href="{$CONF.sitepath}evenimente/conferinte" title="Conferinte">Conferinte</a></li>
						<li><a href="{$CONF.sitepath}evenimente/interviuri" title="Interviuri">Interviuri</a></li>
						<li><a href="{$CONF.sitepath}evenimente/editoriale" title="Editoriale">Editoriale</a></li>
                    </ul>
                {/if}  	
             
                {if $link.0=='editura'}
                	<h3>Editura</h3>
                	 <ul class="sidebar_menu sidebar_separator">
                	 		<li><a href="{$CONF.sitepath}editura/despre-noi" title="Despre noi">Despre noi</a></li>
							<li><a href="{$CONF.sitepath}editura/staff-editorial" title="Staff editorial">Staff editorial</a></li>
                         <!-- <li><a href="{$CONF.sitepath}editura/echipa-redactionala" title="Echipa redactionala">Echipa redactionala</a></li> -->
							<li><a href="{$CONF.sitepath}editura/referenti" title="Referenti">Referenti </a></li>
							<li><a href="{$CONF.sitepath}editura/cariere" title="Cariere">Cariere</a></li>
                	 </ul>
                {/if}
                {if $link.0=='reviste'}                
                	<h3>REVISTE</h3>
                
                    <ul class="sidebar_menu sidebar_separator">   
                     {foreach from=$revista_lista item=rev}
                     <li><a href="{$CONF.sitepath}{if $link.0!='reviste'}reviste/{/if}{$rev->link}" {if $rev->link==$url_sel} id="selectat" {/if}  title="{$rev->CAT_NUME}">{$rev->CAT_NUME}</a></li>
                     {/foreach}
                    </ul>
                  {/if}
                {if $link.0=='colectie'}   
                        
                	<h3>Colectii</h3>
                    <ul class="sidebar_menu sidebar_separator">
                     {if  $colectie_lista|@count > 0} 
                
                     {foreach from=$colectie_lista item=col}
                     <li><a href="{$CONF.sitepath}colectie/{$col->LINK}" title="{$col->TEXT}" {if $col->LINK==$link.1} id="selectat" {/if}>{if $col->TEXT=='I. Tratate' || $col->TEXT=='II. Cursuri universitare'}&nbsp;&nbsp;&nbsp;{$col->TEXT}{else}{$col->TEXT}{/if}</a></li>
                     {/foreach}
                        {/if}
                    </ul>
                    {/if}
   
                    <h3>Catalog</h3>
                    <div class="catalog sidebar_separator" >
                    	<a href="http://www.ujmag.ro/catalog/?pdf=0&mail=0&print=0&numele_from=&email_from=&numele_to=&email_to=&tip=0&categoria=0&editura=1&colectia=" target="_blank"><img src="{$CONF.sitepath}pub/images/sidebar-book.jpg" width="85" height="103" alt="Catalog" /></a>
                        <p>Acum iti poti descarca, printa sau trimite pe email catalogul!</p>
                        <a href="http://www.ujmag.ro/catalog/?pdf=0&mail=0&print=0&numele_from=&email_from=&numele_to=&email_to=&tip=0&categoria=0&editura=1&colectia=" title="Detalii" class="read_more_catalog" target="_blank">Vezi Detalii <span>&gt;</span></a>
                    </div>
                    
                    <h3>Newsletter</h3>
                    <div class="newsletter sidebar_separator">
                        <p>Aboneaza-te la newsletter pentru a fi la curent cu cele mai noi stiri si cele mai bune oferte!</p>
                        <form action="" method="POST" id="newsletter-form">
                            <fieldset>
                                <input type="text" id="newsletter-input" name="newsletterEmail" value="introdu adresa de e-mail" onclick="this.value='';" onfocus="this.select()" onblur="this.value=!this.value?'introdu adresa de e-mail':this.value;" />
                                <input type="submit" id="newsletter-submit" value="" />
                                <input type="hidden" name="newsletter_send" value="1"/>
                            </fieldset>
                        </form>
                  	 </div>
					 
				{if $bannere_slider_right} 
				<div id='right_slider'>
				{foreach from=$bannere_slider_right item=slider key=k}
					<a class='slide_{$k}' href="{$slider->href}" title="#" {if $k!=0}style='display:none;'{/if} >
						<img src="{$CONF.sitepath}pub/img_univers/{$slider->imagine}" width="240" height="247" alt="Promo Sidebar" />
					</a>
				{/foreach}

				</div>
				<script type='text/javascript'>
				{literal}
				$(document).ready(function() {
					var slides_no = {/literal}{$bannere_slider_right|@count};{literal}
					if(slides_no > 1) {
						var current_slide = 0;
						setInterval(function() {
							$("#right_slider a.slide_" + current_slide).hide();
							current_slide++;
							if(current_slide >= slides_no) current_slide = 0;
							$("#right_slider a.slide_" + current_slide).show();
						}, 4000);
					}
				});
				{/literal}
			  </script>
				
				{/if}

               </div>
                <div id="right_bottom"></div>
                
      </div>
      <!--end right -->
	  
	  