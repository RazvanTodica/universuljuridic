     		</div>
        <!--end content -->
        <div class="clear"></div>
        
    </div>
    <!--end content_wrapper -->
    
    <!--begin footer -->
    <div id="footer_wrapper">
        <div id="footer_top">
        	<a href="{$CONF.sitepath}" title="Univerul Juridic"><img src="{$CONF.sitepath}pub/images/logo_footer.png" width="74" height="115" alt="Universul Juridic" class="footer_logo" /></a>
        	<ul class="footer_nav">
            	<li><a href="{$CONF.sitepath}colectie" rel="nofollow" title="Colectii" class="first">Colectii</a></li>
                <li><a href="#" rel="nofollow" title="Reviste de specialitate">Reviste de specialitate</a></li>
                <li><a href="{$CONF.sitepath}noutati-editoriale" rel="nofollow" title="Noutati editoriale">Noutati editoriale</a></li>
                <li><a href="{$CONF.sitepath}colectie/carti-in-curs-de-aparitie" rel="nofollow" title="Carti in curs de aparitie">Carti in curs de aparitie</a></li>
                <li><a href="{$CONF.sitepath}contact" rel="nofollow" title="Contacteaza-ne" class="last">Contacteaza-ne</a></li>
            </ul>
            <ul class="footer_media">
            	<li><a href="https://www.facebook.com/pages/Editura-Universul-Juridic/365178503685930?ref=hl" target="_blank" rel="nofollow" title="Facebook" class="facebook"></a></li>
                <li><a href="https://twitter.com/UniversulJuridi" target="_blank" rel="nofollow" title="Twitter" class="twitter"></a></li>
            	
                
                <!-- <li><a href="http://pinterest.com/ujmag/" data-pin-do="buttonFollow" target="_blank" rel="nofollow" title="Pinterest" class="pinterest"></a></li>-->
                <li ><a href="http://goo.gl/maps/IWw6j" target="_blank" rel="nofollow" title="Google Places" class="google_places"></a></li>
                <li class="last"><a href="http://www.youtube.com/user/UniversulJuridic" target="_blank" rel="nofollow" title="YouTube" class="youtube"></a></li>
                </ul>
            {*<div class="footer_partners">
            	<h6>PARTENERI UNIVERSUL JURIDIC:</h6>


                <div style="padding: 10px; width:1020px; text-align:left; background:#ffffff;">
                    <table style="border-spacing:2px;">
                        <tr>
                            <td style="width: 15%;background: #ccc;border: 1px solid #ccc;">
                                <img src="http://www.ujmag.ro/newsletter/var1/parteneri_premium.jpg" title="PARTENERI PREMIUM" />
                            </td>
                            <td style="background: #FFF;width: 85%;border: 1px solid #ccc;">
                                <div style="margin: 0 auto;">
                                    {foreach from=$parteneri_univers_juridic item=item key=key}
                                        {if $item.tip=='partener-premium'}
                                            {*if $key==4}<div style="float:left;width:80px;">&nbsp;</div>{/if*}
                                           {* <div style="float:left;width:215px;"> *}
                                        {*   <div style="float:left;width:170px;{if $key==4}padding-top: 10px;{/if}">
                                                <a style="display:block;height:70px;line-height:70px;vertical-align:middle;{if ($key+1)%5 eq 0}text-align:right;{else}text-align:center;{/if}" {if $item.link!=''}href="{$item.link}"{/if} title="{$item.alt}" target="_blank" onMouseOver="document.imagine_partener_{$key}.src='{$item.color}';" onMouseOut="document.imagine_partener_{$key}.src='{$item.albnegru}';" rel="nofollow">
                                                    <img src="{$item.albnegru}" name="imagine_partener_{$key}" style="max-width:180px; {if $key==7}height:42px;{/if}" align="absmiddle" alt="{$item.alt}" />
                                                </a>
                                            </div>
                                        {/if}
                                    {/foreach}
                                </div>
                                <div style="clear:both">&nbsp;</div>
                            </td>
                        </tr>

                        <tr><td colspan="2" style="background: #ffffff; height: 1px;">&nbsp;</td></tr>

                        <tr>
                            <td style="width: 15%;background:#ccc;border: 1px solid #ccc;">
                                <img src="http://www.ujmag.ro/newsletter/var1/parteneri.jpg" title="PARTENERI" />
                            </td>
                            <td style="width: 85%;background:#ffffff;border: 1px solid #ccc;">
                                {foreach from=$parteneri_univers_juridic item=item key=key}
                                    {if $item.tip=='partener'}
                                        {if $key==14}<div style="float:left;width:220px;">&nbsp;</div>{/if}
                                            <div style="float:left;width:210px;">
                                            <a style="display:block;height:70px;line-height:70px;vertical-align:middle;{if ($key+1)%5 eq 0}text-align:right;{else}text-align:center;{/if}" {if $item.link!=''}href="{$item.link}"{/if} title="{$item.alt}" target="_blank" onMouseOver="document.imagine_partener_{$key}.src='{$item.color}';" onMouseOut="document.imagine_partener_{$key}.src='{$item.albnegru}';" rel="nofollow">
                                                <img src="{$item.albnegru}" name="imagine_partener_{$key}" style="max-width:180px;" align="absmiddle" alt="{$item.alt}" />
                                            </a>
                                        </div>
                                    {/if}
                                {/foreach}
                                <div style="clear:both">&nbsp;</div>
                            </td>
                        </tr>
                        {*<tr>*}
                            {*<td style="width: 15%;">*}
                                {*<img src="http://www.ujmag.ro/newsletter/var1/parteneri.jpg" title="PARTENERI" />*}
                            {*</td>*}
                            {*<td style="width: 85%;background: #ffffff;">*}
                                {*{foreach from=$parteneri_univers_juridic item=item key=key}*}
                                    {*{if $item.tip=='partener'}*}
                                        {*<div style="float:left;width:200px;">*}
                                            {*<a style="display:block;height:70px;line-height:70px;vertical-align:middle;{if ($key+1)%5 eq 0}text-align:right;{else}text-align:center;{/if}" {if $item.link!=''}href="{$item.link}"{/if} title="{$item.alt}" target="_blank" onMouseOver="document.imagine_partener_{$key}.src='{$item.color}';" onMouseOut="document.imagine_partener_{$key}.src='{$item.albnegru}';" rel="nofollow">*}
                                                {*<img src="{$item.albnegru}" name="imagine_partener_{$key}" style="max-width:180px; {if $key==7}height:42px;{/if}" align="absmiddle" alt="{$item.alt}" />*}
                                            {*</a>*}
                                        {*</div>*}
                                    {*{/if}*}
                                {*{/foreach}*}
                                {*<div style="clear:both">&nbsp;</div>*}
                            {*</td>*}
                        {*</tr>*}

                     {*   <tr><td colspan="2" style="background: #ffffff; height: 1px;">&nbsp;</td></tr>

                        <tr>
                            <td style="width: 15%;background: #ccc;border: 1px solid #ccc;">
                                <img src="http://www.ujmag.ro/newsletter/var1/parteneri_media.jpg" title="PARTENERI MEDIA" />
                            </td>
                            <td style="background: #FFF;width: 85%;border: 1px solid #ccc;">
                                {foreach from=$parteneri_media item=item key=key}
                                    {if $key==3}<div style="float:left;width:210px;">&nbsp;</div>{/if}
                                        <div style="float:left;{if $key >= 0 && $key <=4}width:210px;{else}width:210px;{/if}display:inline-block;margin-bottom: 5px;">
                                        <a style="display:block;height:70px;line-height:70px;vertical-align:middle;text-align:center;" {if $item.link!=''}href="{$item.link}"{/if} title="{$item.alt}" target="_blank" onMouseOver="document.imagine_partener_media_{$key}.src='{$item.color}';" onMouseOut="document.imagine_partener_media_{$key}.src='{$item.albnegru}';" rel="nofollow">
                                            <img src="{$item.albnegru}" name="imagine_partener_media_{$key}" style="max-width:150px;" align="absmiddle" alt="{$item.alt}" />
                                        </a>
                                    </div>
                                {/foreach}
                                <div style="clear:both">&nbsp;</div>
                            </td>
                        </tr>
                        {*<tr>*}
                            {*<td style="width: 15%;">*}
                                {*<img src="http://www.ujmag.ro/newsletter/var1/parteneri_media.jpg" title="PARTENERI MEDIA" />*}
                            {*</td>*}
                            {*<td style="background: #FFF;width: 85%;">*}
                                {*{foreach from=$parteneri_media item=item key=key}*}
                                    {*<div style="float:left;width:280px;text-align:center;">*}
                                        {*<a {if $item.link!=''}href="{$item.link}"{/if} target="_blank" onMouseOver="document.imagine_partener_media_{$key}.src='{$item.color}';" onMouseOut="document.imagine_partener_media_{$key}.src='{$item.albnegru}';" rel="nofollow">*}
                                            {*<img style="max-width:150px;" src="{$item.albnegru}" name="imagine_partener_media_{$key}" alt="{$item.alt}" />*}
                                        {*</a>*}
                                    {*</div>*}
                                {*{/foreach}*}
                                {*<div style="clear:both">&nbsp;</div>*}
                            {*</td>*}
                        {*</tr>*}
                    </table>
                </div>

            {*{if $is_admin}*}

            	{*{foreach from=$parteneri_univers_juridic key=k2 item=item2}*}
            	{*<div class="parteneri_box">*}
							{*<a href="{$item2.link}"  rel="nofollow" target="_blank" onMouseOver="document.imagine_partener_{$k2}.src='{$item2.color}';" onMouseOut="document.imagine_partener_{$k2}.src='{$item2.albnegru}';"><img src="{$item2.albnegru}" name="imagine_partener_{$k2}" alt="{$item2.alt}" border="0" /></a>   *}
				{*</div>*}
            	{*{/foreach}*}
            	{**}
            	{*<div class="clear"></div>*}
            	{*<h6>PARTENERI MEDIA</h6>*}
            	{*{foreach from=$parteneri_media key=k3 item=item3}*}
            	{*<div class="parteneri_box">*}
							{*<a href="{$item3.link}"  rel="nofollow" target="_blank" onMouseOver="document.imagine_partener_media_{$k3}.src='{$item3.color}';" onMouseOut="document.imagine_partener_media_{$k3}.src='{$item3.albnegru}';"><img src="{$item3.albnegru}" name="imagine_partener_media_{$k3}" alt="{$item3.alt}" border="0" /></a>   *}
				{*</div>*}
            	{*{/foreach}*}

            	{*<div class="clear"></div>*}

            {*{/if}*}
{*
            	<div class="clear"></div>
            	<h6>VA RECOMANDAM</h6>

                <div class="parteneri_box" style="margin-bottom:30px; height:100px;">
                    <a href="http://www.avincis.ro/" title="Avincis"  target="_blank" rel="nofollow">
                        <img src="pub/images/Sigla_avincis.jpg" alt="museum" height="100" />
                        </a>
                    </div>

                    <div class="parteneri_box">
							<a href="http://www.evogps.ro/" title="Monitorizare GPS" rel="nofollow" target="_blank"><img src="{$CONF.sitepathuj}s-img/LOGO-EVO-GPS-fara-text.jpg" alt="Evo GPS" height="100" />Monitorizare GPS</a>   
				</div>

				
            	<div class="parteneri_box">
							<a href="http://www.museumrestaurant.ro/"  rel="nofollow" target="_blank"><img src="{$CONF.sitepathuj}s-img/museum.jpg" alt="museum" height="100" /></a>   
				</div>
				
				{*<div class="parteneri_box">
							<a href="http://www.mihut.ro/"  rel="nofollow" target="_blank"><img src="{$CONF.sitepathuj}s-img/mihut.jpg" alt="mihut" height="100" /></a>   
				</div>*}

        <div class="clear" style="height:30px"></div>
    </div>
            <div style="border-bottom:1px solid #999999;">
                <h3 style="font-size:15px">PARTENERI UNIVERSUL JURIDIC</h3>

                <div style="padding: 0px;margin: 0 auto; width:1020px; text-align:left;">
                    <a href="https://www.ujmag.ro/parteneri">
                        <img src="/magazin/lib/images/parteneri/12-09-2018_sigle-parteneri-uj-carti.jpg">
                    </a>
                </div>
                <div class="clear" style="height:30px"></div>
                <h3 style="font-size:15px">VA RECOMANDAM</h3>

                <div class="clear" style="height:30px"></div>
                <div>
                    <div>
                        <div style="float:left;width:200px;">
                            <a style="display:block;height:70px;line-height:70px;vertical-align:middle;text-align:center;"
                               href="http://www.avincis.ro/" title="Avincis" target="_blank" rel="nofollow">
                                <img src="/magazin/lib/images/Avincis-vin-alb-rosu-rose-domeniul-Vila-Dobrusa-dragasani_1.jpg"
                                     alt="museum" height="100"/>
                            </a>
                        </div>
                        <div style="float:left;width:200px;">
                            <a style="display:block;height:70px;line-height:70px;vertical-align:middle;text-align:center;"
                               href="http://www.evogps.ro/" title="Monitorizare GPS" target="_blank" rel="nofollow">
                                <img src="/magazin/lib/images/Evo-GPS-monitorizare-management-rapoarte-flota-auto_1.jpg" alt="EVOGPS"
                                     height="100"/>
                                
                            </a>
                        </div>
                        <div style="float:left;width:200px;">
                            <a style="display:block;height:70px;line-height:70px;vertical-align:middle;text-align:center;"
                               href="http://www.museumrestaurant.ro/" title="museumrestaurant.ro" target="_blank"
                               rel="nofollow">
                                <img src="/magazin/lib/images/Museum-Restaurant-Bucuresti-Eroilor-Strada-Doctor-Clunet_1.jpg"
                                     alt="museum" height="100"/>
                            </a>
                        </div>
                        <div style="float:left;width:200px;">
                            <a style="display:block;height:70px;line-height:70px;vertical-align:middle;text-align:center;"
                               href="http://casadetraduceri.ro/" title="casadetraduceri.ro" target="_blank"
                               rel="nofollow">
                                <img src="/magazin/lib/images/casadetraduceri.jpg"
                                     alt="casadetraduceri" height="100">
                            </a>
                        </div>
                        <div class="clear" style="height:69px"></div>
                    </div>
                    <div class="clear" style="height:30px"></div>
                </div>
                <div style="clear:both"></div>
            </div>
				
            	
            	
            </div>
        </div>
    	<div id="footer_bottom">
        	<ul class="copyright">
            	<li class="first">Copyright &copy; {'Y'|date} Editura Universul Juridic. Toate drepturile rezervate</li>
                <li><a href="{$CONF.sitepath}termeni-conditii" rel="nofollow" title="Termeni si Conditii">Termeni & Conditii</a></li>
            </ul>
    
{literal} 

<!--/Start async trafic.ro/-->
<script type="text/javascript" id="trfc_trafic_script">
//<![CDATA[
t_rid = 'editurauniversuljuridic-ro';
(function(){ t_js_dw_time=new Date().getTime();
t_js_load_src=((document.location.protocol == 'http:')?'http://storage.':'https://secure.')+'trafic.ro/js/trafic.js?tk='+(Math.pow(10,16)*Math.random())+'&t_rid='+t_rid;
if (document.createElement && document.getElementsByTagName && document.insertBefore) {
t_as_js_en=true;var sn = document.createElement('script');sn.type = 'text/javascript';sn.async = true; sn.src = t_js_load_src;
var psn = document.getElementsByTagName('script')[0];psn.parentNode.insertBefore(sn, psn); } else {
document.write(unescape('%3Cscri' + 'pt type="text/javascript" '+'src="'+t_js_load_src+';"%3E%3C/sc' + 'ript%3E')); }})();
//]]>
</script>
<noscript><p><a href="http://www.trafic.ro/statistici/editurauniversuljuridic.ro"><img alt="editurauniversuljuridic.ro" src="http://log.trafic.ro/cgi-bin/pl.dll?rid=editurauniversuljuridic-ro" /></a> <a href="http://www.trafic.ro/">Web analytics</a></p></noscript>
<!--/End async trafic.ro/-->


{/literal}
	</div>
    </div>

</div>
</body>
</html>