 <!--begin header -->
    <div id="header_wrapper">
        <div id="header" class="clearfix">
 
            <!--begin logo -->
            <div id="logo">
                <h1><a href="{$CONF.sitepath}" title="UNIVERSUL JURIDIC">UNIVERSUL JURIDIC</a></h1>
            </div>
            <!--end logo -->
            
            <!--begin select_category -->
            <div class="tni_menu">
            	<div class="tn1">Grupul editorial Universul Juridic</div>
				<ul>
					<li><a href="http://www.universuljuridic.ro/" title="Universul Juridic" target="_blank" rel="nofollow">Universul Juridic</a></li>
					<li><a href="http://www.prouniversitaria.ro/" title="Editura Pro Universitaria" target="_blank" rel="nofollow">Editura Pro Universitaria</a></li>
					<li><a href="http://www.ujmag.ro/cautare/?editura=editura+neverland" title="Editura Neverland" target="_blank" rel="nofollow">Editura Neverland</a></li>
					<li><a href="http://www.ujmag.ro/" title="UJmag.ro" target="_blank" rel="nofollow">UJmag.ro</a></li>
				</ul>
            </div>
            <!--end select_category -->
                        
            <!--begin search_wrapper -->
            <div class="search_wrapper">
                <div>
                    <img src="{$CONF.sitepath}pub/images/search_header.png" width="18" height="17" alt="Search" class="trigger" />
                </div>
                <div class="popup">
                	<form action="{$CONF.sitepath}cauta" method="GET" id="cautare-form">
                        <fieldset>
                            <input type="text" id="cautare-input" name="key_words" value="" onclick="this.value='';"   onfocus="this.select()" onblur="this.value=!this.value?'Cautare':this.value;" />
                            <input type="submit" id="cautare-submit" value="" />
                        </fieldset>
                    </form>
                </div>
            </div>
            <!--end search_wrapper -->
            
            <!--begin top_icons -->
            <ul class="top_icons">
                <li><a href="{$CONF.sitepath}" title="Acasa"><img src="{$CONF.sitepath}pub/images/home_header.png" width="19" height="17" alt="Acasa" /></a></li>
                <li><a href="{$CONF.sitepath}termeni-conditii" title="Informatii" rel="nofollow" class="last"><img src="{$CONF.sitepath}pub/images/info_header.png" width="9" height="17" alt="Informatii" /></a></li>
                <li><a href="{$CONF.sitepath}contact" title="Contact" class="last"><img src="{$CONF.sitepath}pub/images/phone_header.png" width="17" height="17" alt="Contact" /></a></li>
            </ul>
            <!--end top_icons -->
            
          <div style="clear:both;"></div>
          
            <ul id="dd_menu">
				<li>
				<a href="{$CONF.sitepath}colectie" style="margin-left: 10px;"' class="link">Colectii
				
				</a>
				<ul>
					
			<table border="0" cellspacing="0" cellpadding="0" style="width:883px;">
					<td>
						
						{foreach from=$colectie_lista key=k  item=item}
							{if $k < 10}
							<li class="link2"><a href="{$CONF.sitepath}colectie/{$item->LINK}" title="{$item->TEXT}">{$item->TEXT|truncate:45:"..."}</a></li>
						{/if}
						{/foreach}
					</td>
					<td>
						{foreach from=$colectie_lista key=k  item=item}
							{if $k > 10 && $k <=20}
							<li class="link2"><a href="{$CONF.sitepath}colectie/{$item->LINK}" title="{$item->TEXT}">{$item->TEXT|truncate:45:"..."}</a></li>
						{/if}
						{/foreach}
					</td>
					<td>
						{foreach from=$colectie_lista key=k  item=item}
							{if $k > 20 && $k <=30}
							<li class="link2"><a href="{$CONF.sitepath}colectie/{$item->LINK}" title="{$item->TEXT}">{$item->TEXT|truncate:45:"..."}</a></li>
						{/if}
						{/foreach}
					</td>
				</table>
				
				</ul>
				</li>
				<li><a href="{$CONF.sitepath}editura/despre-noi" class="link">Editura</a>
					<ul>
						<li class="link2"><a href="{$CONF.sitepath}editura/despre-noi" title="Despre noi">Despre noi</a></li>
						<li class="link2"><a href="{$CONF.sitepath}editura/staff-editorial" title="Staff editorial">Staff editorial</a></li>
						<li class="link2"><a href="{$CONF.sitepath}editura/echipa-redactionala" title="Echipa redactionala">Echipa redactionala</a></li>
						<li class="link2"><a href="{$CONF.sitepath}editura/referenti" title="Referenti">Referenti </a></li>
						<li class="link2"><a href="{$CONF.sitepath}editura/cariere" title="Cariere">Cariere</a></li>
					</ul>
				</li>
				<li><a href="{$CONF.sitepath}reviste"  class="link">Reviste</a>
				<ul>
					{foreach from=$revista_lista item=item}
						<li class="link2" id="revista"><a href="{$CONF.sitepath}{$item->link}" title="{$item->CAT_NUME}">{$item->CAT_NUME|truncate:45:"..."}</a></li>
					{/foreach}
					<li class="link2" id="revista"><a href="{$CONF.sitepath}reviste/palatul-de-justitie" title="Revista Palatul de Justitie">Palatul de Justitie</a></li>
				</ul>
				
				</li>
				<li class="fourth" ><a href="{$CONF.sitepath}evenimente" class="link">Evenimente</a>
					<ul>
							<li class="link2"><a href="{$CONF.sitepath}evenimente/conferinte">Conferinte</a></li>
							<li class="link2"><a href="{$CONF.sitepath}evenimente/interviuri">Interviuri</a></li>
							<li class="link2"><a href="{$CONF.sitepath}evenimente/editoriale">Editoriale</a></li>
						</ul>
				</li>
				<li><a href="{$CONF.sitepath}autori"  class="link">Autori</a></li>
				<li class="last link"><a href="{$CONF.sitepath}contact">Contact</a></li>
		</ul>
            
            <!--begin nav -->
            <div id="nav" style="margin:0px;"></div>
            <!--end nav -->             
        </div>
    </div>
    <!--end header -->
 
   <!--begin content_wrapper -->
    <div id="content_wrapper">
   
    {if $rezultat->functie == 'acasa' && $banner_header|@count>0}
   
        
		<div class="liquid-slider" id="liquid-slider">
			{foreach from=$banner_header key=k item=item2}
			 <div>
				  <a href='{$item2->url_destinatie}'><img style='width: 1030px; height: 539px;' src='{$item2->imagine}' /></a>
			 </div>
			 {/foreach}
		</div>

	{/if}

        
   <!--begin content -->
   <div id="content">
	
   
 