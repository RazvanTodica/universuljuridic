  <!--begin breadcrumb -->
    {include file="structura/breadcrumb.tpl}
    <!--end breadcrumb -->
    
    <!--begin content_wrapper -->
    <div id="content_wrapper">
       
        <!--begin content -->
        <div id="content">
            
            <!--begin left -->
            <div id="left">
           
            {if $detalii}   
	            	  <h1 class="page_title">{$detalii->staff_nume}</h1>
            	
                <div class="stuff_wrapper">      
                    <div class="stuff_box">
                    
                        <a href="{$CONF.sitepath}editura/staff-editorial/{$detalii->staff_link}" title="{$detalii->staff_nume}" class="left"><img src="{if $detalii->staff_filename}{$detalii->staff_filename|resize_pic:'130x181'}{else}{$CONF.sitepath}pub/images/no_image.png{/if}" width="130" height="181" alt="Picture" class="border_img" /></a>
                        {* <h3><a href="{$CONF.sitepath}editura/staff-editorial/{$detalii->staff_link}" title="{$detalii->staff_nume}">{$detalii->staff_nume}</a></h3>*}
                        <p>{$detalii->staff_descriere}</p>
                    </div>
                 
                  
                </div>
            
            
            {else}	
            
            
                <h1 class="page_title">Staff editorial</h1>
            	
                <div class="stuff_wrapper">
                
                   {foreach from=$staff item=item}
                    <div class="stuff_box">
                    
                        <a href="{$CONF.sitepath}editura/staff-editorial/{$item->staff_link}" title="{$item->staff_nume}" class="left"><img src="{$item->imagine_sfaff}" width="130" height="181" alt="Picture" class="border_img" /></a>
                        <h3><a href="{$CONF.sitepath}editura/staff-editorial/{$item->staff_link}" title="{$item->staff_nume}">{$item->staff_nume}</a></h3><br/><br/>
                        <p>{$item->staff_descriere|clear_font|strip_tags|truncate:600:"..."}</p>
                        <a href="{$CONF.sitepath}editura/staff-editorial/{$item->staff_link}" title="Detalii" class="read_more">Detalii <span>&gt;</span></a>
                    </div>
                    {/foreach}
                  
                </div>
                
            {/if}
                
            </div>
            <!--end left -->
            
            <!--begin right -->
            {include file="structura/dreapta.tpl"}
            <!--end right -->
            
        </div>
        <!--end content -->
        <div class="clear"></div>
        
    </div>
    <!--end content_wrapper -->