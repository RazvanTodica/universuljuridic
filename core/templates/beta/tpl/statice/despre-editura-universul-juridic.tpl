			<!--begin left -->
            <div id="left">
            	<h1 class="page_title">Despre Editura Universul Juridic</h1>

<p>
Infiintata in 2001, Editura Universul Juridic a devenit in 7 ani un actor important al pietei de carte juridica si economica din România.
<br /><br />
De la infiintare si pana in prezent au fost editate un numar de 350 titluri din domeniile juridic si economic, iar prin intermediul retelei noastre de distributie si al colaboratorilor nostri, cartile editurii sunt distribuite in peste 200 de librarii si puncte de vânzare din intreaga tara.
<br /><br />
De asemenea, editura detine si o tipografie proprie cu o capacitate de peste 20.000 exemplare tiparire in exclusivitate sub sigla editurilor Universul Juridic si Pro Universitaria (edituri acreditate de Consiliul National al Cercetarii Stiintifice din Invatamantul Superior).
<br /><br />
Un pas important al editurii Universul Juridic l-a constituit lansarea celei mai mari librarii online de carte juridica si economica din Romania – <a href="http://www.ujmag.ro/" target="_blank">www.ujmag.ro.</a>
<br /><br />
Prin intermediul librariei UJ Magazin, suntem la dispozitia cititorilor nostri 24 de ore din 24 oferindu-le acestora numeroase promotii si reduceri de pret la toate cartile Colectiei Business.
<br /><br /><br />
<i><b>Nicolae Cirstea</b></i><br />
<i>Directorul editurii Universul Juridic</i>
</p>         	
            </div>
            <!--end left -->