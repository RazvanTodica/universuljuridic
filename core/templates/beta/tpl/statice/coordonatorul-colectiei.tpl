			<!--begin left -->
            <div id="left">
            	<h1 class="page_title">Coordonatorul Colectiei</h1>

<p>
Dragos Panainte (n. 1979) este licentiat in Sociologie si Politologie a Facultatii de Filosofie si Stiinte Sociale a Universitatii "Al. I. Cuza" Iasi.
<br /><br />
Este Senior Consultant al Cabinetului de avocatura CARP Iasi si Managing Partner al agentiei de consultanta Indra Communications Iasi.
Este membru al Asociatiei Romane de Sociologie si al Asociatiei Specialistilor in Comunicare Politica.
Intre 2003 si 2006, a fost presedintele Asociatiei Active Iasi.
Coordoneaza si organizeaza seria de conferinte si seminarii de afaceri Law Biz.
In calitatea de autor si coautor a publicat:  
<br /><br />
Ghidul primarului – Editura Institutului European, Iasi, 2000, Colectia Utilis.
Ghidul avocatului de succes - Editura ALL Beck, Bucuresti, 2003, Colectia Cariere. 
Ghdiul studentului la drept - Editura C.H. Beck, Bucuresti, 2006, Colectia Cariere.
Cum sa infiintezi si sa administrezi un SRL. Solutii juridice, fiscale si contabile pentru primii pasi in afaceri - Editura Universul Juridic, Bucuresti, 2009, Colectia Business. 
<br /><br />
</p>         	
            </div>
            <!--end left -->