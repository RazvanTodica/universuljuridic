  <!--begin breadcrumb -->
    {include file="structura/breadcrumb.tpl}
    <!--end breadcrumb -->
    
    <!--begin content_wrapper -->
    <div id="content_wrapper">
       
        <!--begin content -->
        <div id="content">
            
            <!--begin left -->
            <div id="left">
            	
                <h1 class="page_title">Referenti</h1>
            	
                <div class="stuff_wrapper">
					1. Prof. univ. dr. CONSTANTIN MITRACHE
					<br />
					Universitatea din Bucuresti
					<br /><br />
					2. Prof. univ. dr. EMILIAN STANCU
					<br />
					Universitatea din Bucuresti
					<br /><br />
					
					3. Prof. univ. dr. EMIL MOLCUT
					<br />
					Universitatea din Bucuresti
					<br /><br />
                  
                </div>
                
            </div>
            <!--end left -->
            
            <!--begin right -->
            {include file="structura/dreapta.tpl"}
            <!--end right -->
            
        </div>
        <!--end content -->
        <div class="clear"></div>
        
    </div>
    <!--end content_wrapper -->