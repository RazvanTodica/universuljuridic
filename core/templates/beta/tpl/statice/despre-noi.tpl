  <!--begin breadcrumb -->
  {include file="structura/breadcrumb.tpl}
    <!--end breadcrumb -->
    
    <!--begin content_wrapper -->
    <div id="content_wrapper">
       
        <!--begin content -->
        <div id="content">
            
            <!--begin left -->
            <div id="left">
            	
                <h1 class="page_title">Despre noi</h1>
            	
                <div class="stuff_wrapper">
                    <p>&Icirc;nfiin&#355;at&#259; &icirc;n anul 2001, Editura "Universul Juridic" a devenit &icirc;n 15 ani un actor important al pie&#355;ei de carte juridic&#259; din Rom&acirc;nia.</p>
					  <p>Pozi&#355;ia pe care o ocup&#259; &icirc;n acest moment editura pe pia&#355;a de carte juridica se datoreaz&#259; bazelor solide care au fost puse &icirc;nc&#259; de la &icirc;nfiin&#355;are: respect &#351;i seriozitate &icirc;n rela&#355;iile cu colaboratorii no&#351;tri &#351;i dorin&#355;a de a dezvolta &icirc;n permanen&#355;&#259; editura.</p>
					  <p>Din anul 2001 &#351;i p&acirc;n&#259; &icirc;n prezent au fost editate un num&#259;r de peste 5000 de titluri din domeniile juridic &#351;i economic.</p>
					  <p>Majoritatea lucr&#259;rilor ap&#259;rute &icirc;n cadrul Editurii "Universul Juridic" sunt semnate de autori consacra&#355;i dintre care &icirc;i amintim pe: Francisc Deak, Gheorghe Beleiu, Ion P. Filipescu, Stanciu D. Carpenaru, Octavian Capatana, Costic&#259; Bulai, Liviu Pop, Emil Molcu&#355;, Constantin Mitrache, Emilian Stancu, Avram Filipas, Alexandru Ticlea, Lucian Mihai, Dan Lupa&#351;cu, Romeo Popescu, Gheorghe
Florea, Marian Nicolae, etc. &#351;i se adreseaz&#259; tuturor categoriilor de cititori: studen&#355;i, practicieni, teoreticieni c&acirc;t &#351;i nespeciali&#351;tilor care doresc s&#259; se familiarizeze cu elementele problematicii specifice &#351;tiin&#355;elor juridice sau, dup&#259; caz, ale legisla&#355;iei &#351;i practicii judiciare actuale.</p>
					  <p>Printre titlurile noastre se reg&#259;sesc: tratate, cursuri universitare, monografii juridice, acte normative &#351;i acte normative adnotate, culegeri de practic&#259; judiciar&#259; pe materii, structurate pe colec&#355;ii.</p>
					  <p>Prin intermediul re&#355;elei proprii de distribu&#355;ie &#351;i cu ajutorul colaboratorilor, c&#259;r&#355;ile noastre sunt distribuite &icirc;n peste 200 de libr&#259;rii &#351;i puncte de v&acirc;nzare din &icirc;ntreaga &#355;ar&#259;.</p>
					  <p>Din dorin&#355;a de a transforma Editura "Universul Juridic" intr-o editur&#259; specializat&#259; &icirc;n publicarea cu prioritate a titlurilor reprezentative din domeniul juridic (cercetare &#351;tiin&#355;ific&#259;, legisla&#355;ie &#351;i jurispruden&#355;&#259;) s-a &icirc;nfiin&#355;at &icirc;n anul 2004 Editura "Pro Universitaria", editur&#259; specializat&#259; &icirc;n principal pe publicarea de cursuri universitare din alte domenii de activitate.</p>
					 
					  <p>Anul 2008 reprezint&#259; un moment important al dezvolt&#259;rii noastre, fapt ce a impus m&#259;rirea echipei editoriale, &icirc;n scopul dezvolt&#259;rii unor noi colec&#355;ii &#351;i al consolid&#259;rii celor deja existente.</p>
					  <p>De asemenea, dupa ce anul 2007 a marcat apari&#355;ia <a href="http://www.rrdp.ro/">Revistei Rom&acirc;ne de Drept Privat</a>, un eveniment editorial de importan&#355;&#259; major&#259; pe pia&#355;a juridic&#259; &icirc;n Rom&acirc;nia, Editura Universul Juridic lanseaza in anul 2008 <a href="http://www.rrdj.ro/">Revista romana de jurisprudenta</a>.
					  </p>
                </div>
                
            </div>
            <!--end left -->
            
            <!--begin right -->
            {include file="structura/dreapta.tpl"}
            <!--end right -->
            
        </div>
        <!--end content -->
        <div class="clear"></div>
        
    </div>
    <!--end content_wrapper -->