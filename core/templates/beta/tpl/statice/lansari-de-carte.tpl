			<!--begin left -->
            <div id="left">
            	<h1 class="page_title">Lansari de carte</h1>

<p>
In data de 3 decembrie 2009 ora 14.00, sala Dacia, a Camerei de Comert si Industrie a Romaniei, Editura Universul Juridic a lansat trei carti de exceptie din Colectia Business, care au ca tematica actuala criza financiara
<br /><br />
În data de 3 decembrie 2009 ora 14.00, sala Dacia, a Camerei de Comerţ şi Industrie a României, Editura Universul Juridic a lansat trei cărţi de excepţie din Colecţia Business, care au ca tematică actuala criză financiară.
<br /><br />
- Criza, a cincea putere în stat, Managementul crizelor politice şi economice, reflectat în presa timpului în Romania anilor 1930-1939, autor: Daniel Apostol;
- Împotriva curentului, Însemnări despre criza financiară actual, autor: Bogdan Glavan;
- Soluţii fiscale pentru perioade de criză, Reorganizarea afacerilor. Aspecte fiscal, autori: Emilian Duca, Alina Duca.
<br /><br />

Colecţia Business se bucură de două importante parteneriate, unul cu Camera de Comerţ şi Industrie a României partenerul oficial al acestei colecţii şi un parteneriat media cu The Money Channel.
<br /><br />

Evenimentul a fost transmis în direct de postul TV Money Channel şi s-a bucurat de o prezenţă numeroasă, iar la sfârşitul manifestării oaspeţii au fost invitaţi la o sesiune de autografe şi un pahar de şampanie.
<br /><br />


 
 

Editura Universul Juridic a lansat la Iasi, miercuri 16 decembrie 2009, Libraria Carturesti din incinta Complexului Comercial Iulius Mall Iasi lucrarea Contabilitatea nu-i doar pentru contabili

Editura Universul Juridic a lansat la Iaşi, miercuri 16 decembrie 2009, Librăria Cărtureşti din incinta Complexului Comercial Iulius Mall Iaşi lucrarea "Contabilitatea nu-i doar pentru contabili!", autor: Istrate Costel, lector al Facultăţii de Ştiinţe Economice a Universităţii "Al.I. Cuza" din Iaşi. 

Evenimentul a fost moderat de coordonatorul Colecţiei Business, Dragoş Panainte şi s-a bucurat de prezenţa oficialităţilor locale, cadre universitare, studenţi, alături de colegi şi apropiaţi ai autorului.

Printre participanţii evenimentului s-au numărat : Nicolae Cîrstea Directorul General al Editurii Universul Juridic, Gheorghe Pleşu Preşedintele Asociaţiei Oamenilor de Afaceri Iaşi, Gabriel Mardare prof. univ. dr. la Universitatea din Bacău, Pavel Badragan Preşedintele Filialei Iaşi CECCAR, Constantin Toma Director al Departamentului de Administrarea a Afacerilor, de la Facultatea de Economie şi Administrarea Afacerilor Iaşi. 
</p>         	
            </div>
            <!--end left -->