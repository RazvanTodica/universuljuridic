<!--begin breadcrumb -->
{include file="structura/breadcrumb.tpl}
    <!--end breadcrumb -->
    
    <!--begin content_wrapper -->
    <div id="content_wrapper">
       
        <!--begin content -->
        <div id="content">
            
            <!--begin left -->
            <div id="left">
            	
                <h1 class="page_title">Echipa redactionala</h1>
            	
                <div class="stuff_wrapper">
                    
<b>OLIVIU CRÂZNIC</b>
<p>Consilier juridic – Colecția PRO LEGE (coordonator proiect)
    Este licențiat al Facultății de Drept din cadrul Universității din București (2001), consilier juridic din anul 2002, membru al Ordinului Consilierilor Juridici din România și al Colegiului Consilierilor Juridici București, începând cu anul 2008.
    A absolvit cursurile de formare a mediatorilor organizate de Asociația „Pro Medierea” (2009).
    Cu o experiență de 9 ani în domeniul editorial (în principal, carte juridică), coordonează, din anul 2013, Colecția PRO LEGE a Editurii Universul Juridic și, din anul 2014, rubrica „Actualitate legislativă” a portalului UniversulJuridic.ro.
    Face parte din echipa redacțională a Editurii Universul Juridic din mai 2013.</p>

<!--<b>CRISTINA-MARIA CONSTANTIN</b>
<p>Redactor – cărți și reviste (printre altele: Revista Phoenix; Revista română de jurisprudență; Revista română de executare silită; Revista română de executare silită din Republica Moldova).
    Este licențiată a Facultății de Drept din cadrul Universității din București (2005-2009).
    A absolvit cursurile de masterat ale Facultății de Drept din cadrul Universității din București (2009-2010), specializarea „Carieră judiciară”.
    Face parte din echipa redacțională a Editurii Universul Juridic din mai 2013.</p>
-->
<b>ANDRA CRISTESCU</b>
<p>Redactor – Colecția PRO LEGE
    Este licențiată a Facultății de Drept din cadrul Universității din București (2008-2012).
    A absolvit cursurile de master ale Facultății de Drept din cadrul Universității „Nicolae Titulescu” din București (2012-2013), specializarea „Drept internațional și comunitar”.
    Face parte din echipa redacțională a Editurii Universul Juridic din februarie 2014.</p>


{*<b>ROXANA-BEATRIS ENE</b>
<p>Redactor – cărți și reviste (printre altele: Caiete de drept penal; Revista de drept public; Journal of Eastern-European Criminal Law; Romanian Journal of Comparative Law; Law Review; Criminal Law Review)
    Este licențiată a Facultății de Jurnalism și Științele Comunicării din cadrul Universității din București (2002-2006) și a Facultății de Drept din cadrul Universității „Titu Maiorescu” din București (2007-2011).
    Face parte din echipa redacțională a Editurii Universul Juridic din septembrie 2012.</p>*}




<b>ALEXANDRA IONESCU</b>
<p>Redactor – cărți
    Este licențiată a Facultății de Drept din cadrul Universității „Nicolae Titulescu” din București (2009-2013).
    A absolvit cursurile de masterat ale Facultății de Drept din cadrul Universității „Nicolae Titulescu” din București, specializarea „Dreptul afacerilor”.
    Face parte din echipa redacțională a Editurii Universul Juridic din aprilie 2014. </p>

  {*  <b>ALINA-ALEXANDRA IȘTOC</b>
<p>Redactor – cărți și reviste (printre altele: Revista Legal Point; Revista de drept constituțional).
    Este licențiată a Facultății de Drept din cadrul Universității din București (2009-2013).
    A absolvit cursurile de masterat ale Facultății de Drept din cadrul Universității „Nicolae Titulescu” din București (2014-2015), specializarea „Drept financiar, bancar și al asigurărilor”.
    Face parte din echipa redacțională a Editurii Universul Juridic din noiembrie 2013.</p>*}

<b>CRISTINA NICOLAI</b>
<p>Redactor – cărți și reviste (printre altele: Revista română de drept privat).
    Este licențiată a Facultății de Drept din cadrul Universității Babeș-Bolyai din Cluj-Napoca (2007-2011).
    A absolvit cursurile de masterat ale Facultății de Drept din cadrul Universității Babeș-Bolyai din Cluj-Napoca (2011-2012), specializarea „Drept privat comparat” (cu predare în limba franceză).
    Face parte din echipa redacțională a Editurii Universul Juridic din mai 2013.</p>

<b>RALUCA-VALENTINA TEODORU</b>
<p>Redactor – cărți. Este licențiată a Facultății de Drept și Științe Administrative din cadrul Universității din Craiova, specializările Administrație Publică și Drept (2005-2010).
    A absolvit cursurile de masterat ale Facultății de Drept din cadrul Universității din Craiova (2008-2010), specializarea „Științe administrative”.
    Face parte din echipa redacțională a Editurii Universul Juridic din ianuarie 2015.</p>
<b>RĂZVAN-GEORGE NEAȚĂ</b>

<p>Redactor – cărți. Este licenţiat al Facultăţii de Drept din cadrul Universităţii din Bucureşti (2009-2013).
 Face parte din echipa redacţională a Editurii Universul Juridic din octombrie 2018.</p>

<b>ANDREEA-GEORGIANA SAVU</b>
<p>Redactor – cărți și revista Caiete de drept penal.
Este licențiată a Facultății de Drept din cadrul Universității din București (2012-2016).
A absolvit cursurile de masterat ale Facultății de Drept din cadrul Universității București (2016-2017), specializarea „Carieră judiciară”.
Face parte din echipa redacțională a Editurii Universul Juridic din noiembrie 2017.</p>




<b>REDACTORI COLABORATORI:</b><br />
<br />
<b>ALINA-ALEXANDRA GRIGORAȘ</b>
<p>Redactor-șef – Revista Legal Point. Este licențiată a Facultății de Drept din cadrul Universității din București (2009-2013).
A absolvit cursurile de masterat ale Facultății de Drept din cadrul Universității „Nicolae Titulescu” din București (2014-2015), specializarea „Drept financiar, bancar și al asigurărilor”.
Face parte din echipa redacțională a Editurii Universul Juridic din noiembrie 2013.</p>



<b>SIMONA-APRILA ŢĂRANU</b>
<p>Redactor – cărţi. Este licenţiată a Facultăţii de Drept din cadrul Universităţii din Bucureşti (2012-2016).
Urmează cursurile de masterat ale Facultăţii de Drept din cadrul Universităţii din Bucureşti (2017-2018), specializarea „Drept internaţional public”.
Face parte din echipa redacţională a Editurii Universul Juridic din mai 2017. </p>



                </div>
                
            </div>
            <!--end left -->
            
            <!--begin right -->
            {include file="structura/dreapta.tpl"}
            <!--end right -->
            
        </div>
        <!--end content -->
        <div class="clear"></div>
        
    </div>
    <!--end content_wrapper -->