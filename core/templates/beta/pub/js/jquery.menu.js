$(document).ready(function() {
		defaultValue("#searchField");
		menu("#nav2 ul li");
		equalHeight($(""));
});
/*********************
//* jQuery Multi Level CSS Menu #2- By Dynamic Drive: http://www.dynamicdrive.com/
//* Last update: Nov 7th, 08': Limit # of queued animations to minmize animation stuttering
//* Menu avaiable at DD CSS Library: http://www.dynamicdrive.com/style/
*********************/

//Update: April 12th, 10: Fixed compat issue with jquery 1.4x

//Specify full URL to down and right arrow images (23 is padding-right to add to top level LIs with drop downs):
var arrowimages={down:['', ''], right:['', '']}

var arrowPad = 38;  // Set this to the desired padding-right for the arrows
var displayArrows = false;  // Set this to true to turn on the arrow images on the menu

var jquerymenu={

animateduration: {over: 150, out: 150}, //duration of slide in/ out animation, in milliseconds

buildmenu:function(menuid, arrowsvar){
	jQuery(document).ready(function($){
		var $mainmenu=$("#"+menuid+">ul")
		var $headers=$mainmenu.find("ul").parent() // here are the top lists
		$headers.each(function(i){
			var $curobj=$(this) // current top list
			var $subul=$(this).find('ul:eq(0)') // the first submenu
			this._dimensions={w:this.offsetWidth, h:this.offsetHeight, subulw:$subul.outerWidth(), subulh:$subul.outerHeight()}
			this.istopheader=$curobj.parents("ul").length==1? true : false
			$subul.css({top:this.istopheader? this._dimensions.h+"px" : 0})
			if (displayArrows == true) {
				$curobj.children("a:eq(0)").css(this.istopheader? {paddingRight: arrowsvar.down[2]} : {}).append(
					'<img src="'+ (this.istopheader? arrowsvar.down[1] : arrowsvar.right[1])
					+'" class="' + (this.istopheader? arrowsvar.down[0] : arrowsvar.right[0])
					+ '" style="border:0;" />'
				)
			}
			$curobj.hover(
				function(e){
					var $targetul=$(this).children("ul:eq(0)")
					this._offsets={left:$(this).offset().left, top:$(this).offset().top}
					var menuleft=this.istopheader? 0 : this._dimensions.w
					menuleft=(this._offsets.left+menuleft+this._dimensions.subulw>$(window).width())? (this.istopheader? -this._dimensions.subulw+this._dimensions.w : -this._dimensions.w) : menuleft
					if ($targetul.queue().length<=1) //if 1 or less queued animations
						$targetul.css({left:menuleft+"px", width:this._dimensions.subulw+'px'}).slideDown(jquerymenu.animateduration.over)
				},
				function(e){
					var $targetul=$(this).children("ul:eq(0)")
					$targetul.slideUp(jquerymenu.animateduration.out)
				}
			) //end hover
		}) //end $headers.each()
		$mainmenu.find("ul").css({display:'none', visibility:'visible'})
	}) //end document.ready
}
}

jquerymenu.buildmenu("nav", arrowimages);
function menu($tags)
{	
	jQuery($tags).each(function()
		{				
			$(this).find('li:first').addClass('firstItem');
			$(this).find('li:last').addClass('lastItem');						
	});
	jQuery($tags).contents("a").removeAttr('title');	
}
function defaultValue($tags)
{	
	jQuery($tags).each(function()
	{	
		$(this).click(
			function() {
				if (this.value == this.defaultValue) {
					this.value = '';
				}
			}
		);
		$(this).blur(
			function() {
				if (this.value == '') {
				this.value = this.defaultValue;
				}
			}
		);	
	});		
}     
function equalHeight(group) {
   tallest = 0;
   group.each(function() {
      thisHeight = $(this).height();
      if(thisHeight > tallest) {
         tallest = thisHeight;
      }
   });
   group.height(tallest);
}
