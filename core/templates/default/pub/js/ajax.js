var ajaxreload_timp=0;
function noCache(uri){return uri.concat(/\?/.test(uri)?"&":"?","noCache=",(new Date).getTime(),".",Math.random()*1234567)};
function parse_form(tag_name){
	var string='';
	var x=document.getElementsByName(tag_name);
	for (var i=0;i<x.length;i++){
		string+=tag_name;
		if (x.length>1){
			string+='[]';
		}
		string+='='+escape(x[i].value)+'&';
	}
	string+='ajax=1';
	return string;
}
function gen_form(elemente){
	var string='ajax=1';
	for (var i=0;i<elemente.length;i++){
		string+='&'+parse_form(elemente[i]);
	}
	return string;
}
var ajax_evalueaza;
function loadXMLDoc(url,elem,evalueaza) {
	ajax_evalueaza=evalueaza;
	elementid = elem;
	var loading_text= 'Se incarca ...';
	getObject(elementid).innerHTML=loading_text;
	// Internet Explorer
	try { req = new ActiveXObject("Msxml2.XMLHTTP"); }
	catch(e) {
		try { req = new ActiveXObject("Microsoft.XMLHTTP"); }
		catch(oc) { req = null; }
	}

	// Mozilla/Safari
	if (!req && typeof XMLHttpRequest != "undefined") { req = new XMLHttpRequest(); }

	// Call the processChange() function when the page has loaded
	if (req != null) {
		req.onreadystatechange = processChange;
		req.open("GET", url, true);
		req.send(null);
	}
}
function ajaxpost(url,params,elem){
	elementid = elem;
	var loading_text= 'Se incarca ...';
	getObject(elementid).innerHTML=loading_text;
	// Internet Explorer
	try { req = new ActiveXObject("Msxml2.XMLHTTP"); }
	catch(e) {
		try { req = new ActiveXObject("Microsoft.XMLHTTP"); }
		catch(oc) { req = null; }
	}

	// Mozilla/Safari
	if (!req && typeof XMLHttpRequest != "undefined") { req = new XMLHttpRequest(); }

	// Call the processChange() function when the page has loaded
	if (req != null) {


		req.onreadystatechange = processChange;
		req.open("POST", url, true);
		req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		req.setRequestHeader("Content-length", params.length);
		req.setRequestHeader("Connection", "close");

		req.send(params);

	}
}

function processChange() {
	// The page has loaded and the HTTP status code is 200 OK
	if (req.readyState == 4 && req.status == 200) {
		// Write the contents of this URL to the searchResult layer
		getObject(elementid).innerHTML = req.responseText;
		if (ajax_evalueaza){
			eval(ajax_evalueaza);
		}
		if (ajaxreload_timp>0){
			setTimeout('window.location.reload();',ajaxreload_timp);
			ajaxreload_timp=0;
		}
	}
	if (req.readyState == 4 && req.status != 200) {
		getObject(elementid).innerHTML = 'A aparut o eroare.Va rugam incercati inca o data.';
	}
}

function getObject(name) {
	var ns4 = (document.layers) ? true : false;
	var w3c = (document.getElementById) ? true : false;
	var ie4 = (document.all) ? true : false;

	if (ns4) return eval('document.' + name);
	if (w3c) return document.getElementById(name);
	if (ie4) return eval('document.all.' + name);
	return false;
}
