var poateAdaugaProdus = true;
function adaugaProdus(prodID,cale_imagine,id_cos) {
		if(!poateAdaugaProdus)return ;
		poateAdaugaProdus = false;
		new Ajax.Cursor(1);

		var divProdus = $('produs_'+prodID);
		var divCos 	= 	$(id_cos);
		//alert (divProdus.innerHTML);
		//alert (divCos.innerHTML);

		var PozProdus 	= Position.cumulativeOffset(divProdus);
		var PozCos = Position.cumulativeOffset(divCos);
		PozCos[0]+=(divCos.getDimensions().width/2+30);
		PozCos[1]+=(divCos.getDimensions().height/2+30);

		// Imaginea care se va misca
		var divMiscare = document.createElement("div");
		var img = document.createElement("img");
		img.src = cale_imagine;
		divMiscare.appendChild(img);
		divMiscare.style.top = PozProdus[0] + "px";
		divMiscare.style.left= PozProdus[1] + "px";
		divMiscare.style.position = "absolute";
		var rand = Math.random() * 1000 + 1;
		divMiscare.setAttribute("randId",rand);
		document.body.appendChild(divMiscare);
		// Sfarsit creare imagine

		Effect.Fade(divMiscare, {duration:0.5,from:1.0, to:0.0});
		miscaDivCos(divMiscare,PozProdus,PozCos,PozProdus,prodID);
}
function miscaDivCos(divMiscare,PozProdus,PozCos,PozDivInitial,prodID) {
	divMiscare.style.left 	= PozProdus[0] + "px";
	divMiscare.style.top 	= PozProdus[1] + "px";

	var misca_cu_oriz=Math.abs((PozDivInitial[0]-PozCos[0])/20);
	var misca_cu_vert=Math.abs((PozDivInitial[1]-PozCos[1])/20);

	var continua_miscarea=false;
	if(Math.abs(PozCos[0]-PozProdus[0])> misca_cu_oriz && misca_cu_oriz>3) {
	// Orizontala
			if(PozProdus[0] > PozCos[0]){
				PozProdus[0]= PozProdus[0]-misca_cu_oriz;
			}
			if(PozProdus[0] < PozCos[0]){
				PozProdus[0]= PozProdus[0]+misca_cu_oriz;
			}
			continua_miscarea=true;
	}

	if(Math.abs(PozCos[1]-PozProdus[1])> misca_cu_vert && misca_cu_vert>3) {
	// Verticala
			if(PozProdus[1] > PozCos[1]){
				PozProdus[1]= PozProdus[1]-misca_cu_vert;
			}
			if(PozProdus[1] < PozCos[1]){
				PozProdus[1]= PozProdus[1]+misca_cu_vert;
			}
			continua_miscarea=true;
	}
	if (continua_miscarea==true){
		setTimeout(function() { miscaDivCos(divMiscare,PozProdus,PozCos,PozDivInitial,prodID); },10);
	}
	else{
		poateAdaugaProdus = true;
		new Ajax.Updater('container_cos','http://www.ujmag.ro/cart/add_ajax/'+prodID+'/');
	}
}