{include file=box_cautare.tpl}
<h1>Am cautat <em>&quot;{$cautare->q}&quot;</em> {if $cautare->c->clc_id} in colectia <em>&quot;{$cautare->c->clc_nume}&quot;</em>{else} in toate colectiile{/if}</h1>
{foreach from=$publicatii item=carte}
{include file='carte.box.lung.tpl'}
{foreachelse}
Nu am gasit nici o publicatie.
{/foreach}