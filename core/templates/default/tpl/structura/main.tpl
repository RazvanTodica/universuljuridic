<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ro" lang="ro" dir="ltr">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta http-equiv="Content-Style-Type" content="text/css" />
		<meta name="generator" content="mag.filipnet v1.4" />
		<link rel="icon"  type="image/png"   href="{$CONF.sitepath}pub/img/favicon.png" />
		<link rel="shortcut icon" href="{$CONF.sitepath}pub/img/favicon.ico" type="image/vnd.microsoft.icon" />
		<link rel="icon" href="{$CONF.sitepath}pub/img/favicon.ico" type="image/vnd.microsoft.icon" />
		<meta name="title" content="{$META.title}" />
		<meta name="description" content="{$META.description}" />
		<meta name="keywords" content="{$META.keywords}" />
		<meta name="rating" content="general" />
		<meta name="distribution" content="Global" />
		<link rel="stylesheet" href="{$CONF.sitepath}pub/css/stil.css" type="text/css" />
		<link href="{$CONF.sitepath}pub/js/facebox/facebox.css" rel="stylesheet" type="text/css"/>
		<script type="text/javascript" src="{$CONF.sitepath}pub/js/jquery-1.3.2.min.js"></script>
		<script type="text/javascript" src="{$CONF.sitepath}pub/js/facebox/facebox.js"></script>
		<title>{$META.title}</title>
		{literal}
		<style type="text/css">

		.box .header {
			background-color: #{/literal}{$editura->culori->box_header_bg}{literal};
			border-bottom: 1px solid #{/literal}{$editura->culori->box_header_umbra}{literal};
		}
		.box .el {
			background-color: #{/literal}{$editura->culori->box_el}{literal};
			color:#666666;
		}
		.box a.el:hover {
			background-color: #{/literal}{$editura->culori->box_el_hover}{literal};
			color:#666666;
		}
		a {
			color: #666666;
		}
		strong,h1,h2 {
			color: #666666;
		}

		</style>
		{/literal}
		
		<style type="text/css">
		</style>
		<script type="text/javascript">
		{*
		{literal}
		function wmail(user,domeniu){
			var email=user+'@'+domeniu;
			var email_view=user+' [at] '+domeniu;
			document.write('<a href="mailto:'+email+'">'+email_view+'</a>');
		}
		{/literal}
		*}
		</script>
</head>
<body>
		<div class="div_min_width" style="line-height:1px;width:960px;">&nbsp;</div>
		<div style="width:960px;margin:0px auto;position:relative;">
		<div id="div_header">
			<a href="{$CONF.sitepath}" style="display:block;float:left;">
				<img src="{$CONF.sitepath}pub/img/logo.jpg" alt="" />
			</a>
			<div style="float:left">
			{*
	<a href="http://www.ujmag.ro/Marius-Ghenea/Antreprenoriat-drumul-de-la-idei-catre-oportunitati-si-succes-in-afaceri/" target="_blank">
		<img style="margin-left:2px;" src="http://ujmag.ro/s-img/uj-ghenea.gif" />
	</a>*}
	{$banner->id|inregistreaza_afisare_sistem_bannere}
	<a href="http://www.ujmag.ro/banner/?id={$banner->id}" target="_blank">
		<img style="margin-left:2px;" src="http://www.ujmag.ro/fisiere/sistem_bannere/{$banner->poza}" />
	</a>
			<!--
	<a href="http://www.ujmag.ro/Marius-Ghenea/Antreprenoriat-drumul-de-la-idei-catre-oportunitati-si-succes-in-afaceri/" target="_blank">
		<img style="margin-left:2px;" src="http://ujmag.ro/s-img/688x85-UJ-si-PRO-.gif" />
	</a>
			<object width="639" height="84"  type="application/x-shockwave-flash" data="http://www.universuljuridic.ro/img/bannere/7.10.swf">
				<param name="wmode" value="transparent" />
	<param value="http://www.universuljuridic.ro/img/bannere/7.10.swf" name="movie" />
	<p>You need Flash to see it.</p>
</object>
-->

		</div>{*
			<a href="http://www.ujmag.ro" style="display:block;float:right;" target="_blank">
				<img src="http://www.ujmag.ro/s-img/logo.jpg" alt="universul juridic magazin" />
			</a>*}	
<div class="meniu_sus">
	<a href="{$CONF.sitepath}" ><img src="{$CONF.sitepath}pub/img/meniu_sus/home.png" alt="home" /></a>
	<a href="{$CONF.sitepath}editura/" ><img src="{$CONF.sitepath}pub/img/meniu_sus/editura.png" alt="editura" /></a>
	<a href="{$CONF.sitepath}referenti/" ><img src="{$CONF.sitepath}pub/img/meniu_sus/butref.jpg" alt="referenti" /></a>
	<a href="{$CONF.sitepath}colectii/" ><img style="margin-top:2px;" src="{$CONF.sitepath}pub/img/meniu_sus/colectii.png" alt="colectii" /></a>
	{*<a href="{$CONF.sitepath}publicatii-periodice/" ><img src="{$CONF.sitepath}pub/img/meniu_sus/periodice.png" alt="periodice" /></a>*}
	<a href="{$CONF.sitepath}evenimente/" ><img src="{$CONF.sitepath}pub/img/meniu_sus/evenimente.png" alt="evenimente" /></a>
	<a href="{$CONF.sitepath}cariere/" ><img src="{$CONF.sitepath}pub/img/meniu_sus/cariere.png" alt="cariere" /></a>
	<a href="{$CONF.sitepath}contact/" ><img src="{$CONF.sitepath}pub/img/meniu_sus/contact.png" alt="contact" /></a>
</div>
		<div style="clear:both;"></div>
		</div>
		<div id="continut" class="g16">
			<div class="g3">
				{include file=structura/stanga.tpl}
			</div>
			<div class="g13">
			{if $cu_dreapta}
				<div class="g9 centru">
			{else}
				<div class="g13 centru">
			{/if}
                    xxx
				{$continut}
			</div>
			{if $cu_dreapta}
			{include file="structura/dreapta.tpl"}
			{/if}
			
			</div>
		</div>
		<div class="clear"></div>
	

<div id="footer">
 <a   href="{$CONF.sitepath}">Home</a> | 
 <a   href="{$CONF.sitepath}editura/">Editura</a> |
 <a   href="{$CONF.sitepath}evenimente/">Evenimente</a> | 
 <a   href="{$CONF.sitepath}cariere/">Cariere</a> | 
 <a   href="{$CONF.sitepath}contact/">Contact</a> | 
 <a href="{$CONF.sitepath}regulament/">Regulament</a> |
 <a href="http://www.anpc.gov.ro" target="_blank">ANPC</a>
 <a style="float:right;" href="http://www.filipnet.ro/" title="Platforma online" target="_blank" >platforma Filip Net</a>
</div>

</div>
{literal}
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-5423591-8']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
{/literal}
<div id="ajax_loader" style="position:absolute;display:none;">X</div>
</body>
</html>