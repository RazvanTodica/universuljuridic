{include file=box_cautare.tpl}
<h1>Prezentare / About us</h1>
<p style="text-indent:15px;">
Parte a Grupului editorial Universul Juridic, Editura PROUNIVERSITARIA a fost înfiinţată în anul 2004 din dorinţa de a acoperi o paletă mai largă de domenii de pe piaţa cursurilor universitare. Acreditată C.N.C.S.I.S., cu un număr de peste 1.500 de titluri publicate, PROUNIVERSITARIA vine în întâmpinarea nevoilor cadrelor universitare din majoritatea panelurilor: arte și științe umaniste, științe sociale, matematică-informatică, ştiinţe inginereşti.</p>
<p style="text-indent:15px;">
PROUNIVERSITARIA are o bogată experienţă în publicarea de carte universitară, in cei 8 ani de activitate devenind o editură cu prestigiu recunoscut în domeniul universitar, asigurând nevoile publicistice a mai multor instituţii de învăţământ superior, printre care Universitatea Creştină Dimitrie Cantemir, Universitatea Româno-Americană, Universitatea Titulescu, Universitatea din Craiova, Universitatea Ecologică, Universitatea Valahia din Târgovişte, Academia de Poliţie, Universitatea Danubius din Galaţi şi multe altele.</p>
<p style="text-indent:15px;">
Ceea ce oferă PROUNIVERSITARIA nu se limitează simplu la tipărirea de cursuri universitare, ci acoperă toate nevoile editoriale (şi nu numai) ale unei universităţi : suportul activităţii de cercetare prin editarea revistelor (un număr de peste 25 de titluri, coordonate de universităţi), note de curs, caiete de seminar, broşuri pentru licenţă, o gamă completă de materiale promoţionale ca suport pentru conferinţele organizate de universităţi, organizarea de evenimente. Suportul integrat oferit de PROUNIVERSITARIA permite universităţilor să îşi concentreze eforturile spre activitatea lor principală, şi anume cea scolastică - aducând un plus de profesionalism şi un bun renume către clienţii săi.</p>
<p style="text-indent:15px;">
Eforturile editurii noastre de a asigura servicii de excepţie sunt susţinute de existenţa unei tipografii proprii, cu utilaje performante, care permit realizarea unor lucrări de o calitate grafică ireproşabilă, cu o legare (broşare) impecabilă, la termene foarte scurte, integrand toate acestea cu servicii complete de tehnoredactare, tipărire şi distribuţie pentru orice tiraj sau format şi oriunde în ţară.</p>


<br>
<br>

***
<br>
<br>

<p style="text-indent:15px;">
As part of the Editorial Publishing Company Universul Juridic, the Publishing House PROUNIVERSITARIA was established in 2004 out of the desire to cover a wider range of areas of expertise in the university courses market. Benefiting from CNCSIS accreditation, with over 1500 titles published, PROUNIVERSITARIA is helping satisfying the necessities of the university lecturers in most of the domains: arts and humanist sciences, social sciences, mathematics-informatics, engineer sciences. </p>
<p style="text-indent:15px;">
PROUNIVERSITARIA has a large experience in publishing university books, in its 8 years of activity achieving the acknowledgement of the prestige in the university field, by ensuring the publishing needs of many higher educational institutions, amongst which: The Christian University (Universitatea Creştină) Dimitrie Cantemir, The Romanian – American University (Universitatea Româno-Americană), the University Nicolae Titulescu (Universitatea Titulescu), the University form Craiova (Universitatea din Craiova), the Ecological University (Universitatea Ecologică), the Valahia University from Târgovişte (Universitatea Valahia din Târgovişte), The Police Academy (Academia de Poliţie), The University Danubius from Galaţi (Universitatea Danubius din Galaţi) and many others.</p>
<p style="text-indent:15px;">
The offer of PROUNIVERSITARIA is not limited to the simple printing of university courses, but also covers all the editorial needs (and not solely) of a university: the support of the research activity throughout editing magazines (over 25 titles, coordinated by the universities), courses’ notes, seminar notebooks, brochures for the academic degree, a complete range of promotional materials as support for conferences organized by the universities, organization of events. The integrated support offered by PROUNIVERSITARIA allows the universities to focus their efforts towards their main activity, namely the scholastic one – bringing an added value of professionalism and good reputation for their clients.  </p>
<p style="text-indent:15px;">
The efforts of our publishing house to ensure exceptional services are seconded by the existence of an own printing house, with state of the art equipment that allows the accomplishment of works with a faultlessly graphic quality, with an impeccable binding, in very short deadlines, with the integration of all of this with complete services of editing, printing and distribution for any number of copies and anywhere in the country.  
</p>

<br />
<hr />
<br />
<a href="http://www.prouniversitaria.ro/pub/prouniversitaria_acreditare_completare_1.rar"><b>Pentru acreditarea editurii, partea 1</b></a>

{*
<p style="text-indent:15px;">Înfiinţată în anul 2001, Editura "Universul Juridic" a devenit în 7 ani un actor important al pieţei de carte juridică din România.</p>

<p style="text-indent:15px;">Poziţia pe care o ocupă în acest moment editura pe piaţa de carte juridica se datorează bazelor solide care au fost puse încă de la înfiinţare: respect şi seriozitate în relaţiile cu colaboratorii noştri şi dorinţa de a dezvolta în permanenţă editura.</p>

<p style="text-indent:15px;">Din anul 2001 şi până în prezent au fost editate un număr de peste 350 de titluri din domeniile juridic şi economic.</p>

<p style="text-indent:15px;">Majoritatea lucrărilor apărute în cadrul Editurii "Universul Juridic" sunt semnate de autori consacraţi dintre care îi amintim pe: Francisc Deak, Gheorghe Beleiu, Ion P. Filipescu, Stanciu D. Carpenaru, Octavian Capatana, Costică Bulai, Liviu Pop, Emil Molcuţ, Constantin Mitrache, Emilian Stancu, Avram Filipas, Alexandru Ticlea, Lucian Mihai, Mihaela Tăbârcă, Dan Lupaşcu, Romeo Popescu, Gheorghe Florea, Marian Nicolae, etc. şi se adresează tuturor categoriilor de cititori: studenţi, practicieni, teoreticieni cât şi nespecialiştilor care doresc să se familiarizeze cu elementele problematicii specifice ştiinţelor juridice sau, după caz, ale legislaţiei şi practicii judiciare actuale.</p>

<p style="text-indent:15px;">Printre titlurile noastre se regăsesc: tratate, cursuri universitare, monografii juridice, acte normative şi acte normative adnotate, culegeri de practică judiciară pe materii, structurate pe colecţii.</p>

<p style="text-indent:15px;">Prin intermediul reţelei proprii de distribuţie şi cu ajutorul colaboratorilor, cărţile noastre sunt distribuite în peste 200 de librării şi puncte de vânzare din întreaga ţară.</p>

<p style="text-indent:15px;">Din dorinţa de a transforma Editura "Universul Juridic" intr-o editură specializată în publicarea cu prioritate a titlurilor reprezentative din domeniul juridic (cercetare ştiinţifică, legislaţie şi jurisprudenţă) s-a înfiinţat în anul 2004 Editura "Pro Universitaria", editură specializată în principal pe publicarea de cursuri universitare din alte domenii de activitate.</p>

<p style="text-indent:15px;">Editura noastră deţine şi o tipografie proprie cu o capacitate de peste 20.000 de exemplare tipărite în exclusivitate sub sigla editurilor "Universul Juridic" sau "Pro Universitaria" (edituri acreditate de către Consiliul Naţional al Cercetării Ştiinţifice din învăţământul Superior).</p>

<p style="text-indent:15px;">Anul 2008 reprezintă un moment important al dezvoltării noastre, fapt ce a impus mărirea echipei editoriale, în scopul dezvoltării unor noi colecţii şi al consolidării celor deja existente.</p>

<p style="text-indent:15px;">De asemenea, dupa ce anul 2007 a marcat apariţia Revistei Române de Drept Privat, un eveniment editorial de importanţă majoră pe piaţa juridică în România, Editura Universul Juridic lanseaza in anul 2008 Revista romana de jurisprudenta.</p>
*}