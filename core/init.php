<?php

$editura=new stdClass();

switch($domeniu){

	default:

		{

			$editura->id=12;

			$editura->culori->box_header_bg='cccc9a';

			$editura->culori->box_header_umbra='999965';

			$editura->culori->box_el='ecebd7';

			$editura->culori->box_el_hover='cccc9a';

			$editura->nume='PRO Universitaria';



			foreach(db::obj_array("SELECT * FROM `COLECTII` WHERE `editura_id`='?' ORDER BY CLC_ORDER ASC",$editura->id) as $c){

				$copii[$c->CLC_ID_PARENT][]=$c;

			}

			$editura->colectii=flatten_tree($copii,0,'CLC_ID');



			break;

		}

}

template::assign('cu_dreapta',true);

function flatten_tree($array,$parinte_id,$key_id,$adancime=0){

	$copii=array ();

	foreach($array[$parinte_id] as $c){

		$c->adancime=$adancime;

		$copii[]=$c;

		if(isset($array[$c->$key_id])){

			$copii=array_merge($copii,flatten_tree($array,$c->$key_id,$key_id,$adancime+1));

		}

	}

	return $copii;

}

template::assign('editura',$editura);

function curata($str){

	$str=preg_replace("@<style.*?style>@is","",$str);

	$str=preg_replace("@class=\".*?\"@is","",$str);

	//$str=iconv('ISO-8859-2','utf-8',$str);
	return repara_xhtml($str);

}

function pagineaza($str){

	$str=explode('###',$str);

	$nstr=array ();

	$nr_pagini=count($str);

	foreach($str as $k=>$v){

		$n='';

		$nr_pagina=$k+1;

		if($k==0){

			$n.='<div class="pagina'.$nr_pagina.'">';

		}

		else{

			$n.='<div class="pagina'.$nr_pagina.'" style="display:none" >';

		}

		$n.=repara_xhtml($v);

		$n.='</div>';

		$nstr[]=$n;

	}

	$nstr=implode('',$nstr);

	if($nr_pagini>1){

		$nstr.='<a class="but_paginare" style="float:left;display:none;" rel="prev">pagina anterioară</a><a class="but_paginare" style="float:right;" rel="next">pagina următoare</a>';

	}

	return "<div class=\"paginare\">{$nstr}</div>";

}

?>
