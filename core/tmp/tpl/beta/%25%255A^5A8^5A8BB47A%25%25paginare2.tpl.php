<?php /* Smarty version 2.6.19, created on 2018-05-10 15:35:57
         compiled from paginare2.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'cat', 'paginare2.tpl', 4, false),)), $this); ?>
                  
<div <?php if ($this->_tpl_vars['link']['0'] == 'autori'): ?> class="pagination_autori" <?php else: ?> class="pagination" <?php endif; ?>>
<?php if ($this->_tpl_vars['carti_numar'] > 1): ?>
<?php $this->assign('pagina_uri', ((is_array($_tmp='&pagina=')) ? $this->_run_mod_handler('cat', true, $_tmp, $_GET['pagina']) : smarty_modifier_cat($_tmp, $_GET['pagina']))); ?>
<?php unset($this->_sections['i']);
$this->_sections['i']['name'] = 'i';
$this->_sections['i']['loop'] = is_array($_loop=$this->_tpl_vars['carti_numar']+1) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['i']['start'] = (int)1;
$this->_sections['i']['show'] = true;
$this->_sections['i']['max'] = $this->_sections['i']['loop'];
$this->_sections['i']['step'] = 1;
if ($this->_sections['i']['start'] < 0)
    $this->_sections['i']['start'] = max($this->_sections['i']['step'] > 0 ? 0 : -1, $this->_sections['i']['loop'] + $this->_sections['i']['start']);
else
    $this->_sections['i']['start'] = min($this->_sections['i']['start'], $this->_sections['i']['step'] > 0 ? $this->_sections['i']['loop'] : $this->_sections['i']['loop']-1);
if ($this->_sections['i']['show']) {
    $this->_sections['i']['total'] = min(ceil(($this->_sections['i']['step'] > 0 ? $this->_sections['i']['loop'] - $this->_sections['i']['start'] : $this->_sections['i']['start']+1)/abs($this->_sections['i']['step'])), $this->_sections['i']['max']);
    if ($this->_sections['i']['total'] == 0)
        $this->_sections['i']['show'] = false;
} else
    $this->_sections['i']['total'] = 0;
if ($this->_sections['i']['show']):

            for ($this->_sections['i']['index'] = $this->_sections['i']['start'], $this->_sections['i']['iteration'] = 1;
                 $this->_sections['i']['iteration'] <= $this->_sections['i']['total'];
                 $this->_sections['i']['index'] += $this->_sections['i']['step'], $this->_sections['i']['iteration']++):
$this->_sections['i']['rownum'] = $this->_sections['i']['iteration'];
$this->_sections['i']['index_prev'] = $this->_sections['i']['index'] - $this->_sections['i']['step'];
$this->_sections['i']['index_next'] = $this->_sections['i']['index'] + $this->_sections['i']['step'];
$this->_sections['i']['first']      = ($this->_sections['i']['iteration'] == 1);
$this->_sections['i']['last']       = ($this->_sections['i']['iteration'] == $this->_sections['i']['total']);
?>
<?php $this->assign('pagina_c', $this->_sections['i']['index']); ?>
<?php if ($this->_sections['i']['first']): ?>
<?php if ($this->_tpl_vars['pagina_curenta'] > 1): ?>
<span class="inactive_arrow">
 <a href="<?php if ($this->_tpl_vars['pagina_curenta']-1 == 0): ?><?php else: ?><?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
<?php if ($this->_tpl_vars['link']['0'] == 'colectie'): ?>colectie<?php elseif ($this->_tpl_vars['link']['0'] == 'reviste'): ?>reviste<?php elseif ($this->_tpl_vars['link']['0'] == 'autori'): ?>autori<?php elseif ($this->_tpl_vars['link']['0'] == 'evenimente'): ?>evenimente<?php else: ?>colectie<?php endif; ?><?php if ($this->_tpl_vars['link']['1'] != ''): ?>/<?php echo $this->_tpl_vars['link']['1']; ?>
<?php endif; ?>?<?php if ($this->_tpl_vars['new_query']): ?><?php echo $this->_tpl_vars['new_query']; ?>
&<?php endif; ?>pagina=<?php echo $this->_tpl_vars['pagina_curenta']-1; ?>
<?php if ($_GET['an'] || $this->_tpl_vars['link']['0'] == 'evenimente'): ?>&an=<?php echo $_GET['an']; ?>
<?php endif; ?><?php endif; ?><?php if ($_GET['noi'] == '1'): ?>&noi=1<?php if ($_GET['ordonare']): ?>&ordonare=<?php echo $_GET['ordonare']; ?>
<?php endif; ?><?php endif; ?>"><span class="pagination_arrow">&lsaquo;  Pagina anterioara</span></a>
</span>
<?php endif; ?>
<?php endif; ?>  

<?php if ($this->_tpl_vars['pagina_c'] == ( $this->_tpl_vars['pagina_curenta']-2 ) && $this->_tpl_vars['pagina_c'] != 1): ?>...<?php endif; ?>

<?php if ($this->_tpl_vars['pagina_c'] == 1 || $this->_tpl_vars['pagina_c'] == $this->_tpl_vars['carti_numar'] || ( $this->_tpl_vars['pagina_c'] > ( $this->_tpl_vars['pagina_curenta']-2 ) && $this->_tpl_vars['pagina_c'] < ( $this->_tpl_vars['pagina_curenta']+2 ) )): ?>
	<span class="<?php if ($_GET['pagina'] == $this->_sections['i']['index'] || ( ! $_GET['pagina'] && $this->_sections['i']['index'] == 1 )): ?>active<?php else: ?>inactive<?php endif; ?>">
	<a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
<?php if ($this->_tpl_vars['link']['0'] == 'cauta'): ?>cauta<?php else: ?><?php if ($this->_tpl_vars['link']['0'] == 'colectie'): ?>colectie<?php elseif ($this->_tpl_vars['link']['0'] == 'reviste'): ?>reviste<?php elseif ($this->_tpl_vars['link']['0'] == 'autori'): ?>autori<?php elseif ($this->_tpl_vars['link']['0'] == 'evenimente'): ?>evenimente<?php else: ?>colectie<?php endif; ?><?php endif; ?><?php if ($this->_tpl_vars['link']['1'] != ''): ?>/<?php echo $this->_tpl_vars['link']['1']; ?>
<?php endif; ?>?<?php if ($this->_tpl_vars['new_query']): ?><?php echo $this->_tpl_vars['new_query']; ?>
&<?php endif; ?>pagina=<?php echo $this->_tpl_vars['pagina_c']; ?>
<?php if ($_GET['an'] || $this->_tpl_vars['link']['0'] == 'evenimente'): ?>&an=<?php echo $_GET['an']; ?>
<?php endif; ?><?php if ($this->_tpl_vars['link']['0'] == 'cauta'): ?>&<?php echo $this->_tpl_vars['filters']; ?>
<?php endif; ?><?php if ($_GET['noi'] == '1'): ?>&noi=1<?php if ($_GET['ordonare']): ?>&ordonare=<?php echo $_GET['ordonare']; ?>
<?php endif; ?><?php endif; ?>"><?php echo $this->_tpl_vars['pagina_c']; ?>
</a>
</span>

<?php endif; ?>
<?php if ($this->_tpl_vars['pagina_c'] == ( $this->_tpl_vars['pagina_curenta']+2 ) && $this->_tpl_vars['pagina_c'] != $this->_tpl_vars['carti_numar']): ?>...<?php endif; ?>
<?php endfor; endif; ?>
<?php if ($this->_tpl_vars['numar_carti'] > 9): ?><?php endif; ?><?php if ($this->_tpl_vars['pagina_curenta']+1 <= $this->_tpl_vars['pagina_c']): ?>
<?php endif; ?>
<?php if ($this->_sections['i']['last']): ?><?php if ($this->_tpl_vars['pagina_curenta']+1 <= $this->_tpl_vars['carti_numar']): ?>
  <span class="inactive_arrow">
	<a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
<?php if ($this->_tpl_vars['link']['0'] == 'colectie'): ?>colectie<?php elseif ($this->_tpl_vars['link']['0'] == 'reviste'): ?>reviste<?php elseif ($this->_tpl_vars['link']['0'] == 'autori'): ?>autori<?php elseif ($this->_tpl_vars['link']['0'] == 'evenimente'): ?>evenimente<?php else: ?>colectie<?php endif; ?><?php if ($this->_tpl_vars['link']['1'] != ''): ?>/<?php echo $this->_tpl_vars['link']['1']; ?>
<?php endif; ?>?<?php if ($this->_tpl_vars['new_query']): ?><?php echo $this->_tpl_vars['new_query']; ?>
&<?php endif; ?>pagina=<?php echo $this->_tpl_vars['pagina_curenta']+1; ?>
<?php if ($_GET['an'] || $this->_tpl_vars['link']['0'] == 'evenimente'): ?>&an=<?php echo $_GET['an']; ?>
<?php endif; ?><?php if ($_GET['noi'] == '1'): ?>&noi=1<?php if ($_GET['ordonare']): ?>&ordonare=<?php echo $_GET['ordonare']; ?>
<?php endif; ?><?php endif; ?>"><span class="pagination_arrow">Pagina urmatoare &rsaquo;</span></a><?php endif; ?> <?php endif; ?>
	</span>
<?php endif; ?>




</div>
                  
             