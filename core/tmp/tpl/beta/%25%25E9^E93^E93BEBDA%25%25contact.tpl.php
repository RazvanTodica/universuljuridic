<?php /* Smarty version 2.6.19, created on 2018-05-10 21:57:39
         compiled from contact.tpl */ ?>
 <!--begin breadcrumb -->
 	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "structura/breadcrumb.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>       
    <!--end breadcrumb -->
    
    <!--begin content_wrapper -->
    <div id="content_wrapper">
       
        <!--begin content -->
        <div id="content">
            
            <h1 class="page_title">Contact</h1>
             
            <iframe class="map" width="1030" height="340" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=ro&amp;geocode=&amp;q=universul+juridic+Bd.+I.+Maniu,+nr.+7,+sector+6,+Bucure%C5%9Fti+060274,+Rom%C3%A2nia&amp;aq=&amp;sll=44.473025,29.015801&amp;sspn=0.066559,0.169086&amp;g=Bd.+I.+Maniu,+nr.+7,+sector+6,++Bucure%C8%99ti+060274,+Rom%C3%A2nia&amp;ie=UTF8&amp;t=m&amp;ll=44.472025,26.085968&amp;spn=0.079674,0.353279&amp;z=12&amp;iwloc=A&amp;output=embed"></iframe>
            
            <div class="contact_left">       
            	<h2 class="contact_adresa">Adresa</h2>
                <p>SC UNIVERSUL JURIDIC SRL<br />
                Bd. I. Maniu, nr. 7, Cotroceni Business Center, corp C, parter,<br />
                cod postal 060274, sector 6,<br />
                Bucuresti, Romania
                </p>
                <h2 class="contact_transport">Mijloace de transport</h2>
                <p>Metrou:   M3 Politehnica<br />
				   Autobuz:  Liniile 136, 236, 336,105, 139<br />
				   Tramvai:   Liniile 1, 11, 93<br />
				   Troleibuz: Liniile 61, 62, 71, 93<br />
                </p>
                <div class="contact_left_half">
                    <h2 class="contact_redactie">Departament Distributie</h2>
                    <p>Telefon: 021/314.93.15<br />
                       Mobil:&nbsp;&nbsp;&nbsp; 0733.674.222<br />
                       Fax:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 021/314.93.16<br />
                       E-mail:&nbsp; distributie [at] universuljuridic.ro
                    </p>
                </div>
                <div class="contact_left_half">
                    <h2 class="contact_no_img">Departament Redactie</h2>
                    <p>Telefon: 021/314.93.13<br />
                       Mobil:&nbsp;&nbsp;&nbsp; 0732.320.666<br />
                       Fax:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 021.314.93.16<br />
                       E-mail:&nbsp; redactie [at] universuljuridic.ro
                    </p>
                </div>
            </div>
               
            <div class="contact_right">
            	<h3>Scrie-ne</h3>
            	<div id="errors-box">
            		<?php if ($this->_tpl_vars['trimis'] == 1): ?>Mesajul a fost trimis.<?php endif; ?>
	            	<?php if ($this->_tpl_vars['errors']['nume'] == 1): ?><span>Campul nume trebuie completat.</span><br/><?php endif; ?>
		            <?php if ($this->_tpl_vars['errors']['mail'] == 1): ?><span>Campul email nu este completat/nevalid.</span><br/><?php endif; ?>
		            <?php if ($this->_tpl_vars['errors']['subiect'] == 1): ?><span>Campul subiect nu este completat.</span><br/><?php endif; ?>
		            <?php if ($this->_tpl_vars['errors']['total'] == 1): ?><span>Suma numerelor nu este corecta.</span><br/><?php endif; ?>
		            <?php if ($this->_tpl_vars['errors']['mesaj'] == 1): ?><span>Campul mesaj nu este completat.</span><br/><?php endif; ?>
	            </div>
                <form method="POST" action="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
contact" class="contact_form">
                    <div class="form_left">
                        <label class="contact_label" >Nume</label>
                        <label class="contact_label">E-mail</label>
                        <label class="contact_label">Subiect</label>
                        <label class="contact_label_textarea" name="mesaj" >Mesaj</label>   
                        <label class="contact_label" >18+2=</label>
                    </div>
                    <div class="form_right">
                    	<input type="hidden" name="send" value="1"/>
                        <input type="text" value="<?php if ($_POST['nume']): ?><?php echo $_POST['nume']; ?>
<?php endif; ?>"  name="nume" class="contact_input" />
                        <input type="text" value="<?php if ($_POST['email']): ?><?php echo $_POST['email']; ?>
<?php endif; ?>" name="email" class="contact_input" />
                        <input type="text" value="<?php if ($_POST['subiect']): ?><?php echo $_POST['subiect']; ?>
<?php endif; ?>"  name="subiect" class="contact_input" />
                        <textarea  rows="2" cols="20" class="contact_textarea" name="mesaj" ><?php if ($_POST['mesaj']): ?><?php echo $_POST['mesaj']; ?>
<?php endif; ?></textarea>
                        <input type="text" value="<?php if ($_POST['total']): ?><?php echo $_POST['total']; ?>
<?php endif; ?>" class="contact_input" name="total" />
                    </div>
                    <div class="clear"></div>  
                    <input type="submit" value="" class="contact_submit" />
                </form>
            </div>
            
        </div>
        <!--end content -->
        <div class="clear"></div>
        
    </div>
    <!--end content_wrapper -->