<?php /* Smarty version 2.6.19, created on 2019-09-02 18:40:38
         compiled from structura/main.tpl */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
           
<link rel="shortcut icon"
	href="http://www.editurauniversuljuridic.ro/templates/beta/pub/images/favicon_uj.ico"
	type="image/vnd.microsoft.icon" />
<link type="text/css" rel="stylesheet" href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
pub/css/style_new.css" />
<link type="text/css" rel="stylesheet" href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
pub/css/qtip.css" />
<script src="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
pub/js/jquery-1.8.3.min.js" type="text/javascript"></script>
<script src="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
pub/js/jquery.search-tooltip.js" type="text/javascript"></script>


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<script src="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
pub/js/jquery.menu.js" type="text/javascript"></script>
<script src="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
pub/js/jquery.qtip.min.js" type="text/javascript"></script>
<script src="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
pub/js/main.js" type="text/javascript"></script>
  
<!-- begin slider script -->

<link rel="stylesheet" href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
pub/css/animate.css"> <!-- Optional -->
<link rel="stylesheet" href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
pub/css/liquid-slider.css">

<script type="text/javascript"  src="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
pub/js/jquery.easing.1.3.js"></script>
<script type="text/javascript"  src="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
pub/js/jquery.touchSwipe.min.js"></script>
<script type="text/javascript"  src="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
pub/js/jquery.liquid-slider.min.js"></script>
<?php echo '
<script>
$(function(){
    $(function(){
		$(\'#liquid-slider\').liquidSlider({
			\'dynamicTabs\' : false,
			\'includeTitle\' : false,
			\'panelTitleSelector\' : false,
			\'autoSlide\' : true,
			\'minHeight\' : 250,
			\'hideSideArrows\': false,
			\'hoverArrows\' : false
		});
	});
});
</script>
'; ?>

<!-- end slider script -->
<title><?php echo $this->_tpl_vars['META']['title']; ?>
</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="title" content="<?php echo $this->_tpl_vars['META']['title']; ?>
" />
<meta name="description" content="<?php echo $this->_tpl_vars['META']['description']; ?>
" />
<meta name="robots" content="index, follow">
<meta name="author" content="" />

<meta name="verify-v1" content="upAxCM2xjpbJmukD3S3iH28y3R75gx102zjezXpOhzI=" />

</head>

<body>
<?php echo '
<script>
  (function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,\'script\',\'//www.google-analytics.com/analytics.js\',\'ga\');

  ga(\'create\', \'UA-8509274-13\', \'universuljuridic.ro\');
  ga(\'send\', \'pageview\');
</script>
'; ?>


<div id="fb-root"></div>
<?php echo '
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ro_RO/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, \'script\', \'facebook-jssdk\'));</script>
'; ?>

<?php echo '
<script type="text/javascript">



$(document).ready(function() {

	var tip = '; ?>
<?php echo $this->_tpl_vars['alert']; ?>
<?php echo ';
	if(tip) {
	
	 $.each(tip, function( key, value ) {
		
		
		if(value==1){
			alert(\'Adresa de email este invalida.\');
		}
		if(value==2){
			alert(\'Adresa de email exista deja.\');
		}
		if(value==3){
			alert(\'Multumim pentru abonarea la newsletter-ul Universul Juridic.\');
		}
		
		if(value==4){
			alert(\'Adresa de email a fost confirmata cu succes.\');
		}
	});
}
});

</script>
'; ?>

<div id="main">
	<div id="center-main">

    <!--begin header -->
    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "structura/header.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
    <!--end header -->

                
      <?php echo $this->_tpl_vars['continut']; ?>

            
      
      <?php if ($this->_tpl_vars['functie_curenta'] == 'acasa'): ?>
        <!--begin right -->
         <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "structura/dreapta.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
       <!--end right -->
       <?php endif; ?>
       
    <!--begin footer -->
    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "structura/footer.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
    <!--end footer -->
    </div>
</div>
</body>
</html>