<?php /* Smarty version 2.6.19, created on 2018-05-10 15:35:57
         compiled from eveniment.lista.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'sizeof', 'eveniment.lista.tpl', 52, false),array('modifier', 'truncate', 'eveniment.lista.tpl', 61, false),array('modifier', 'clear_font', 'eveniment.lista.tpl', 63, false),array('modifier', 'strip_tags', 'eveniment.lista.tpl', 63, false),)), $this); ?>
<?php echo '
<script type="text/javascript">
function aplica_filtre(a,b){
	var f = \'\'
	if(a == \'ordonare\'){
		window.location = \''; ?>
<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
evenimente<?php if ($this->_tpl_vars['link']['1'] != ''): ?>/<?php echo $this->_tpl_vars['link']['1']; ?>
<?php endif; ?>?ordonare='+b+'<?php if ($_GET['luna']): ?>&luna=<?php echo $_GET['luna']; ?>
<?php endif; ?><?php echo '\';
	}else if(a == \'an\'){
		window.location = \''; ?>
<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
evenimente<?php if ($this->_tpl_vars['link']['1'] != ''): ?>/<?php echo $this->_tpl_vars['link']['1']; ?>
<?php endif; ?>?an='+b+'<?php echo '\';
	}else if(a == \'carti\'){
		window.location = \''; ?>
<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
evenimente?<?php if ($_GET['an']): ?>an=<?php echo $_GET['an']; ?>
<?php endif; ?><?php if ($_GET['luna']): ?>&luna=<?php echo $_GET['luna']; ?>
<?php endif; ?>&carti='+b+'<?php echo '\';
	}
}
</script>
'; ?>


<!--begin breadcrumb -->
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "structura/breadcrumb.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
    <!--end breadcrumb -->
    
    <!--begin content_wrapper -->
    <div id="content_wrapper">
       
        <!--begin content -->
        <div id="content">
            
            <!--begin left -->
            <div id="left">
                
                <h1 class="page_title margin_bottom"><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
evenimente" title="Evenimente">Evenimente</a></h1>
                
            	<!-- begin pagination -->
                <div class="pagination_wrapper">
                	<div class="afiseaza_pagination">
                    	<span>Se afiseaza <span class="afiseaza_bold"><?php if ($this->_tpl_vars['start'] == 0): ?>1<?php else: ?><?php echo $this->_tpl_vars['start']; ?>
<?php endif; ?> - <?php echo $this->_tpl_vars['end']; ?>
</span> din <span class="afiseaza_bold"><?php echo $this->_tpl_vars['total']; ?>
</span> Evenimente</span>
                    </div>
                	<form method="post" action="" class="rezultate_form">
                        <select name="1" class="clasa_select" onchange="aplica_filtre('an',this.value)">
                        	<option value="0" <?php if ($_GET['an'] == 0): ?>selected<?php endif; ?>>Alege anul</option>
                        	<option value="2013" <?php if ($_GET['an'] == 2013): ?>selected<?php endif; ?>>2013</option> 
                        	<option value="2012" <?php if ($_GET['an'] == 2012): ?>selected<?php endif; ?>>2012</option> 
                        	<option value="2011" <?php if ($_GET['an'] == 2011): ?>selected<?php endif; ?>>2011</option> 
                        	<option value="2010" <?php if ($_GET['an'] == 2010): ?>selected<?php endif; ?>>2010</option> 
                        	<option value="2009" <?php if ($_GET['an'] == 2009): ?>selected<?php endif; ?>>2009</option>
                        	<option value="2008" <?php if ($_GET['an'] == 2008): ?>selected<?php endif; ?>>2008</option> 
                        	<option value="2007" <?php if ($_GET['an'] == 2007): ?>selected<?php endif; ?>>2007</option>
                            <option value="2006" <?php if ($_GET['an'] == 2006): ?>selected<?php endif; ?>>2006</option>
                        </select>
                    </form>
                </div>
            	<!-- end pagination -->
            	
            	<?php if (sizeof($this->_tpl_vars['evenimente']) == 0): ?>
            	<p>Nu exista evenimente.</p>
            	<?php else: ?>
            	<?php $this->assign('start', '1'); ?>
            	<?php $_from = $this->_tpl_vars['evenimente']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
            	<?php $this->assign('nr', $this->_tpl_vars['start']++); ?>
            	
                <div class="evenimente_box <?php if ($this->_tpl_vars['nr'] == $this->_tpl_vars['carti_pe_pagina']): ?>last<?php endif; ?>">
                    <a href="<?php if ($this->_tpl_vars['url_eveniment'] == 'evenimente/editoriale'): ?><?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
evenimente/editoriale/<?php echo $this->_tpl_vars['item']->link_nou; ?>
<?php else: ?><?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
evenimente/<?php echo $this->_tpl_vars['item']->link_nou; ?>
<?php endif; ?>" title="<?php echo $this->_tpl_vars['item']->ev_titlu; ?>
" class="left"><img src="<?php if ($this->_tpl_vars['link']['1'] == 'editoriale'): ?><?php echo $this->_tpl_vars['CONF']['sitepathuj']; ?>
fisiere/evenimente/mica_<?php echo $this->_tpl_vars['item']->ev_poza; ?>
<?php else: ?><?php echo $this->_tpl_vars['CONF']['sitepathuj']; ?>
resize_pic/194x128/<?php echo $this->_tpl_vars['item']->ev_poza; ?>
<?php endif; ?>"  alt="<?php echo $this->_tpl_vars['item']->ev_titlu; ?>
" class="small_border_img" /></a>
                    <h2><a href="<?php if ($this->_tpl_vars['url_eveniment'] == 'evenimente/editoriale'): ?><?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
evenimente/editoriale/<?php echo $this->_tpl_vars['item']->link_nou; ?>
<?php else: ?><?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
evenimente/<?php echo $this->_tpl_vars['item']->link_nou; ?>
<?php endif; ?>" title="<?php echo $this->_tpl_vars['item']->ev_titlu; ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['item']->ev_titlu)) ? $this->_run_mod_handler('truncate', true, $_tmp, 100, "...") : smarty_modifier_truncate($_tmp, 100, "...")); ?>
</a></h2>
                    <span class="date_accesari"><a href="#" title="#"><?php echo $this->_tpl_vars['item']->ev_data; ?>
</a> | <a href="#" title="#"><?php echo $this->_tpl_vars['item']->accesari; ?>
 accesari</a></span><br/><br/>
                    <a href="<?php if ($this->_tpl_vars['url_eveniment'] == 'evenimente/editoriale'): ?><?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
evenimente/editoriale/<?php echo $this->_tpl_vars['item']->link_nou; ?>
<?php else: ?><?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
evenimente/<?php echo $this->_tpl_vars['item']->link_nou; ?>
<?php endif; ?>" title="<?php echo $this->_tpl_vars['item']->ev_titlu; ?>
"><?php echo ((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['item']->ev_continut)) ? $this->_run_mod_handler('clear_font', true, $_tmp) : clear_font($_tmp)))) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)))) ? $this->_run_mod_handler('truncate', true, $_tmp, 200, "...") : smarty_modifier_truncate($_tmp, 200, "...")); ?>
</a>
                </div>
                <?php endforeach; endif; unset($_from); ?>
                <?php endif; ?>
                <!-- begin pagination -->
                <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "paginare2.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                
            </div>
            <!--end left -->
            
            <!--begin right -->
            <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "structura/dreapta.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
            <!--end right -->
        </div>
        <!--end content -->
        <div class="clear"></div>
        
    </div>
    <!--end content_wrapper -->