<?php /* Smarty version 2.6.19, created on 2019-09-02 18:40:38
         compiled from structura/footer.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date', 'structura/footer.tpl', 232, false),)), $this); ?>
     		</div>
        <!--end content -->
        <div class="clear"></div>
        
    </div>
    <!--end content_wrapper -->
    
    <!--begin footer -->
    <div id="footer_wrapper">
        <div id="footer_top">
        	<a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
" title="Univerul Juridic"><img src="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
pub/images/logo_footer.png" width="74" height="115" alt="Universul Juridic" class="footer_logo" /></a>
        	<ul class="footer_nav">
            	<li><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
colectie" rel="nofollow" title="Colectii" class="first">Colectii</a></li>
                <li><a href="#" rel="nofollow" title="Reviste de specialitate">Reviste de specialitate</a></li>
                <li><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
noutati-editoriale" rel="nofollow" title="Noutati editoriale">Noutati editoriale</a></li>
                <li><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
colectie/carti-in-curs-de-aparitie" rel="nofollow" title="Carti in curs de aparitie">Carti in curs de aparitie</a></li>
                <li><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
contact" rel="nofollow" title="Contacteaza-ne" class="last">Contacteaza-ne</a></li>
            </ul>
            <ul class="footer_media">
            	<li><a href="https://www.facebook.com/pages/Editura-Universul-Juridic/365178503685930?ref=hl" target="_blank" rel="nofollow" title="Facebook" class="facebook"></a></li>
                <li><a href="https://twitter.com/UniversulJuridi" target="_blank" rel="nofollow" title="Twitter" class="twitter"></a></li>
            	
                
                <!-- <li><a href="http://pinterest.com/ujmag/" data-pin-do="buttonFollow" target="_blank" rel="nofollow" title="Pinterest" class="pinterest"></a></li>-->
                <li ><a href="http://goo.gl/maps/IWw6j" target="_blank" rel="nofollow" title="Google Places" class="google_places"></a></li>
                <li class="last"><a href="http://www.youtube.com/user/UniversulJuridic" target="_blank" rel="nofollow" title="YouTube" class="youtube"></a></li>
                </ul>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     </table>
                </div>

            
            	            												            	            	            	            	            	            												            	
            	
            
        <div class="clear" style="height:30px"></div>
    </div>
            <div style="border-bottom:1px solid #999999;">
                <h3 style="font-size:15px">PARTENERI UNIVERSUL JURIDIC</h3>

                <div style="padding: 0px;margin: 0 auto; width:1020px; text-align:left;">
                    <a href="https://www.ujmag.ro/parteneri">
                        <img src="/magazin/lib/images/parteneri/12-09-2018_sigle-parteneri-uj-carti.jpg">
                    </a>
                </div>
                <div class="clear" style="height:30px"></div>
                <h3 style="font-size:15px">VA RECOMANDAM</h3>

                <div class="clear" style="height:30px"></div>
                <div>
                    <div>
                        <div style="float:left;width:200px;">
                            <a style="display:block;height:70px;line-height:70px;vertical-align:middle;text-align:center;"
                               href="http://www.avincis.ro/" title="Avincis" target="_blank" rel="nofollow">
                                <img src="/magazin/lib/images/Avincis-vin-alb-rosu-rose-domeniul-Vila-Dobrusa-dragasani_1.jpg"
                                     alt="museum" height="100"/>
                            </a>
                        </div>
                        <div style="float:left;width:200px;">
                            <a style="display:block;height:70px;line-height:70px;vertical-align:middle;text-align:center;"
                               href="http://www.evogps.ro/" title="Monitorizare GPS" target="_blank" rel="nofollow">
                                <img src="/magazin/lib/images/Evo-GPS-monitorizare-management-rapoarte-flota-auto_1.jpg" alt="EVOGPS"
                                     height="100"/>
                                
                            </a>
                        </div>
                        <div style="float:left;width:200px;">
                            <a style="display:block;height:70px;line-height:70px;vertical-align:middle;text-align:center;"
                               href="http://www.museumrestaurant.ro/" title="museumrestaurant.ro" target="_blank"
                               rel="nofollow">
                                <img src="/magazin/lib/images/Museum-Restaurant-Bucuresti-Eroilor-Strada-Doctor-Clunet_1.jpg"
                                     alt="museum" height="100"/>
                            </a>
                        </div>
                        <div style="float:left;width:200px;">
                            <a style="display:block;height:70px;line-height:70px;vertical-align:middle;text-align:center;"
                               href="http://casadetraduceri.ro/" title="casadetraduceri.ro" target="_blank"
                               rel="nofollow">
                                <img src="/magazin/lib/images/casadetraduceri.jpg"
                                     alt="casadetraduceri" height="100">
                            </a>
                        </div>
                        <div class="clear" style="height:69px"></div>
                    </div>
                    <div class="clear" style="height:30px"></div>
                </div>
                <div style="clear:both"></div>
            </div>
				
            	
            	
            </div>
        </div>
    	<div id="footer_bottom">
        	<ul class="copyright">
            	<li class="first">Copyright &copy; <?php echo ((is_array($_tmp='Y')) ? $this->_run_mod_handler('date', true, $_tmp) : date($_tmp)); ?>
 Editura Universul Juridic. Toate drepturile rezervate</li>
                <li><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
termeni-conditii" rel="nofollow" title="Termeni si Conditii">Termeni & Conditii</a></li>
            </ul>
    
<?php echo ' 

<!--/Start async trafic.ro/-->
<script type="text/javascript" id="trfc_trafic_script">
//<![CDATA[
t_rid = \'editurauniversuljuridic-ro\';
(function(){ t_js_dw_time=new Date().getTime();
t_js_load_src=((document.location.protocol == \'http:\')?\'http://storage.\':\'https://secure.\')+\'trafic.ro/js/trafic.js?tk=\'+(Math.pow(10,16)*Math.random())+\'&t_rid=\'+t_rid;
if (document.createElement && document.getElementsByTagName && document.insertBefore) {
t_as_js_en=true;var sn = document.createElement(\'script\');sn.type = \'text/javascript\';sn.async = true; sn.src = t_js_load_src;
var psn = document.getElementsByTagName(\'script\')[0];psn.parentNode.insertBefore(sn, psn); } else {
document.write(unescape(\'%3Cscri\' + \'pt type="text/javascript" \'+\'src="\'+t_js_load_src+\';"%3E%3C/sc\' + \'ript%3E\')); }})();
//]]>
</script>
<noscript><p><a href="http://www.trafic.ro/statistici/editurauniversuljuridic.ro"><img alt="editurauniversuljuridic.ro" src="http://log.trafic.ro/cgi-bin/pl.dll?rid=editurauniversuljuridic-ro" /></a> <a href="http://www.trafic.ro/">Web analytics</a></p></noscript>
<!--/End async trafic.ro/-->


'; ?>

	</div>
    </div>

</div>
</body>
</html>