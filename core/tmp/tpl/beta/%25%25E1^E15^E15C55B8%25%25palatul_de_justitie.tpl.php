<?php /* Smarty version 2.6.19, created on 2018-05-10 22:09:02
         compiled from palatul_de_justitie.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'truncate', 'palatul_de_justitie.tpl', 41, false),)), $this); ?>
<!--begin breadcrumb -->
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "structura/breadcrumb.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
    <!--end breadcrumb -->
    
    <!--begin content_wrapper -->
    <div id="content_wrapper">
       
        <!--begin content -->
        <div id="content">

        <!--begin left -->
            <div id="left">
                
                <h1 class="page_title margin_bottom">Palatul de Justitie</h1>
                
            	<!-- begin pagination -->
                <div class="pagination_wrapper">
                	<div class="afiseaza_pagination">
                    	<span>Se afiseaza <span class="afiseaza_bold"><?php if ($this->_tpl_vars['start'] == 0): ?>1<?php else: ?><?php echo $this->_tpl_vars['start']; ?>
<?php endif; ?> - <?php echo $this->_tpl_vars['end']; ?>
</span> din <span class="afiseaza_bold"><?php echo $this->_tpl_vars['total']; ?>
</span> Produse</span>
                    </div>
                	<form method="GET" name='form_sortare' action="" class="rezultate_form">
                        <select name="sortare" class="clasa_select" onchange="document.form_sortare.submit()">
                            <option value="" <?php if ($_GET['ordonare'] == 1): ?>selected<?php endif; ?>>Ordoneaza</option>
                            <option value="aparitie-crescator" <?php if ($_GET['ordonare'] == 2): ?>selected<?php endif; ?>>Data aparitiei crescator</option>
                            <option value="aparitie-descrescator" <?php if ($_GET['ordonare'] == 3): ?>selected<?php endif; ?>>Data aparitiei descrescator</option>
                        </select>
                    </form>
                </div>
            	<!-- end pagination -->
                
            	
            	<ul class="colectia_item_wrapper">
            	
            		<?php $this->assign('i', 0); ?>
            		<?php $_from = $this->_tpl_vars['carti']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['item']):
?>
            			<?php if (is_numeric ( $this->_tpl_vars['k'] )): ?>
            				<?php $this->assign('i', $this->_tpl_vars['i']+1); ?>
                   		 <li class="colectia_item<?php if (!($this->_tpl_vars['i'] % 4)): ?> last<?php endif; ?>">
	                        <a target="_blank" href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
reviste/palatul-de-justitie?id=<?php echo $this->_tpl_vars['item']->id; ?>
" title="<?php echo $this->_tpl_vars['item']->titlu; ?>
"><img src="<?php if ($this->_tpl_vars['item']->coperta == ''): ?><?php echo $this->_tpl_vars['CONF']['sitepathuj']; ?>
resize_pic/cache/110x155/1no_pic.jpg<?php else: ?><?php echo $this->_tpl_vars['CONF']['sitepathuj']; ?>
resize_pic/110x155/<?php echo $this->_tpl_vars['item']->coperta; ?>
<?php endif; ?>" width="110" height="155" alt="<?php echo $this->_tpl_vars['item']->titlu; ?>
" /></a>
	                        <a target="_blank" href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
reviste/palatul-de-justitie?id=<?php echo $this->_tpl_vars['item']->id; ?>
" title="<?php echo $this->_tpl_vars['item']->titlu; ?>
" class="colectia_item_description">
	                       		<?php echo ((is_array($_tmp=$this->_tpl_vars['item']->titlu)) ? $this->_run_mod_handler('truncate', true, $_tmp, '45', "") : smarty_modifier_truncate($_tmp, '45', "")); ?>
		                        
	                        </a>
		                 <?php endif; ?>
               		 <?php endforeach; endif; unset($_from); ?>
				    
				 </ul>
            	
                <!-- begin pagination -->
                <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "paginare2.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                
            </div>
            <!--end left -->
            
            <!--begin right -->
            <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "structura/dreapta.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
            <!--end right -->
        </div>
        <!--end content -->
        <div class="clear"></div>
        
    </div>
    <!--end content_wrapper -->
   