<?php /* Smarty version 2.6.19, created on 2019-09-10 12:47:44
         compiled from carte.detalii.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'dataRom', 'carte.detalii.tpl', 19, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => 'box_cautare.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
	<a style="float:right;" href="http://www.ujmag.ro/resize_pic/550x550/<?php echo $this->_tpl_vars['carte']->pbl_filename; ?>
"  rel="facebox" class="imagine"><img src="http://www.ujmag.ro/resize_pic/300x300/<?php echo $this->_tpl_vars['carte']->pbl_filename; ?>
" alt="<?php echo $this->_tpl_vars['carte']->pbl_titlu; ?>
" title="<?php echo $this->_tpl_vars['carte']->pbl_titlu; ?>
" /></a>

<h1><?php echo $this->_tpl_vars['carte']->pbl_titlu; ?>
</h1>
<div class="pret" style="float:right;text-align:center;">

<?php if ($this->_tpl_vars['carte']->pbl_pret > 0): ?><?php echo $this->_tpl_vars['carte']->pbl_pret; ?>
 lei <span class="extra_pret">(cu TVA)</span><?php endif; ?><br/>
<?php if ($this->_tpl_vars['carte']->no_sale != '1'): ?><a target="_blank" href="http://www.ujmag.ro/<?php echo $this->_tpl_vars['carte']->link; ?>
"><img src="http://www.universuljuridic.ro/buton_uj_2.jpg" alt="Comanda" /></a><?php endif; ?>

</div>
<br/><b>Cod</b>: <?php echo $this->_tpl_vars['carte']->cod_pbl; ?>

<?php if ($this->_tpl_vars['carte']->pbl_isbn): ?><br/><b><?php echo $this->_tpl_vars['carte']->pbl_isbn; ?>
</b><?php endif; ?>
<?php $_from = $this->_tpl_vars['carte']->autori; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['titlu_autor'] => $this->_tpl_vars['autor']):
?>
		<br/><b><?php echo $this->_tpl_vars['titlu_autor']; ?>
:</b>
		<?php unset($this->_sections['j']);
$this->_sections['j']['name'] = 'j';
$this->_sections['j']['loop'] = is_array($_loop=$this->_tpl_vars['autor']) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['j']['show'] = true;
$this->_sections['j']['max'] = $this->_sections['j']['loop'];
$this->_sections['j']['step'] = 1;
$this->_sections['j']['start'] = $this->_sections['j']['step'] > 0 ? 0 : $this->_sections['j']['loop']-1;
if ($this->_sections['j']['show']) {
    $this->_sections['j']['total'] = $this->_sections['j']['loop'];
    if ($this->_sections['j']['total'] == 0)
        $this->_sections['j']['show'] = false;
} else
    $this->_sections['j']['total'] = 0;
if ($this->_sections['j']['show']):

            for ($this->_sections['j']['index'] = $this->_sections['j']['start'], $this->_sections['j']['iteration'] = 1;
                 $this->_sections['j']['iteration'] <= $this->_sections['j']['total'];
                 $this->_sections['j']['index'] += $this->_sections['j']['step'], $this->_sections['j']['iteration']++):
$this->_sections['j']['rownum'] = $this->_sections['j']['iteration'];
$this->_sections['j']['index_prev'] = $this->_sections['j']['index'] - $this->_sections['j']['step'];
$this->_sections['j']['index_next'] = $this->_sections['j']['index'] + $this->_sections['j']['step'];
$this->_sections['j']['first']      = ($this->_sections['j']['iteration'] == 1);
$this->_sections['j']['last']       = ($this->_sections['j']['iteration'] == $this->_sections['j']['total']);
?>
			<a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
<?php echo $this->_tpl_vars['autor'][$this->_sections['j']['index']]->autor_seo; ?>
/"><?php echo $this->_tpl_vars['autor'][$this->_sections['j']['index']]->autor_nume; ?>
</a><?php if ($this->_tpl_vars['autor'][$this->_sections['j']['index_next']]->autor_nume): ?>,<?php endif; ?>
		<?php endfor; endif; ?>
<?php endforeach; endif; unset($_from); ?>
<br/><b>Data aparitiei:</b> <?php echo ((is_array($_tmp=$this->_tpl_vars['carte']->pbl_data_aparitie)) ? $this->_run_mod_handler('dataRom', true, $_tmp) : dataRom($_tmp)); ?>

<br/><b>Numar de pagini:</b> <?php echo $this->_tpl_vars['carte']->pbl_pagini; ?>

<br/><b>Format:</b> <?php echo $this->_tpl_vars['carte']->pbl_format; ?>

<?php if ($this->_tpl_vars['carte']->pbl_pret > 0): ?>
<br/><b>Pret:</b> <?php echo $this->_tpl_vars['carte']->pbl_pret; ?>
 RON
<?php endif; ?>
<br/>
<?php if ($this->_tpl_vars['carte']->pbl_descriere): ?>
<br/><b>Descriere:</b><br/>
<p style="text-indent:15px;">
<?php echo $this->_tpl_vars['carte']->pbl_descriere; ?>

</p>
<?php endif; ?>