<?php /* Smarty version 2.6.19, created on 2018-05-10 15:31:13
         compiled from eveniment.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', 'eveniment.tpl', 36, false),array('modifier', 'count', 'eveniment.tpl', 56, false),array('modifier', 'toAscii', 'eveniment.tpl', 71, false),)), $this); ?>
<?php echo '
<script type="text/javascript">
	$(function(){
		$(\'#myGallery\').galleryView({

    panel_width:650,
    panel_height:500
		});
		$(".poza_fancy").fancybox();
	});
</script>
'; ?>

			<!--begin left -->
            <div id="left">
            	<h1 class="page_title">
				<?php if ($this->_tpl_vars['url_eveniment'] == 'evenimente/interviuri'): ?>
                    	<?php echo $this->_tpl_vars['eveniment']->titlu; ?>

                 <?php else: ?>
                 		<?php echo $this->_tpl_vars['eveniment']->ev_titlu; ?>

                <?php endif; ?>
            	
            	
            	 </h1>
            	
                <div class="evenimente_single_box">
                    <a href="
						<?php if ($this->_tpl_vars['url_eveniment'] == 'evenimente/interviuri'): ?><?php echo $this->_tpl_vars['CONF']['sitepathuj']; ?>
fisiere/interviu/<?php echo $this->_tpl_vars['eveniment']->poza; ?>
 
						<?php else: ?><?php echo $this->_tpl_vars['CONF']['sitepathuj']; ?>
resize_pic/194x128/<?php echo $this->_tpl_vars['eveniment']->ev_poza; ?>
<?php endif; ?>
						<?php if ($this->_tpl_vars['url_eveniment'] == 'evenimente/editoriale'): ?><?php echo $this->_tpl_vars['CONF']['sitepathuj']; ?>
fisiere/evenimente/mica_<?php echo $this->_tpl_vars['eveniment']->poza; ?>
<?php endif; ?>" class="poza_fancy left" title="<?php echo $this->_tpl_vars['eveniment']->ev_titlu; ?>
">
                    	<img src="<?php if ($this->_tpl_vars['url_eveniment'] == 'evenimente/interviuri'): ?><?php echo $this->_tpl_vars['CONF']['sitepathuj']; ?>
fisiere/interviu/<?php echo $this->_tpl_vars['eveniment']->poza; ?>
<?php endif; ?>
						<?php if ($this->_tpl_vars['link']['0'] == 'evenimente' && $this->_tpl_vars['link']['1'] != 'editoriale' && $this->_tpl_vars['link']['1'] != 'interviuri'): ?><?php echo $this->_tpl_vars['CONF']['sitepathuj']; ?>
resize_pic/194x128/<?php echo $this->_tpl_vars['eveniment']->ev_poza; ?>
<?php endif; ?>  
						<?php if ($this->_tpl_vars['url_eveniment'] == 'evenimente/editoriale'): ?><?php echo $this->_tpl_vars['CONF']['sitepathuj']; ?>
fisiere/evenimente/mica_<?php echo $this->_tpl_vars['eveniment']->ev_poza; ?>
<?php endif; ?>"  alt="Picture" class="small_border_img" /></a>  
                   
                   
                    <?php if ($this->_tpl_vars['url_eveniment'] == 'evenimente/interviuri'): ?>
                      <span class="date_accesari"><?php echo ((is_array($_tmp=$this->_tpl_vars['eveniment']->data)) ? $this->_run_mod_handler('date_format', true, $_tmp, '%d %B %Y') : smarty_modifier_date_format($_tmp, '%d %B %Y')); ?>
</a> | <?php if ($this->_tpl_vars['eveniment']->accesari): ?><?php echo $this->_tpl_vars['eveniment']->accesari; ?>
<?php else: ?>0<?php endif; ?> accesari</span><br/><br/>
						<?php echo $this->_tpl_vars['eveniment']->text; ?>

						<ol >
						<?php $_from = $this->_tpl_vars['eveniment']->intrebari; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['raspuns']):
?>
							<li>
							<p>
								<b><?php echo $this->_tpl_vars['raspuns']->intrebare; ?>
</b><br>
								<?php echo $this->_tpl_vars['raspuns']->text; ?>

							</p><br>
							</li>
						<?php endforeach; endif; unset($_from); ?>
						</ol>
                    <?php else: ?>
                     <span class="date_accesari"><?php echo ((is_array($_tmp=$this->_tpl_vars['eveniment']->ev_data)) ? $this->_run_mod_handler('date_format', true, $_tmp, '%d %B %Y') : smarty_modifier_date_format($_tmp, '%d %B %Y')); ?>
</a> | <?php if ($this->_tpl_vars['eveniment']->accesari): ?><?php echo $this->_tpl_vars['eveniment']->accesari; ?>
<?php else: ?>0<?php endif; ?> accesari</span><br/><br/>
					<?php echo $this->_tpl_vars['eveniment']->ev_continut; ?>

					<?php endif; ?>
                </div>
                
               
                
                <?php if (count($this->_tpl_vars['eveniment']->poze) > 0): ?>
                <div class="evenimente_second_box">
                    <ul id="myGallery">
                    <?php $_from = $this->_tpl_vars['eveniment']->poze; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
                                            <li><img src="http://www.ujmag.ro/resize_pic/650x500/<?php echo $this->_tpl_vars['item']->imagine_poza; ?>
" alt="" /></li>
                	<?php endforeach; endif; unset($_from); ?>
                    </ul>
                </div>
                <?php endif; ?>
                <ul class="evenimente_list">
                	<li><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
evenimente" title="Vezi toate evenimentele">Vezi toate evenimentele &rsaquo;&rsaquo;</a></li>
                    <li>
<?php echo '
<div style="margin-top:45px">
<div id="fb-root"></div><script src="http://connect.facebook.net/en_US/all.js#xfbml=1"></script><fb:comments href="'; ?>
<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
lansari-de-carte/<?php echo ((is_array($_tmp=$this->_tpl_vars['eveniment']->ev_titlu)) ? $this->_run_mod_handler('toAscii', true, $_tmp) : toAscii($_tmp)); ?>
/<?php echo $this->_tpl_vars['eveniment']->ev_id; ?>
<?php echo '" num_posts="2" width="650"></fb:comments>
</div>
'; ?>

                    </li>
                </ul>
            </div>
            <!--end left -->
            
            <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "structura/dreapta.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>