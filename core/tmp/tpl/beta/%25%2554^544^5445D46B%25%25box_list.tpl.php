<?php /* Smarty version 2.6.19, created on 2018-05-10 15:45:13
         compiled from box_list.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'toAscii', 'box_list.tpl', 18, false),array('modifier', 'truncate', 'box_list.tpl', 33, false),)), $this); ?>
<ul class="colectia_item_wrapper">
            	
            		<?php $this->assign('i', 0); ?>
            			<?php $_from = $this->_tpl_vars['carti']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['item']):
?>
            			<?php if (is_numeric ( $this->_tpl_vars['k'] )): ?>
            				<?php $this->assign('i', $this->_tpl_vars['i']+1); ?>
                   		 <li class="colectia_item<?php if (!($this->_tpl_vars['i'] % 4)): ?> last<?php endif; ?>">
		                        <a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
<?php if ($this->_tpl_vars['link']['0'] == 'colectie'): ?>carte<?php else: ?>revista<?php endif; ?>/<?php echo $this->_tpl_vars['item']->PBL_SEO; ?>
" title="<?php echo $this->_tpl_vars['item']->PBL_TITLU; ?>
"><img src="<?php if ($this->_tpl_vars['item']->PBL_FILENAME == ''): ?><?php echo $this->_tpl_vars['CONF']['sitepathuj']; ?>
resize_pic/cache/110x155/1no_pic.jpg<?php else: ?><?php echo $this->_tpl_vars['CONF']['sitepathuj']; ?>
resize_pic/110x155/<?php echo $this->_tpl_vars['item']->PBL_FILENAME; ?>
<?php endif; ?>" width="110" height="155" alt="<?php echo $this->_tpl_vars['item']->PBL_TITLU; ?>
" /></a>
		                                                        <?php $this->assign('am_afisat', 0); ?>
                                <?php $_from = $this->_tpl_vars['item']->autori; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['autor']):
?>
                                    <?php if ($this->_tpl_vars['autor']->autor_default && ! $this->_tpl_vars['am_afisat']): ?>
                                        <a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
<?php if ($this->_tpl_vars['link']['0'] == 'colectie'): ?>carte<?php else: ?>revista<?php endif; ?>/<?php echo ((is_array($_tmp=$this->_tpl_vars['autor']->autor_nume)) ? $this->_run_mod_handler('toAscii', true, $_tmp) : toAscii($_tmp)); ?>
/<?php echo $this->_tpl_vars['autor']->autor_nume; ?>
" title="<?php echo $this->_tpl_vars['autor']->autor_nume; ?>
" class="colectia_item_title"><?php echo $this->_tpl_vars['autor']->autor_nume; ?>
</a>
                                        <?php $this->assign('am_afisat', 1); ?>
                                    <?php endif; ?>
                                <?php endforeach; endif; unset($_from); ?>
                                <?php $_from = $this->_tpl_vars['item']->autori; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['autor']):
?>
                                    <?php if ($this->_tpl_vars['autor']->editie == 'Autor' && ! $this->_tpl_vars['am_afisat']): ?>
                                        <a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
<?php if ($this->_tpl_vars['link']['0'] == 'colectie'): ?>carte<?php else: ?>revista<?php endif; ?>/<?php echo ((is_array($_tmp=$this->_tpl_vars['autor']->autor_nume)) ? $this->_run_mod_handler('toAscii', true, $_tmp) : toAscii($_tmp)); ?>
/<?php echo $this->_tpl_vars['autor']->autor_nume; ?>
" title="<?php echo $this->_tpl_vars['autor']->autor_nume; ?>
" class="colectia_item_title"><?php echo $this->_tpl_vars['autor']->autor_nume; ?>
</a>
                                        <?php $this->assign('am_afisat', 1); ?>
                                    <?php endif; ?>
                                <?php endforeach; endif; unset($_from); ?>
								<?php if ($this->_tpl_vars['item']->autori['0']->autor_id && ! $this->_tpl_vars['am_afisat']): ?>
								<a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
<?php if ($this->_tpl_vars['link']['0'] == 'colectie'): ?>carte<?php else: ?>revista<?php endif; ?>/<?php echo ((is_array($_tmp=$this->_tpl_vars['item']->autori['0']->autor_nume)) ? $this->_run_mod_handler('toAscii', true, $_tmp) : toAscii($_tmp)); ?>
/<?php echo $this->_tpl_vars['item']->autori['0']->autor_id; ?>
" title="<?php echo $this->_tpl_vars['item']->autori['0']->autor_nume; ?>
" class="colectia_item_title"><?php echo $this->_tpl_vars['item']->autori['0']->autor_nume; ?>
</a>
								<?php endif; ?>

		                        <a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
<?php if ($this->_tpl_vars['link']['0'] == 'colectie'): ?>carte<?php else: ?>revista<?php endif; ?>/<?php echo $this->_tpl_vars['item']->PBL_SEO; ?>
" title="<?php echo $this->_tpl_vars['item']->PBL_TITLU; ?>
" class="colectia_item_description">
		                       		<?php echo ((is_array($_tmp=$this->_tpl_vars['item']->PBL_TITLU)) ? $this->_run_mod_handler('truncate', true, $_tmp, '45', "") : smarty_modifier_truncate($_tmp, '45', "")); ?>
		                        
		                        </a>
                    	</li>
                    	<?php if ($this->_tpl_vars['i'] == 4): ?>      
    						<li class="separator_listing"></li>
                    	<?php endif; ?>
                    	<?php endif; ?>
                <?php endforeach; endif; unset($_from); ?>
               
    
 </ul>