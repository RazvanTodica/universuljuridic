<?php /* Smarty version 2.6.19, created on 2019-09-02 18:50:05
         compiled from acasa.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'clear_font', 'acasa.tpl', 64, false),array('modifier', 'strip_tags', 'acasa.tpl', 64, false),array('modifier', 'truncate', 'acasa.tpl', 64, false),array('modifier', 'resize_pic', 'acasa.tpl', 89, false),)), $this); ?>
<!--begin home_carti -->
	<div id='carti-container'>
		<div id="home_carti" style='border-right: 1px dotted #ddd;'>
	         <h2 class="page_title"><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
colectie/carti-in-curs-de-aparitie" title="Carti in curs de apartie">Carti in curs de aparitie <span>(vezi toate <?php echo $this->_tpl_vars['carti_apariti']['nr_total_aparitii']; ?>
)</span></a></h2>
	          <ul class="lista_carti_homepage">
	          <?php $_from = $this->_tpl_vars['carti_apariti']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['cart']):
?>   
	          <?php if (is_numeric ( $this->_tpl_vars['k'] )): ?>
	           <li <?php if ($this->_tpl_vars['k'] == 5): ?>class="last qPbl" <?php endif; ?> class="qPbl"><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
carte/<?php echo $this->_tpl_vars['cart']->PBL_SEO; ?>
" title="<?php echo $this->_tpl_vars['cart']->PBL_TITLU; ?>
"><img src="<?php echo $this->_tpl_vars['cart']->imagine; ?>
" width="110" height="155" alt="<?php echo $this->_tpl_vars['cart']->PBL_TITLU; ?>
" /></a>
	           
	           <span class="qtipData qtip_box">
	          	<a  class="title" href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
carte/<?php echo $this->_tpl_vars['cart']->PBL_SEO; ?>
"><?php echo $this->_tpl_vars['cart']->PBL_TITLU; ?>
</a>
	          	<div class="autor">
	          	
	          	<?php $_from = $this->_tpl_vars['cart']->autori; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['autor']):
?>
	          		<a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
<?php echo $this->_tpl_vars['autor']->link; ?>
" title="<?php echo $this->_tpl_vars['autor']->seo; ?>
"><?php echo $this->_tpl_vars['autor']->autor_nume; ?>
</a><br/>
	          	<?php endforeach; endif; unset($_from); ?>
	          	</div>
	          	<span class="pret"><?php echo $this->_tpl_vars['cart']->PBL_PRET; ?>
 Lei</span>
	          	<a class="add" href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
carte/<?php echo $this->_tpl_vars['cart']->PBL_SEO; ?>
">Detalii &raquo;</a>
	          </span>
						           
	           </li>
	           <?php endif; ?>
	          
	          <?php endforeach; endif; unset($_from); ?>
	           </ul>
	       </div>
	       <div id="home_carti" class='right'>
	         <h2 class="page_title"><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
colectie?ordonare=2&noi=1" title="Noutati editoriale">Noutati editoriale <span>(vezi toate <?php echo $this->_tpl_vars['numar_carti_noi']; ?>
)</span></a></h2>
	          <ul class="lista_carti_homepage">
	          <?php $_from = $this->_tpl_vars['carti_noi']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['cart']):
?>   
	          <?php if (is_numeric ( $this->_tpl_vars['k'] )): ?>
	           <li <?php if ($this->_tpl_vars['k'] == 2): ?>class="last qPbl" <?php endif; ?> class="qPbl"><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
carte/<?php echo $this->_tpl_vars['cart']->PBL_SEO; ?>
" title="<?php echo $this->_tpl_vars['cart']->PBL_TITLU; ?>
"><img src="<?php echo $this->_tpl_vars['cart']->imagine; ?>
" width="110" height="155" alt="<?php echo $this->_tpl_vars['cart']->PBL_TITLU; ?>
" /></a>
	           
	           <span class="qtipData qtip_box">
	          	<a  class="title" href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
carte/<?php echo $this->_tpl_vars['cart']->PBL_SEO; ?>
"><?php echo $this->_tpl_vars['cart']->PBL_TITLU; ?>
</a>
	          	<div class="autor">
	          	
	          	<?php $_from = $this->_tpl_vars['cart']->autori; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['autor']):
?>
	          		<a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
<?php echo $this->_tpl_vars['autor']->link; ?>
" title="<?php echo $this->_tpl_vars['autor']->seo; ?>
"><?php echo $this->_tpl_vars['autor']->autor_nume; ?>
</a><br/>
	          	<?php endforeach; endif; unset($_from); ?>
	          	</div>
	          	<span class="pret"><?php echo $this->_tpl_vars['cart']->PBL_PRET; ?>
 Lei</span>
	          	<a class="add" href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
carte/<?php echo $this->_tpl_vars['cart']->PBL_SEO; ?>
">Detalii &raquo;</a>
	          </span>
						           
	           </li>
	           <?php endif; ?>
	          
	          <?php endforeach; endif; unset($_from); ?>
	           </ul>
	       </div>
	  </div>
<!--end home_carti -->


 <!--begin left -->
            <div id="left">
            
            	<div class="left_box_editorial">
            	                    <a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
evenimente/editoriale/<?php echo $this->_tpl_vars['editorial']->link_corect; ?>
"  title="Picture" class="left"><img src="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
pub/images/paper-logo.png" alt="Picture" class="" /></a>
                    <div style='float:right; width: 565px;'>
                    	<a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
evenimente/editoriale/<?php echo $this->_tpl_vars['editorial']->link_corect; ?>
" class="editorial_homepage"><?php echo ((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['editorial']->ev_continut)) ? $this->_run_mod_handler('clear_font', true, $_tmp) : clear_font($_tmp)))) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)))) ? $this->_run_mod_handler('truncate', true, $_tmp, 300, "...") : smarty_modifier_truncate($_tmp, 300, "...")); ?>
</a>
                    	<p class="autor_editorial">Nicolae Cirstea<br /> <span>Presedinte Universul Juridic</span></p>
                    </div>
                </div>
                
                <div class="left_home_4col_wrapper">
                    <div class="left_home_4col">
                        <h2 class="page_title"><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
evenimente" title="Evenimente">Evenimente</a></h2>
                        <a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
evenimente/<?php echo $this->_tpl_vars['even']->link_nou; ?>
" title="<?php echo $this->_tpl_vars['even']->ev_titlu; ?>
" class="left"><img src="<?php echo $this->_tpl_vars['CONF']['sitepathuj']; ?>
resize_pic/90x85/<?php echo $this->_tpl_vars['even']->ev_poza; ?>
" width="90" height="85" alt="Picture" class="border_img" /></a>  
                        <a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
evenimente/<?php echo $this->_tpl_vars['even']->link_nou; ?>
" title="Detalii" class="read_more_home">Detalii <span>&gt;</span></a>
                        <h5><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
evenimente/<?php echo $this->_tpl_vars['even']->link_nou; ?>
"  title="<?php echo $this->_tpl_vars['even']->ev_titlu; ?>
"><?php echo ((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['even']->ev_titlu)) ? $this->_run_mod_handler('clear_font', true, $_tmp) : clear_font($_tmp)))) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)))) ? $this->_run_mod_handler('truncate', true, $_tmp, 60, "...") : smarty_modifier_truncate($_tmp, 60, "...")); ?>
</a></h5>
                        <p class="home_4col_details"><?php echo $this->_tpl_vars['even']->ev_data; ?>
</p>
                    </div>
                    <div class="left_home_4col last_box">
                  	  <h2 class="page_title"><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
evenimente/conferinte" title="Conferinte">Conferinte</a></h2>
                  	 <a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
evenimente/<?php echo $this->_tpl_vars['eveniment']->link_nou; ?>
" title="Detalii" class="read_more_home">Detalii <span>&gt;</span></a>
                        <a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
evenimente/<?php echo $this->_tpl_vars['eveniment']->link_nou; ?>
" title="<?php echo $this->_tpl_vars['eveniment']->ev_titlu; ?>
" class="left"><img src="<?php echo $this->_tpl_vars['CONF']['sitepathuj']; ?>
resize_pic/90x85/<?php echo $this->_tpl_vars['eveniment']->ev_poza; ?>
" width="90" height="85" alt="Picture" class="border_img" /></a>  
                        <h5><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
evenimente/<?php echo $this->_tpl_vars['eveniment']->link_nou; ?>
"  title="<?php echo $this->_tpl_vars['eveniment']->ev_titlu; ?>
"><?php echo ((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['eveniment']->ev_titlu)) ? $this->_run_mod_handler('clear_font', true, $_tmp) : clear_font($_tmp)))) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)))) ? $this->_run_mod_handler('truncate', true, $_tmp, 60, "...") : smarty_modifier_truncate($_tmp, 60, "...")); ?>
</a></h5>
                        <p class="home_4col_details"><?php echo $this->_tpl_vars['eveniment']->ev_data; ?>
</p>
                   
                    </div>
                    <div class="left_home_4col">
                        <h2 class="page_title"><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
reviste" title="Reviste">Reviste</a></h2>
                        <?php $_from = $this->_tpl_vars['ultima_revista']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
                        <a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
revista/<?php echo $this->_tpl_vars['item']->PBL_SEO; ?>
" title="Detalii" class="read_more_home">Detalii <span>&gt;</span></a>
                        <a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
revista/<?php echo $this->_tpl_vars['item']->PBL_SEO; ?>
" title="<?php echo $this->_tpl_vars['item']->PBL_TITLU; ?>
" class="left"><img src="<?php echo ((is_array($_tmp=$this->_tpl_vars['item']->PBL_FILENAME)) ? $this->_run_mod_handler('resize_pic', true, $_tmp, '90x85') : resize_pic($_tmp, '90x85')); ?>
" width="90" height="85" alt="<?php echo $this->_tpl_vars['item']->PBL_TITLU; ?>
" class="border_img" /></a>
                        <h5><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
revista/<?php echo $this->_tpl_vars['item']->PBL_SEO; ?>
" title="<?php echo $this->_tpl_vars['item']->PBL_TITLU; ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['item']->PBL_TITLU)) ? $this->_run_mod_handler('truncate', true, $_tmp, 45, "...") : smarty_modifier_truncate($_tmp, 45, "...")); ?>
</a></h5>
                        
                        <p class="home_4col_details">
                        <?php $_from = $this->_tpl_vars['item']->autori; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['autor']):
?>
                        <?php if ($this->_tpl_vars['autor']->titulatura): ?><?php echo $this->_tpl_vars['autor']->titulatura; ?>
<?php echo $this->_tpl_vars['autor']->autor_nume; ?>
<?php else: ?><?php echo $this->_tpl_vars['autor']->editie; ?>
 <?php echo $this->_tpl_vars['autor']->autor_nume; ?>
<?php endif; ?><br/>
                        <?php endforeach; endif; unset($_from); ?>   
                        </p>

                        <?php endforeach; endif; unset($_from); ?>
                    </div>
                    
                    
                    <div class="left_home_4col last_box">
                        <h2 class="page_title"><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
autori"  title="Autori">Autori </a></h2>
                       <a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
autor/<?php echo $this->_tpl_vars['autori']->autor_seo; ?>
"  title="Detalii" class="read_more_home">Detalii <span>&gt;</span></a>
                        <a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
autor/<?php echo $this->_tpl_vars['autori']->autor_seo; ?>
"  title="Picture" class="left"><img src="<?php echo $this->_tpl_vars['autori']->imagine; ?>
" width="90" height="85" alt="Picture" class="border_img" /></a>
                        <h5><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
autor/<?php echo $this->_tpl_vars['autori']->autor_seo; ?>
" title="<?php echo $this->_tpl_vars['autori']->autor_nume; ?>
"><?php echo $this->_tpl_vars['autori']->nume; ?>
 <?php echo $this->_tpl_vars['autori']->prenume; ?>
</a></h5>
                        <a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
autor/<?php echo $this->_tpl_vars['autori']->autor_seo; ?>
" target="_blank" title="#" class="home_4col_details">
                        <?php echo ((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['autori']->autor_descriere)) ? $this->_run_mod_handler('clear_font', true, $_tmp) : clear_font($_tmp)))) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)))) ? $this->_run_mod_handler('truncate', true, $_tmp, 100, "...") : smarty_modifier_truncate($_tmp, 100, "...")); ?>

                        </a>
                       
                    </div>
                </div>
				
                <?php if ($this->_tpl_vars['banner_bottom']): ?>
                	<a href="<?php echo $this->_tpl_vars['banner_bottom']->href; ?>
" title="" target="_blank" class="interviu_logo_homepage"><img src="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
pub/img_univers/<?php echo $this->_tpl_vars['banner_bottom']->imagine; ?>
"/></a>
                <?php endif; ?>
			
                   
            </div>
            <!--end left -->