<?php /* Smarty version 2.6.19, created on 2018-05-10 16:01:42
         compiled from reviste.tpl */ ?>
<?php echo '
<script type="text/javascript">
function aplica_filtre(a,b){
	var f = \'\'
	if(a == \'ordonare\'){
		window.location = \''; ?>
<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
reviste<?php if ($this->_tpl_vars['link']['1'] != ''): ?>/<?php echo $this->_tpl_vars['link']['1']; ?>
<?php endif; ?>?pagina=<?php if ($_GET['pagina']): ?><?php echo $_GET['pagina']; ?>
<?php else: ?>1<?php endif; ?>&ordonare='+b+'<?php if ($_GET['luna']): ?>&luna=<?php echo $_GET['luna']; ?>
<?php endif; ?><?php echo '\';
	}else if(a == \'luna\'){
		window.location = \''; ?>
<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
reviste?pagina=<?php if ($_GET['pagina']): ?><?php echo $_GET['pagina']; ?>
<?php else: ?>1<?php endif; ?><?php if ($_GET['an']): ?>&an=<?php echo $_GET['an']; ?>
<?php endif; ?>&luna='+b+'&carti=<?php if ($_GET['carti']): ?><?php echo $_GET['carti']; ?>
<?php else: ?>9<?php endif; ?><?php echo '\';
	}else if(a == \'carti\'){
		window.location = \''; ?>
<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
reviste?pagina=<?php if ($_GET['pagina']): ?><?php echo $_GET['pagina']; ?>
<?php else: ?>1<?php endif; ?><?php if ($_GET['an']): ?>&an=<?php echo $_GET['an']; ?>
<?php endif; ?><?php if ($_GET['luna']): ?>&luna=<?php echo $_GET['luna']; ?>
<?php endif; ?>&carti='+b+'<?php echo '\';
	}
}
</script>
'; ?>


<!--begin breadcrumb -->
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "structura/breadcrumb.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
    <!--end breadcrumb -->
    
    <!--begin content_wrapper -->
    <div id="content_wrapper">
       
        <!--begin content -->
        <div id="content">
            
            <!--begin left -->
            <div id="left">
                
                <h1 class="page_title margin_bottom"><a href="<?php if ($this->_tpl_vars['colectie']): ?><?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
colectie/<?php echo $this->_tpl_vars['colectie']->CLC_SEO; ?>
<?php else: ?><?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
colectie<?php endif; ?>" title="#"> <?php if ($this->_tpl_vars['revista']): ?> <?php echo $this->_tpl_vars['revista']->CAT_NUME; ?>
 <?php else: ?>Ultimele reviste aparute<?php endif; ?></a></h1>
                
            	<!-- begin pagination -->
                <div class="pagination_wrapper">
                	<div class="afiseaza_pagination">
                    	<span>Se afiseaza <span class="afiseaza_bold"><?php if ($this->_tpl_vars['start'] == 0): ?>1<?php else: ?><?php echo $this->_tpl_vars['start']; ?>
<?php endif; ?> - <?php echo $this->_tpl_vars['end']; ?>
</span> din <span class="afiseaza_bold"><?php echo $this->_tpl_vars['total']; ?>
</span> Produse</span>
                    </div>
                	<form method="post" action="" class="rezultate_form">
                        <select name="1" class="clasa_select" onchange="aplica_filtre('ordonare',this.value)">
                            <option value="1" <?php if ($_GET['ordonare'] == 1): ?>selected<?php endif; ?>>Ordoneaza</option>
                            <option value="2" <?php if ($_GET['ordonare'] == 2): ?>selected<?php endif; ?>>Cele mai noi</option>
                            <option value="4" <?php if ($_GET['ordonare'] == 4): ?>selected<?php endif; ?>>Pret descrescator</option>
                            <option value="3" <?php if ($_GET['ordonare'] == 3): ?>selected<?php endif; ?>>Pret crescator</option>
                        </select>
                    </form>
                </div>
            	<!-- end pagination -->
                
            	<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "box_list.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                <!-- begin pagination -->
                <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "paginare2.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
                
            </div>
            <!--end left -->
            
            <!--begin right -->
            <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "structura/dreapta.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
            <!--end right -->
        </div>
        <!--end content -->
        <div class="clear"></div>
        
    </div>
    <!--end content_wrapper -->
   