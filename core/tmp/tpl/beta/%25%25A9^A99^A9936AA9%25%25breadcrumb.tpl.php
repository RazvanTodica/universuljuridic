<?php /* Smarty version 2.6.19, created on 2019-09-02 18:41:10
         compiled from structura/breadcrumb.tpl */ ?>
 <div class="breadcrumb">
 <a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
" title="Acasa">Acasa</a>
 <?php $_from = $this->_tpl_vars['breadcrumb']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
 <?php if ($this->_tpl_vars['item']->link != ''): ?>
 <a href="<?php echo $this->_tpl_vars['item']->link; ?>
" title="<?php echo $this->_tpl_vars['item']->title; ?>
"><?php echo $this->_tpl_vars['item']->title; ?>
</a>
 <?php else: ?>
 <span class="current_crumb"><?php echo $this->_tpl_vars['item']->title; ?>
</span>
 <?php endif; ?>
 <?php endforeach; endif; unset($_from); ?>   

 </div>