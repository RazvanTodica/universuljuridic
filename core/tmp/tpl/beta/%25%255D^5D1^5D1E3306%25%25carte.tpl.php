<?php /* Smarty version 2.6.19, created on 2019-06-04 12:22:54
         compiled from carte.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'date_format', 'carte.tpl', 49, false),array('modifier', 'number_format', 'carte.tpl', 85, false),array('modifier', 'clear_font', 'carte.tpl', 158, false),array('modifier', 'strip_tags', 'carte.tpl', 158, false),array('modifier', 'truncate', 'carte.tpl', 158, false),array('modifier', 'toAscii', 'carte.tpl', 192, false),)), $this); ?>
<div id="trimite-popup" class='popup-box' <?php if ($this->_tpl_vars['message']): ?>style='display: block'<?php endif; ?>>
        <a href="#" class="close">X</a>
        <?php if ($this->_tpl_vars['message']): ?>
        	<h1><?php echo $this->_tpl_vars['message']; ?>
</h1>
        <?php else: ?>
        	<h2 class='title'>Trimite unui prieten</h2>
        <?php endif; ?>
        <form action="" method="POST">
        	<input type="hidden" name="trimite_prieten" value="true" />
            <p><label>Numele tau</label> <input name="nume" type="text" /></p>
            <p><label>Adresa ta de email</label> <input name="adresa_ta" type="text" /></p>
            <p><label>Adresa de email a prietenului</label> <input name="adresa_prieten" type="text" /></p>
            <p><input name="" type="submit" value="Trimite" /></p>
        </form>
</div>
</div>
<!--begin breadcrumb -->
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "structura/breadcrumb.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
    <!--end breadcrumb -->
    
    <!--begin content_wrapper -->
    <div id="content_wrapper">
       
        <!--begin content -->
        <div id="content">
     
            <!--begin product_wrapper -->
            <div class="product_wrapper">
            	<div class="product_left">
                	<a href="#" title="#"><img src="<?php echo $this->_tpl_vars['CONF']['sitepathuj']; ?>
resize_pic/266x375/<?php echo $this->_tpl_vars['carte']->PBL_FILENAME; ?>
" width="266" height="375" alt="<?php echo $this->_tpl_vars['carte']->PBL_FILENAME; ?>
" /></a>
                    <div class="detalii_carte">
                    	<ul class="rasfoieste_cuprins">

							<?php if ($this->_tpl_vars['carte']->cale_server): ?>
								<li ><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
<?php echo $this->_tpl_vars['link']['0']; ?>
/<?php echo $this->_tpl_vars['carte']->PBL_SEO; ?>
/cuprins/" title="Cuprins" type="iframe" data-width="780" data-height="550" class="modalIf orange nohover">Vezi cuprins</a></li>
															<?php endif; ?>
					
							<?php if ($this->_tpl_vars['carte']->pdf_rasfoire): ?>
								<li class="last"><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
<?php echo $this->_tpl_vars['link']['0']; ?>
/<?php echo $this->_tpl_vars['carte']->PBL_SEO; ?>
/rasfoire/" title="Rasfoire" type="iframe" data-width="780" data-height="550" class="modalIf orange nohover">Rasfoieste cartea</a></li>
															<?php endif; ?>
		
                        </ul>
                        <div class="clearBoth"></div>
                        <p><strong>Cod:</strong> <?php echo $this->_tpl_vars['carte']->PBL_COD; ?>
</p>
                        <p><strong>ISBN:</strong> <?php echo $this->_tpl_vars['carte']->PBL_ISBN; ?>
</p>
                        <p><strong>Editura:</strong> <?php echo $this->_tpl_vars['carte']->editura->editura_nume; ?>
</p>
                        <p><strong>Data aparitiei:</strong> <?php echo ((is_array($_tmp=$this->_tpl_vars['carte']->PBL_DATA_APARITIE)) ? $this->_run_mod_handler('date_format', true, $_tmp, "%Y") : smarty_modifier_date_format($_tmp, "%Y")); ?>
</p>
                        <p><strong>Colectia:</strong> <?php if ($this->_tpl_vars['colectie']): ?><?php echo $this->_tpl_vars['colectie']->CLC_NUME; ?>
<?php endif; ?></p>
                        <?php if ($this->_tpl_vars['carte']->PBL_PAGINI): ?><p><strong>Pagini:</strong> <?php echo $this->_tpl_vars['carte']->PBL_PAGINI; ?>
</p><?php endif; ?>
                        <p><strong>Disponibilitate:</strong> <span class="green"><?php if ($this->_tpl_vars['carte']->PBL_STOC): ?>in stoc<?php else: ?>rezervare<?php endif; ?></span></p>

                    </div>
                </div>
                <div class="product_right">
                	<h1 class="product_title no_margin"><a href="#" title="#"><?php echo $this->_tpl_vars['carte']->PBL_TITLU; ?>
</a></h1>
                    <p class="functie_revista" style="width: 100%">
                        <?php $_from = $this->_tpl_vars['carte']->autori; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['autori'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['autori']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['item']):
        $this->_foreach['autori']['iteration']++;
?>
                            <?php if ($this->_tpl_vars['item']->editie == 'Autor'): ?>
                                <?php if (! $this->_tpl_vars['autor']): ?>
                                    <?php $this->assign('autor', $this->_tpl_vars['item']); ?>
                                <?php endif; ?>
                                <span style='float: left; width: 100px;'><?php if ($this->_tpl_vars['item']->editie == ''): ?>Autor: <?php else: ?><?php echo $this->_tpl_vars['item']->editie; ?>
:</span><?php endif; ?><span style='float: none;' class="functie_name"><?php echo $this->_tpl_vars['item']->autor_nume; ?>
</span><br/>
                            <?php endif; ?>
                        <?php endforeach; endif; unset($_from); ?>
                        <?php $_from = $this->_tpl_vars['carte']->autori; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['autori'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['autori']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['item']):
        $this->_foreach['autori']['iteration']++;
?>
                            <?php if ($this->_tpl_vars['item']->editie != 'Autor'): ?>
                                <?php if (! $this->_tpl_vars['autor']): ?>
                                    <?php $this->assign('autor', $this->_tpl_vars['item']); ?>
                                <?php endif; ?>
                            <span style='float: left; width: 100px;'><?php if ($this->_tpl_vars['item']->editie == ''): ?>Autor: <?php else: ?><?php echo $this->_tpl_vars['item']->editie; ?>
:</span><?php endif; ?><span style='float: none;' class="functie_name"><?php echo $this->_tpl_vars['item']->autor_nume; ?>
</span><br/>
                            <?php endif; ?>
                        <?php endforeach; endif; unset($_from); ?>
                    </p>
                    <div class="product_share">
                    	<a href="#" title="#" class="trimite_button" onclick="arataPopupBox('#trimite-popup')" >Trimite unui prieten</a>
                        <div class="fb_share">
                        <div class="fb-like" data-href="http://www.facebook.com/pages/Editura-Universul-Juridic/106631099383053?fref=ts" data-send="true" data-width="450" data-show-faces="false"  style="float:left"></div>
							<div class="clear"></div>
                        </div>
                    </div>
                    <div class="clearBoth"></div>
                    <div class="product_cumpara">
                    	<p class="price"><?php echo ((is_array($_tmp=$this->_tpl_vars['carte']->PBL_PRET)) ? $this->_run_mod_handler('number_format', true, $_tmp, 2) : number_format($_tmp, 2)); ?>
 <span>lei</span></p>
                    	<?php if ($this->_tpl_vars['in_curs']): ?>
                    		<a href="<?php echo $this->_tpl_vars['CONF']['sitepathuj']; ?>
<?php echo $this->_tpl_vars['carte']->link; ?>
" title="<?php echo $this->_tpl_vars['carte']->PBL_TITLU; ?>
" class="cumpara_button">precomanda din ujmag.ro</a>
                    	<?php elseif ($this->_tpl_vars['carte']->PBL_STOC < 1): ?>
                    		<a href="<?php echo $this->_tpl_vars['CONF']['sitepathuj']; ?>
<?php echo $this->_tpl_vars['carte']->link; ?>
" title="<?php echo $this->_tpl_vars['carte']->PBL_TITLU; ?>
" class="cumpara_button">rezerva din ujmag.ro</a>
                    	<?php else: ?>
                    		<a href="<?php echo $this->_tpl_vars['CONF']['sitepathuj']; ?>
<?php echo $this->_tpl_vars['carte']->link; ?>
" title="<?php echo $this->_tpl_vars['carte']->PBL_TITLU; ?>
" class="cumpara_button">cumpara din ujmag.ro</a>
                    	<?php endif; ?>
                    </div>
                    <div class="clearBoth"></div>
                    <p><?php echo $this->_tpl_vars['carte']->PBL_DESCRIERE; ?>
</p>
                </div>
            </div>
            <!--end product_wrapper -->
            
            <?php if ($this->_tpl_vars['abonamente']): ?>
            <div class="abonamente_anuare_wrapper">             
                <div class="product_revista_one_half">
                    <h2 class="page_title">Abonamente</h2>
                    
                    <a href="#" title="Picture" class="left"><img src="<?php echo $this->_tpl_vars['CONF']['sitepathuj']; ?>
resize_pic/110x110/<?php echo $this->_tpl_vars['abonamente']['0']->PBL_FILENAME; ?>
" width="110" height="110" alt="Picture" /></a>
                    <h4><a href="#" title="<?php echo $this->_tpl_vars['abonamente']['0']->PBL_TITLU; ?>
"><?php echo $this->_tpl_vars['abonamente']['0']->PBL_TITLU; ?>
</a></h4>
                    <div class="pret_abonare">
                    	<a href="#" title="<?php echo $this->_tpl_vars['abonamente']['0']->PBL_TITLU; ?>
" class="abonamente_pret"><?php echo ((is_array($_tmp=$this->_tpl_vars['abonamente']['0']->PBL_PRET)) ? $this->_run_mod_handler('number_format', true, $_tmp, 2, ".", ",") : number_format($_tmp, 2, ".", ",")); ?>
 <span>lei</span></a>
                        <a href="#" title="<?php echo $this->_tpl_vars['abonamente']['0']->PBL_TITLU; ?>
" class="abonamente_abonare">MA ABONEZ</a>
                    </div>
                </div>
             <?php endif; ?>
                <div class="product_revista_one_half last">
                                    
                </div>
            </div>
            
            <?php if ($this->_tpl_vars['link']['0'] == 'revista'): ?>
	            <div class="product_page_wrapper">
	            	<h2 class="page_title"><?php echo $this->_tpl_vars['carte']->PBL_TITLU; ?>
 - Conducere</h2>
	            	
	            	<?php $_from = $this->_tpl_vars['carte']->autori; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['autori'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['autori']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['item']):
        $this->_foreach['autori']['iteration']++;
?>
	                 
	                <div class="product_revista_one_half <?php if ($this->_tpl_vars['k']%2 == 0): ?>last<?php endif; ?>">
	                	
	                    	<a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
autor/<?php echo $this->_tpl_vars['item']->autor_seo; ?>
" title="<?php echo $this->_tpl_vars['item']->autor_nume; ?>
" class="left"><img src="<?php if ($this->_tpl_vars['item']->autor_poza == ''): ?><?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
pub/images/no_image.png<?php else: ?><?php echo $this->_tpl_vars['CONF']['sitepathuj']; ?>
resize_pic/130x181/<?php echo $this->_tpl_vars['item']->autor_poza; ?>
<?php endif; ?>" width="130" height="181" alt="<?php echo $this->_tpl_vars['item']->autor_nume; ?>
" class="border_img" /></a>
	                     
	                    <h3><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
autor/<?php echo $this->_tpl_vars['item']->autor_seo; ?>
" title="<?php echo $this->_tpl_vars['item']->autor_nume; ?>
"><?php echo $this->_tpl_vars['item']->autor_nume; ?>
<br /><span class="functie"> <?php if ($this->_tpl_vars['item']->editie == ''): ?>Autor<?php else: ?><?php echo $this->_tpl_vars['item']->editie; ?>
 <?php endif; ?></span></a></h3>
	                    <p><?php echo ((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['item']->autor_descriere)) ? $this->_run_mod_handler('clear_font', true, $_tmp) : clear_font($_tmp)))) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)))) ? $this->_run_mod_handler('truncate', true, $_tmp, 300, "...") : smarty_modifier_truncate($_tmp, 300, "...")); ?>
</p>
	                    <a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
autor/<?php echo $this->_tpl_vars['item']->autor_seo; ?>
" title="Detalii" class="read_more">Detalii <span>&gt;</span></a>
	                </div>
	               
	                <?php endforeach; endif; unset($_from); ?>
	            	</div>
            <?php else: ?>

            <?php if (((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['autor']->autor_descriere)) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)))) ? $this->_run_mod_handler('clear_font', true, $_tmp) : clear_font($_tmp))): ?>
	        		 <div class="product_page_wrapper">
	        		 
	            	<h2 class="page_title">Despre autor</h2>
	                <a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
autor/<?php echo $this->_tpl_vars['autor']->autor_seo; ?>
" title="Picture" class="left"><img src="<?php if ($this->_tpl_vars['autor']->autor_poza == ''): ?><?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
pub/images/no_image.png<?php else: ?><?php echo $this->_tpl_vars['CONF']['sitepathuj']; ?>
resize_pic/130x181/<?php echo $this->_tpl_vars['autor']->autor_poza; ?>
<?php endif; ?>" width="130" height="181" alt="Picture" class="border_img" /></a>
	                <h3><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
autor/$carte->autori.0->autor_seo}" title="<?php echo $this->_tpl_vars['autor']->autor_nume; ?>
"><?php echo $this->_tpl_vars['autor']->autor_nume; ?>
</a></h3>
	                <p><?php echo ((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['autor']->autor_descriere)) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)))) ? $this->_run_mod_handler('clear_font', true, $_tmp) : clear_font($_tmp)))) ? $this->_run_mod_handler('truncate', true, $_tmp, 200, "...") : smarty_modifier_truncate($_tmp, 200, "...")); ?>
</p>
	                <a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
autor/<?php echo $this->_tpl_vars['autor']->autor_seo; ?>
" title="Detalii" class="read_more">Detalii <span>&gt;</span></a>
	           	 </div>
	          <?php endif; ?>
            
            <?php endif; ?>
            
            
            
            
            <h2 class="page_title left_margin"><?php if ($this->_tpl_vars['link']['0'] == 'revista'): ?>Alte numere ale revistei<?php else: ?>Carti din aceeasi colectie<?php endif; ?></h2>
            <ul class="product_item_wrapper">
            <?php $_from = $this->_tpl_vars['din_categorie']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['din_categorie'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['din_categorie']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['item']):
        $this->_foreach['din_categorie']['iteration']++;
?>
            	 <li class="product_item<?php if (($this->_foreach['din_categorie']['iteration'] == $this->_foreach['din_categorie']['total'])): ?> last<?php endif; ?>">
                    <a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
<?php echo $this->_tpl_vars['link']['0']; ?>
/<?php echo $this->_tpl_vars['item']->PBL_SEO; ?>
" title="<?php echo $this->_tpl_vars['item']->PBL_TITLU; ?>
"><img src="<?php echo $this->_tpl_vars['CONF']['sitepathuj']; ?>
resize_pic/110x155/<?php echo $this->_tpl_vars['item']->PBL_FILENAME; ?>
" width="110" height="155" alt="<?php echo $this->_tpl_vars['item']->PBL_TITLU; ?>
" /></a>

                                         <?php $this->assign('am_afisat', 0); ?>
                     <?php $_from = $this->_tpl_vars['item']->autori; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['autor']):
?>
                         <?php if ($this->_tpl_vars['autor']->autor_default && ! $this->_tpl_vars['am_afisat']): ?>
                             <a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
<?php if ($this->_tpl_vars['link']['0'] == 'colectie'): ?>carte<?php else: ?>revista<?php endif; ?>/<?php echo ((is_array($_tmp=$this->_tpl_vars['autor']->autor_nume)) ? $this->_run_mod_handler('toAscii', true, $_tmp) : toAscii($_tmp)); ?>
/<?php echo $this->_tpl_vars['autor']->autor_nume; ?>
" title="<?php echo $this->_tpl_vars['autor']->autor_nume; ?>
" class="colectia_item_title"><?php echo $this->_tpl_vars['autor']->autor_nume; ?>
</a>
                             <?php $this->assign('am_afisat', 1); ?>
                         <?php endif; ?>
                     <?php endforeach; endif; unset($_from); ?>
                     <?php $_from = $this->_tpl_vars['item']->autori; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['autor']):
?>
                         <?php if ($this->_tpl_vars['autor']->editie == 'Autor' && ! $this->_tpl_vars['am_afisat']): ?>
                             <a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
<?php if ($this->_tpl_vars['link']['0'] == 'colectie'): ?>carte<?php else: ?>revista<?php endif; ?>/<?php echo ((is_array($_tmp=$this->_tpl_vars['autor']->autor_nume)) ? $this->_run_mod_handler('toAscii', true, $_tmp) : toAscii($_tmp)); ?>
/<?php echo $this->_tpl_vars['autor']->autor_nume; ?>
" title="<?php echo $this->_tpl_vars['autor']->autor_nume; ?>
" class="colectia_item_title"><?php echo $this->_tpl_vars['autor']->autor_nume; ?>
</a>
                             <?php $this->assign('am_afisat', 1); ?>
                         <?php endif; ?>
                     <?php endforeach; endif; unset($_from); ?>
                     <?php if ($this->_tpl_vars['item']->autori['0']->autor_id && ! $this->_tpl_vars['am_afisat']): ?>
                         <a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
autor/<?php echo $this->_tpl_vars['item']->autori['0']->autor_seo; ?>
" title="<?php echo $this->_tpl_vars['item']->autori['0']->autor_nume; ?>
" class="colectia_item_title"><?php echo $this->_tpl_vars['item']->autori['0']->autor_nume; ?>
</a>
                     <?php endif; ?>
                    <a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
<?php echo $this->_tpl_vars['link']['0']; ?>
/<?php echo $this->_tpl_vars['item']->PBL_SEO; ?>
" title="<?php echo $this->_tpl_vars['item']->PBL_TITLU; ?>
" class="colectia_item_description"><?php echo ((is_array($_tmp=$this->_tpl_vars['item']->PBL_TITLU)) ? $this->_run_mod_handler('truncate', true, $_tmp, 50) : smarty_modifier_truncate($_tmp, 50)); ?>
</a>
                </li>
            <?php endforeach; endif; unset($_from); ?>
            </ul> 
        </div>
        <!--end content -->
        <div class="clear"></div>
        
    </div>
    <!--end content_wrapper -->
