<?php /* Smarty version 2.6.19, created on 2018-05-10 15:45:26
         compiled from autor.tpl */ ?>
<!--begin breadcrumb -->
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "structura/breadcrumb.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
    <!--end breadcrumb -->
    
    <!--begin content_wrapper -->
    <div id="content_wrapper">
       
        <!--begin content -->
        <div id="content">
            
            <!--begin left -->
            <div id="left">
            	
                <h1 class="page_title"><?php echo $this->_tpl_vars['autor']->autor_nume; ?>
</h1>
            
            	<div class="detail_page_box">
            	   <?php if ($this->_tpl_vars['autor']->autor_poza != ''): ?>
                    <a href="#" title="Picture" class="left"><img src="<?php echo $this->_tpl_vars['autor']->poza; ?>
" width="130" height="181" alt="Picture" class="small_border_img" /></a>
                    <?php endif; ?>
                    <p>
                    <?php if ($this->_tpl_vars['autor']->autor_descriere != ''): ?>
                    <?php echo $this->_tpl_vars['autor']->autor_descriere; ?>

                    <?php else: ?>
               			<center>Acest autor nu are descriere momentan.</center>
                    <?php endif; ?>
                    </p>
                </div>
                <div class="clear"></div>
                
                <h1 class="page_title">Carti ale acestui autor</h1>
                 <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "box_list.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
            <!-- begin pagination -->
            <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "paginare2.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
            <!--end left -->
                
            </div>
            
           
            
            <!--begin right -->
            <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "structura/dreapta.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
            <!--end right -->
            
        </div>
        <!--end content -->
        <div class="clear"></div>
        
    </div>
    <!--end content_wrapper -->