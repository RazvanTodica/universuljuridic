<?php /* Smarty version 2.6.19, created on 2019-09-10 11:58:49
         compiled from structura/header.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'truncate', 'structura/header.tpl', 61, false),array('modifier', 'count', 'structura/header.tpl', 122, false),)), $this); ?>
 <!--begin header -->
    <div id="header_wrapper">
        <div id="header" class="clearfix">
 
            <!--begin logo -->
            <div id="logo">
                <h1><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
" title="UNIVERSUL JURIDIC">UNIVERSUL JURIDIC</a></h1>
            </div>
            <!--end logo -->
            
            <!--begin select_category -->
            <div class="tni_menu">
            	<div class="tn1">Grupul editorial Universul Juridic</div>
				<ul>
					<li><a href="http://www.universuljuridic.ro/" title="Universul Juridic" target="_blank" rel="nofollow">Universul Juridic</a></li>
					<li><a href="http://www.prouniversitaria.ro/" title="Editura Pro Universitaria" target="_blank" rel="nofollow">Editura Pro Universitaria</a></li>
					<li><a href="http://www.ujmag.ro/cautare/?editura=editura+neverland" title="Editura Neverland" target="_blank" rel="nofollow">Editura Neverland</a></li>
					<li><a href="http://www.ujmag.ro/" title="UJmag.ro" target="_blank" rel="nofollow">UJmag.ro</a></li>
				</ul>
            </div>
            <!--end select_category -->
                        
            <!--begin search_wrapper -->
            <div class="search_wrapper">
                <div>
                    <img src="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
pub/images/search_header.png" width="18" height="17" alt="Search" class="trigger" />
                </div>
                <div class="popup">
                	<form action="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
cauta" method="GET" id="cautare-form">
                        <fieldset>
                            <input type="text" id="cautare-input" name="key_words" value="" onclick="this.value='';"   onfocus="this.select()" onblur="this.value=!this.value?'Cautare':this.value;" />
                            <input type="submit" id="cautare-submit" value="" />
                        </fieldset>
                    </form>
                </div>
            </div>
            <!--end search_wrapper -->
            
            <!--begin top_icons -->
            <ul class="top_icons">
                <li><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
" title="Acasa"><img src="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
pub/images/home_header.png" width="19" height="17" alt="Acasa" /></a></li>
                <li><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
termeni-conditii" title="Informatii" rel="nofollow" class="last"><img src="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
pub/images/info_header.png" width="9" height="17" alt="Informatii" /></a></li>
                <li><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
contact" title="Contact" class="last"><img src="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
pub/images/phone_header.png" width="17" height="17" alt="Contact" /></a></li>
            </ul>
            <!--end top_icons -->
            
          <div style="clear:both;"></div>
          
            <ul id="dd_menu">
				<li>
				<a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
colectie" style="margin-left: 10px;"' class="link">Colectii
				
				</a>
				<ul>
					
			<table border="0" cellspacing="0" cellpadding="0" style="width:883px;">
					<td>
						
						<?php $_from = $this->_tpl_vars['colectie_lista']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['item']):
?>
							<?php if ($this->_tpl_vars['k'] < 10): ?>
							<li class="link2"><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
colectie/<?php echo $this->_tpl_vars['item']->LINK; ?>
" title="<?php echo $this->_tpl_vars['item']->TEXT; ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['item']->TEXT)) ? $this->_run_mod_handler('truncate', true, $_tmp, 45, "...") : smarty_modifier_truncate($_tmp, 45, "...")); ?>
</a></li>
						<?php endif; ?>
						<?php endforeach; endif; unset($_from); ?>
					</td>
					<td>
						<?php $_from = $this->_tpl_vars['colectie_lista']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['item']):
?>
							<?php if ($this->_tpl_vars['k'] > 10 && $this->_tpl_vars['k'] <= 20): ?>
							<li class="link2"><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
colectie/<?php echo $this->_tpl_vars['item']->LINK; ?>
" title="<?php echo $this->_tpl_vars['item']->TEXT; ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['item']->TEXT)) ? $this->_run_mod_handler('truncate', true, $_tmp, 45, "...") : smarty_modifier_truncate($_tmp, 45, "...")); ?>
</a></li>
						<?php endif; ?>
						<?php endforeach; endif; unset($_from); ?>
					</td>
					<td>
						<?php $_from = $this->_tpl_vars['colectie_lista']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['item']):
?>
							<?php if ($this->_tpl_vars['k'] > 20 && $this->_tpl_vars['k'] <= 30): ?>
							<li class="link2"><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
colectie/<?php echo $this->_tpl_vars['item']->LINK; ?>
" title="<?php echo $this->_tpl_vars['item']->TEXT; ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['item']->TEXT)) ? $this->_run_mod_handler('truncate', true, $_tmp, 45, "...") : smarty_modifier_truncate($_tmp, 45, "...")); ?>
</a></li>
						<?php endif; ?>
						<?php endforeach; endif; unset($_from); ?>
					</td>
				</table>
				
				</ul>
				</li>
				<li><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
editura/despre-noi" class="link">Editura</a>
					<ul>
						<li class="link2"><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
editura/despre-noi" title="Despre noi">Despre noi</a></li>
						<li class="link2"><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
editura/staff-editorial" title="Staff editorial">Staff editorial</a></li>
						<li class="link2"><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
editura/echipa-redactionala" title="Echipa redactionala">Echipa redactionala</a></li>
						<li class="link2"><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
editura/referenti" title="Referenti">Referenti </a></li>
						<li class="link2"><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
editura/cariere" title="Cariere">Cariere</a></li>
					</ul>
				</li>
				<li><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
reviste"  class="link">Reviste</a>
				<ul>
					<?php $_from = $this->_tpl_vars['revista_lista']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
						<li class="link2" id="revista"><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
<?php echo $this->_tpl_vars['item']->link; ?>
" title="<?php echo $this->_tpl_vars['item']->CAT_NUME; ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['item']->CAT_NUME)) ? $this->_run_mod_handler('truncate', true, $_tmp, 45, "...") : smarty_modifier_truncate($_tmp, 45, "...")); ?>
</a></li>
					<?php endforeach; endif; unset($_from); ?>
					<li class="link2" id="revista"><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
reviste/palatul-de-justitie" title="Revista Palatul de Justitie">Palatul de Justitie</a></li>
				</ul>
				
				</li>
				<li class="fourth" ><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
evenimente" class="link">Evenimente</a>
					<ul>
							<li class="link2"><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
evenimente/conferinte">Conferinte</a></li>
							<li class="link2"><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
evenimente/interviuri">Interviuri</a></li>
							<li class="link2"><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
evenimente/editoriale">Editoriale</a></li>
						</ul>
				</li>
				<li><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
autori"  class="link">Autori</a></li>
				<li class="last link"><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
contact">Contact</a></li>
		</ul>
            
            <!--begin nav -->
            <div id="nav" style="margin:0px;"></div>
            <!--end nav -->             
        </div>
    </div>
    <!--end header -->
 
   <!--begin content_wrapper -->
    <div id="content_wrapper">
   
    <?php if ($this->_tpl_vars['rezultat']->functie == 'acasa' && count($this->_tpl_vars['banner_header']) > 0): ?>
   
        
		<div class="liquid-slider" id="liquid-slider">
			<?php $_from = $this->_tpl_vars['banner_header']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['item2']):
?>
			 <div>
				  <a href='<?php echo $this->_tpl_vars['item2']->url_destinatie; ?>
'><img style='width: 1030px; height: 539px;' src='<?php echo $this->_tpl_vars['item2']->imagine; ?>
' /></a>
			 </div>
			 <?php endforeach; endif; unset($_from); ?>
		</div>

	<?php endif; ?>

        
   <!--begin content -->
   <div id="content">
	
   
 