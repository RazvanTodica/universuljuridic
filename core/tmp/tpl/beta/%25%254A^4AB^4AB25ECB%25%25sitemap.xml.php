<?php /* Smarty version 2.6.19, created on 2018-05-11 05:01:30
         compiled from sitemap.xml */ ?>
<?php echo '<?xml'; ?>
 version="1.0" encoding="UTF-8"<?php echo '?>'; ?>

<urlset
      xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9
            http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">

<?php $_from = $this->_tpl_vars['linkuri']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
<url> 
	<loc><?php echo $this->_tpl_vars['item']; ?>
</loc> 
	<changefreq>weekly</changefreq> 
	<priority>0.8</priority>
</url> 
<?php endforeach; endif; unset($_from); ?>
</urlset>