<?php /* Smarty version 2.6.19, created on 2018-05-10 16:36:34
         compiled from statice/staff.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'resize_pic', 'statice/staff.tpl', 20, false),array('modifier', 'clear_font', 'statice/staff.tpl', 41, false),array('modifier', 'strip_tags', 'statice/staff.tpl', 41, false),array('modifier', 'truncate', 'statice/staff.tpl', 41, false),)), $this); ?>
  <!--begin breadcrumb -->
    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "structura/breadcrumb.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
    <!--end breadcrumb -->
    
    <!--begin content_wrapper -->
    <div id="content_wrapper">
       
        <!--begin content -->
        <div id="content">
            
            <!--begin left -->
            <div id="left">
           
            <?php if ($this->_tpl_vars['detalii']): ?>   
	            	  <h1 class="page_title"><?php echo $this->_tpl_vars['detalii']->staff_nume; ?>
</h1>
            	
                <div class="stuff_wrapper">      
                    <div class="stuff_box">
                    
                        <a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
editura/staff-editorial/<?php echo $this->_tpl_vars['detalii']->staff_link; ?>
" title="<?php echo $this->_tpl_vars['detalii']->staff_nume; ?>
" class="left"><img src="<?php if ($this->_tpl_vars['detalii']->staff_filename): ?><?php echo ((is_array($_tmp=$this->_tpl_vars['detalii']->staff_filename)) ? $this->_run_mod_handler('resize_pic', true, $_tmp, '130x181') : resize_pic($_tmp, '130x181')); ?>
<?php else: ?><?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
pub/images/no_image.png<?php endif; ?>" width="130" height="181" alt="Picture" class="border_img" /></a>
                                                <p><?php echo $this->_tpl_vars['detalii']->staff_descriere; ?>
</p>
                    </div>
                 
                  
                </div>
            
            
            <?php else: ?>	
            
            
                <h1 class="page_title">Staff editorial</h1>
            	
                <div class="stuff_wrapper">
                
                   <?php $_from = $this->_tpl_vars['staff']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
                    <div class="stuff_box">
                    
                        <a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
editura/staff-editorial/<?php echo $this->_tpl_vars['item']->staff_link; ?>
" title="<?php echo $this->_tpl_vars['item']->staff_nume; ?>
" class="left"><img src="<?php echo $this->_tpl_vars['item']->imagine_sfaff; ?>
" width="130" height="181" alt="Picture" class="border_img" /></a>
                        <h3><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
editura/staff-editorial/<?php echo $this->_tpl_vars['item']->staff_link; ?>
" title="<?php echo $this->_tpl_vars['item']->staff_nume; ?>
"><?php echo $this->_tpl_vars['item']->staff_nume; ?>
</a></h3><br/><br/>
                        <p><?php echo ((is_array($_tmp=((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['item']->staff_descriere)) ? $this->_run_mod_handler('clear_font', true, $_tmp) : clear_font($_tmp)))) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)))) ? $this->_run_mod_handler('truncate', true, $_tmp, 600, "...") : smarty_modifier_truncate($_tmp, 600, "...")); ?>
</p>
                        <a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
editura/staff-editorial/<?php echo $this->_tpl_vars['item']->staff_link; ?>
" title="Detalii" class="read_more">Detalii <span>&gt;</span></a>
                    </div>
                    <?php endforeach; endif; unset($_from); ?>
                  
                </div>
                
            <?php endif; ?>
                
            </div>
            <!--end left -->
            
            <!--begin right -->
            <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "structura/dreapta.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
            <!--end right -->
            
        </div>
        <!--end content -->
        <div class="clear"></div>
        
    </div>
    <!--end content_wrapper -->