<?php /* Smarty version 2.6.19, created on 2019-09-02 18:41:10
         compiled from structura/dreapta.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'structura/dreapta.tpl', 42, false),)), $this); ?>
<!--begin right -->

   			<div id="right_wrapper">
            
                <div id="right_top"></div>
                
                <div id="right">
                
                <?php if ($this->_tpl_vars['link']['0'] == 'evenimente'): ?>
                <h3>Evenimente</h3>
                
                    <ul class="sidebar_menu sidebar_separator">
                    	<li><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
evenimente/conferinte" title="Conferinte">Conferinte</a></li>
						<li><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
evenimente/interviuri" title="Interviuri">Interviuri</a></li>
						<li><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
evenimente/editoriale" title="Editoriale">Editoriale</a></li>
                    </ul>
                <?php endif; ?>  	
             
                <?php if ($this->_tpl_vars['link']['0'] == 'editura'): ?>
                	<h3>Editura</h3>
                	 <ul class="sidebar_menu sidebar_separator">
                	 		<li><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
editura/despre-noi" title="Despre noi">Despre noi</a></li>
							<li><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
editura/staff-editorial" title="Staff editorial">Staff editorial</a></li>
                         <!-- <li><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
editura/echipa-redactionala" title="Echipa redactionala">Echipa redactionala</a></li> -->
							<li><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
editura/referenti" title="Referenti">Referenti </a></li>
							<li><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
editura/cariere" title="Cariere">Cariere</a></li>
                	 </ul>
                <?php endif; ?>
                <?php if ($this->_tpl_vars['link']['0'] == 'reviste'): ?>                
                	<h3>REVISTE</h3>
                
                    <ul class="sidebar_menu sidebar_separator">   
                     <?php $_from = $this->_tpl_vars['revista_lista']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['rev']):
?>
                     <li><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
<?php if ($this->_tpl_vars['link']['0'] != 'reviste'): ?>reviste/<?php endif; ?><?php echo $this->_tpl_vars['rev']->link; ?>
" <?php if ($this->_tpl_vars['rev']->link == $this->_tpl_vars['url_sel']): ?> id="selectat" <?php endif; ?>  title="<?php echo $this->_tpl_vars['rev']->CAT_NUME; ?>
"><?php echo $this->_tpl_vars['rev']->CAT_NUME; ?>
</a></li>
                     <?php endforeach; endif; unset($_from); ?>
                    </ul>
                  <?php endif; ?>
                <?php if ($this->_tpl_vars['link']['0'] == 'colectie'): ?>   
                        
                	<h3>Colectii</h3>
                    <ul class="sidebar_menu sidebar_separator">
                     <?php if (count($this->_tpl_vars['colectie_lista']) > 0): ?> 
                
                     <?php $_from = $this->_tpl_vars['colectie_lista']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['col']):
?>
                     <li><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
colectie/<?php echo $this->_tpl_vars['col']->LINK; ?>
" title="<?php echo $this->_tpl_vars['col']->TEXT; ?>
" <?php if ($this->_tpl_vars['col']->LINK == $this->_tpl_vars['link']['1']): ?> id="selectat" <?php endif; ?>><?php if ($this->_tpl_vars['col']->TEXT == 'I. Tratate' || $this->_tpl_vars['col']->TEXT == 'II. Cursuri universitare'): ?>&nbsp;&nbsp;&nbsp;<?php echo $this->_tpl_vars['col']->TEXT; ?>
<?php else: ?><?php echo $this->_tpl_vars['col']->TEXT; ?>
<?php endif; ?></a></li>
                     <?php endforeach; endif; unset($_from); ?>
                        <?php endif; ?>
                    </ul>
                    <?php endif; ?>
   
                    <h3>Catalog</h3>
                    <div class="catalog sidebar_separator" >
                    	<a href="http://www.ujmag.ro/catalog/?pdf=0&mail=0&print=0&numele_from=&email_from=&numele_to=&email_to=&tip=0&categoria=0&editura=1&colectia=" target="_blank"><img src="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
pub/images/sidebar-book.jpg" width="85" height="103" alt="Catalog" /></a>
                        <p>Acum iti poti descarca, printa sau trimite pe email catalogul!</p>
                        <a href="http://www.ujmag.ro/catalog/?pdf=0&mail=0&print=0&numele_from=&email_from=&numele_to=&email_to=&tip=0&categoria=0&editura=1&colectia=" title="Detalii" class="read_more_catalog" target="_blank">Vezi Detalii <span>&gt;</span></a>
                    </div>
                    
                    <h3>Newsletter</h3>
                    <div class="newsletter sidebar_separator">
                        <p>Aboneaza-te la newsletter pentru a fi la curent cu cele mai noi stiri si cele mai bune oferte!</p>
                        <form action="" method="POST" id="newsletter-form">
                            <fieldset>
                                <input type="text" id="newsletter-input" name="newsletterEmail" value="introdu adresa de e-mail" onclick="this.value='';" onfocus="this.select()" onblur="this.value=!this.value?'introdu adresa de e-mail':this.value;" />
                                <input type="submit" id="newsletter-submit" value="" />
                                <input type="hidden" name="newsletter_send" value="1"/>
                            </fieldset>
                        </form>
                  	 </div>
					 
				<?php if ($this->_tpl_vars['bannere_slider_right']): ?> 
				<div id='right_slider'>
				<?php $_from = $this->_tpl_vars['bannere_slider_right']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['k'] => $this->_tpl_vars['slider']):
?>
					<a class='slide_<?php echo $this->_tpl_vars['k']; ?>
' href="<?php echo $this->_tpl_vars['slider']->href; ?>
" title="#" <?php if ($this->_tpl_vars['k'] != 0): ?>style='display:none;'<?php endif; ?> >
						<img src="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
pub/img_univers/<?php echo $this->_tpl_vars['slider']->imagine; ?>
" width="240" height="247" alt="Promo Sidebar" />
					</a>
				<?php endforeach; endif; unset($_from); ?>

				</div>
				<script type='text/javascript'>
				<?php echo '
				$(document).ready(function() {
					var slides_no = '; ?>
<?php echo count($this->_tpl_vars['bannere_slider_right']); ?>
;<?php echo '
					if(slides_no > 1) {
						var current_slide = 0;
						setInterval(function() {
							$("#right_slider a.slide_" + current_slide).hide();
							current_slide++;
							if(current_slide >= slides_no) current_slide = 0;
							$("#right_slider a.slide_" + current_slide).show();
						}, 4000);
					}
				});
				'; ?>

			  </script>
				
				<?php endif; ?>

               </div>
                <div id="right_bottom"></div>
                
      </div>
      <!--end right -->
	  
	  