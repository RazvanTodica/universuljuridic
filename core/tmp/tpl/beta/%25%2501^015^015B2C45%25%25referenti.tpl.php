<?php /* Smarty version 2.6.19, created on 2018-05-10 22:04:23
         compiled from statice/referenti.tpl */ ?>
  <!--begin breadcrumb -->
    <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "structura/breadcrumb.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
    <!--end breadcrumb -->
    
    <!--begin content_wrapper -->
    <div id="content_wrapper">
       
        <!--begin content -->
        <div id="content">
            
            <!--begin left -->
            <div id="left">
            	
                <h1 class="page_title">Referenti</h1>
            	
                <div class="stuff_wrapper">
					1. Prof. univ. dr. CONSTANTIN MITRACHE
					<br />
					Universitatea din Bucuresti
					<br /><br />
					2. Prof. univ. dr. EMILIAN STANCU
					<br />
					Universitatea din Bucuresti
					<br /><br />
					
					3. Prof. univ. dr. EMIL MOLCUT
					<br />
					Universitatea din Bucuresti
					<br /><br />
                  
                </div>
                
            </div>
            <!--end left -->
            
            <!--begin right -->
            <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "structura/dreapta.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
            <!--end right -->
            
        </div>
        <!--end content -->
        <div class="clear"></div>
        
    </div>
    <!--end content_wrapper -->