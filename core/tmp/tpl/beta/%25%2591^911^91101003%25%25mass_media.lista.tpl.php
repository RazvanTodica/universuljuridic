<?php /* Smarty version 2.6.19, created on 2018-06-20 08:28:03
         compiled from mass_media.lista.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'count', 'mass_media.lista.tpl', 19, false),array('modifier', 'toAscii', 'mass_media.lista.tpl', 27, false),array('modifier', 'date_format', 'mass_media.lista.tpl', 29, false),array('modifier', 'strip_tags', 'mass_media.lista.tpl', 30, false),array('modifier', 'truncate', 'mass_media.lista.tpl', 30, false),)), $this); ?>
<?php echo '
<script type="text/javascript">
function aplica_filtre(a,b){
	var f = \'\'
	if(a == \'an\'){
		window.location = \''; ?>
<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
aparitii-in-media?pagina=<?php if ($_GET['pagina']): ?><?php echo $_GET['pagina']; ?>
<?php else: ?>1<?php endif; ?>&an='+b+'<?php if ($_GET['luna']): ?>&luna=<?php echo $_GET['luna']; ?>
<?php endif; ?>&carti=<?php if ($_GET['carti']): ?><?php echo $_GET['carti']; ?>
<?php else: ?>9<?php endif; ?><?php echo '\';
	}else if(a == \'luna\'){
		window.location = \''; ?>
<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
aparitii-in-media?pagina=<?php if ($_GET['pagina']): ?><?php echo $_GET['pagina']; ?>
<?php else: ?>1<?php endif; ?><?php if ($_GET['an']): ?>&an=<?php echo $_GET['an']; ?>
<?php endif; ?>&luna='+b+'&carti=<?php if ($_GET['carti']): ?><?php echo $_GET['carti']; ?>
<?php else: ?>9<?php endif; ?><?php echo '\';
	}else if(a == \'carti\'){
		window.location = \''; ?>
<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
aparitii-in-media?pagina=<?php if ($_GET['pagina']): ?><?php echo $_GET['pagina']; ?>
<?php else: ?>1<?php endif; ?><?php if ($_GET['an']): ?>&an=<?php echo $_GET['an']; ?>
<?php endif; ?><?php if ($_GET['luna']): ?>&luna=<?php echo $_GET['luna']; ?>
<?php endif; ?>&carti='+b+'<?php echo '\';
	}
}
</script>
'; ?>

			<!--begin left -->
            <div id="left">
            	<h1 class="page_title">Mass Media</h1>
            	
            	<?php if (count($this->_tpl_vars['evenimente']) > 0 || $_GET['pagina']): ?>
                <!-- begin pagination -->
                <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "paginare.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
            	<!-- end pagination -->
            	
            	
            	<?php $_from = $this->_tpl_vars['evenimente']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['item']):
?>
            	<div class="evenimente_box">
                    <a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
aparitii-in-media/<?php echo ((is_array($_tmp=$this->_tpl_vars['item']->ev_titlu)) ? $this->_run_mod_handler('toAscii', true, $_tmp) : toAscii($_tmp)); ?>
/<?php echo $this->_tpl_vars['item']->ev_id; ?>
" title="<?php echo $this->_tpl_vars['item']->ev_titlu; ?>
" class="left"><?php if ($this->_tpl_vars['item']->ev_poza): ?><img src="http://www.ujmag.ro/resize_pic/194x128/<?php echo $this->_tpl_vars['item']->ev_poza; ?>
" width="194" height="128" alt="<?php echo $this->_tpl_vars['item']->ev_titlu; ?>
" class="small_border_img" /><?php else: ?><div style="width:202px;height:136px;float:left;background-color:#cbcbcb"></div><?php endif; ?></a>
                    <h2><a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
aparitii-in-media/<?php echo ((is_array($_tmp=$this->_tpl_vars['item']->ev_titlu)) ? $this->_run_mod_handler('toAscii', true, $_tmp) : toAscii($_tmp)); ?>
/<?php echo $this->_tpl_vars['item']->ev_id; ?>
" title="<?php echo $this->_tpl_vars['item']->ev_titlu; ?>
"><?php echo $this->_tpl_vars['item']->ev_titlu; ?>
</a></h2>
                    <span class="date_accesari"><a><?php echo ((is_array($_tmp=$this->_tpl_vars['item']->ev_data)) ? $this->_run_mod_handler('date_format', true, $_tmp, '%d %B %Y') : smarty_modifier_date_format($_tmp, '%d %B %Y')); ?>
</a> | <a><?php if ($this->_tpl_vars['item']->accesari): ?><?php echo $this->_tpl_vars['item']->accesari; ?>
<?php else: ?>0<?php endif; ?> accesari</a></span>
                    <a href="<?php echo $this->_tpl_vars['CONF']['sitepath']; ?>
aparitii-in-media/<?php echo ((is_array($_tmp=$this->_tpl_vars['item']->ev_titlu)) ? $this->_run_mod_handler('toAscii', true, $_tmp) : toAscii($_tmp)); ?>
/<?php echo $this->_tpl_vars['item']->ev_id; ?>
" title="<?php echo $this->_tpl_vars['item']->ev_titlu; ?>
"><?php echo ((is_array($_tmp=((is_array($_tmp=$this->_tpl_vars['item']->ev_continut)) ? $this->_run_mod_handler('strip_tags', true, $_tmp) : smarty_modifier_strip_tags($_tmp)))) ? $this->_run_mod_handler('truncate', true, $_tmp, 340) : smarty_modifier_truncate($_tmp, 340)); ?>
</a>
                </div>
                <?php endforeach; endif; unset($_from); ?>
                
                <!-- begin pagination -->
                <?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => "paginare.tpl", 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>
            	<!-- end pagination -->
            	<?php else: ?>
            	<p>Nu sunt aparitii in Mass Media.</p>
            	<?php endif; ?>
            	
            </div>
            <!--end left -->